//
//  AboutPageController.h
//  NewYork
//
//  Created by Tim Autin on 04/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewYorkController.h"

@interface AboutPageController : UIViewController
{
    CGRect _usableFrame;
}

@end
