//
//  AboutPageController.m
//  NewYork
//
//  Created by Tim Autin on 04/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SystemVersion.h"
#import <QuartzCore/QuartzCore.h>

#import "AboutPageController.h"
#import "Colors.h"

#define PADDING_TOP 70
#define TITLE_TEXT_VIEW_HEIGHT 110

@implementation AboutPageController


// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void)loadView
{
    // Call super :
    [super loadView];
    
    // Set the background color :
    self.view.backgroundColor = [Colors backgroundColor];
    
    // Compute the usable frame :
    int toolBarHeight = self.navigationController.toolbar.frame.size.height;
    int tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    _usableFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - tabBarHeight - toolBarHeight);
    
    // Create the title :
    UITextView* titleTextView;
    UIImageView* logo;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        logo = [[UIImageView alloc] initWithFrame:CGRectMake(_usableFrame.size.width / 2 - 100, 50, 200, 200)];
        logo.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"logohd" ofType:@"png"]];
        titleTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, PADDING_TOP + 200, _usableFrame.size.width, TITLE_TEXT_VIEW_HEIGHT)];
    }
    else
    {
        logo = [[UIImageView alloc] initWithFrame:CGRectMake(_usableFrame.size.width / 2 - 50, 50, 100, 100)];
        logo.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"logohd" ofType:@"png"]];
        titleTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, PADDING_TOP + 80, _usableFrame.size.width, TITLE_TEXT_VIEW_HEIGHT)];
    }
    titleTextView.text = NSLocalizedString(@"new_york_tab_about_page_controller_title", @"About :");
    titleTextView.text = [titleTextView.text stringByAppendingString:@"\n\n"];
    titleTextView.text = [titleTextView.text stringByAppendingString:NSLocalizedString(@"new_york_tab_about_page_controller_app_name", @"New York Good Tips")];
    titleTextView.textAlignment = UITextAlignmentCenter;
    titleTextView.backgroundColor = [UIColor clearColor];
    titleTextView.textColor = [UIColor whiteColor];
    titleTextView.font = [UIFont boldSystemFontOfSize:18.0];
    
    if(SYSTEM_VERSION < 7.0)
    {
        titleTextView.textColor = [Colors lightTextColor];
        titleTextView.layer.shadowColor   = [[UIColor blackColor] CGColor];
        titleTextView.layer.shadowOffset  = CGSizeMake(0.0f, -1.0f);
        titleTextView.layer.shadowOpacity = 1.0f;
        titleTextView.layer.shadowRadius  = 0.5f;
    }
    else
    {
        titleTextView.textColor = [Colors darkTextColor];
    }
    
    [self.view addSubview:titleTextView];
    [self.view addSubview:logo];
    
    
    // Get the build version :
    //NSString* build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    NSString* build = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    
    // Create a text view :
    UITextView* descriptionTextView;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        descriptionTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, PADDING_TOP + TITLE_TEXT_VIEW_HEIGHT + 200, _usableFrame.size.width, _usableFrame.size.height - PADDING_TOP - TITLE_TEXT_VIEW_HEIGHT)];
    else
        descriptionTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, PADDING_TOP + TITLE_TEXT_VIEW_HEIGHT + 50, _usableFrame.size.width, _usableFrame.size.height - PADDING_TOP - TITLE_TEXT_VIEW_HEIGHT)];
    
    descriptionTextView.userInteractionEnabled = NO;
    
    descriptionTextView.text = NSLocalizedString(@"new_york_tab_about_page_controller_author", @"Author : Alex les bons plans");
    descriptionTextView.text = [descriptionTextView.text stringByAppendingString:@"\n"];
    descriptionTextView.text = [descriptionTextView.text stringByAppendingString:NSLocalizedString(@"new_york_tab_about_page_controller_developper", @"Developer : Tim Autin - Moustoifa Moumini")];
    descriptionTextView.text = [descriptionTextView.text stringByAppendingString:@"\n\n"];
    descriptionTextView.text = [descriptionTextView.text stringByAppendingString:NSLocalizedString(@"new_york_tab_about_page_controller_copyright_route_me", @"Uses Route-Me map library, © 2008-2010 Route-Me Contributors")];
    descriptionTextView.text = [descriptionTextView.text stringByAppendingString:@"\n\n"];
    descriptionTextView.text = [descriptionTextView.text stringByAppendingString:NSLocalizedString(@"new_york_tab_about_page_controller_copyright_openstreetmap", @"Uses OpenStreetMap, © OpenStreetMap Contributors")];
    descriptionTextView.text = [descriptionTextView.text stringByAppendingString:@"\n\n"];
    descriptionTextView.text = [descriptionTextView.text stringByAppendingString:NSLocalizedString(@"new_york_tab_about_page_controller_copyright", @"Copyright © All rights reserved")];
    descriptionTextView.text = [descriptionTextView.text stringByAppendingString:@"\n"];
    descriptionTextView.text = [descriptionTextView.text stringByAppendingString:NSLocalizedString(@"new_york_tab_about_page_controller_date_version", @"December 2014 - v ")];
    descriptionTextView.text = [descriptionTextView.text stringByAppendingString:build];
    
    descriptionTextView.textAlignment = UITextAlignmentCenter;
    descriptionTextView.backgroundColor = [UIColor clearColor];
    descriptionTextView.font = [UIFont systemFontOfSize:14.0];
    
    if(SYSTEM_VERSION < 7.0)
    {
        descriptionTextView.textColor = [Colors lightTextColor];
        descriptionTextView.layer.shadowColor   = [[UIColor blackColor] CGColor];
        descriptionTextView.layer.shadowOffset  = CGSizeMake(0.0f, -1.0f);
        descriptionTextView.layer.shadowOpacity = 1.0f;
        descriptionTextView.layer.shadowRadius  = 0.5f;
    }
    else
    {
        descriptionTextView.textColor = [Colors darkTextColor];
    }
    
    [self.view addSubview:descriptionTextView];
    
    
    
    
    // Create the back button :
    UIBarButtonItem* backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"new_york_tab_about_page_controller_back_button", @"Back") style:UIBarButtonItemStyleBordered target:self action:@selector(dismiss)];
     UIBarButtonItem* tuto = [[UIBarButtonItem alloc] initWithTitle: NSLocalizedString(@"new_york_tab_about_page_controller_tutorial_button", "Tutorial") style:UIBarButtonItemStyleBordered target:self action:@selector(repeat_tuto)];
    if(SYSTEM_VERSION < 7.0) backButton.tintColor = [Colors lightOrange];
    
    UIBarButtonItem* flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    
    UIToolbar* toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, _usableFrame.size.height - 50, self.view.frame.size.width, 50)];
    if(SYSTEM_VERSION < 7.0) toolbar.barStyle = UIBarStyleBlackTranslucent;
    else toolbar.barStyle = UIBarStyleDefault;
    [toolbar setItems:[NSArray arrayWithObjects: backButton, flexibleSpace, tuto, nil]];
    
    [self.view addSubview:toolbar];
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - Screen autorotate

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
- (void) dismiss
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void) repeat_tuto
{
     [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"firstlaunch"];
     [[NSUserDefaults standardUserDefaults] synchronize];
    [self dismissModalViewControllerAnimated:YES];
   
}
// ----------------------------------------------------------------------------------------------------

@end
