//
//  Activity.h
//  NewYork
//
//  Created by Tim Autin on 20/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {MORNING, AFTERNOON, EVENING} DayTimeType;

@interface Activity : NSObject
{
    int _uniqueId;
    NSString* _description;
    DayTimeType _dayTime;
    int _day;
}

@property (nonatomic, assign) int uniqueId;
@property (nonatomic, copy) NSString* description;
@property (nonatomic, assign) DayTimeType dayTime;
@property (nonatomic, assign) int day;

- (id)initWithUniqueId:(int)uniqueId description:(NSString*)description dayTime:(DayTimeType)dayTime day:(int)day;
- (NSString*) toString;

@end