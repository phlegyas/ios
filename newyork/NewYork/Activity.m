//
//  Activity.m
//  NewYork
//
//  Created by Tim Autin on 20/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Activity.h"
#import "YourTravelsDBHandler.h"

@implementation Activity

@synthesize uniqueId = _uniqueId;
@synthesize description = _description;
@synthesize dayTime = _dayTime;
@synthesize day = _day;

- (id)initWithUniqueId:(int)uniqueId description:(NSString*)description dayTime:(DayTimeType)dayTime day:(int)day
{
    if ((self = [super init]))
    {
        self.uniqueId = uniqueId;
        self.description = description;
        self.dayTime = dayTime;
        self.day = day;
    }
    return self;
}

- (void) dealloc
{
    self.description = nil;
}

- (NSString*) toString
{
    return [NSString stringWithFormat:@"ACTIVITY : %d, %@, %@, %d", self.uniqueId, self.description, [YourTravelsDBHandler dayTimeTypeToString:self.dayTime], self.day];
}

@end
