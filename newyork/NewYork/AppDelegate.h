//
//  AppDelegate.h
//  NewYork
//
//  Created by Tim Autin on 05/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HotspotsUpdateHandler.h"
#import "HotspotsUpdateController.h"
#import "DropBoxHandler.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate, HotspotsUpdateHandlerDelegate, DropBoxHandlerDelegate>
{
    HotspotsUpdateHandler* _hotspotsUpdateHandler;
    HotspotsUpdateController* _hotspotsUpdateController;
}

@property (strong, nonatomic) UIWindow* window;
@property (strong, nonatomic) DropBoxHandler* dropBoxHandler;

- (BOOL) pingServer:(NSURL*)urlString;
- (void) copyInstallFiles;
- (void) setStandardUserDefaults;
+ (BOOL) addSkipBackupAttributeToItemAtURL:(NSURL *)url;
+ (void) addSkipBackupAttributeToFolder:(NSURL*)folder;

+ (AppDelegate*) sharedDelegate;

@end
