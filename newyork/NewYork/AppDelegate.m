//
//  AppDelegate.m
//  NewYork
//
//  Created by Tim Autin on 05/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SystemVersion.h"
#import "SBJson.h"

#import "AppDelegate.h"
#import "ConversionRateUpdateHandler.h"
#import "HotspotsUpdateHandler.h"
#import "HotspotsUpdateController.h"
#import "NewYorkController.h"
#import "YourTravelsController.h"
#import "SynchronisationViewController.h"
#import "AppHotspotsDBHandler.h"
#import "UserHotspotsDBHandler.h"
#import "Logger.h"

#import "ConversionRatePoundUpdateHandler.h"

@implementation AppDelegate

#define HOTSPOTS_UPDATE_AVAILABLE_ALERT_VIEW 0
#define HOTSPOTS_UPDATE_FAILED_ALERT_VIEW 1
#define HOTSPOTS_UPDATE_SUCCEED_ALERT_VIEW 2



// ----------------------------------------------------------------------------------------------------
#pragma mark - Init the App

- (id) init
{
    self = [super init];
    if (self)
    {
        // Merge the user defaults and the standard user defaults :
        [self setStandardUserDefaults];
        
        // If the install_files folder has not already been copied in the documents folder, do it :
        if(![[NSUserDefaults standardUserDefaults] boolForKey:@"copyInstallFilesDone"]) [self copyInstallFiles];
        
        // Check if some install files or database needs to be updated :
        [self checkForInstallFilesUpdate];
        [self checkForDatabaseSchemaUpdate];
        
        // Save the current app version :
        [self setCurrentAppVersion];
    }
    
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Utils

- (BOOL) pingServer:(NSURL*)url
{
    NSURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:4.0];
    NSHTTPURLResponse *response = nil;
    NSError* error;
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    return (response != nil);
}

+ (AppDelegate*) sharedDelegate
{
    return (AppDelegate*) [[UIApplication sharedApplication] delegate];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Files installation

- (void) setStandardUserDefaults
{
    // Get the standard user defaults from the plist file :
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"default_preferences" ofType:@"plist"];
    NSMutableDictionary* defaultValues = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
    
    // Set the standard user defaults from the plist file :
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultValues];
}

- (void) copyInstallFiles
{
    // Get a file manager :
    NSFileManager* manager = [NSFileManager defaultManager];
    NSError* error = nil;
    
    // Get the paths :
    NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* destinationFolder = documentsDirectory;
    NSString* sourceFolder = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"/install_files"];
    
    Log(self.class, @"**************************");
    Log(self.class, @"Copy install files...");
    NSLog(@"\n");
    Log(self.class, @"Source : %@/*", sourceFolder);
    Log(self.class, @"Destination : %@/", destinationFolder);
    NSLog(@"\n");
    
    // Try to copy each file in "install_files" :
    NSArray* files = [manager contentsOfDirectoryAtPath:sourceFolder error:&error];
    for(NSString* file in files)
    {
        NSString* sourceFile = [sourceFolder stringByAppendingPathComponent:file];
        NSString* destinationFile = [destinationFolder stringByAppendingPathComponent:file];
        
        NSURL* sourceURL = [NSURL fileURLWithPath:sourceFile];
        NSURL* destinationURL = [NSURL fileURLWithPath:destinationFile];
        
        if([manager copyItemAtURL:sourceURL toURL:destinationURL error:&error])
        {
            [AppDelegate addSkipBackupAttributeToFolder:destinationURL];
            Log(self.class, @"File \"%@\" successfully copied", file);
        }
        else
        {
            Log(self.class, @"Unable to copy folder \"%@\" (%@)", file, [error localizedDescription]);
        }
    }
    
    NSLog(@"\n");
    Log(self.class, @"Copy install files done");
    Log(self.class, @"**************************");
    NSLog(@"\n");
    
    if (error == nil)
    {
        // Remember that the moving has been successfully done :
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"copyInstallFilefsDone"];
    }
}

+ (void) addSkipBackupAttributeToFolder:(NSURL*)folder
{
    [AppDelegate addSkipBackupAttributeToItemAtURL:folder];
    
    NSError* error = nil;
    NSArray* folderContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[folder path] error:&error];
    
    for (NSString* item in folderContent)
    {
        NSString* path = [folder.path stringByAppendingPathComponent:item];
        [AppDelegate addSkipBackupAttributeToFolder:[NSURL fileURLWithPath:path]];
    }
}

+ (BOOL) addSkipBackupAttributeToItemAtURL:(NSURL *)url
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [url path]]);
    
    NSError *error = nil;
    BOOL success = NO;
    if(SYSTEM_VERSION >= 5.1) success = [url setResourceValue: [NSNumber numberWithBool: YES] forKey: NSURLIsExcludedFromBackupKey error: &error];
    
    if(!success){
        Log(self.class, @"Error excluding %@ from backup %@", [url lastPathComponent], error);
    }
    return success;
}

- (void) checkForInstallFilesUpdate
{
    // Code added in version 5.1 - Used fo updates compatibility, do NOT remove ANYTHING !
    
    // Get the current app version and the one as the last launch :
    float lastLaunchAppVersion = [[NSUserDefaults standardUserDefaults] floatForKey:@"appVersion"]; // 0 if not found
    
    Log(self.class, @"**************************");
    Log(self.class, @"Check for install files update (last launched version : %f)...", lastLaunchAppVersion);
    
    if(lastLaunchAppVersion > 0.0f && lastLaunchAppVersion < 5.7f)
    {
        // 5.5 : icons redesigned and db updated
        // 5.6 : translations
        
        [self updateInstallFilesFolder:@"good_tips"];
        [self updateInstallFilesFolder:@"new_york/app_hotspots"];
        
        // Delete the RMTileCache.db file from the Documents folder, as it is now saved in the Library/Caches folder :
        NSFileManager* manager = [NSFileManager defaultManager];
        NSError* error = nil;
        NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString* routeMeTileCacheFile = [documentsDirectory stringByAppendingPathComponent:@"RMTileCache.db"];
        [manager removeItemAtPath:routeMeTileCacheFile error:&error];
    }
    else
    {
        Log(self.class, @"No update needed");
    }
    
    Log(self.class, @"**************************");
    NSLog(@"\n");
}

- (void) checkForDatabaseSchemaUpdate
{
    // Get the current app version and the one as the last launch :
    float lastLaunchAppVersion = [[NSUserDefaults standardUserDefaults] floatForKey:@"appVersion"]; // 0 if not found
    
    Log(self.class, @"**************************");
    Log(self.class, @"Check for database schema update (last launched version : %f)...", lastLaunchAppVersion);
    
    if(lastLaunchAppVersion < 5.8f)
    {
        // We need to update the database schema (added the isFavorite filed to the hotposts) :
        Log(self.class, @"-> Need to update database...");
        
        [[UserHotspotsDBHandler instance] execute:@"CREATE TABLE FAVORITE_HOTSPOTS (id INTEGER PRIMARY KEY AUTOINCREMENT, hotspot INTEGER NOT NULL UNIQUE);"];
        [[UserHotspotsDBHandler instance] execute:@"ALTER TABLE USER_HOTSPOTS RENAME TO HOTSPOTS;"];
        
        [[AppHotspotsDBHandler instanceWithLanguage:@"fr"] execute:@"CREATE TABLE FAVORITE_HOTSPOTS (id INTEGER PRIMARY KEY AUTOINCREMENT, hotspot INTEGER NOT NULL UNIQUE);"];
        [[AppHotspotsDBHandler instanceWithLanguage:@"en"] execute:@"CREATE TABLE FAVORITE_HOTSPOTS (id INTEGER PRIMARY KEY AUTOINCREMENT, hotspot INTEGER NOT NULL UNIQUE);"];
        [[AppHotspotsDBHandler instanceWithLanguage:@"es"] execute:@"CREATE TABLE FAVORITE_HOTSPOTS (id INTEGER PRIMARY KEY AUTOINCREMENT, hotspot INTEGER NOT NULL UNIQUE);"];
        
        Log(self.class, @"-> Done.");
    }
    
    if(lastLaunchAppVersion >= 5.8f)
    {
        Log(self.class, @"No update needed");
    }
    
    Log(self.class, @"**************************");
    NSLog(@"\n");
}

- (void) setCurrentAppVersion
{
    // Set the current app version :
    //float currentAppVersion = [((NSNumber*)[[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey]) floatValue];
    float currentAppVersion = [((NSNumber*)[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]) floatValue];
    [[NSUserDefaults standardUserDefaults] setFloat:currentAppVersion forKey:@"appVersion"];
}

- (void) updateInstallFilesFolder:(NSString*)folderPath
{
    // This method will update the given folder in the install_files folder
    // For example, you can do :
    // - updateInstallFilesFolder:@"good_tips"
    // - updateInstallFilesFolder:@"new_york/app_hotspots"
    
    // Get a file manager :
    NSFileManager* manager = [NSFileManager defaultManager];
    NSError* error = nil;
    
    // Get the paths :
    NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* destinationFolder = documentsDirectory;
    NSString* sourceFolder = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"/install_files"];
    
    // First remove the old folder :
    NSString* sourceFile = [sourceFolder stringByAppendingPathComponent:folderPath];
    NSString* destinationFile = [destinationFolder stringByAppendingPathComponent:folderPath];
    NSURL* sourceURL = [NSURL fileURLWithPath:sourceFile];
    NSURL* destinationURL = [NSURL fileURLWithPath:destinationFile];
    
    Log(self.class, @"-> Need to update \"%@\" folder...", folderPath);
    Log(self.class, @"-> Source : %@", sourceURL);
    
    error = nil;
    if([manager removeItemAtURL:destinationURL error:&error])
    {
        Log(self.class, @"-> Old folder successfully deleted");
    }
    else
    {
        Log(self.class, @"-> Unable to delete folder \"%@\" (%@)", folderPath, [error localizedDescription]);
    }
    
    // Then copy the new one :
    error = nil;
    if([manager copyItemAtURL:sourceURL toURL:destinationURL error:&error])
    {
        [AppDelegate addSkipBackupAttributeToFolder:destinationURL];
        Log(self.class, @"-> \"%@\" folder successfully copied", folderPath);
    }
    else
    {
        Log(self.class, @"-> Unable to copy folder \"%@\" (%@)", folderPath, [error localizedDescription]);
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Application lifecycle

- (BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //GOOGLE ANALYTICS
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    // Initialize tracker. Replace with your tracking ID.
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-56383476-1"];
    
    
    // Try to update the conversion rate :
      if ( ! [[NSUserDefaults standardUserDefaults]boolForKey:@"firstlaunch"] )
      {
          [[NSUserDefaults standardUserDefaults] setFloat:1.25 forKey:@"conversionRate_EUR_USD_save"];
          [[NSUserDefaults standardUserDefaults] setFloat:1.6 forKey:@"conversionRate_GBP_USD_save"];
      }
    if([self pingServer:[ConversionRateUpdateHandler conversionRateRetrievingURL]])
    {
        ConversionRateUpdateHandler* conversionRateUpdateHandler = [[ConversionRateUpdateHandler alloc] init];
        [conversionRateUpdateHandler updateConversionRate];
    }
    else
    {
        Log(self.class, @"Conversion rate retrieving URL is unreachable");
         [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"updated"];
    }
   
    // Try to update the conversion rate for pound :
    if([self pingServer:[ConversionRatePoundUpdateHandler conversionRateRetrievingURL]])
    {
        ConversionRatePoundUpdateHandler* conversionRatePoundUpdateHandler = [[ConversionRatePoundUpdateHandler alloc] init];
        [conversionRatePoundUpdateHandler updateConversionRate];
    }
    else
    {
        Log(self.class, @"Conversion rate for pound retrieving URL is unreachable");
         [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"updated"];
    }
    
    
    // Try to update the hotspots :
    if([self pingServer:[HotspotsUpdateHandler hotspotsJSONFileUrl]])
    {
        _hotspotsUpdateHandler = [[HotspotsUpdateHandler alloc] init];
        _hotspotsUpdateHandler.delegate = self;
        [_hotspotsUpdateHandler checkForUpdate];
    }
    else
    {
        Log(self.class, @"Hotspots JSON file URL is unreachable");
    }
    
    // Handle DropBox synchronization :
    //_dropBoxHandler = [[DropBoxHandler alloc] initWithDelegate:self];
    //[_dropBoxHandler connectToDropBox];
    
    return YES;
}
							
- (void) applicationWillResignActive:(UIApplication *)application {}
- (void) applicationDidEnterBackground:(UIApplication *)application { }
- (void) applicationWillEnterForeground:(UIApplication *)application { }
- (void) applicationDidBecomeActive:(UIApplication *)application { }
- (void) applicationWillTerminate:(UIApplication *)application { }
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - HotspotsUpdateHandlerDelegate delegate

- (void) hotspotsUpdateHandlerDidFindUpdate
{
    UIAlertView* updateAvailableAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"app_delegate_hotspots_update_available_popup_title", @"Update available !")
                                                                       message:NSLocalizedString(@"app_delegate_hotspots_update_available_popup_message", @"")
                                                                      delegate:self
                                                             cancelButtonTitle:NSLocalizedString(@"app_delegate_hotspots_update_available_popup_later_button", @"")
                                                             otherButtonTitles:NSLocalizedString(@"app_delegate_hotspots_update_available_popup_ok_button", @""),nil];
    [updateAvailableAlertView setTag:HOTSPOTS_UPDATE_AVAILABLE_ALERT_VIEW];
    
    [updateAvailableAlertView show];
}

- (void) hotspotsUpdateHandlerDidProgress:(float)progress description:(NSString *)description
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [_hotspotsUpdateController setProgress:progress withDescription:description];
    });
    
}

- (void) hotspotsUpdateHandlerDidFailWithError:(int)error
{
    NSString* title = NSLocalizedString(@"app_delegate_hotspots_update_failed_popup_title", @"Update failed");
    NSString* message = @"";
    
    switch (error) {
        
        case UPDATE_FAILURE_WHILE_DOWLOADING_IMAGES:
        {
            message = NSLocalizedString(@"app_delegate_hotspots_update_failed_popup_message_failure_while_downloading_images", @"...");
            
            break;
        }
        case UPDATE_FAILURE_WHILE_DELETING_HOTSPOTS:
        {
            message = NSLocalizedString(@"app_delegate_hotspots_update_failed_popup_message_failure_while_deleting_hotspots", @"...");
            break;
        }
        case UPDATE_FAILURE_WHILE_UPDATING_DATABASE:
        {
            message = NSLocalizedString(@"app_delegate_hotspots_update_failed_popup_message_failure_while_updating_database", @"...");
            break;
        }
        default: break;
    }
    
    // Inform the user :
    UIAlertView* updateSucceedAlertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:NSLocalizedString(@"app_delegate_hotspots_update_failed_popup_ok_button", @"OK") otherButtonTitles:nil];
    [updateSucceedAlertView setTag:HOTSPOTS_UPDATE_FAILED_ALERT_VIEW];
    
    [updateSucceedAlertView show];
}

- (void) hotspotsUpdateHandlerDidSucceed
{
    // Inform the user :
    UIAlertView* updateSucceedAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"app_delegate_hotspots_update_succeed_popup_title", @"Update succeed !") message:NSLocalizedString(@"app_delegate_hotspots_update_succeed_popup_message", @"The hotspots have successfully been updated.") delegate:self cancelButtonTitle:NSLocalizedString(@"app_delegate_hotspots_update_succeed_popup_ok_button", @"OK") otherButtonTitles:nil];
    [updateSucceedAlertView setTag:HOTSPOTS_UPDATE_SUCCEED_ALERT_VIEW];
    [updateSucceedAlertView show];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - UIAlertView delegate

- (void) alertView:(UIAlertView*)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag) {
            
        case HOTSPOTS_UPDATE_AVAILABLE_ALERT_VIEW:
        {
            // If the user wants to update the app :
            if(buttonIndex == 1)
            {
                _hotspotsUpdateController = [[HotspotsUpdateController alloc] initWithHandler:_hotspotsUpdateHandler];
                [self.window.rootViewController presentModalViewController:_hotspotsUpdateController animated:YES];
            }
            
            break;
        }
        case HOTSPOTS_UPDATE_SUCCEED_ALERT_VIEW:
        {
            // Reload the hotspots :
            UITabBarController* tabController = (UITabBarController*) self.window.rootViewController;
            NewYorkController* newYorkController = [[[[tabController childViewControllers] objectAtIndex:0] childViewControllers] objectAtIndex:0];
            
            if(newYorkController != NULL) [newYorkController resetHotspots];
            
            // Dismiss the update view :
            [self.window.rootViewController dismissModalViewControllerAnimated:YES];
            
            break;
        }
        case HOTSPOTS_UPDATE_FAILED_ALERT_VIEW:
        {
            // Dismiss the update view :
            [self.window.rootViewController dismissModalViewControllerAnimated:YES];
            
            break;
        }
        default: break;
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - DropBoxHandler delegate methods :

- (BOOL) application:(UIApplication *)app openURL:(NSURL *)url sourceApplication:(NSString *)source annotation:(id)annotation
{
    BOOL linked = [_dropBoxHandler application:app openURL:url sourceApplication:source annotation:annotation];
 
    if(linked)
    {
        // Inform the Sync tab :
        UITabBarController* tabController = (UITabBarController*) self.window.rootViewController;
        SynchronisationViewController* controller = [[[[tabController childViewControllers] objectAtIndex:4] childViewControllers] objectAtIndex:0];
        [controller didStartConnectingToDropBox];
    }
    
    return linked;
}

- (void) didConnectToDropBox
{
    Log(self.class, @"didConnectToDropBox");
    
    dispatch_async(dispatch_get_main_queue(), ^()
    {
        // Inform the Sync tab :
        UITabBarController* tabController = (UITabBarController*) self.window.rootViewController;
        SynchronisationViewController* controller = [[[[tabController childViewControllers] objectAtIndex:4] childViewControllers] objectAtIndex:0];
        [controller didConnectToDropBox];
    });
    
    // Get the documents directory path :
    NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    // Get the user_hotspots.sqlite3 and your_travels.sqlite3 files pathes :
    NSString* userHotspotsFilePath = @"/new_york/user_hotspots/database/user_hotspots.sqlite3";
    NSString* yourTravelsFilePath = @"/your_travels/database/your_travels.sqlite3";
    
    // Get the pathes on DropBox :
    DBPath* userHotspotsFilePathOnDropBox = [[DBPath root] childPath:userHotspotsFilePath];
    DBPath* yourTravelsFilePathOnDropBox = [[DBPath root] childPath:yourTravelsFilePath];
    
    // Link the files :
    [_dropBoxHandler linkFileAtLocalPath:[documentsDirectory stringByAppendingString:userHotspotsFilePath] toFileAtRemotePath:userHotspotsFilePathOnDropBox];
    [_dropBoxHandler linkFileAtLocalPath:[documentsDirectory stringByAppendingString:yourTravelsFilePath] toFileAtRemotePath:yourTravelsFilePathOnDropBox];
    
    // Get the user hotsopots images folder path :
    NSString* imagesFolderPath = @"/new_york/user_hotspots/images/";
    DBPath* imagesFolderPathOnDropBox = [[DBPath root] childPath:imagesFolderPath];
    
    // Link the folder :
    [_dropBoxHandler linkFolderAtLocalPath:[documentsDirectory stringByAppendingString:imagesFolderPath] toFolderAtRemotePath:imagesFolderPathOnDropBox];
}

- (BOOL) mustSyncFileWithName:(NSString*)fileName inFolder:(DBPath*)folderPath
{
    if([folderPath.name isEqualToString:@"images"])
    {
        NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:@"hotspot_[0-9]*\\.(png|jpg)" options:NSRegularExpressionCaseInsensitive error:nil];
        
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:fileName options:0 range:NSMakeRange(0, [fileName length])];
        
        return numberOfMatches > 0;
    }
    
    return NO;
}

- (void) fileUpdated:(DBPathMapping*)pathMapping
{
    if([pathMapping.file.info.path.name isEqualToString:@"user_hotspots.sqlite3"])
    {
        // Reload the hotspots :
        UITabBarController* tabController = (UITabBarController*) self.window.rootViewController;
        NewYorkController* newYorkController = [[[[tabController childViewControllers] objectAtIndex:0] childViewControllers] objectAtIndex:0];
        
        if(newYorkController != NULL) [newYorkController resetHotspots];
    }
    else if([pathMapping.file.info.path.name isEqualToString:@"your_travels.sqlite3"])
    {
        // Reload the travel :
        UITabBarController* tabController = (UITabBarController*) self.window.rootViewController;
        YourTravelsController* yourTravelsController = [[[[tabController childViewControllers] objectAtIndex:2] childViewControllers] objectAtIndex:0];
        
        if(yourTravelsController != NULL) [yourTravelsController resetTravel];
    }
    else
    {
        
    }
}

- (void) fileDeletedAtLocalPath:(NSString*)localPath
{
    
}
// ----------------------------------------------------------------------------------------------------


@end




