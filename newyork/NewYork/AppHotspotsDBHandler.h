//
//  AppHotspotsDBHandler.h
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

#import "HotspotsDBHandler.h"


@interface AppHotspotsDBHandler : HotspotsDBHandler

+ (AppHotspotsDBHandler*) instanceWithLanguage:(NSString*)language;

@end


