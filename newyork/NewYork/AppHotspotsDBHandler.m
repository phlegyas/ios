//
//  HotspotsDBHandler.m
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppHotspotsDBHandler.h"
#import "Logger.h"

@implementation AppHotspotsDBHandler

static NSMutableDictionary* _instances;

// ----------------------------------------------------------------------------------------------------
#pragma mark - Get a static instance of the database

+ (AppHotspotsDBHandler*) instance
{
    NSString* language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    
    return [AppHotspotsDBHandler instanceWithLanguage:language];
}

+ (AppHotspotsDBHandler*) instanceWithLanguage:(NSString*)language
{
    if (_instances == nil)
    {
        _instances = [NSMutableDictionary dictionary];
    }
    
    AppHotspotsDBHandler* instance = [_instances objectForKey:language];
    
    if (instance == nil)
    {
        instance = [[AppHotspotsDBHandler alloc] initWithLanguage:language];
        [_instances setObject:instance forKey:language];
    }
    
    return instance;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Handler lifecycle

- (id)initWithLanguage:(NSString*)language
{
    if(![language isEqual:@"fr"] && ![language isEqual:@"en"] && ![language isEqual:@"es"]) {
        
        return nil;
    }
    
    NSString* databaseFile = [NSString stringWithFormat:@"/new_york/app_hotspots/database/app_hotspots_%@.sqlite3", language];
    
    if ((self = [super initWithPath:databaseFile]))
    {
        //_tableName = @"HOTSPOTS";
    }
    
    return self;
}
// ----------------------------------------------------------------------------------------------------

@end













