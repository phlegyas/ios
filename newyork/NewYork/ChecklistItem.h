//
//  ChecklistItem.h
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChecklistItem : NSObject
{
    int _uniqueId;
    int _page;
    int _position;
    BOOL _checked;
    NSString* _title;
    NSString* _description;
}

@property (nonatomic, assign) int uniqueId;
@property (nonatomic, assign) int page;
@property (nonatomic, assign) int position;
@property (nonatomic, assign) BOOL checked;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *description;

- (id)initWithUniqueId:(int)uniqueId page:(int)page position:(int)position checked:(BOOL)checked title:(NSString *)title description:(NSString*)description;
- (NSString*) toString;

@end
