//
//  ChecklistItem.m
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChecklistItem.h"

@implementation ChecklistItem

@synthesize uniqueId = _uniqueId;
@synthesize page = _page;
@synthesize position = _position;
@synthesize checked = _checked;
@synthesize title = _title;
@synthesize description = _description;

- (id)initWithUniqueId:(int)uniqueId page:(int)page position:(int)position checked:(BOOL)checked title:(NSString *)title description:(NSString*)description;
{
    if ((self = [super init]))
    {
        self.uniqueId = uniqueId;
        self.page = page;
        self.position = position;
        self.checked = checked;
        self.title = title;
        self.description = description;
    }
    return self;
}

- (void) dealloc
{
    self.title = nil;
}

- (NSString*) toString
{
    return [NSString stringWithFormat:@"CHEKLISTITEM : %d, %d, %d, %@, %@", self.uniqueId, self.page, self.position, self.title, self.description];
}

@end

