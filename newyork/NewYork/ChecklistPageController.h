//
//  ChecklistPageController.h
//  NewYork
//
//  Created by Tim Autin on 11/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UITopView.h"
#import "ChecklistItem.h"


@interface ChecklistPageController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    int _pageId;
    CGRect _usableFrame;
    NSArray* _checklistItems;
    UITableView* _tableView;
    NSIndexPath* _expandedTableViewRow;
    UIFont* _titleLabelFont;
    UIFont* _descriptionLabelFont;
    CGRect _switchFrame;
    NSArray* _cellsBackgroundColors;
}

- (id) initWithPageId:(int)pageId;

- (void) toggleItemChecked:(id)sender;

@end
