//
//  ChecklistPageController.m
//  NewYork
//
//  Created by Tim Autin on 11/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SystemVersion.h"
#import <QuartzCore/QuartzCore.h>

#import "ChecklistPageController.h"
#import "GoodTipsDBHandler.h"
#import "Colors.h"

@implementation ChecklistPageController

#define CELL_CONTENT_WIDTH (SYSTEM_VERSION < 7.0 ? 300.0f : 320.0f)
#define CELL_CONTENT_MIN_HEIGHT 44.0f
#define CELL_CONTENT_MARGIN 10.0f

// ----------------------------------------------------------------------------------------------------
#pragma mark - Init function

- (id)initWithPageId:(int)pageId
{
    self = [super init];
    
    // Init the table at the requested location :
    if (self) _pageId = pageId;
    
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void)loadView
{
    // Call super :
    [super loadView];
    
    // Get the checklistItems at the requested page :
    _checklistItems = [[GoodTipsDBHandler instance] checklistItemsWithPageId:_pageId];
    
    // Set the window's title :
    self.title = [[GoodTipsDBHandler instance] locationWithId:[[GoodTipsDBHandler instance] pageWithId:_pageId].location ].title;
    
    // Set the view's background color :
    self.view.backgroundColor = [Colors backgroundColor];
    
    // Create the tableView :
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.backgroundView = nil;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.01f, 0.01f)];
    
    // Avoid the overlap of the tabbar on the webview :
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    // Add it to the view :
    [self.view addSubview:_tableView];
    
    // Set the _descriptionLabelFont :
    _descriptionLabelFont = [UIFont fontWithName:@"Helvetica" size:14.0f];
    _titleLabelFont = [UIFont boldSystemFontOfSize:16.0];
    
    // There is no expanded TableViewRow :
    _expandedTableViewRow = nil;
    
    // Save the frame of a switch :
    UISwitch* aSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
    _switchFrame = aSwitch.frame;
    
    // Save the colors :
    _cellsBackgroundColors = [[NSArray alloc] initWithObjects:
                              [Colors darkYellow],
                              [Colors lightYellow ],
                              nil];
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - Screen autorotate

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_checklistItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    // If the row is expanded :
    if([indexPath isEqual:_expandedTableViewRow])
    {
        // Get the description label :
        UILabel* descriptionLabel = (UILabel*) [[self tableView:_tableView cellForRowAtIndexPath:indexPath] viewWithTag:100];
        
        return CELL_CONTENT_MIN_HEIGHT + descriptionLabel.frame.size.height + CELL_CONTENT_MARGIN;
    }
    else
    {
        return CELL_CONTENT_MIN_HEIGHT;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get the cell :
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        // Do some init stuff :
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [_cellsBackgroundColors objectAtIndex:(indexPath.row % _cellsBackgroundColors.count)];
        cell.clipsToBounds = YES;
        
        // Get the item :
        ChecklistItem* item = [_checklistItems objectAtIndex:indexPath.row];
        
        // Create 2 images for the arrow :
        UIImage* leftArrow = [UIImage imageNamed:@"navigation_arrow_right.png"];
        UIImageView* leftArrowImageView = [[UIImageView alloc] initWithImage:leftArrow];
        UIView* leftArrowView = [[UIView alloc] initWithFrame:CGRectMake(CELL_CONTENT_MARGIN, (CELL_CONTENT_MIN_HEIGHT - leftArrow.size.height)/2, leftArrow.size.width, CELL_CONTENT_MIN_HEIGHT)];
        [leftArrowView addSubview:leftArrowImageView];
        
        // Create a UILabel tha will contains the title :
        UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CELL_CONTENT_MARGIN + leftArrow.size.width + CELL_CONTENT_MARGIN, cell.bounds.origin.y, CELL_CONTENT_WIDTH - 3 * CELL_CONTENT_MARGIN - leftArrow.size.width, CELL_CONTENT_MIN_HEIGHT - 1)];
        titleLabel.text = item.title;
        titleLabel.font = _titleLabelFont;
        titleLabel.backgroundColor = cell.backgroundColor;
        
        // Create a UISwitch :
        UISwitch* checkedSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(CELL_CONTENT_WIDTH - _switchFrame.size.width - CELL_CONTENT_MARGIN, (CELL_CONTENT_MIN_HEIGHT - _switchFrame.size.height)/2, 0, 0)];
        checkedSwitch.tag = item.uniqueId;
        [checkedSwitch setOn:item.checked];
        [checkedSwitch addTarget:self action:@selector(toggleItemChecked:) forControlEvents:UIControlEventValueChanged];
        
        // Compute descriptionLabel size :
        /*
        CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
        CGSize size = [item.description sizeWithFont:_descriptionLabelFont constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];*/
        
        // Create the descriptionLabel :
        UILabel* descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(CELL_CONTENT_MARGIN, CELL_CONTENT_MIN_HEIGHT, CELL_CONTENT_WIDTH - 2 * CELL_CONTENT_MARGIN, 0)];
        descriptionLabel.lineBreakMode = UILineBreakModeWordWrap;
        descriptionLabel.numberOfLines = 0;
        descriptionLabel.text = item.description;
        descriptionLabel.font = _descriptionLabelFont;
        descriptionLabel.backgroundColor = cell.backgroundColor;
        descriptionLabel.tag = 100;
        
        // Add the items :
        [cell.contentView addSubview:leftArrowView];
        [cell.contentView addSubview:titleLabel];
        [cell.contentView addSubview:checkedSwitch];
        [cell.contentView addSubview:descriptionLabel];
        
        [descriptionLabel sizeToFit];
    }
    return cell;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Update previous _expandedTableViewRow arrow :
    if(_expandedTableViewRow != nil)
    {
        // Update the image :
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:_expandedTableViewRow];
        UIView* leftArrowView = [cell.contentView.subviews objectAtIndex:0];
        UIImageView* leftArrowImageView = [leftArrowView.subviews objectAtIndex:0];
        
        leftArrowImageView.image = [UIImage imageNamed:@"navigation_arrow_right.png"];
    }
    
    // Get the leftArrowImageView of the cell :
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIView* leftArrowView = [cell.contentView.subviews objectAtIndex:0];
    UIImageView* leftArrowImageView = [leftArrowView.subviews objectAtIndex:0];
    
    // If the cell isn't expanded :
    if(![indexPath isEqual:_expandedTableViewRow])
    {
        // Remember the raw clicked :
        _expandedTableViewRow = indexPath;
        
        // Update the image :
        leftArrowImageView.image = [UIImage imageNamed:@"navigation_arrow_bottom.png"];
    }
    else
    {
        // Remember that there is no raw expanded :
        _expandedTableViewRow = nil;
        
        // Update the image :
        leftArrowImageView.image = [UIImage imageNamed:@"navigation_arrow_right.png"];
    }
    
    
    // Update the _tableView :
    [_tableView beginUpdates];
    [_tableView endUpdates];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Handle item checking

- (void)toggleItemChecked:(id)sender
{
    if (![sender isKindOfClass: [UISwitch class]]) return;
    
    int itemId = (int) ((UISwitch*) sender).tag;
    [[GoodTipsDBHandler instance] toggleItemChecked:itemId];
}
// ----------------------------------------------------------------------------------------------------



@end



