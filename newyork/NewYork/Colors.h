//
//  Colors.h
//  NewYork
//
//  Created by Tim Autin on 13/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Colors : NSObject

+ (UIColor*) lightBlue;
+ (UIColor*) darkBlue;

+ (UIColor*) lightGreen;
+ (UIColor*) darkGreen;

+ (UIColor*) lightPink;
+ (UIColor*) darkPink;

+ (UIColor*) lightOrange;
+ (UIColor*) darkOrange;

+ (UIColor*) lightYellow;
+ (UIColor*) darkYellow;

+ (UIColor*) lightTextColor;
+ (UIColor*) darkTextColor;

+ (UIColor*) backgroundColor;

@end
