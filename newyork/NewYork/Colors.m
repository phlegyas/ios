//
//  Colors.m
//  NewYork
//
//  Created by Tim Autin on 13/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SystemVersion.h"
#import "Colors.h"

@implementation Colors

+ (UIColor*) lightBlue { return [UIColor colorWithRed:125.0/255.0 green:190.0/255.0 blue:240.0/255.0 alpha:1.0];}
+ (UIColor*) darkBlue { return [UIColor colorWithRed:51.0/255.0 green:143.0/255.0 blue:213.0/255.0 alpha:1.0]; }

+ (UIColor*) lightPink { return [UIColor colorWithRed:223.0/255.0 green:113.0/255.0 blue:148.0/255.0 alpha:1.0]; }
+ (UIColor*) darkPink { return [UIColor colorWithRed:193.0/255.0 green:63.0/255.0 blue:94.0/255.0 alpha:1.0]; }

+ (UIColor*) lightGreen { return [UIColor colorWithRed:197.0/255.0 green:232.0/255.0 blue:82.0/255.0 alpha:1.0]; }
+ (UIColor*) darkGreen { return [UIColor colorWithRed:164.0/255.0 green:214.0/255.0 blue:0.0/255.0 alpha:1.0]; }

+ (UIColor*) lightOrange { return [UIColor colorWithRed:246.0/255.0 green:171.0/255.0 blue:55.0/255.0 alpha:1.0]; }
+ (UIColor*) darkOrange { return [UIColor colorWithRed:232.0/255.0 green:96.0/255.0 blue:56.0/255.0 alpha:1.0]; }

+ (UIColor*) lightYellow { return [UIColor colorWithRed:255.0/255.0 green:220.0/255.0 blue:70.0/255.0 alpha:1.0]; }
+ (UIColor*) darkYellow { return [UIColor colorWithRed:255.0/255.0 green:200.0/255.0 blue:0.0/255.0 alpha:1.0]; }

+ (UIColor*) lightTextColor { return [UIColor whiteColor]; }
+ (UIColor*) darkTextColor { return [UIColor darkGrayColor]; }

+ (UIColor*) backgroundColor
{
    if(SYSTEM_VERSION < 7.0) return [UIColor scrollViewTexturedBackgroundColor];
    else return [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"background.jpg"]];
}

@end
