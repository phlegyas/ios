//
//  CompassView.h
//  NewYork
//
//  Created by Moustoifa MOUMINI <mmoumini@student.42.fr> on 30/09/2014.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "NewYorkController.h"

@interface CompassView : UIView <CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    CGFloat currentAngle;
    
    UIView *compassContainer;
}
@property (nonatomic, retain) CLLocationManager *locationManager;

@end
