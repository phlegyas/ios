//
//  CompassView.m
//  NewYork
//
//  Created by Moustoifa MOUMINI <mmoumini@student.42.fr>  on 30/09/2014.
//
//

#import "CompassView.h"

#define degreesToRadians(x) (M_PI * x / 180.0)

@implementation CompassView
@synthesize locationManager;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIImageView *arrowImg;
        //Compass Images
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
            arrowImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 120, 120)];
        else
            arrowImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
        arrowImg.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"compassArrow" ofType:@"png"]];
        
        //Compass Container
        
        compassContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        
   //     [compassContainer.layer  insertSublayer:myCompassLayer atIndex:0];
        [compassContainer addSubview:arrowImg];
        
        [self addSubview:compassContainer];
    //    [compassContainer release];
    //    [arrowImg release];
        
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.delegate=self;
        
        //Start the compass updates.
        [self.locationManager startUpdatingHeading];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopUpdating) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startUpdating) name:UIApplicationDidBecomeActiveNotification object:nil];
    }
    return self;
}

- (void) startUpdating
{
    NSLog(@"starting");
    
    [self.locationManager startUpdatingHeading];
}

- (void) stopUpdating
{
    NSLog(@"Stopping");
    
    [self.locationManager stopUpdatingHeading];
}

/*

- (void) dealloc
{
    [locationManager release];
    
    [super dealloc];
}
 */
 
#pragma mark -
#pragma mark Geo Points methods

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{


    
    
    NSInteger magneticAngle = newHeading.magneticHeading;
    NSInteger trueAngle = newHeading.trueHeading;
    
   
 //   NSLog(@"New magnetic heading: %d", magneticAngle);
    

//    NSLog(@"New true heading: %d", trueAngle);
    
    
    //This is set by a switch in my apps settings //
    
    NSUserDefaults *prefs= [NSUserDefaults standardUserDefaults];
    BOOL magneticNorth = [prefs boolForKey:@"UseMagneticNorth"];
    
    if (magneticNorth == YES) {
 //       NSLog(@"using magnetic north");
        
        CGAffineTransform rotate = CGAffineTransformMakeRotation(degreesToRadians(-magneticAngle));
        [compassContainer setTransform:rotate];
    }
    else {
 //       NSLog(@"using true north");
        
        CGAffineTransform rotate = CGAffineTransformMakeRotation(degreesToRadians(-trueAngle));
        [compassContainer setTransform:rotate];
    }
}


@end
