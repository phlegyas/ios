//
//  ConversionRatePoundUpdateHandler.h
//  NewYork
//
//  Created by Moustoifa Moumini on 22/10/14.
//  Copyright (c) 2014 __AV PROD LTD__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConversionRatePoundUpdateHandler : NSObject <NSURLConnectionDelegate>
{
    NSMutableData* _responseData;
}

+ (NSURL*) conversionRateRetrievingURL;

- (void) updateConversionRate;
- (void) conversionRateReceived;

@end
