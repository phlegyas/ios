//
//  ConversionRateUpdateHandler.h
//  NewYork
//
//  Created by Tim Autin on 11/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConversionRateUpdateHandler : NSObject <NSURLConnectionDelegate>
{
    NSMutableData* _responseData;
}

+ (NSURL*) conversionRateRetrievingURL;

- (void) updateConversionRate;
- (void) conversionRateReceived;

@end
