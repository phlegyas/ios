//
//  ConversionRateUpdateHandler.m
//  NewYork
//
//  Created by Tim Autin on 11/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ConversionRateUpdateHandler.h"
#import "SBJson.h"
#import "Logger.h"

@implementation ConversionRateUpdateHandler


// ----------------------------------------------------------------------------------------------------
#pragma mark - Init function

- (id) init
{
    self = [super init];
    if (self)
    {
        
    }
    
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Class functions

+ (NSURL*) conversionRateRetrievingURL
{
    //return [NSURL URLWithString:@"http://www.google.com/ig/calculator?hl=en&q=1EUR=?USD"];
    return [NSURL URLWithString:@"http://rate-exchange.appspot.com/currency?from=EUR&to=USD"];
}

+ (BOOL) isNSNumber:(NSString*)string
{
    // Ensure the string is a number :
    NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier: @"en_US"]];
    
    if([formatter numberFromString:string] == nil) return NO;
    
    return YES;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark Conversion rate update handling

- (void) updateConversionRate
{
    Log(self.class, @"Updating conversion rate...");
    
    // Start a connection to retrieve the rate :
    _responseData = [NSMutableData data];
    NSURLRequest* request = [NSURLRequest requestWithURL:[ConversionRateUpdateHandler conversionRateRetrievingURL] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:4.0];
    NSURLConnection* connection = nil;
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) conversionRateReceived
{
    NSDate *myDates = [[NSDate alloc] init];
    [[NSUserDefaults standardUserDefaults] setObject:myDates forKey:@"myDateKey"];
    
    NSError* localError = nil;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:&localError];
    
    if (localError != nil) { Log(self.class, @"Error reading JSON conversion rate WebService."); return; }
    
    NSString* conversionRateString = [NSString stringWithFormat:@"%@", [json objectForKey:@"rate"]];
    
    if([ConversionRateUpdateHandler isNSNumber:conversionRateString])
    {
        // Save the value :
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setFloat:[conversionRateString floatValue] forKey:@"conversionRate_EUR_USD"];
        [prefs setFloat:[conversionRateString floatValue] forKey:@"conversionRate_EUR_USD_save"];
        
        Log(self.class, @"Conversion rate updated : %@", conversionRateString);
    }
}
// ----------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------
#pragma mark NSURLConnectionDelegate methods

- (void) connection:(NSURLConnection*)connection didReceiveData:(NSData *)data
{
	[_responseData appendData:data];
}

- (void) connection:(NSURLConnection*)connection didFailWithError:(NSError *)error
{
    Log(self.class, @"Conversion rate update failed : %@", [error localizedDescription]);
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"updated"];
}

- (void) connectionDidFinishLoading:(NSURLConnection*)connection
{
    [self conversionRateReceived];
}
// ----------------------------------------------------------------------------------------------------

@end



