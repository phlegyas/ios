//
//  CurrencyConverterPageController.h
//  NewYork
//
//  Created by Tim Autin on 16/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SBJson.h"
#import "UITopView.h"
#import "GAITrackedViewController.h"

@interface CurrencyConverterPageController : GAITrackedViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>
{
    CGRect _usableFrame;
    
    NSURLConnection* _connection;
    NSMutableData* _responseData;
    
    float _conversionRate_EUR_USD;
    float _conversionRate_GBP_USD;
    
    float _Rate;
    UIImageView* _FlagImageView;
    
    UITextField* _eurosTextField;
    UITextField* _dollarsTextField;
    
    UITopView* _topView;
    UITableView* _tableView;
    NSArray* _cellsBackgroundColors;
}

- (BOOL) pingServer:(NSURL*)urlString;

// Utils :
- (BOOL) stringContains:(NSString*)string serched:(NSString*)searched;
- (int) decimalsCount:(NSString*)string;
- (BOOL) isNSNumber:(NSString*)string;

// Event handling :
- (void) converterButtonClicked:(id)sender;
- (void) convertCurrency;

// Top view stuff :
- (void) settingsButtonClicked;
- (void) conversionRateAutoUpdateSwitchChecked;
- (void) topBarCancelButtonClicked;
- (void) topBarSaveButtonClicked;




@end
