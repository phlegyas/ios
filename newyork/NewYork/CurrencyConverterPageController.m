//
//  CurrencyConverterPageController.m
//  NewYork
//
//  Created by Tim Autin on 16/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SystemVersion.h"
#import "CurrencyConverterPageController.h"
#import "Colors.h"
#import "ConversionRateUpdateHandler.h"
#import "ConversionRatePoundUpdateHandler.h"

#define MAX_TEXTFIELD_LENGTH 12

@implementation CurrencyConverterPageController


// ----------------------------------------------------------------------------------------------------
#pragma mark - Init function

- (id) init
{
    self = [super init];
    if (self)
    {
        float s_save = [[NSUserDefaults standardUserDefaults] floatForKey:@"conversionRate_EUR_USD_save"];
        float s_save_p = [[NSUserDefaults standardUserDefaults] floatForKey:@"conversionRate_GBP_USD_save"];
        [[NSUserDefaults standardUserDefaults] setFloat:s_save forKey:@"conversionRate_EUR_USD"];
        // Try to update the conversion rate :
        if([self pingServer:[ConversionRateUpdateHandler conversionRateRetrievingURL]])
        {
            ConversionRateUpdateHandler* conversionRateUpdateHandler = [[ConversionRateUpdateHandler alloc] init];
            [conversionRateUpdateHandler updateConversionRate];
            ConversionRatePoundUpdateHandler* conversionRatePoundUpdateHandler = [[ConversionRatePoundUpdateHandler alloc] init];
            [conversionRatePoundUpdateHandler updateConversionRate];
           
        }
        else
        {
            NSLog( @"Conversion rate retrieving URL is unreachable");
            [[NSUserDefaults standardUserDefaults] setFloat:s_save_p forKey:@"conversionRate_GBP_USD"];
        }
        // Retrieve the conversion rate from the prefs :
        _conversionRate_EUR_USD = [[NSUserDefaults standardUserDefaults] floatForKey:@"conversionRate_EUR_USD"];
        _conversionRate_GBP_USD = [[NSUserDefaults standardUserDefaults] floatForKey:@"conversionRate_GBP_USD"];
        _Rate = _conversionRate_EUR_USD;
        
       // [[NSUserDefaults standardUserDefaults] setFloat:_Rate forKey:@"c_rate"];
    }

    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) loadView
{
    // Call super :
    [super loadView];
    self.screenName = @"Convertisseur";
    
    
    // Compute the usable frame :
    int statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    int toolBarHeight = self.navigationController.toolbar.frame.size.height;
    int tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    
    if(SYSTEM_VERSION < 7.0)
    {
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
        _usableFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - toolBarHeight - tabBarHeight);
    }
    else
    {
        _usableFrame = CGRectMake(0, statusBarHeight + toolBarHeight, self.view.frame.size.width, self.view.frame.size.height - statusBarHeight - toolBarHeight - tabBarHeight);
    }
    
    // Set the background color :
    self.view.backgroundColor = [Colors backgroundColor];
    
    // Set the view's title :
    self.title = NSLocalizedString(@"good_tips_tab_currency_converter_page_controller_title", @"Converter");
    
    // Set the colors :
    UIColor* buttonsTintColor = [Colors lightOrange];
    _cellsBackgroundColors = [[NSArray alloc] initWithObjects:
                              [Colors darkYellow],
                              [Colors lightYellow ],
                              nil];
    
    // Init some variables :
    int topItemsHeight = 50;
    int topItemsverticalMargin = 10;
    int flagsImagesWidth = topItemsHeight;
    int buttonWidth = 70;
    int buttonHeight = 50;
    int buttonsHorizontalMargin = 5;
    int buttonsVerticalMargin = 5;
    float horizontalMargin = (_usableFrame.size.width - 4*buttonWidth - 3*buttonsVerticalMargin) / 2.0;
    
    float verticalFlexibleSpace = (_usableFrame.size.height - topItemsverticalMargin - 2*topItemsHeight - 4*buttonHeight - 3*buttonsVerticalMargin) / 3.0;
    
    // Add the flags images :
    _FlagImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"EU_flag.png"]];
    UIImageView* USAFlagImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"USA_flag.png"]];
    
    // Set the images frames :
    _FlagImageView.frame = CGRectMake(horizontalMargin, _usableFrame.origin.y + verticalFlexibleSpace, flagsImagesWidth, topItemsHeight);
    USAFlagImageView.frame = CGRectMake(horizontalMargin, _usableFrame.origin.y + verticalFlexibleSpace + topItemsHeight + topItemsverticalMargin, flagsImagesWidth, topItemsHeight);
    
    // Add the flags :
    [self.view addSubview:_FlagImageView];
    [self.view addSubview:USAFlagImageView];
    

    NSDate *myDate = (NSDate *)[[NSUserDefaults standardUserDefaults] objectForKey:@"myDateKey"];
    NSLocale *locale = [NSLocale currentLocale];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];

    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setLocale:locale];
    
    UILabel *lastupdate = [[UILabel alloc] initWithFrame:CGRectMake(_usableFrame.origin.x / 2, _usableFrame.origin.y + 20,  _usableFrame.size.width, topItemsHeight)];
    lastupdate.textColor = [UIColor colorWithRed:(189/255.f) green:(195/255.f) blue:(199/255.f) alpha:1.0];
    lastupdate.lineBreakMode = NSLineBreakByWordWrapping;
    lastupdate.numberOfLines = 0;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [lastupdate setFont:[UIFont fontWithName:@"BanglaSangamMN-Bold" size: 16]];
        lastupdate.text = [NSString stringWithFormat:@"%@ \n %@", NSLocalizedString(@"good_tips_tab_currency_converter_page_controller_infos_update_message", @"Last update of the exchange rate :"),[formatter stringFromDate:myDate]];
        lastupdate.textAlignment = NSTextAlignmentCenter;
    }
    else
    {
        lastupdate.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"good_tips_tab_currency_converter_page_controller_infos_update_message", @"Last update of the exchange rate :"),[formatter stringFromDate:myDate]];
       [lastupdate setFont:[UIFont fontWithName:@"BanglaSangamMN-Bold" size: 10]];
        lastupdate.frame = CGRectMake(lastupdate.frame.origin.x, _usableFrame.origin.y, lastupdate.frame.size.width, 15);
      
        [lastupdate setNumberOfLines:0];
       // [lastupdate sizeToFit];
        lastupdate.textAlignment = NSTextAlignmentCenter;
        if ( ![[NSUserDefaults standardUserDefaults] objectForKey:@"myDateKey"])
        {
            lastupdate.frame = CGRectMake(lastupdate.frame.origin.x, _usableFrame.origin.y, lastupdate.frame.size.width, 28);
        
            if (verticalFlexibleSpace <= 14)
            {
                lastupdate.frame = CGRectMake(lastupdate.frame.origin.x, _usableFrame.origin.y, lastupdate.frame.size.width, 15);
                [lastupdate setFont:[UIFont fontWithName:@"BanglaSangamMN-Bold" size: 4]];
            }
        }
    }
    
    if ( ![[NSUserDefaults standardUserDefaults] objectForKey:@"myDateKey"])
        lastupdate.text = [NSString stringWithFormat:@"%@", NSLocalizedString(@"good_tips_tab_currency_converter_page_controller_infos_noupdate_message", "The exchange rate has never been updated. To update the exchange rate, connect your device to the Internet.")];
    
    [self.view addSubview:lastupdate];
    
    // Create the text fields :
    
    // Euros textfield :
    _eurosTextField = [[UITextField alloc] initWithFrame:CGRectMake(horizontalMargin + flagsImagesWidth + buttonsHorizontalMargin, _usableFrame.origin.y + verticalFlexibleSpace, _usableFrame.size.width - (2*horizontalMargin + flagsImagesWidth + buttonsHorizontalMargin), topItemsHeight)];
    _eurosTextField.tag = 0;
    _eurosTextField.borderStyle = UITextBorderStyleRoundedRect;
    _eurosTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _eurosTextField.inputView = [[UIView alloc] initWithFrame:CGRectZero]; // Trick to avoid the keyboard apparition
    _eurosTextField.placeholder = @"0.00";
    _eurosTextField.delegate = self;
    //[_eurosTextField addTarget:self action:@selector(convertCurrency) forControlEvents:UIControlEventEditingChanged];
    
    UILabel* eurosLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, _usableFrame.origin.y, 60, topItemsHeight)];
    eurosLabel.text = @"€";
    eurosLabel.textColor = [UIColor darkGrayColor];
    [eurosLabel sizeToFit];
    CGRect frame = eurosLabel.frame;
    frame.size.width += 5;
    eurosLabel.frame = frame;
    _eurosTextField.rightViewMode = UITextFieldViewModeAlways;
    _eurosTextField.rightView = eurosLabel;
    
    // Set the focus on the euros textfield :
    [_eurosTextField becomeFirstResponder];
    
    // Dollars textfield :
    _dollarsTextField = [[UITextField alloc] initWithFrame:CGRectMake(horizontalMargin + flagsImagesWidth + buttonsHorizontalMargin, _usableFrame.origin.y + verticalFlexibleSpace + topItemsHeight + topItemsverticalMargin, _usableFrame.size.width - (2*horizontalMargin + flagsImagesWidth + buttonsHorizontalMargin), topItemsHeight)];
    _dollarsTextField.tag = 1;
    _dollarsTextField.borderStyle = UITextBorderStyleRoundedRect;
    _dollarsTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _dollarsTextField.inputView = [[UIView alloc] initWithFrame:CGRectZero]; // Trick to avoid the keyboard apparition
    _dollarsTextField.placeholder = @"0.00";
    _dollarsTextField.delegate = self;
    //[_dollarsTextField addTarget:self action:@selector(convertCurrency) forControlEvents:UIControlEventEditingChanged];
    
    UILabel* dollarsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, _usableFrame.origin.y, 60, topItemsHeight)];
    dollarsLabel.text = @"$";
    dollarsLabel.textColor = [UIColor darkGrayColor];
    [dollarsLabel sizeToFit];
    frame = dollarsLabel.frame;
    frame.size.width += 5;
    dollarsLabel.frame = frame;
    _dollarsTextField.rightViewMode = UITextFieldViewModeAlways;
    _dollarsTextField.rightView = dollarsLabel;
    
    // Add the text fields :
    [self.view addSubview:_eurosTextField];
    [self.view addSubview:_dollarsTextField];
    
    
    
    // Create the buttons :
    
    // 1-9 numbers :
    int x, y;
    for(int i=0; i<9; i++)
    {
        // Compute position :
        x = horizontalMargin + (i%3) * (buttonWidth + buttonsHorizontalMargin);
        y =  _usableFrame.origin.y + 2*verticalFlexibleSpace + topItemsverticalMargin + 2*topItemsHeight + i/3 * (buttonHeight + buttonsVerticalMargin);
        
        // Create the button :
        UIButton* button = [[UIButton alloc] initWithFrame:CGRectMake( x, y, buttonWidth, buttonHeight)];
        [button setTag: i+1];
        [button setTitle:[NSString stringWithFormat:(@"%d"), i+1 ] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"button_1_light_gray.png"] forState:UIControlStateNormal];

        // Add the button to the view :
        [self.view addSubview:button];
    }
    
    // O number :
    
    // Create the button :
    UIButton* button0 = [[UIButton alloc] initWithFrame:CGRectMake(horizontalMargin,  _usableFrame.origin.y + 2*verticalFlexibleSpace + topItemsverticalMargin + 2*topItemsHeight + 3*(buttonHeight + buttonsVerticalMargin), 2*buttonWidth + buttonsHorizontalMargin, buttonHeight)];
    [button0 setTag: 0];
    [button0 setTitle:[NSString stringWithFormat:(@"%d"), 0] forState:UIControlStateNormal];
    [button0 setBackgroundImage:[UIImage imageNamed:@"button_2_light_gray.png"] forState:UIControlStateNormal];
    
    // Add the button to the view :
    [self.view addSubview:button0];
    
    // . button :
    
    // Create the button :
    UIButton* buttonDot = [[UIButton alloc] initWithFrame:CGRectMake(horizontalMargin + 2*(buttonWidth + buttonsHorizontalMargin),  _usableFrame.origin.y + 2*verticalFlexibleSpace + topItemsverticalMargin + 2*topItemsHeight + 3*(buttonHeight + buttonsVerticalMargin), buttonWidth, buttonHeight)];
    [buttonDot setTag: 10];
    [buttonDot setTitle:@"." forState:UIControlStateNormal];
    [buttonDot setBackgroundImage:[UIImage imageNamed:@"button_1_light_gray.png"] forState:UIControlStateNormal];
    [buttonDot setEnabled:NO];
    
    // Add the button to the view :
    [self.view addSubview:buttonDot];
    
    // c button :
    
    // Create the button :
    UIButton* buttonC = [[UIButton alloc] initWithFrame:CGRectMake(horizontalMargin + 3*(buttonWidth + buttonsHorizontalMargin),  _usableFrame.origin.y + 2*verticalFlexibleSpace + topItemsverticalMargin + 2*topItemsHeight, buttonWidth, buttonHeight)];
    [buttonC setTag: 11];
    [buttonC setTitle:@"c" forState:UIControlStateNormal];
    [buttonC setBackgroundImage:[UIImage imageNamed:@"button_1_red.png"] forState:UIControlStateNormal];
    
    // Add the button to the view :
    [self.view addSubview:buttonC];
    
    // del button :
    
    // Create the button :
    UIButton* buttonDel = [[UIButton alloc] initWithFrame:CGRectMake(horizontalMargin + 3*(buttonWidth + buttonsHorizontalMargin),  _usableFrame.origin.y + 2*verticalFlexibleSpace + topItemsverticalMargin + 2*topItemsHeight + buttonHeight + buttonsVerticalMargin, buttonWidth, 3*buttonHeight + 2*buttonsVerticalMargin)];
    [buttonDel setTag: 12];
    [buttonDel setBackgroundImage:[UIImage imageNamed:@"button_3_yellow.png"] forState:UIControlStateNormal];
    
    // Add the button to the view :
    [self.view addSubview:buttonDel];
    
    // Add target for each button :
    for(UIButton* button in self.view.subviews)
    {
        if([button isKindOfClass:UIButton.class])
        {
            [button addTarget:self action:@selector(converterButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    
    
    // Create the _topView :
    _topView = [[UITopView alloc] initWithHeight:250 superview:self.view marginTop:_usableFrame.origin.y toolBarStyle:UITopViewToolbarStyleSaveCancel];
    [_topView setSaveButtonTitle:NSLocalizedString(@"good_tips_tab_currency_converter_page_controller_top_view_save_button", @"Save") target:self action:@selector(topBarSaveButtonClicked)];
    [_topView setCancelButtonTitle:NSLocalizedString(@"good_tips_tab_currency_converter_page_controller_top_view_cancel_button", @"Cancel") target:self action:@selector(topBarCancelButtonClicked)];
    _topView.saveButton.tintColor = [Colors darkGreen];
    _topView.cancelButton.tintColor = buttonsTintColor;
    _topView.view.backgroundColor = [Colors backgroundColor];
        
    // Add a settings button to the topbar :
    UIBarButtonItem* settingsButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"settings.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(settingsButtonClicked)];
    if(SYSTEM_VERSION < 7.0) settingsButtonItem.tintColor = buttonsTintColor;
    
    // Create the tableView :
    _tableView = [[UITableView alloc] initWithFrame:_topView.view.bounds style:UITableViewStyleGrouped];
    _tableView.backgroundView = nil;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    
    // Add the views :
    self.navigationItem.rightBarButtonItem = settingsButtonItem;
    [_topView.view addSubview:_tableView];
    [self.view addSubview:_topView];
    

}


// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - Screen autorotate

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Utils functions

- (BOOL) stringContains:(NSString*)string serched:(NSString*)searched
{
    // Check whether "string" contains "searched" or not :
    NSRange range = [string rangeOfString : searched];
    return range.location != NSNotFound;
}

- (int) decimalsCount:(NSString*)string
{
    // Find the position of the "." caracter :
    NSRange range = [string rangeOfString:@"." ];
    
    // If not found, there is 0 decimals :
    if(range.location == NSNotFound) return 0;
    
    // Else return the string length minus the position of the "." :
    return (int) (string.length - range.location -1);
}

- (BOOL) isNSNumber:(NSString*)string
{
    // Ensure the string is a number :
    NSLocale *l_us = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US"];
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    [f setLocale: l_us];

    if([f numberFromString:string] == nil)
    {
        return NO;
    }
    
    return YES;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark Main view stuff

- (void) converterButtonClicked:(id)sender
{
    // Get the button tag :
    int tag = (int) ((UIButton*)sender).tag;
    
    // Know which textField is selected  and which one isn't:
    UITextField* selectedTextField = _eurosTextField;
    if(_dollarsTextField.isEditing == YES) selectedTextField = _dollarsTextField;
    
    // Update the selected textfield value :
    switch (tag)
    {        
        // Numbers :
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
        {
            // Save the current value :
            NSString* save = selectedTextField.text;
            
            // Insert the text :
            [selectedTextField insertText:[NSString stringWithFormat:@"%d", tag]];
            
            // If the new value is not a number with a maximum of 2 decimals :
            if(! [self isNSNumber:selectedTextField.text] )
            {
                // Cancel changes :
                selectedTextField.text = save;
            }
            if([self decimalsCount:selectedTextField.text] > 2 )
            {
                // Cancel changes :
                selectedTextField.text = save;
            }
            if(selectedTextField.text.length > MAX_TEXTFIELD_LENGTH)
            {
                // Cancel changes :
                selectedTextField.text = save;
            }
            
            // If the selectedTextfield doesn't contains dot :
            if(![self stringContains:selectedTextField.text serched:@"."])
            {
                // Enable the dot button :
                [(UIButton *)[self.view viewWithTag:10] setEnabled:YES];
            }
            
            
            break;
        }
            
        // Dot
        case 10:
        {
            // If the selectedTextfield doesn't contains dot :
            if(![self stringContains:selectedTextField.text serched:@"."] && selectedTextField.text.length < MAX_TEXTFIELD_LENGTH)
            {
                // Add the dot :
                selectedTextField.text = [selectedTextField.text stringByAppendingString:@"."];
                
                // Disable the dot button :
                [(UIButton *)[self.view viewWithTag:10] setEnabled:NO];
            }
            
            break;
        }
            
        // Cancel
        case 11:
        {
            // Empty the text fields and disable the dot button :
            _dollarsTextField.text = @"";
            _eurosTextField.text = @"";
            [(UIButton *)[self.view viewWithTag:10] setEnabled:NO];
            
            break;
        }
            
        // Del
        case 12:
        {
            // If the textfield contains text, strip the last caracter :
            if (selectedTextField.text.length > 0)
            {
                //selectedTextField.text = [selectedTextField.text substringToIndex:selectedTextField.text.length-1];
                [selectedTextField deleteBackward];
                
                // If the selectedTextfield doesn't contains dot and it's text length is still > 0:
                if(![self stringContains:selectedTextField.text serched:@"."] && selectedTextField.text.length > 0)
                {
                    // Enable the dot button :
                    [(UIButton *)[self.view viewWithTag:10] setEnabled:YES];
                }
                else
                {
                    // Disable the dot button :
                    [(UIButton *)[self.view viewWithTag:10] setEnabled:NO];
                }
            }
            
            break;
        }
            
        default: break;
    }
    
    [self convertCurrency];
}

- (void) convertCurrency
{
    // Select source and destination textfields :
    UITextField* sourceTextField = nil;
    UITextField* destinationTextField = nil;
 //   long double rate = _conversionRate_EUR_USD;
    long double rate = _Rate;
    
    if(_eurosTextField.isFirstResponder == YES)
    {
        sourceTextField = _eurosTextField;
        destinationTextField = _dollarsTextField;
    }
    else if(_dollarsTextField.isFirstResponder == YES)
    {
        sourceTextField = _dollarsTextField;
        destinationTextField = _eurosTextField;
        rate = 1.0 / _Rate;
    //    rate = 1.0 / _conversionRate_EUR_USD;
    }
    
    // Convert the value and update the other textfield :
    if(sourceTextField != nil && destinationTextField != nil)
    {
        // If the textfield is empty, empty the other one so that the placeholder will be displayed :
        if(sourceTextField.text.length == 0)
        {
            destinationTextField.text = @"";
        }
        else
        {
            // Get the value to convert :
            long double value = [sourceTextField.text doubleValue];
            long double convertedValue = value * rate;
            
            // Update the dollars textfield :
            destinationTextField.text = [NSString stringWithFormat:@"%0.2Lf", convertedValue];
        }
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark TextFieldDelegate methods

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // If the textfield is the one of the topview :
    if(textField.tag == 2) return YES;
    
    // Else it is one of the 2 textfields of the main view :
    else
    {
        // If the string is empty return YES (to allow the CUT action) :
        if(string.length == 0)
        {
            // Convert the currency :
            [self convertCurrency];
            
            return YES;
        }
        
        // If there is already a "." entered and the pasted value contains another one, dont paste :
        if([self stringContains:textField.text serched:@"."] && [self stringContains:string serched:@"."])
        {
            return NO;
        }
        
        // If there is already a "." entered and pasting the string after the "." will exceed the 2 decimals :
        if([self stringContains:textField.text serched:@"."])
        {
            if(range.location > [textField.text rangeOfString:@"."].location && [self decimalsCount:textField.text] + string.length > 2)
            {
                return NO;
            }
        }
        
        // If the text will exceed the MAX_TEXTFIELD_LENGTH :
        if(textField.text.length + string.length - range.length > MAX_TEXTFIELD_LENGTH)
        {
            return NO;
        }
        
        // Return YES if the string is a number :
        return [self isNSNumber:string];
    }
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return NO;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField.tag == 0 || textField.tag == 1)
    {
        // Make the conversion :
        [self convertCurrency];
        
        // If the clicked textfield contains a dot, disable the dot button :
        if([self stringContains:textField.text serched:@"."])
        {
            // Disable the dot button :
            [(UIButton *)[self.view viewWithTag:10] setEnabled:NO];
        }
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Table view data source and delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
    view.backgroundColor=[UIColor clearColor];
    
    UILabel *label=[[UILabel alloc]initWithFrame:view.frame];
    label.textAlignment = UITextAlignmentCenter;
    label.backgroundColor=[UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:16.0];
    label.text = NSLocalizedString(@"good_tips_tab_currency_converter_page_controller_top_view_title", @"Exchange rate update");
    
    if(SYSTEM_VERSION < 7.0)
    {
        label.textColor = [Colors lightYellow];
        label.shadowColor = [UIColor darkGrayColor];
        label.shadowOffset = CGSizeMake(1, 1);
    }
    else
    {
        label.textColor = [Colors darkTextColor];
    }
    
    [view addSubview:label];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{ 
    return 44; 
} 

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get the conversionRateAutoUpdate from the prefs :
    BOOL conversionRateAutoUpdate = [[NSUserDefaults standardUserDefaults] boolForKey:@"conversionRateAutoUpdate"];
   // float conversionRate_EUR_USD = [[NSUserDefaults standardUserDefaults] floatForKey:@"conversionRate_EUR_USD"];
    float conversionRate_EUR_USD = [[NSUserDefaults standardUserDefaults] floatForKey:@"c_rate"];

    // Get the cell :
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        // Get the cell :
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        // Set the cell's background color and selection style :
        cell.backgroundColor = [_cellsBackgroundColors objectAtIndex:(indexPath.row % _cellsBackgroundColors.count)];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        switch (indexPath.row)
        {
            case 0 :
            {
                // Set the cell's title :
                cell.textLabel.text = NSLocalizedString(@"good_tips_tab_currency_converter_page_controller_top_view_automatic_choice", @"Automatic");
                
                // Create a UISwitch :
                UISwitch* conversionRateAutoUpdateSwitch = [[UISwitch alloc] init];
                [conversionRateAutoUpdateSwitch setOn:conversionRateAutoUpdate];
                [conversionRateAutoUpdateSwitch addTarget:self action:@selector(conversionRateAutoUpdateSwitchChecked)  forControlEvents:UIControlEventValueChanged];
                
                cell.accessoryView = conversionRateAutoUpdateSwitch;
                cell.textLabel.enabled = conversionRateAutoUpdate;
                
                break;
            }
            case 1 :
            {
                // Set the cell's title :
                cell.textLabel.text = NSLocalizedString(@"good_tips_tab_currency_converter_page_controller_top_view_manual_choice", @"Manual (1€ = ?$) : ");
                cell.textLabel.enabled = ! conversionRateAutoUpdate;
                
                // Create a textfield :
                UITextField* conversionRateTextfield = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 105, 30)];
                conversionRateTextfield.tag = 2;
                conversionRateTextfield.borderStyle = UITextBorderStyleRoundedRect;
                conversionRateTextfield.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                conversionRateTextfield.delegate = self;
                conversionRateTextfield.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                
                conversionRateTextfield.enabled = ! conversionRateAutoUpdate;
                conversionRateTextfield.text = [NSString stringWithFormat:@"%f", conversionRate_EUR_USD];
                
                if(conversionRateAutoUpdate) conversionRateTextfield.textColor = [UIColor darkGrayColor];
                else conversionRateTextfield.textColor = [UIColor blackColor];
                
                // Set the textfield as the accessory view of the cell :
                cell.accessoryView = conversionRateTextfield;
                
                break;
            }
                
            case 2 :
            {
                
                
                // Set the cell's title :
                // cell.textLabel.text = NSLocalizedString(@"good_tips_tab_currency_converter_page_controller_top_view_manual_choice", @"Manual (1€ = ?$) : ");
                cell.textLabel.text = @"Euro";
                cell.textLabel.enabled = conversionRateAutoUpdate;
                
                
                UISwitch* euroswitch = [[UISwitch alloc] init];
                euroswitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"rateEuro"];
                if (euroswitch.on)
                {
                    _Rate = _conversionRate_EUR_USD;
                    _FlagImageView.image = [UIImage imageNamed: @"EU_flag.png"];
                    [[NSUserDefaults standardUserDefaults] setFloat:_Rate forKey:@"c_rate"];
                }
                [euroswitch addTarget:self action:@selector(euro)  forControlEvents:UIControlEventValueChanged];
                
                cell.accessoryView = euroswitch;
                
                break;
            }
                
            case 3 :
            {
                
                
                // Set the cell's title :
               // cell.textLabel.text = NSLocalizedString(@"good_tips_tab_currency_converter_page_controller_top_view_manual_choice", @"Manual (1€ = ?$) : ");
                cell.textLabel.text = NSLocalizedString(@"good_tips_tab_currency_converter_page_controller_top_view_pound_sterling", @"Pound Sterling");
                cell.textLabel.enabled = conversionRateAutoUpdate;
                

                UISwitch* livreswitch = [[UISwitch alloc] init];
                livreswitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"ratePound"];
                if (livreswitch.on)
                {
                    _Rate = _conversionRate_GBP_USD;
                    _FlagImageView.image = [UIImage imageNamed: @"EN_flag.png"];
                    [[NSUserDefaults standardUserDefaults] setFloat:_Rate forKey:@"c_rate"];
                }
                [livreswitch addTarget:self action:@selector(pound)  forControlEvents:UIControlEventValueChanged];
                
                cell.accessoryView = livreswitch;
                
            /*    // Create a textfield :
                UITextField* conversionRateTextfield = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 105, 30)];
                conversionRateTextfield.tag = 2;
                conversionRateTextfield.borderStyle = UITextBorderStyleRoundedRect;
                conversionRateTextfield.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                conversionRateTextfield.delegate = self;
                conversionRateTextfield.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                
                conversionRateTextfield.enabled = ! conversionRateAutoUpdate;
                conversionRateTextfield.text = [NSString stringWithFormat:@"%f", conversionRate_EUR_USD];
                if(conversionRateAutoUpdate) conversionRateTextfield.textColor = [UIColor darkGrayColor];
                else conversionRateTextfield.textColor = [UIColor blackColor];
                
                // Set the textfield as the accessory view of the cell :
                cell.accessoryView = conversionRateTextfield;
              */
                break;
            }
            
            default : break;
        }
    }
  
  //  [self checkRate];
    return cell;
    
    
    
}

- (void) euro
{
    UITableViewCell* cell1 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    UITableViewCell* cell2 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    UITableViewCell* cell3 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    
    UISwitch* Switch1 = (UISwitch*)cell2.accessoryView;
    UISwitch* Switch2 = (UISwitch*)cell3.accessoryView;

    Switch1.on = YES;
    Switch2.on = NO;
    _Rate = _conversionRate_EUR_USD;
    [[NSUserDefaults standardUserDefaults] setFloat:_Rate forKey:@"c_rate"];
    _FlagImageView.image = [UIImage imageNamed: @"EU_flag.png"];
    
    UITextField* conversionRateTextfield = (UITextField*)cell1.accessoryView;
    conversionRateTextfield.text = [NSString stringWithFormat:@"%f", _Rate];
    [self convertCurrency];
    
    return;
}

- (void) pound
{
    UITableViewCell* cell1 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    UITableViewCell* cell2 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    UITableViewCell* cell3 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    
    UISwitch* Switch1 = (UISwitch*)cell2.accessoryView;
    UISwitch* Switch2 = (UISwitch*)cell3.accessoryView;
    
    if (Switch2.on)
    {
        Switch1.on = NO;
        Switch2.on = YES;
        _Rate = _conversionRate_GBP_USD;
        [[NSUserDefaults standardUserDefaults] setFloat:_Rate forKey:@"c_rate"];
        _FlagImageView.image = [UIImage imageNamed: @"EN_flag.png"];
        
    }
    else
    {
        Switch1.on = YES;
        Switch2.on = NO;
        _Rate = _conversionRate_EUR_USD;
        [[NSUserDefaults standardUserDefaults] setFloat:_Rate forKey:@"c_rate"];
        _FlagImageView.image = [UIImage imageNamed: @"EU_flag.png"];
    }
    UITextField* conversionRateTextfield = (UITextField*)cell1.accessoryView;
    conversionRateTextfield.text = [NSString stringWithFormat:@"%f", _Rate];
    return;
}

- (void) checkRate
{
    UITableViewCell* cell1 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    UITableViewCell* cell2 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    UITableViewCell* cell3 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    
    UISwitch* Switch1 = (UISwitch*)cell2.accessoryView;
    UISwitch* Switch2 = (UISwitch*)cell3.accessoryView;
    if (Switch2.on == YES)
    {
        _Rate = _conversionRate_GBP_USD;
        [[NSUserDefaults standardUserDefaults] setFloat:_Rate forKey:@"c_rate"];
        _FlagImageView.image = [UIImage imageNamed: @"EN_flag.png"];
    }
    else if (Switch1.on == YES)
    {
        _Rate = _conversionRate_EUR_USD;
        [[NSUserDefaults standardUserDefaults] setFloat:_Rate forKey:@"c_rate"];
        _FlagImageView.image = [UIImage imageNamed: @"EU_flag.png"];
    }
    UITextField* conversionRateTextfield = (UITextField*)cell1.accessoryView;
    conversionRateTextfield.text = [NSString stringWithFormat:@"%f", _Rate];
    return;
}

- (BOOL) pingServer:(NSURL*)url
{
    NSURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:2.0];
    NSHTTPURLResponse *response = nil;
    NSError* error;
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    return (response != nil);
}

// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Top view stuff

- (void) settingsButtonClicked
{
    if(_topView.isVisible)
    {
        [self topBarCancelButtonClicked];
    }
    else
    {
        [_eurosTextField resignFirstResponder];
        [_dollarsTextField resignFirstResponder];
        [_topView show];
    }
}

- (void) conversionRateAutoUpdateSwitchChecked
{
    // Get the cells :
    UITableViewCell* cell0 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    UITableViewCell* cell1 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    UITableViewCell* cell2 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    UITableViewCell* cell3 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];

    
    // Get the labels, the switch and the textfield :
    UISwitch* conversionRateAutoUpdateSwitch = (UISwitch*)cell0.accessoryView;
    UITextField* conversionRateTextfield = (UITextField*)cell1.accessoryView;
    
    // Update the labels and the textfield :
    cell0.textLabel.enabled = conversionRateAutoUpdateSwitch.on;
    cell1.textLabel.enabled = ! conversionRateAutoUpdateSwitch.on;
    cell2.textLabel.enabled = conversionRateAutoUpdateSwitch.on;
    cell3.textLabel.enabled = conversionRateAutoUpdateSwitch.on;
    conversionRateTextfield.enabled = ! conversionRateAutoUpdateSwitch.on;
    
    if(conversionRateAutoUpdateSwitch.on)
    {
        conversionRateTextfield.textColor = [UIColor darkGrayColor];
        conversionRateTextfield.text = [NSString stringWithFormat:@"%f", _Rate];
    }
    else conversionRateTextfield.textColor = [UIColor blackColor];
}

- (void) topBarCancelButtonClicked
{
    // Get the cells :
    UITableViewCell* cell0 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    UITableViewCell* cell1 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    UITableViewCell* cell2 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    UITableViewCell* cell3 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];

    
    // Get the labels, the switch and the textfield :
    UISwitch* conversionRateAutoUpdateSwitch = (UISwitch*)cell0.accessoryView;
    UITextField* conversionRateTextfield = (UITextField*)cell1.accessoryView;
    UISwitch* Switch1 = (UISwitch*)cell2.accessoryView;
    UISwitch* Switch2 = (UISwitch*)cell3.accessoryView;
    
    // Get the values :
    BOOL conversionRateAutoUpdate = [[NSUserDefaults standardUserDefaults] boolForKey:@"conversionRateAutoUpdate"];
    float conversionRate_EUR_USD = [[NSUserDefaults standardUserDefaults] floatForKey:@"conversionRate_EUR_USD"];
    Switch1.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"rateEuro"];
    Switch2.on = [[NSUserDefaults standardUserDefaults] boolForKey:@"ratePound"];
    [self checkRate];

    
    // Hide the top view :
    [_topView hide];
    
    // Hide the keboard and give the focus to the euros textfield :
    if([conversionRateTextfield isFirstResponder])
    {
        // Hide the keyboard :
        [conversionRateTextfield resignFirstResponder];
        
        // Set the focus on the euros textfield once the keyboard is hidden :
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    }
    else
    {
        // Set the focus on the euros textfield :
        [_eurosTextField becomeFirstResponder];
    }
    
    // Set the values :
    [conversionRateAutoUpdateSwitch setOn:conversionRateAutoUpdate animated:YES];
    conversionRateTextfield.enabled = ! conversionRateAutoUpdate;
    cell0.textLabel.enabled = conversionRateAutoUpdate;
    cell1.textLabel.enabled = ! conversionRateAutoUpdate;
    conversionRateTextfield.text = [NSString stringWithFormat:@"%f", conversionRate_EUR_USD];
    if(conversionRateAutoUpdateSwitch.on) conversionRateTextfield.textColor = [UIColor darkGrayColor];
    else conversionRateTextfield.textColor = [UIColor blackColor];
}

- (void) topBarSaveButtonClicked
{
    // Get the cells :
    UITableViewCell* cell0 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    UITableViewCell* cell1 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    UITableViewCell* cell2 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    UITableViewCell* cell3 = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    
    
    // Get the labels, the switch and the textfield :
    UISwitch* conversionRateAutoUpdateSwitch = (UISwitch*)cell0.accessoryView;
    UITextField* conversionRateTextfield = (UITextField*)cell1.accessoryView;
    UISwitch* Switch1 = (UISwitch*)cell2.accessoryView;
    UISwitch* Switch2 = (UISwitch*)cell3.accessoryView;
    
    // Get the values :
    BOOL conversionRateAutoUpdate = conversionRateAutoUpdateSwitch.on;
    NSString* stringConversionRate_EUR_USD = conversionRateTextfield.text;
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:Switch1.on] forKey:@"rateEuro"];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:Switch2.on] forKey:@"ratePound"];
    
    if([self isNSNumber:stringConversionRate_EUR_USD])
    {
        float conversionRate_EUR_USD = [stringConversionRate_EUR_USD floatValue];
        
        // Save the values :
    //    _conversionRate_EUR_USD = conversionRate_EUR_USD;
        _Rate = conversionRate_EUR_USD;
        [[NSUserDefaults standardUserDefaults] setFloat:_Rate forKey:@"c_rate"];
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:conversionRateAutoUpdate] forKey:@"conversionRateAutoUpdate"];
        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithFloat:conversionRate_EUR_USD] forKey:@"conversionRate_EUR_USD"];
        
        // Hide the top view :
        [_topView hide];
        
        // Set the focus on the euros textfield :
        [_eurosTextField becomeFirstResponder];
    }
    else
    {
        // Show an alert view :
        UIAlertView *message = [[UIAlertView alloc]
                                initWithTitle:@"Erreur"
                                message:@"Le taux de change doit être un nombre."
                                delegate:nil
                                cancelButtonTitle:@"OK"
                                otherButtonTitles:nil];
        [message show];
    }
}

- (void) keyboardDidHide:(id)sender
{
    [_eurosTextField becomeFirstResponder];
}
// ----------------------------------------------------------------------------------------------------



@end



