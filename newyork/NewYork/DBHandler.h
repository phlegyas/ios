//
//  DBHandler.h
//  NewYork
//
//  Created by Tim Autin on 02/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DBHandler : NSObject
{
    NSString* _dbPath;
    sqlite3* _db;
}

- (id) initWithPath:(NSString*)path;
- (void) displayErrorMessageFrom:(NSString*)from message:(NSString*)message;

- (void) openConnexion;
- (void) closeConnexion;
- (void) reinitialiseConnexion;

@end
