//
//  DBHandler.m
//  NewYork
//
//  Created by Tim Autin on 02/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DBHandler.h"
#import "Logger.h"

@implementation DBHandler



// ----------------------------------------------------------------------------------------------------
#pragma mark - Handler lifecycle

- (id) initWithPath:(NSString*)path
{
    if ((self = [super init]))
    {
        // Save the path :
        _dbPath = path;
        
        [self openConnexion];
    }
    
    return self;
}

- (void) dealloc
{
    sqlite3_close(_db);
    _db = nil;
}

- (void) openConnexion
{
    // Open a connection to the database :
    NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* absolutePath = [NSString stringWithFormat:@"%@%@", documentsDirectory, _dbPath];

    if (sqlite3_open([absolutePath UTF8String], &_db) != SQLITE_OK)
    {
        Log(self.class, @"Failed to open database (%@) !", _dbPath);
    }
}

- (void) closeConnexion
{
    sqlite3_shutdown();
    sqlite3_close(_db);
    _db = nil;
}

- (void) reinitialiseConnexion
{
    [self closeConnexion];
    [self openConnexion];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Utils functions

- (void) displayErrorMessageFrom:(NSString*)from message:(NSString*)message
{
    NSLog(@"%@ -> %@ -> SQL error : %@", [self class], from, message);
}
// ----------------------------------------------------------------------------------------------------

@end
