//
//  DBPathMapping.h
//  DropBoxTest
//
//  Created by Tim Autin on 11/08/13.
//  Copyright (c) 2013 Tim Autin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Dropbox/Dropbox.h>

@interface DBPathMapping : NSObject

@property (strong, nonatomic) NSString* localPath;
@property (strong, nonatomic) DBFile* file;

@end