//
//  Day.h
//  NewYork
//
//  Created by Tim Autin on 20/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Day : NSObject
{
    int _uniqueId;
    int _travel;
    NSDate* _date;
}

@property (nonatomic, assign) int uniqueId;
@property (nonatomic, assign) int travel;
@property (nonatomic, copy) NSDate* date;

- (id)initWithUniqueId:(int)uniqueId travel:(int)travel date:(NSDate*)date;
- (NSString*) toString;

@end