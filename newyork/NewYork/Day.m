//
//  Day.m
//  NewYork
//
//  Created by Tim Autin on 20/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Day.h"

@implementation Day

@synthesize uniqueId = _uniqueId;
@synthesize travel = _travel;
@synthesize date = _date;

- (id)initWithUniqueId:(int)uniqueId travel:(int)travel date:(NSDate*)date
{
    if ((self = [super init]))
    {
        self.uniqueId = uniqueId;
        self.travel = travel;
        self.date = date;
    }
    return self;
}

- (void) dealloc
{
    self.date = nil;
}

- (NSString*) toString
{
    return [NSString stringWithFormat:@"DAY : %d, %d, %@", self.uniqueId, self.travel, self.date];
}

@end

