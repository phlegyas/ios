//
//  DayView.h
//  NewYork
//
//  Created by Tim Autin on 21/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Day.h"


@class DayView;
@protocol DayViewDelegate <NSObject>
@optional

- (void) dayViewWillStartEditingMode:(DayView*)dayView;
- (void) dayViewDidStartEditingMode:(DayView*)dayView;
- (void) dayViewWillStopEditingMode:(DayView*)dayView;
- (void) dayViewDidStopEditingMode:(DayView*)dayView;
- (void) dayViewHeightDidChange:(DayView*)dayView;

@end



@interface DayView : UIView <UIGestureRecognizerDelegate, UITextViewDelegate>
{
    id <DayViewDelegate> _delegate;
    Day* _day;
    UIScrollView* _scrollView;
    BOOL _isEditing;
    NSString* _textViewPlaceHolder;
    UITextView* _activeTextView;
    
    CGRect _viewSavedFrame;
    CGRect _scrollViewSavedFrame;
    
    CGSize _textViewTempContentSize;
}

@property(nonatomic, strong) id <DayViewDelegate> delegate;
@property(nonatomic, assign) BOOL isEditing;
@property(nonatomic, copy) UIScrollView* scrollView;
@property(nonatomic, copy) UITextView* activeTextView;

- (id) initWithFrame:(CGRect)frame day:(Day*)day;
- (void) startEditingMode:(id) sender;
- (void) stopEditingMode;

@end

