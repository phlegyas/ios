//
//  DayView.m
//  NewYork
//
//  Created by Tim Autin on 21/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SystemVersion.h"
#import <QuartzCore/QuartzCore.h>

#import "DayView.h"
#import "YourTravelsDBHandler.h"
#import "Colors.h"

@implementation DayView

@synthesize delegate = _delegate;
@synthesize isEditing = _isEditing;
@synthesize scrollView = _scrollView;
@synthesize activeTextView = _activeTextView;

#define DAY_TITLE_LABEL_HEIGHT 40
#define DAY_TIME_LABEL_HEIGHT 30
#define EDITING_MODE_TEXTVIEW_HEIGHT 45
#define MIN_TEXTVIEW_HEIGHT 45
#define HORIZONTAL_MARGIN 10
#define VERTICAL_PADDING 5



// ----------------------------------------------------------------------------------------------------
#pragma mark - Init function

- (id) initWithFrame:(CGRect)frame day:(Day*)day
{
    self = [super init];
    
    if (self)
    {
        // Save the day :
        _day = day;
        
        // Editing mode is off by default :
        _isEditing = NO;
        _activeTextView = nil;
        
        // Set the textview's placeholder :
        _textViewPlaceHolder = NSLocalizedString(@"your_travels_tab_day_view_textviews_placeholder", @"Double-tap to edit");
        
        // Init the view :
        CGRect viewFrame = frame;
        viewFrame.origin.y += VERTICAL_PADDING;
        viewFrame.size.height -= 2*VERTICAL_PADDING;
        
        self.frame = viewFrame;
        self.backgroundColor = [UIColor whiteColor];
        
        self.layer.cornerRadius = 5;
        self.layer.borderWidth = 2;
        self.layer.borderColor = [[Colors lightYellow] CGColor];
        
        self.layer.shadowColor = [[UIColor blackColor] CGColor];
        self.layer.shadowOpacity = 0.8;
        self.layer.shadowRadius = 3.0;
        self.layer.shadowOffset = CGSizeMake(2.0, 2.0);
        
        // Format the date :
        NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
        //NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"fr_FR"];
        //NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier: @"fr_FR"];
        NSLocale *locale = [NSLocale currentLocale];
        [dateFormater setLocale:locale];
        [dateFormater setDateFormat:@"cccc dd LLLL"];
        NSString* dateString = [dateFormater stringFromDate:_day.date];
        
        // Create the date label :
        UILabel* dayTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, DAY_TITLE_LABEL_HEIGHT)];
        dayTitleLabel.textAlignment = UITextAlignmentCenter;
        dayTitleLabel.backgroundColor = [UIColor clearColor];
        dayTitleLabel.text = dateString.capitalizedString;        
        
        // Add it to the scrollview :
        [self addSubview:dayTitleLabel];
        
        
        
        // Create the scroll view :
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, DAY_TITLE_LABEL_HEIGHT, self.frame.size.width, self.frame.size.height - DAY_TITLE_LABEL_HEIGHT)];
        _scrollView.backgroundColor = [UIColor clearColor];
        
        // Add the 3 activities (morning, afternoon, evenning) :
        int y = 0;
        for(int i=0; i<3; i++)
        {
            // Init some variables :
            DayTimeType dayTime;
            NSString* dayTimeString;
            
            switch (i)
            {
                case 0 :
                {
                    dayTime = MORNING;
                    dayTimeString = NSLocalizedString(@"your_travels_tab_day_view_activity_day_time_morning", @"Morning");
                    break;
                }
                case 1 :
                {
                    dayTime = AFTERNOON;
                    dayTimeString = NSLocalizedString(@"your_travels_tab_day_view_activity_day_time_afternoon", @"Morning");
                    break;
                }
                case 2 :
                {
                    dayTime = EVENING;
                    dayTimeString = NSLocalizedString(@"your_travels_tab_day_view_activity_day_time_evening", @"Evening");
                    break;
                }
            }
            
            
            
            // Get the activity :
            Activity* activity = [[YourTravelsDBHandler instance] activityOfDay:_day.uniqueId dayTime:dayTime];
            
            
            
            // Create a wrapper view :
            UIView* wrapperView = [[UIView alloc] initWithFrame:CGRectMake(0, y, _scrollView.bounds.size.width, DAY_TIME_LABEL_HEIGHT + MIN_TEXTVIEW_HEIGHT)];
            wrapperView.tag = i*3 +1;
            
            // Handle double tap on the wrapper view :
            UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(startEditingMode:)];
            tapRecognizer.numberOfTapsRequired = 2;
            tapRecognizer.numberOfTouchesRequired = 1;
            [wrapperView addGestureRecognizer:tapRecognizer];
            
            
            // Create the day time label :
            UILabel* dayTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(HORIZONTAL_MARGIN, 0, wrapperView.bounds.size.width - 2*HORIZONTAL_MARGIN, DAY_TIME_LABEL_HEIGHT)];
            dayTimeLabel.tag = i*3 +2;
            dayTimeLabel.text = dayTimeString;
            dayTimeLabel.textColor = [UIColor darkGrayColor];
            dayTimeLabel.backgroundColor = [UIColor clearColor];
            
            
            // Create the activity description textviews :
            UITextView* textView = [[UITextView alloc] initWithFrame:CGRectMake(2*HORIZONTAL_MARGIN, DAY_TIME_LABEL_HEIGHT, wrapperView.bounds.size.width - 3*HORIZONTAL_MARGIN, MIN_TEXTVIEW_HEIGHT)];
            textView.tag = i*3 +3;
            textView.text = activity.description;
            textView.backgroundColor = [UIColor clearColor];
            textView.font = [UIFont fontWithName:@"Helvetica" size:14.0f];
            textView.editable = NO;
            textView.userInteractionEnabled = NO;
            textView.clipsToBounds = YES;
            textView.delegate = self;
            if([activity.description isEqualToString:@""])
            {
                textView.text = _textViewPlaceHolder;
                textView.textColor = [UIColor lightGrayColor];
            }
            
            textView.scrollEnabled = NO;
            
            // Create a toolbar with a centered button :
            CGRect inputAccessoryViewFrame = CGRectMake(0, 0, textView.inputView.frame.size.width, 50);
            UIToolbar* toolBar = [[UIToolbar alloc] initWithFrame:inputAccessoryViewFrame];
            UIBarButtonItem* flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
            UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"your_travels_tab_day_view_keyboard_ok_button", @"OK") style:UIBarButtonItemStyleDone target:textView action:@selector(resignFirstResponder)];
            [toolBar setItems:[NSArray arrayWithObjects:flexibleSpace, doneButton, flexibleSpace, nil]];
            textView.inputAccessoryView = toolBar;
            
            
            // Add the widgets to the scrollview :
            [wrapperView addSubview:dayTimeLabel];
            [wrapperView addSubview:textView];
            [_scrollView addSubview:wrapperView];
            
            
            
            // Set textview height = textview content height (must be called once the textview has been added to the scrollview) :
            CGRect textViewFrame = textView.frame;
            if(SYSTEM_VERSION < 7.0) textViewFrame.size.height = textView.contentSize.height;
            else
            {
                [textView sizeToFit];
                textViewFrame.size.height = (textView.frame.size.height > EDITING_MODE_TEXTVIEW_HEIGHT) ? textView.frame.size.height : EDITING_MODE_TEXTVIEW_HEIGHT;
            }
            textView.frame = textViewFrame;
            
            CGRect wrapperViewFrame = wrapperView.frame;
            wrapperViewFrame.size.height = textViewFrame.size.height + DAY_TIME_LABEL_HEIGHT;
            wrapperView.frame = wrapperViewFrame;
            
            
            // Increment y :
            y += wrapperView.frame.size.height;
        }
        
        
        
        // Update the content size of the scroll view :
        CGSize contentSize = CGSizeMake(_scrollView.bounds.size.width, y);
        _scrollView.contentSize = contentSize;
        
        // Add the scrollview :
        [self addSubview:_scrollView];
        
        // Save the frames :
        _viewSavedFrame = self.frame;
        _scrollViewSavedFrame = _scrollView.frame;
    }
    return self;
}
// ----------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------
#pragma mark - Editing mode handling

- (void) startEditingMode:(id) sender
{
    // Notify the delegate :
    if ([_delegate respondsToSelector:@selector(dayViewWillStartEditingMode:)]) [_delegate dayViewWillStartEditingMode:self];
    
    // Compute new frames :
    CGRect newViewframe = self.frame;
    newViewframe.size.height = DAY_TITLE_LABEL_HEIGHT + _scrollView.contentSize.height;
    
    CGRect newScrollViewframe = _scrollView.frame;
    newScrollViewframe.size = _scrollView.contentSize;
    
    [UIView animateWithDuration:0.2 animations:^
    {
        self.frame = newViewframe;
        _scrollView.frame = newScrollViewframe;
    }
    completion:^(BOOL finished)
    {
        // Make the textviews editable :
        for(int i=0; i<3; i++)
        {
            // Get the widgets :
            UITextView* textView = (UITextView*) [self viewWithTag:i*3 +3];
            
            // Grant user interraction on the textView :
            textView.editable = YES;
            textView.userInteractionEnabled = YES;
        }
        
        // Set the taped view's textview as first responder :
        int textViewtag = (int) [(UIGestureRecognizer *)sender view].tag + 2;
        [[self viewWithTag:textViewtag] becomeFirstResponder];
        
        // Remember the state :
        _isEditing = YES;
        
        // Notify te delegate :
        if([_delegate respondsToSelector:@selector(dayViewDidStartEditingMode:)]) [_delegate dayViewDidStartEditingMode:self];
    }];
}

- (void) stopEditingMode
{
    // Notify the delegate :
    if ([_delegate respondsToSelector:@selector(dayViewWillStopEditingMode:)]) [_delegate dayViewWillStopEditingMode:self];
    
    // Make the textviews uneditable :
    for(int i=0; i<3; i++)
    {
        // Get the widgets :
        UITextView* textView = (UITextView*) [self viewWithTag:i*3 +3];
        
        // Forbid user interraction on the textView :
        textView.editable = NO;
        textView.userInteractionEnabled = NO;
        
        // Update the DB :
        DayTimeType dayTime;
        if(i==0) dayTime = MORNING;
        else if(i==1) dayTime = AFTERNOON;
        else dayTime = EVENING;
        NSString* description = textView.text;
        if([textView.textColor isEqual:[UIColor lightGrayColor]]) description = @"";
        
        [[YourTravelsDBHandler instance] updateActivityOfDay:_day.uniqueId dayTime:dayTime description:description];
    }
    
    // Saved the state :
    _isEditing = NO;
    
    // Animate the views :
    [UIView animateWithDuration:0.2 animations:^
    {
        self.frame = _viewSavedFrame;
        _scrollView.frame = _scrollViewSavedFrame;
    }
    completion:^(BOOL finished)
    {
         // Resign first responder (should be already done, iOS 7 bug ?) :
         [_activeTextView resignFirstResponder];
         _activeTextView = nil;
         
        // Notify the delegate :
        if ([_delegate respondsToSelector:@selector(dayViewDidStopEditingMode:)]) [_delegate dayViewDidStopEditingMode:self];
    }];
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - TextView delegate

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    _activeTextView = textView;
    
    if([textView.textColor isEqual:[UIColor lightGrayColor]])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    
    return YES;
}

- (BOOL) textViewShouldEndEditing:(UITextView *)textView
{
    if(textView.text.length == 0)
    {
        textView.textColor = [UIColor lightGrayColor];
        textView.text = _textViewPlaceHolder;
    }
    
    return YES;
}


- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    // Save the textView's content size :
    _textViewTempContentSize = textView.contentSize;
    
    return YES;
}

- (void) textViewDidChange:(UITextView *)textView
{
    // Set textview height = textview content height (must be called once the textview has been added to the scrollview) :
    CGRect textViewFrame = textView.frame;
    if(SYSTEM_VERSION < 7.0) textViewFrame.size.height = textView.contentSize.height;
    else
    {
        [textView sizeToFit];
        textViewFrame.size.height = (textView.frame.size.height > EDITING_MODE_TEXTVIEW_HEIGHT) ? textView.frame.size.height : EDITING_MODE_TEXTVIEW_HEIGHT;
    }
    textView.frame = textViewFrame;
    
    if(textView.frame.size.height != _textViewTempContentSize.height)
    {
        [self contentSizeChanged];
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
- (void) contentSizeChanged
{
    // Update the wrappers views height and y :
    int y = 0;
    for(int i=0; i<3; i++)
    {
        UIView* wrapperView = [_scrollView viewWithTag:(i*3 +1)];
        UITextView* textView = (UITextView*) [wrapperView viewWithTag:(i*3 +3)];
        
        CGRect frame = wrapperView.frame;
        frame.origin.y = y;
        frame.size.height = DAY_TIME_LABEL_HEIGHT + textView.frame.size.height;
        wrapperView.frame = frame;
        
        y += frame.size.height;
    }
    
    // Update the scrollView and view height :
    UIView* lastWrapperView = [_scrollView viewWithTag:(7)];
    
    CGSize size = _scrollView.contentSize;
    CGRect frame = _scrollView.frame;
    frame.size.height = lastWrapperView.frame.origin.y + lastWrapperView.frame.size.height;
    size.height = frame.size.height;
    
    _scrollView.contentSize = size;
    _scrollView.frame = frame;
    
    frame = self.frame;
    frame.size.height = DAY_TITLE_LABEL_HEIGHT + _scrollView.frame.size.height;
    self.frame = frame;
    
    // Notify delegate :
    if([_delegate respondsToSelector:@selector(dayViewHeightDidChange:)]) [_delegate dayViewHeightDidChange:self];
}
// ----------------------------------------------------------------------------------------------------

@end





