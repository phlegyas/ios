//
//  DropBoxHandler.h
//  DropBoxTest
//
//  Created by Tim Autin on 10/08/13.
//  Copyright (c) 2013 Tim Autin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Dropbox/Dropbox.h>

#import "DBPathMapping.h"

// ----------------------------------------------------------------------------------------------------
#pragma mark - DropBoxHandlerListener, a listener for the DropBoxHandler

@protocol DropBoxHandlerDelegate <NSObject>
- (void) didConnectToDropBox;
- (BOOL) mustSyncFileWithName:(NSString*)fileName inFolder:(DBPath*)folderPath;
- (void) fileUpdated:(DBPathMapping*)pathMapping;
- (void) fileDeletedAtLocalPath:(NSString*)localPath;
@end
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - FileVersion enum

typedef enum FileVersion : NSUInteger {
    LOCAL,
    REMOTE
} FileVersion;
// ----------------------------------------------------------------------------------------------------



@interface DropBoxHandler : NSObject
{
    NSMutableDictionary* _linkedFiles;      // NSString* -> DBPathMapping
}

@property (assign, nonatomic) FileVersion fileVersionToUse;
@property (assign, nonatomic) BOOL firstLink;
@property (assign, nonatomic) BOOL connectedToDropBox;
@property (strong, nonatomic) id<DropBoxHandlerDelegate> delegate;

- (id) initWithDelegate:(id<DropBoxHandlerDelegate>)delegate;
- (void) connectToDropBox;
- (void) unlinkFromDropBox;
- (BOOL) application:(UIApplication *)app openURL:(NSURL *)url sourceApplication:(NSString *)source annotation:(id)annotation;

- (void) linkFileAtLocalPath:(NSString*)localPath toFileAtRemotePath:(DBPath*)remotePath;
- (void) linkFolderAtLocalPath:(NSString*)localPath toFolderAtRemotePath:(DBPath*)remotePath;

- (void) updateRemoteFileAtPath:(DBPath*)path;
- (void) deleteRemoteFileAtPath:(DBPath*)path;


@end



