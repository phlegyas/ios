//
//  DropBoxHandler.m
//  DropBoxTest
//
//  Created by Tim Autin on 10/08/13.
//  Copyright (c) 2013 Tim Autin. All rights reserved.
//

#import "DropBoxHandler.h"
#import "AppDelegate.h"
#import "Logger.h"

@implementation DropBoxHandler


// ----------------------------------------------------------------------------------------------------
#pragma mark - Init method

- (id) init
{
    if ((self = [super init]))
    {
        self.connectedToDropBox = NO;
        self.fileVersionToUse = REMOTE;
        self.firstLink = NO;
        
        _linkedFiles = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

- (id) initWithDelegate:(id<DropBoxHandlerDelegate>)delegate
{
    if ((self = [super init]))
    {
        self.connectedToDropBox = NO;
        self.fileVersionToUse = REMOTE;
        self.delegate = delegate;
        
        _linkedFiles = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

- (void) setDelegate:(id<DropBoxHandlerDelegate>)delegate
{
    _delegate = delegate;
    if(self.connectedToDropBox) [self.delegate didConnectToDropBox];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - DropBox connection and linking handling

- (void) connectToDropBox
{
    DBAccountManager* accountManager = [[DBAccountManager alloc] initWithAppKey:@"w4oy2orqamfgyyf" secret:@"j47n8ka4zmocxzc"];
    [DBAccountManager setSharedManager:accountManager];
    
    DBAccount* account = accountManager.linkedAccount;
    if (account)
    {
        Log(self.class, @"App already linked");
        self.firstLink = NO;
        
        DBFilesystem* fileSystem = [[DBFilesystem alloc] initWithAccount:account];
        [DBFilesystem setSharedFilesystem:fileSystem];
        
        // Make an asynchronous call to listFolder to wait until first sync :
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^()
        {
            [fileSystem listFolder:[DBPath root] error:nil];
            [self didConnectToDropBox];
        });
    }
    else Log(self.class, @"App not linked");
}

- (BOOL) application:(UIApplication *)app openURL:(NSURL *)url sourceApplication:(NSString *)source annotation:(id)annotation
{
    DBAccount* account = [[DBAccountManager sharedManager] handleOpenURL:url];
    if (account)
    {
        self.firstLink = YES;
        
        Log(self.class, @"[INFO] App linked successfully!");
        
        // Set the shared fileSystem :
        DBFilesystem* fileSystem = [[DBFilesystem alloc] initWithAccount:account];
        [DBFilesystem setSharedFilesystem:fileSystem];
        
        // Make an asynchronous call to listFolder to wait until first sync :
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^()
        {
            [fileSystem listFolder:[DBPath root] error:nil];
            [self didConnectToDropBox];
        });
        
        return YES;
    }
    else
    {
        Log(self.class, @"[INFO] User cancelled linking");
    }
    
    return NO;
}

- (void) unlinkFromDropBox
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"linkedToDropBox"];
    self.connectedToDropBox = NO;
    
    _linkedFiles = [[NSMutableDictionary alloc] init];
    
    [[DBAccountManager sharedManager].linkedAccount unlink];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - DropBox connection has been established

- (void) didConnectToDropBox
{
    Log(self.class, @"[INFO] didConnectToDropBox");
 
    self.connectedToDropBox = YES;
    
    [self.delegate didConnectToDropBox];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Files handling (from DropBox to disk) :

- (void) linkFileAtLocalPath:(NSString*)localPath toFileAtRemotePath:(DBPath*)remotePath
{
    if(!self.connectedToDropBox) return;
    
    Log(self.class, @"[INFO] Link file \"%@\"", remotePath.name);
    
    // Check if we already have a mapping for this file :
    DBPathMapping* mapping = [_linkedFiles objectForKey:remotePath.stringValue];
    
    // If the file is already linked, print a warning and exit :
    if(mapping)
    {
        if(mapping.localPath != nil)
        {
            Log(self.class, @"[WARNING] File \"%@\" already linked", remotePath.name);
            return;
        }
    }
    else
    {
        // If the file is not already linked, create a mapping :
        mapping = [[DBPathMapping alloc] init];
        mapping.localPath = localPath;
    }
    
    // Create som variables :
    DBFile* file = nil;
    DBError* error = nil;
    
    // Check if the DBFile exists :
    BOOL fileExists = [self fileExistsAtPath:remotePath];
    
    if (!fileExists || (fileExists && self.fileVersionToUse == LOCAL && self.firstLink))
    {
        if(!fileExists) Log(self.class, @"[INFO] File does not exist, create it");
        else Log(self.class, @"[INFO] First sync, existing remote file will be replaced by local file");
        
        // Create the file :
        error = nil;
        if(fileExists) [[DBFilesystem sharedFilesystem] deletePath:remotePath error:&error];
        file = [[DBFilesystem sharedFilesystem] createFile:remotePath error:&error];
        if(error) Log(self.class, @"[ERROR] Error creating file : %@", error.localizedDescription);
        
        // Write the data on the file :
        NSData* data = [NSData dataWithContentsOfFile:mapping.localPath];
        error = nil;
        [file writeData:data error:&error];
        if(error) Log(self.class, @"[ERROR] Error writing on file : %@", error.localizedDescription);
    }
    else
    {
        Log(self.class, @"[INFO] File exists, open it");
        
        // Open the file :
        error = nil;
        file = [[DBFilesystem sharedFilesystem] openFile:remotePath error:&error];
        if(error) Log(self.class, @"[ERROR] Error opening file : %@", error.localizedDescription);
    }
    
    // Save the file and the mapping :
    mapping.file = file;
    [_linkedFiles setObject:mapping forKey:remotePath.stringValue];
    
    [file addObserver:self block:^{ [self updateLocalFileAtPath:mapping]; }];
    
    if(self.fileVersionToUse == REMOTE) [self updateLocalFileAtPath:mapping];
}

- (void) linkFolderAtLocalPath:(NSString*)localFolderPath toFolderAtRemotePath:(DBPath*)remoteFolderPath
{
    if(!self.connectedToDropBox) return;
    
    Log(self.class, @"[INFO] Link folder \"%@\"", remoteFolderPath.stringValue);
    
    // Get an array of the images (on the iPhone) :
    NSArray* files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:localFolderPath error:nil];
    for(NSString* fileName in files)
    {
        if([self.delegate mustSyncFileWithName:fileName inFolder:remoteFolderPath])
        {
            NSString* localFilePath = [localFolderPath stringByAppendingPathComponent:fileName];
            DBPath* remoteFilePath = [remoteFolderPath childPath:fileName];
            
            [self linkFileAtLocalPath:localFilePath toFileAtRemotePath:remoteFilePath];
        }
    }
    
    // Add an observer to the folder :
    [[DBFilesystem sharedFilesystem] addObserver:self forPathAndChildren:remoteFolderPath block:^
    {
        [self updateLocalFolderAtPath:localFolderPath remoteFolderPath:remoteFolderPath];
    }];
    
    [self updateLocalFolderAtPath:localFolderPath remoteFolderPath:remoteFolderPath];
}

- (void) updateLocalFolderAtPath:(NSString*)localFolderPath remoteFolderPath:(DBPath*)remoteFolderPath
{
    if(!self.connectedToDropBox) return;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^()
    {
        // Get the files names :
        NSArray* fileInfos = [[DBFilesystem sharedFilesystem] listFolder:remoteFolderPath error:nil];
        
        // Ensure that the files matches the pattern given by the delegate :
        NSIndexSet* validFileNames = [fileInfos indexesOfObjectsPassingTest:^BOOL(DBFileInfo* obj, NSUInteger idx, BOOL *stop)
        {
            NSString* fileName = obj.path.name;
            return [self.delegate mustSyncFileWithName:fileName inFolder:remoteFolderPath];
        }];
        
        // Remove the file that doesn't match :
        fileInfos = [fileInfos objectsAtIndexes:validFileNames];
        
        // For each file in the folder :
        for (DBFileInfo* fileInfo in fileInfos)
        {
            // If the file is not in the _linkedFiles, link it :
            if(![_linkedFiles objectForKey:fileInfo.path.stringValue])
            {
                // Get the path :
                NSString* localFilePath = [localFolderPath stringByAppendingPathComponent:fileInfo.path.name];
                DBPath* remoteFilePath = [remoteFolderPath childPath:fileInfo.path.name];
                
                // Get the remote file :
                DBError* error;
                DBFile* file = [[DBFilesystem sharedFilesystem] openFile:remoteFilePath error:&error];
                if(error) Log(self.class, @"[ERROR] Error openning file \"%@\" : %@", remoteFilePath.name, error.localizedDescription);
                
                // Create the local file :
                error = nil;
                [[NSFileManager defaultManager] createFileAtPath:localFilePath contents:[file readData:&error] attributes:nil];
                if(error) Log(self.class, @"[ERROR] Error reading file \"%@\" : %@", remoteFilePath.name, error.localizedDescription);
                
                // Close the remote file :
                [file close];
                
                // Link the file :
                [self linkFileAtLocalPath:localFilePath toFileAtRemotePath:remoteFilePath];
            }
        }
    });
}

- (void) updateLocalFileAtPath:(DBPathMapping*)mapping
{
    if(!self.connectedToDropBox) return;
    
    DBFileStatus* status = mapping.file.newerStatus != nil ? mapping.file.newerStatus : mapping.file.status;
    
    NSString* newerStatus = mapping.file.newerStatus != nil ? @"newerStatus" : @"status";
    NSString* statusCached = status.cached ? @"(cached)" : @"(not yet cached)";
    NSString* statusState = @"";
    switch (status.state)
    {
        case DBFileStateDownloading: statusState = @"downloading..."; break;
        case DBFileStateUploading: statusState = @"uploading..."; break;
        case DBFileStateIdle: statusState = @"idle"; break;
        default: break;
    }
    
    Log(self.class, @"[INFO] %@ : %@ %@", newerStatus, statusState, statusCached);
    
    if(![self fileExistsAtPath:mapping.file.info.path])
    {
        Log(self.class, @"[INFO] File deleted");
        
        [_linkedFiles removeObjectForKey:mapping.file.info.path.stringValue];
        [[NSFileManager defaultManager] removeItemAtPath:mapping.localPath error:nil];
        mapping.file = nil; // Need to be done, else when openning a file at the same path an error is thrown
        
        [self.delegate fileDeletedAtLocalPath:mapping.localPath];
    }
    else if(mapping.file.newerStatus.cached || (self.firstLink && mapping.file.status.cached && mapping.file.status.state == DBFileStateIdle) )
    {
        // Update the file :
        [mapping.file update:nil];
        
        // Write the file :
        NSData* data = [mapping.file readData:nil];
        [data writeToFile:mapping.localPath atomically:YES];
        
        // Notify the delegate :
        [self.delegate fileUpdated:mapping];
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - File update and delete (from disk to DropBox) :

- (void) updateRemoteFileAtPath:(DBPath*)path
{
    if(!self.connectedToDropBox) return;
    
    // Get the path mapping :
    DBPathMapping* mapping = [_linkedFiles objectForKey:path.stringValue];
    
    // If we still not found the mapping, that means that the file is not linked. Print a warning and return :
    if(!mapping)
    {
        Log(self.class, @"[WARNING] File \"%@\" not linked", path.name);
        return;
    }
    
    // Get the data :
    NSData* data = [[NSFileManager defaultManager] contentsAtPath:mapping.localPath];
    
    // Update the DBFile :
    DBError* error;
    [mapping.file writeData:data error:&error];
    if(error) Log(self.class, @"[ERROR] Error writing on \"%@\" : %@", mapping.file.info.path.name, error.localizedDescription);
    
    Log(self.class, @"[INFO] Updating \"%@\" file on DropBox...", mapping.file.info.path.name);
}

- (void) deleteRemoteFileAtPath:(DBPath*)path
{
    if(!self.connectedToDropBox) return;
    
    // Get the mapping :
    DBPathMapping* mapping = [_linkedFiles objectForKey:path.stringValue];
    
    // If we still not found the mapping, that means that the file is not linked. Print a warning and return :
    if(!mapping)
    {
        Log(self.class, @"[WARNING] File \"%@\" not linked", path.name);
        return;
    }
    
    // Get the file :
    DBFile* file = mapping.file;
    
    Log(self.class, @"[INFO] Deleting \"%@\" file on DropBox...", file.info.path.name);
    
    // Remove the file from the containers and remove its observer:
    [_linkedFiles removeObjectForKey:path.stringValue];
    [file removeObserver:self];
    
    DBError* error;
    [[DBFilesystem sharedFilesystem] deletePath:file.info.path error:&error];
    if(error) Log(self.class, @"[ERROR] Error deleting image \"%@\" : %@", file.info.path.name, error.localizedDescription);
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Utils

- (BOOL) fileExistsAtPath:(DBPath*)path
{
    DBError* error = nil;
    if (![[DBFilesystem sharedFilesystem] fileInfoForPath:path error:&error])
    {
        // Handle unknow errors :
		if ([error code] != DBErrorParamsNotFound) Log(self.class, @"[ERROR] Error getting file info : %@", error.localizedDescription);
        
        // The file does not exist :
        return NO;
    }
    
    return YES;
}
// ----------------------------------------------------------------------------------------------------

@end



