//
//  EditHotspotController.h
//  NewYork
//
//  Created by Tim Autin on 05/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HotspotEditionController.h"


@class EditHotspotController;
@protocol EditHotspotControllerDelegate <NSObject>
@optional
- (void) editHotspotController:(EditHotspotController *)editHotspotController didFinishEditingHotspot:(Hotspot*)hotspot;
@end


@interface EditHotspotController : HotspotEditionController
{
    __weak id <EditHotspotControllerDelegate> _delegate;
}

@property (nonatomic, weak) id <EditHotspotControllerDelegate> delegate;

@end
