//
//  EditHotspotController.m
//  NewYork
//
//  Created by Tim Autin on 05/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EditHotspotController.h"
#import "UserHotspotsDBHandler.h"
#import "Logger.h"

@implementation EditHotspotController

@synthesize delegate = _delegate;


// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) loadView
{
    // Call super :
    [super loadView];
    
    // Set the view's title :
    [_hotspotEditionView setViewTitle:_newHotspot.title];

    // Set the data :
    [_hotspotEditionView setTitle:_newHotspot.title];
    [_hotspotEditionView setDescription:_newHotspot.description];
    [_hotspotEditionView setType:_newHotspot.type];
    [_hotspotEditionView setImage:[[UserHotspotsDBHandler instance] imageOfHotspot:_newHotspot]];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Screen autorotate

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Save the hotspot

- (void) saveHotspot:(Hotspot *)hotspot image:(UIImage *)image mustSaveImage:(BOOL)mustSaveImage
{
    // Save the hotspot in the DB :
    [[UserHotspotsDBHandler instance] updateHotspotWithId:hotspot.uniqueId setTitle:hotspot.title description:hotspot.description longitude:hotspot.longitude latitude:hotspot.latitude type:hotspot.type];
    
    // If the image is not null :
    if(mustSaveImage)
    {
        if(image != nil)
        {
            // Save the image :
            [[UserHotspotsDBHandler instance] saveImage:image forHotspot:hotspot];
        }
        else
        {
            // Delete the previous image if any :
            if([[UserHotspotsDBHandler instance] imageOfHotspot:_newHotspot] != nil) [[UserHotspotsDBHandler instance] deleteImageOfHotspot:hotspot];
        }
    }
    
    // Open an alert to inform the user :
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"new_york_tab_edit_hotspot_controller_edition_saved_confirmation_popup_title", @"Confirmation") message:NSLocalizedString(@"new_york_tab_edit_hotspot_controller_edition_saved_confirmation_popup_message", @"Your hotspot has successfully been updated") delegate:self cancelButtonTitle:NSLocalizedString(@"new_york_tab_edit_hotspot_controller_edition_saved_confirmation_popup_ok_button", @"OK") otherButtonTitles: nil];
    alert.tag = 3;
    [alert show];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - UIAlertView delegate

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    // Call super :
    [super alertView:alertView didDismissWithButtonIndex:buttonIndex];
    
    if(alertView.tag == 3)
    {
        // Notify delegate :
        if([self.delegate respondsToSelector:@selector(editHotspotController:didFinishEditingHotspot:)])
        {
            [self.delegate editHotspotController:self didFinishEditingHotspot:_newHotspot];
        }
    }
}
// ----------------------------------------------------------------------------------------------------

@end



