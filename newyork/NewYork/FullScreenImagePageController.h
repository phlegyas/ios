//
//  FullScreenImagePageController.h
//  NewYork
//
//  Created by Tim Autin on 07/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GAITrackedViewController.h"

@interface FullScreenImagePageController : GAITrackedViewController
{
    CGRect _usableFrame;
    int _pageId;
}

- (id)initWithPageId:(int)pageId;
- (void) handleTap;

@end
