//
//  FullScreenImagePageController.m
//  NewYork
//
//  Created by Tim Autin on 07/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FullScreenImagePageController.h"
#import "GoodTipsDBHandler.h"
#import "UIZoomableImageView.h"
#import "Colors.h"

@implementation FullScreenImagePageController

// ----------------------------------------------------------------------------------------------------
#pragma mark - Init function

- (id)initWithPageId:(int)pageId
{
    self = [super init];
    if (self)
    {
        // Save the id :
        _pageId = pageId;
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
    // Call super :
    [super loadView];
    self.screenName = @"l'affiche I love BPVNY";
    
    // Set the window's title :
    self.title = [[GoodTipsDBHandler instance] locationWithId:[[GoodTipsDBHandler instance] pageWithId:_pageId].location].title;
    self.view.backgroundColor = [Colors backgroundColor];
    
    // Compute the usable frame :
    int toolBarHeight = self.navigationController.toolbar.frame.size.height;
    _usableFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - toolBarHeight);
    
    // Get the image file path :
    NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString* imgFileName = [NSString stringWithFormat:@"%@/good_tips/images/%d_%@.png", documentsDirectory, _pageId, language];
    
    // Create an image view :
    UIImage* image = [UIImage imageWithContentsOfFile:imgFileName];
    UIImageView* imageView = [[UIImageView alloc] initWithImage:image];
    
    CGRect frame;
    float width;
    float height;
    
    float ratio = image.size.height / image.size.width;
    
    if(image.size.width < image.size.height)
    {
        width = image.size.width > _usableFrame.size.width ? _usableFrame.size.width : image.size.width;
        height = width * ratio;
    }
    else
    {
        height = image.size.height > _usableFrame.size.height ? _usableFrame.size.height : image.size.height;
        width = height / ratio;
    }
    int x = (_usableFrame.size.width - width)/2;
    int y = (_usableFrame.size.height - height)/2;
    frame = CGRectMake(x, y, width, height);
    
    imageView.frame = frame;
    
    // Add tap gesture recognizer :
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap)];
    [self.view addGestureRecognizer:tap];
    
    // Add the view :
    //[self.view addSubview:zoomableImageView];
    [self.view addSubview:imageView];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
// Handle tap :
- (void) handleTap
{
    [self dismissModalViewControllerAnimated:YES];
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - Screen autorotate

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
// ----------------------------------------------------------------------------------------------------

@end

