//
//  GoodTipsController.h
//  NewYork
//
//  Created by Tim Autin on 14/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIGridView.h"


@interface GoodTipsController : GAITrackedViewController <UIScrollViewDelegate, UIGridViewDelegate>
{
    CGRect _usableFrame;
    NSArray* _displayedLocations;
    CGPoint _touchedPoint;
    UIGridView* _gridView;
    //notification
    NSArray     *_checklistItems;
    int         c;
}

- (void) checkTheList;

@end
