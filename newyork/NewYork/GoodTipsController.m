//
//  GoodTipsController.m
//  NewYork
//
//  Created by Tim Autin on 14/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SystemVersion.h"
#import "GoodTipsController.h"
#import "GoodTipsDBHandler.h"
#import "LocationController.h"
#import "HTMLPageController.h"
#import "PlanningPageController.h"
#import "ChecklistPageController.h"
#import "ImagePageController.h"
#import "FullScreenImagePageController.h"
#import "CurrencyConverterPageController.h"
#import "TranslationsPageController.h"
#import "TipsComputerPageController.h"
#import "Colors.h"

@implementation GoodTipsController


// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) loadView
{
    
    // Call super :
    [super loadView];
    self.screenName = @"Bons plans";
 
     [self checkTheList];
    // Set the background color :
    self.view.backgroundColor = [Colors backgroundColor];
    
    // Set the back button title (for descendants controllers) :
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"good_tips_tab_good_tips_controller_back_button", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    
    // Compute the usable frame :
    int statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    int toolBarHeight = self.navigationController.toolbar.frame.size.height;
    int tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    
    if(SYSTEM_VERSION < 7.0)
    {
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
        _usableFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - toolBarHeight - tabBarHeight);
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = NO;
        _usableFrame = CGRectMake(0, statusBarHeight + toolBarHeight, self.view.frame.size.width, self.view.frame.size.height - statusBarHeight - toolBarHeight - tabBarHeight);
    }
    
    // Get the locations at the root location :
    _displayedLocations = [[GoodTipsDBHandler instance] locationsAtLocation:0];
    
    // Compute the sizes :
    int imageWidth = 50;
    int imageHeight = 50;
    int labelHeight = 50;
    int cellHeight = _usableFrame.size.height/3.0;
    int cellPadding = (cellHeight - (imageHeight + labelHeight) ) / 2.0;
    
    // Init the grid view :
    CGRect gridViewFrame = _usableFrame;
    _gridView = [[UIGridView alloc] initWithFrame:gridViewFrame columnCount:3 rowHeight:cellHeight];
    _gridView.delegate = self;

    // Insert the views :
    for(int i=0; i<_displayedLocations.count; i++)
    {
        // Create and add the cell :
        UIGridViewCell* cell = [_gridView addEmptyCell];
        
        cell.backgroundColor = [UIColor clearColor];
        
        // Get the location :
        Location* location = [_displayedLocations objectAtIndex:i];
        cell.contentTag = [NSNumber numberWithInt:location.uniqueId];
        
        // Create an image :
        NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString* iconImgFileName = [NSString stringWithFormat:@"%@/good_tips/icons/%d.png", documentsDirectory, location.uniqueId];
        UIImageView* imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:iconImgFileName]];
        imageView.frame = CGRectMake(cell.bounds.origin.x + cell.bounds.size.width/2.0 - imageWidth/2.0, cellPadding, imageWidth, imageHeight);
        
        // Create a label :
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, cellPadding + imageHeight, cell.bounds.size.width, labelHeight)];
        
        NSString* convertedString = [location.title mutableCopy];
        CFStringRef transform = CFSTR("Any-Hex/Java");
        CFStringTransform((__bridge CFMutableStringRef)convertedString, NULL, transform, YES);
        
        label.text = [convertedString stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
        
        label.font = [UIFont boldSystemFontOfSize:12.0];
        label.backgroundColor = cell.backgroundColor;
        label.numberOfLines = 0;
        label.lineBreakMode = UILineBreakModeWordWrap;
        label.textAlignment = UITextAlignmentCenter;
        
        if(SYSTEM_VERSION < 7.0)
        {
            label.textColor = [UIColor whiteColor];
            label.shadowColor = [UIColor darkGrayColor];
            label.shadowOffset = CGSizeMake(1, 1);
        }
        else
        {
            label.textColor = [UIColor darkTextColor];
        }
        
        // Add the label and image :
        [cell addSubview:imageView];
        [cell addSubview:label];
        
        if (i == 1)
        {
            //init notif
            UILabel     *notif = [[UILabel alloc] initWithFrame:CGRectMake(cell.bounds.origin.x + cell.bounds.size.width/2.0 + imageWidth/3.0, cellPadding - imageHeight/3.0, imageWidth, imageHeight)];
            if (c >= 10)
                notif.textColor = [UIColor redColor];
            else
                notif.textColor = [UIColor blueColor];
            if (c)
                notif.text = [NSString stringWithFormat:@"%i",  c];
            else
            {
                notif.text = [NSString stringWithFormat:@"OK"];
                notif.textColor = [UIColor greenColor];
            }
            [cell addSubview: notif];
            [GoodTipsDBHandler instance]->notif = notif;
            [GoodTipsDBHandler instance]->c = c;
        }
    }
    
    [self.view addSubview:_gridView];
}

- (void) checkTheList
{
    int i;
    
    c = 0;
    for (i = 16; i < 20; i++) {
        _checklistItems = [[GoodTipsDBHandler instance] checklistItemsWithPageId: i];
        
        for (ChecklistItem* item in _checklistItems ) {
            if  (item.checked == NO)
                c++;
        }
    }
    //       _checklistItems = [[GoodTipsDBHandler instance] checklistItemsWithPageId: 16];
    //   ChecklistItem* item = [_checklistItems objectAtIndex:0];
    //   item.checked;
    
}

- (void) viewWillAppear:(BOOL)animated
{
    // Call super
    [super viewWillAppear:animated];
    
    // For each view, fade the background color to clearColor :
    [_gridView fadeAllViewsBackgroundColorToClearColorWithDuration:0.2 delay:0.3];
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - Screen autorotate

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - UIGridView delegate

- (void) cellTouched:(UIGridViewCell*)cell
{
    // Get the id of the location clicked :
    int locationClicked = (int) [cell.contentTag integerValue];
    
    // If the location clicked contains a page :
    if([[GoodTipsDBHandler instance] locationContainsPage:locationClicked])
    {
        // Get the page :
        Page* page = [[GoodTipsDBHandler instance] pageAtLocation:locationClicked];
        
        switch (page.type)
        {
            case HTML :
            {
                // Create and push a HTMLPageController :
                HTMLPageController* HTMLPage = [[HTMLPageController alloc] initWithPageId:page.uniqueId];
                
                // Display it :
                [self.navigationController pushViewController:HTMLPage animated:YES];
                
                break;
            }
            case PLANNING :
            {
                // Create and push a PlanningPageController :
                PlanningPageController* planningPage = [[PlanningPageController alloc] initWithPageId:page.uniqueId];
                
                // Display it :
                [self.navigationController pushViewController:planningPage animated:YES];
                
                break;
            }
            case CHECKLIST :
            {
                // Create and push a ChecklistPageController :
                ChecklistPageController* checklistPage = [[ChecklistPageController alloc] initWithPageId:page.uniqueId];
                
                // Display it :
                [self.navigationController pushViewController:checklistPage animated:YES];
                
                break;
            }
            case IMAGE :
            {
                // Create and push an ImagePageController :
                ImagePageController* checklistPage = [[ImagePageController alloc] initWithPageId:page.uniqueId];
                
                // Display it :
                [self.navigationController pushViewController:checklistPage animated:YES];
                
                break;
            }
            case FULLSCREEN_IMAGE :
            {
                // Create and push a FullScreenImagePageController :
                FullScreenImagePageController* fullscreenImagePage = [[FullScreenImagePageController alloc] initWithPageId:page.uniqueId];
                
                // Display it :
                [self.navigationController presentModalViewController:fullscreenImagePage animated:YES];
                
                break;
            }
            case CURRENCY_CONVERTER :
            {
                // Create and push a CurrencyConverterPageController :
                CurrencyConverterPageController* currencyConverterPage = [[CurrencyConverterPageController alloc] init];
                
                // Display it :
                [self.navigationController pushViewController:currencyConverterPage animated:YES];
                
                break;
            }
            case TRANSLATION :
            {
                // Create and push a TranslationsPageController :
                TranslationsPageController* translationsPage = [[TranslationsPageController alloc] initWithPageId:page.uniqueId];
                
                // Display it :
                [self.navigationController pushViewController:translationsPage animated:YES];
                
                break;
            }
            case TIPS_COMPUTER :
            {
                // Create and push an TipsComputerPageController :
                TipsComputerPageController* tipsComputerPage = [self.storyboard instantiateViewControllerWithIdentifier:@"TipsComputerPageController"];
                
                // Display it :
                [self.navigationController pushViewController:tipsComputerPage animated:YES];
                
                break;
            }
            default:
            {
                NSLog(@"default page");
                break;
            }
        }
    }
    else
    {
        // Create and push a LocationController :
        LocationController *locationClickedContent = [[LocationController alloc] initWithLocationId:locationClicked];
        [self.navigationController pushViewController:locationClickedContent animated:YES];
    }
}
// ----------------------------------------------------------------------------------------------------



@end
