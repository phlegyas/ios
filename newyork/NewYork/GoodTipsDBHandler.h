//
//  GoodTipsDBHandler.h
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

#import "DBHandler.h"

#import "Location.h"
#import "Page.h"
#import "Planning.h"
#import "ChecklistItem.h"
#import "Translation.h"


@interface GoodTipsDBHandler : DBHandler {
@public
    UILabel *notif;
    int     c;
}

+ (GoodTipsDBHandler*) instance;

+ (PageType) stringToPageType:(NSString*)string;
+ (NSString*) pageTypeToString:(PageType)pageType;

- (NSArray *) locations;
- (Location*) locationWithId:(int)locationId;
- (NSArray *) locationsAtLocation:(int)location;
- (BOOL) locationContainsPage:(int)locationId;

- (NSArray *) pages;
- (Page*) pageWithId:(int)pageId;
- (Page*) pageAtLocation:(int)location;

- (ChecklistItem*) checklistItemWithId:(int)itemId;
- (NSArray*) planningsWithPageId:(int)pageId;
- (NSArray*) checklistItemsWithPageId:(int)pageId;
- (void) toggleItemChecked:(int)itemId;

- (NSArray*) translationsWithPageId:(int)pageId;

@end


