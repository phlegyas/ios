//
//  GoodTipsDBHandler.m
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GoodTipsDBHandler.h"

@implementation GoodTipsDBHandler

static GoodTipsDBHandler *_instance;


// ----------------------------------------------------------------------------------------------------
#pragma mark - Get a static instance of the database

+ (GoodTipsDBHandler*) instance
{
    if (_instance == nil)
    {
        _instance = [[GoodTipsDBHandler alloc] init];
    }
    
    return _instance;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Handler lifecycle
- (id)init
{
    NSString* language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString* databaseFile = [NSString stringWithFormat:@"/good_tips/database/good_tips_%@.sqlite3", language];
    
    if ((self = [super initWithPath:databaseFile]))
    {
        
    }
    
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Other class functions

+ (PageType) stringToPageType:(NSString*)string
{
    if([string isEqualToString:@"HTML"]) return HTML;
    else if([string isEqualToString:@"PLANNING"]) return PLANNING;
    else if([string isEqualToString:@"CHECKLIST"]) return CHECKLIST;
    else if([string isEqualToString:@"IMAGE"]) return IMAGE;
    else if([string isEqualToString:@"FULLSCREEN_IMAGE"]) return FULLSCREEN_IMAGE;
    else if([string isEqualToString:@"CURRENCY_CONVERTER"]) return CURRENCY_CONVERTER;
    else if([string isEqualToString:@"TRANSLATION"]) return TRANSLATION;
    else if([string isEqualToString:@"TIPS_COMPUTER"]) return TIPS_COMPUTER;
    
    else return -1;
}

+ (NSString*) pageTypeToString:(PageType)pageType
{
    if(pageType == HTML) return @"HTML";
    else if(pageType == PLANNING) return @"PLANNING";
    else if(pageType == CHECKLIST) return @"CHECKLIST";
    else if(pageType == IMAGE) return @"IMAGE";
    else if(pageType == FULLSCREEN_IMAGE) return @"FULLSCREEN_IMAGE";
    else if(pageType == CURRENCY_CONVERTER) return @"CURRENCY_CONVERTER";
    else if(pageType == TRANSLATION) return @"TRANSLATION";
    else if(pageType == TIPS_COMPUTER) return @"TIPS_COMPUTER";
    
    else return nil;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Locations

- (NSArray *) locations
{    
    NSMutableArray *locations = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT id, location, position, title FROM LOCATIONS ORDER BY position";
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            int location = sqlite3_column_int(statement, 1);
            int position = sqlite3_column_int(statement, 2);
            //char *titleChar = (char *) sqlite3_column_text(statement, 3);
            //NSString *title = [[NSString alloc] initWithUTF8String:titleChar];
            NSString* title = [NSString stringWithCString:(char *)sqlite3_column_text(statement, 3) encoding:NSASCIIStringEncoding];
            
            Location *loc = [[Location alloc]   initWithUniqueId:uniqueId
                                                        location:location
                                                        position:position
                                                           title:title];                        
            [locations addObject:loc];
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"locations" message:sqlErrorMsg];
    }
    
    return locations;
}

- (Location*) locationWithId:(int)locationId
{    
    Location* loc = nil;
    NSString* query = [NSString stringWithFormat:@"SELECT id, location, position, title FROM LOCATIONS WHERE id='%d'", locationId];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            int location = sqlite3_column_int(statement, 1);
            int position = sqlite3_column_int(statement, 2);
            NSString* title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
            
            loc = [[Location alloc] initWithUniqueId:uniqueId location:location position:position title:title];
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"locationWithId" message:sqlErrorMsg];
    }
    
    return loc;
}

- (NSArray*) locationsAtLocation:(int)location
{
    NSMutableArray *locations = [[NSMutableArray alloc] init];
    NSString *query = [NSString stringWithFormat:@"SELECT id, location, position, title FROM LOCATIONS WHERE location='%d' ORDER BY position", location];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            int location = sqlite3_column_int(statement, 1);
            int position = sqlite3_column_int(statement, 2);
            NSString* title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
            
            Location *loc = [[Location alloc]   initWithUniqueId:uniqueId
                                                        location:location
                                                        position:position
                                                           title:title];                        
            [locations addObject:loc];
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"locationsAtLocation" message:sqlErrorMsg];
    }
    
    return locations;
}

- (BOOL) locationContainsPage:(int)locationId
{
    Page* page = [self pageAtLocation:locationId];
    
    if(page == nil) return NO;
    else return YES;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Pages

- (NSArray*) pages
{    
    NSMutableArray *pages = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT id, location, type, content FROM PAGES";
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            int location = sqlite3_column_int(statement, 1);
            char *typeChar = (char *) sqlite3_column_text(statement, 2);
            char *contentChar = (char *) sqlite3_column_text(statement, 3);
            PageType type = [GoodTipsDBHandler stringToPageType:[[NSString alloc] initWithUTF8String:typeChar]];
            NSString *content = [[NSString alloc] initWithUTF8String:contentChar];
            
            Page *page = [[Page alloc]  initWithUniqueId:uniqueId location:location type:type content:content];                        
            [pages addObject:page];
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"pages" message:sqlErrorMsg];
    }
    
    return pages;
}

- (Page*) pageWithId:(int)pageId
{    
    Page* page = nil;
    NSString* query = [NSString stringWithFormat:@"SELECT id, location, type, content FROM PAGES WHERE id='%d'", pageId];
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            int location = sqlite3_column_int(statement, 1);
            char *typeChar = (char *) sqlite3_column_text(statement, 2);
            char *contentChar = (char *) sqlite3_column_text(statement, 3);
            PageType type = [GoodTipsDBHandler stringToPageType:[[NSString alloc] initWithUTF8String:typeChar]];
            NSString *content = [[NSString alloc] initWithUTF8String:contentChar];
            
            page = [[Page alloc] initWithUniqueId:uniqueId location:location type:type content:content];  
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"pageWithId" message:sqlErrorMsg];
    }
    
    return page;
}

- (Page*) pageAtLocation:(int)location
{
    Page *page = nil;
    NSString *query = [NSString stringWithFormat:@"SELECT id, location, type, content FROM PAGES WHERE location='%d'", location];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            int location = sqlite3_column_int(statement, 1);
            char *typeChar = (char *) sqlite3_column_text(statement, 2);
            char *contentChar = (char *) sqlite3_column_text(statement, 3);
            PageType type = [GoodTipsDBHandler stringToPageType:[[NSString alloc] initWithUTF8String:typeChar]];
            NSString *content = [[NSString alloc] initWithUTF8String:contentChar];
            
            page = [[Page alloc]  initWithUniqueId:uniqueId location:location type:type content:content];
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"pageAtLocation" message:sqlErrorMsg];
    }
    
    return page;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Plannings & checklist items

- (NSArray*) planningsWithPageId:(int)pageId
{
    NSMutableArray *plannings = [NSMutableArray array];
    NSString *query = [NSString stringWithFormat:@"SELECT id, page, position, title FROM PLANNINGS WHERE page='%d'", pageId];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            int page = sqlite3_column_int(statement, 1);
            int position = sqlite3_column_int(statement, 2);
            char* titleChar = (char *) sqlite3_column_text(statement, 3);
            NSString* title = [[NSString alloc] initWithUTF8String:titleChar];
            
            Planning* planning = [[Planning alloc] initWithUniqueId:uniqueId page:page position:position title:title];
            [plannings addObject:planning];
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"planningsWithPageId" message:sqlErrorMsg];
    }
    
    return plannings;
}

- (ChecklistItem*) checklistItemWithId:(int)itemId
{    
    ChecklistItem* item = nil;
    NSString* query = [NSString stringWithFormat:@"SELECT id, page, position, checked, title, description FROM CHECKLIST_ITEMS WHERE id='%d'", itemId];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            int page = sqlite3_column_int(statement, 1);
            int position = sqlite3_column_int(statement, 2);
            BOOL checked = sqlite3_column_int(statement, 3);
            char *titleChar = (char *) sqlite3_column_text(statement, 4);
            char *descriptionChar = (char *) sqlite3_column_text(statement, 5);
            NSString *title = [[NSString alloc] initWithUTF8String:titleChar];
            NSString *description = [[NSString alloc] initWithUTF8String:descriptionChar];
            
            item = [[ChecklistItem alloc] initWithUniqueId:uniqueId page:page position:position checked:checked title:title description:description];
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"checklistItemWithId" message:sqlErrorMsg];
    }
    
    return item;
}

- (NSArray*) checklistItemsWithPageId:(int)pageId
{
    NSMutableArray *items = [NSMutableArray array];
    NSString *query = [NSString stringWithFormat:@"SELECT id, page, position, checked, title, description FROM CHECKLIST_ITEMS WHERE page='%d'", pageId];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            int page = sqlite3_column_int(statement, 1);
            int position = sqlite3_column_int(statement, 2);
            BOOL checked = sqlite3_column_int(statement, 3);
            char *titleChar = (char *) sqlite3_column_text(statement, 4);
            char *descriptionChar = (char *) sqlite3_column_text(statement, 5);
            NSString *title = [[NSString alloc] initWithUTF8String:titleChar];
            NSString *description = [[NSString alloc] initWithUTF8String:descriptionChar];
            
            ChecklistItem *item = [[ChecklistItem alloc] initWithUniqueId:uniqueId page:page position:position checked:checked title:title description:description];
            
            [items addObject:item];
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"checklistItemsWithPageId" message:sqlErrorMsg];
    }
    
    return items;
}

- (void) toggleItemChecked:(int)itemId
{
    ChecklistItem* item = [self checklistItemWithId:itemId];
    int checked = 1;
    if(item.checked) checked = 0;
    
    //update notification
    c = c - checked;
    if (checked == 0)
        c++;
    if (c >= 10)
        notif.textColor = [UIColor redColor];
    else
        notif.textColor = [UIColor blueColor];
    if (c)
        notif.text = [NSString stringWithFormat:@"%i",  c];
    else
    {
        notif.text = [NSString stringWithFormat:@"OK"];
        notif.textColor = [UIColor greenColor];
    }
    
    // notif.text = [NSString stringWithFormat:@"%i",  c];
    
    NSString *query = [NSString stringWithFormat:@"UPDATE CHECKLIST_ITEMS set checked=%d where id='%d'", checked, itemId];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        sqlite3_step(statement);
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"toggleItemChecked" message:sqlErrorMsg];
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Translations

- (NSArray*) translationsWithPageId:(int)pageId
{
    NSMutableArray* translations = [[NSMutableArray alloc] init];
    NSString* query = [NSString stringWithFormat:@"SELECT id, page, position, frenchSentence, englishSentence FROM TRANSLATIONS WHERE page='%d' ORDER BY position", pageId];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            int page = sqlite3_column_int(statement, 1);
            int position = sqlite3_column_int(statement, 2);
            NSString* frenchSentence = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
            NSString* englishSentence = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
            
            Translation* translation = [[Translation alloc] initWithUniqueId:uniqueId
                                                                        page:page
                                                                    position:position
                                                              frenchSentence:frenchSentence
                                                             englishSentence:englishSentence];
            [translations addObject:translation];
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"translationsAtLocation" message:sqlErrorMsg];
    }
    
    return translations;
}
// ----------------------------------------------------------------------------------------------------

@end




