//
//  HTMLPageController.h
//  NewYork
//
//  Created by Tim Autin on 09/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GAITrackedViewController.h"

@interface HTMLPageController : GAITrackedViewController <UIWebViewDelegate>
{
    int _pageId;
}

- (id)initWithPageId:(int)pageId;

@end
