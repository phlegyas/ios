//
//  HTMLPageController.m
//  NewYork
//
//  Created by Tim Autin on 09/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HTMLPageController.h"
#import "GoodTipsDBHandler.h"
#import "Colors.h"


@implementation HTMLPageController


// ----------------------------------------------------------------------------------------------------
#pragma mark - Init function

- (id) initWithPageId:(int)pageId
{
    self = [super init];
    
    if (self)
    {
        _pageId = pageId;
    }
    
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) loadView
{
    // Call super :
    [super loadView];
    if (_pageId == 24)
        self.screenName = @"Numéros utiles à New York";
    
    // Get the content of the requested page :
    Page* page = [[GoodTipsDBHandler instance] pageWithId:_pageId];
    NSString* pageContent = page.content;
    
    // Set the window's title :
    NSString* locationTitle = [[GoodTipsDBHandler instance] locationWithId:page.location].title;
    locationTitle = [locationTitle stringByReplacingOccurrencesOfString:@"\\n" withString:@" "];
    self.title = locationTitle;
    
    // Set the background color :
    self.view.backgroundColor = [Colors backgroundColor];
    
    // Create a webview :
    UIWebView* webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    webView.delegate = self;
    webView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    webView.backgroundColor = [UIColor clearColor];
    
    // Set the webview's text :
    [webView loadHTMLString:pageContent baseURL:nil];
    
    // Handle the scroll speed (to slow by default) :
    webView.scrollView.decelerationRate = UIScrollViewDecelerationRateNormal;
    
    // Set the webview as the controller's view :
    [self.view addSubview:webView];
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - Screen autorotate

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - UIWebViewDelegate, in order to open links in safari

-(BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked )
    {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    
    return YES;
}
// ----------------------------------------------------------------------------------------------------


@end
