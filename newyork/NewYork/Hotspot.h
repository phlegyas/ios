//
//  Hotspot.h
//  NewYork
//
//  Created by Tim Autin on 02/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {FEEDING, HOUSING, SHOPPING, CULTURE, PICTURE, LEISURE, PASTRY} HotspotType;

@interface Hotspot : NSObject
{
    int _uniqueId;
    NSString* _title;
    NSString* _description;
    float _longitude;
    float _latitude;
    HotspotType _type;
}

@property (nonatomic, assign) int uniqueId;
@property (nonatomic, copy) NSString* title;
@property (nonatomic, copy) NSString* description;
@property (nonatomic, assign) float longitude;
@property (nonatomic, assign) float latitude;
@property (nonatomic, assign) HotspotType type;

- (id) initWithUniqueId:(int)uniqueId title:(NSString*)title description:(NSString*)description longitude:(float)longitude latitude:(float)latitude type:(HotspotType)type;
- (NSString*) toString;

@end
