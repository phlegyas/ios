//
//  Hotspot.m
//  NewYork
//
//  Created by Tim Autin on 02/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Hotspot.h"
#import "HotspotsDBHandler.h"

@implementation Hotspot

@synthesize uniqueId = _uniqueId;
@synthesize title = _title;
@synthesize description = _description;
@synthesize longitude = _longitude;
@synthesize latitude = _latitude;
@synthesize type = _type;


- (id) initWithUniqueId:(int)uniqueId title:(NSString*)title description:(NSString*)description longitude:(float)longitude latitude:(float)latitude type:(HotspotType)type
{
    if ((self = [super init]))
    {
        self.uniqueId = uniqueId;
        self.title = title;
        self.description = description;
        self.longitude = longitude;
        self.latitude = latitude;
        self.type = type;
    }
    return self;
}

- (void) dealloc
{
    self.title = nil;
    self.description = nil;
}

- (NSString*) toString
{
    return [NSString stringWithFormat:@"HOTSPOT : %d, %@, %@, %f, %f, %@", self.uniqueId, self.title, self.description, self.longitude, self.latitude, [HotspotsDBHandler hotspotTypeToString:self.type]];
}

@end
