//
//  HotspotEditionController.h
//  NewYork
//
//  Created by Tim Autin on 05/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RMMapView.h"
#import "UIImageCropController.h"
#import "Hotspot.h"
#import "HotspotEditionView.h"


@interface HotspotEditionController : UIViewController <UITextFieldDelegate, UITextViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPopoverControllerDelegate, HotspotEditionViewDelegate, UIImageCropControllerDelegate>
{
    CGRect _usableFrame;
    HotspotEditionView* _hotspotEditionView;
    Hotspot* _newHotspot;
    BOOL _imageChanged;
    UIEdgeInsets _scrollViewInsets;
}

@property (nonatomic, retain) UIPopoverController *popoverController;

- (id) initWithLocation:(CLLocationCoordinate2D)location;
- (id) initWithHospot:(Hotspot*)hotspot;

- (UIImage *) imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

- (void) keyboardDidShow:(NSNotification*)notification;
- (void) keyboardWillHide:(NSNotification*)notification;

- (void) deletePicture;
- (void) getPictureFromPhotoLibrary;
- (void) getPictureFromCamera;

- (void) saveHotspot:(Hotspot *)hotspot image:(UIImage *)image mustSaveImage:(BOOL)mustSaveImage;
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex;

@end
