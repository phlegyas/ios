//
//  HotspotEditionController.m
//  NewYork
//
//  Created by Tim Autin on 05/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SystemVersion.h"
#import <QuartzCore/QuartzCore.h>

#import "HotspotEditionController.h"
#import "Colors.h"
#import "UserHotspotsDBHandler.h"

#define SCROLL_TO_ACTIVE_VIEW_MARGIN_BOTTOM 5
#define CROP_CONTROLLER_IMAGE_RATIO 250.0/130.0

@implementation HotspotEditionController

@synthesize popoverController;

// ----------------------------------------------------------------------------------------------------
#pragma mark - Init function

- (id) initWithLocation:(CLLocationCoordinate2D)location
{
    self = [super init];
    if (self)
    {
        _newHotspot = [[Hotspot alloc] init];
        _newHotspot.latitude = location.latitude;
        _newHotspot.longitude = location.longitude;
        _imageChanged = NO;
    }
    return self;
}

- (id) initWithHospot:(Hotspot*)hotspot
{
    self = [super init];
    if (self)
    {
        _newHotspot = hotspot;
        _imageChanged = NO;
    }
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) loadView
{
    // Call super :
    [super loadView];
    
    // Set the background color :
    self.view.backgroundColor = [Colors backgroundColor];
    
    // Compute the usable frame :
    int statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    int toolBarHeight = self.navigationController.toolbar.frame.size.height;
    int tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    
    if(SYSTEM_VERSION < 7.0)
    {
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
        _usableFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - toolBarHeight - tabBarHeight);
    }
    else
    {
        _usableFrame = CGRectMake(0, statusBarHeight + toolBarHeight, self.view.frame.size.width, self.view.frame.size.height - statusBarHeight - toolBarHeight - tabBarHeight);
    }
    
    // Create the Hotspot edition view :
    if(SYSTEM_VERSION < 7.0) _hotspotEditionView = [[HotspotEditionView alloc] initWithFrame:_usableFrame usableFrame:_usableFrame];
    else _hotspotEditionView = [[HotspotEditionView alloc] initWithFrame:self.view.frame usableFrame:_usableFrame];
    _hotspotEditionView.delegate = self;
    [self.view addSubview:_hotspotEditionView];
    
    // Create the save button :
    UIBarButtonItem* saveButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"new_york_tab_hotspot_edition_controller_save_button", @"Save") style:UIBarButtonItemStyleDone target:self action:@selector(saveButtonClicked)];
    if(SYSTEM_VERSION < 7.0) saveButton.tintColor = [Colors lightGreen];
    self.navigationItem.rightBarButtonItem = saveButton;
    
    // Add a notification for the keyboard showing :
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardDidShow:) name: UIKeyboardDidShowNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardWillHide:) name: UIKeyboardWillHideNotification object: nil];
}

- (void) viewDidAppear:(BOOL)animated
{
    _scrollViewInsets = _hotspotEditionView.wrapperScrollView.contentInset;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();    
    UIGraphicsEndImageContext();
    
    return newImage;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Screen autorotate

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Keyboard events

- (void) keyboardDidShow:(NSNotification*)notification
{
    // Get the keyboard size :
    NSDictionary* info = [notification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    // Get the tabbar height :
    int tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    
    // Add an inset under the content :
    UIEdgeInsets newInsets = _scrollViewInsets;
    newInsets.bottom = keyboardSize.height; if(SYSTEM_VERSION < 7.0) newInsets.bottom -= tabBarHeight;
    _hotspotEditionView.wrapperScrollView.contentInset = newInsets;
    _hotspotEditionView.wrapperScrollView.scrollIndicatorInsets = newInsets;
    
    // Compute the visible rect :
    CGRect visibleRect = _usableFrame;
    visibleRect.size.height -= (keyboardSize.height - tabBarHeight);
    
    // Compute the y axis of the active textfield :
    float y = _hotspotEditionView.backgroundView.frame.origin.y +
    _hotspotEditionView.activeView.superview.frame.origin.y +
    _hotspotEditionView.activeView.frame.origin.y +
    _hotspotEditionView.activeView.frame.size.height +
    SCROLL_TO_ACTIVE_VIEW_MARGIN_BOTTOM;
    
    // Compute the offset :
    CGPoint pointToShow = CGPointMake(0, y - _hotspotEditionView.wrapperScrollView.contentOffset.y);
    CGPoint offsetToShow = CGPointMake(0, y - visibleRect.origin.y - visibleRect.size.height );
    
    // Animate to the offset :
    if (!CGRectContainsPoint(visibleRect, pointToShow) )
    {
        [_hotspotEditionView.wrapperScrollView setContentOffset:offsetToShow animated:YES];
    }
}

- (void) keyboardWillHide:(NSNotification*)notification
{
    // Remove the inset under the content :
    [UIView animateWithDuration:0.3 animations:^
     {
         _hotspotEditionView.wrapperScrollView.contentInset = _scrollViewInsets;
         _hotspotEditionView.wrapperScrollView.scrollIndicatorInsets = _scrollViewInsets;
     }];
    
    // There is no active view :
    _hotspotEditionView.activeView = nil;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Hotspot edition view delegate

- (void) hotspotEditionViewGetPictureButtonClicked
{
    UIAlertView* alert;
    if(_hotspotEditionView.image != nil)
    {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"new_york_tab_hotspot_edition_controller_select_picture_popup_title", @"Picture") message:@"" delegate:self cancelButtonTitle:NSLocalizedString(@"new_york_tab_hotspot_edition_controller_select_picture_popup_cancel_button", @"Cancel") otherButtonTitles:NSLocalizedString(@"new_york_tab_hotspot_edition_controller_select_picture_popup_take_picture_button", @"Take a new picture"), NSLocalizedString(@"new_york_tab_hotspot_edition_controller_select_picture_popup_choose_in_photo_library_button", @"Choose in photo library"), NSLocalizedString(@"new_york_tab_hotspot_edition_controller_select_picture_popup_delete_photo_button", @"Delet the picture"), nil];
    }
    else
    {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"new_york_tab_hotspot_edition_controller_select_picture_popup_title", @"Picture") message:@"" delegate:self cancelButtonTitle:NSLocalizedString(@"new_york_tab_hotspot_edition_controller_select_picture_popup_cancel_button", @"Cancel") otherButtonTitles: NSLocalizedString(@"new_york_tab_hotspot_edition_controller_select_picture_popup_take_picture_button", @"Take a new picture"), NSLocalizedString(@"new_york_tab_hotspot_edition_controller_select_picture_popup_choose_in_photo_library_button", @"Choose in photo library"), nil];
    }
    
    alert.tag = 1;
    [alert show];
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - Picture handling

- (void) deletePicture
{
    [_hotspotEditionView setImage:nil];
    _imageChanged = YES;
}

- (void) getPictureFromCamera
{
    UIImagePickerController* controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;    
    controller.sourceType = UIImagePickerControllerSourceTypeCamera;
    //controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    controller.allowsEditing = NO;
    controller.editing = NO;
    
    [self presentModalViewController:controller animated:YES];
}

- (void) getPictureFromPhotoLibrary
{
    UIImagePickerController* controller = [[UIImagePickerController alloc] init];
    controller.delegate = self;
    controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    controller.allowsEditing = NO;
    controller.editing = NO;
    
    // If we are running on iPad :
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.popoverController = [[UIPopoverController alloc] initWithContentViewController:controller];
        self.popoverController.delegate = self;
        [self.popoverController setPopoverContentSize:CGSizeMake(400, 400)];
        
        [self.popoverController presentPopoverFromRect:CGRectMake(_usableFrame.origin.x, _usableFrame.origin.y, _usableFrame.size.width, 50) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    
    // If we are running on iPhone :
    else
    {
        [self presentModalViewController:controller animated:YES];
    }
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    // Create the controller :
    UIImageCropController* controller = [[UIImageCropController alloc] initWithImage:[info objectForKey:@"UIImagePickerControllerOriginalImage"] ratio:CROP_CONTROLLER_IMAGE_RATIO];
    controller.delegate = self;
    
    // If we are running on iPad and if we are picking from the library :
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary)
    {
        [self.popoverController dismissPopoverAnimated:YES];
        
        UINavigationController* parent = [[UINavigationController alloc] init];
        [parent pushViewController:controller animated:NO];
        
        //[self.navigationController pushViewController:controller animated:YES];
        [self.navigationController presentViewController:parent animated:YES completion:nil];
    }
    else
    {
        // Ensure the navigation bar showing :
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent animated:YES];
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
        [picker setNavigationBarHidden:NO animated:YES];
        
        // Push the controller :
        [picker pushViewController:controller animated:YES];
    }
}

- (void) imageCropController:(UIImageCropController *)cropController didFinishCroppingImage:(UIImage*)image
{
    // Dismiss the controller :
    [cropController.navigationController dismissModalViewControllerAnimated:YES];
    
    _imageChanged = YES;
    [_hotspotEditionView setImage:image];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Save the hotspot

- (void) saveButtonClicked
{
    // Check if the form is complete :
    NSString* errorMessage = @"";
    if([_hotspotEditionView.title isEqualToString:@""])
    {
        errorMessage = NSLocalizedString(@"new_york_tab_hotspot_edition_controller_error_popup_message_fill_title", @"Please fill the title");
    }
   else if(_hotspotEditionView.isDescriptionEmpty)
    {
        _hotspotEditionView.description = @" ";
       // errorMessage = NSLocalizedString(@"new_york_tab_hotspot_edition_controller_error_popup_message_fill_description", @"Please fill the description");
    }
    else if(!_hotspotEditionView.isTypeSelected)
    {
        errorMessage = NSLocalizedString(@"new_york_tab_hotspot_edition_controller_error_popup_message_fill_hotspot_type", @"Please fill the hotspot type");
    }
    
    // If not, alert the user :
    if(![errorMessage isEqualToString:@""])
    {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"new_york_tab_hotspot_edition_controller_error_popup_title", @"Error") message:errorMessage delegate:self cancelButtonTitle:NSLocalizedString(@"new_york_tab_hotspot_edition_controller_error_popup_ok_button", @"OK") otherButtonTitles: nil];
        alert.tag = 2;
        [alert show];
    }
    
    // Else add the hotspot :
    else
    {
        // Save the new values :
        [_newHotspot setTitle:_hotspotEditionView.title];
        [_newHotspot setDescription:_hotspotEditionView.description];
        [_newHotspot setType:_hotspotEditionView.type];
        
        [self saveHotspot:_newHotspot image:_hotspotEditionView.image mustSaveImage:_imageChanged];
    }
}

- (void) saveHotspot:(Hotspot *)hotspot image:(UIImage *)image mustSaveImage:(BOOL)mustSaveImage
{
    // Abstract method
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - UIAlertView delegate

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    // Select photo source alert view :
    if(alertView.tag == 1)
    {
        if(buttonIndex == 1)
        {
            [self getPictureFromCamera];
        }
        else if(buttonIndex == 2)
        {
            [self getPictureFromPhotoLibrary];
        }
        else if(buttonIndex == 3)
        {
            [self deletePicture];
        }
    }
}
// ----------------------------------------------------------------------------------------------------


@end



