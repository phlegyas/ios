//
//  HotspotEditionView.h
//  NewYork
//
//  Created by Tim Autin on 05/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIImageCropController.h"
#import "Hotspot.h"

@class HotspotEditionView;

@protocol HotspotEditionViewDelegate <NSObject>
@optional
- (void) hotspotEditionViewGetPictureButtonClicked;
@end

@interface HotspotEditionView : UIView <UITextFieldDelegate, UITextViewDelegate>
{
    __weak id <HotspotEditionViewDelegate> _delegate;
    
    CGRect _usableFrame;
    
    UIScrollView* _wrapperScrollView;
    UIView* _backgroundView;
    UIView* _activeView;
    NSString* _textViewPlaceHolder;
    
    UILabel* _viewTitleLabel;
    
    UITextField* _titleTextField;
    UITextView* _descriptionTextView;
    UISegmentedControl* _hotspotTypeSegmentedControl;
    
    UIImageView* _hotspotImageImageView;
    UITextField* _hotspotImageTextField;
}

@property (nonatomic, weak) id <HotspotEditionViewDelegate> delegate;
@property (strong) UIScrollView* wrapperScrollView;
@property (strong) UIView* backgroundView;
@property (strong) UIView* activeView;

- (id) initWithFrame:(CGRect)frame usableFrame:(CGRect)usableFrame;
- (void) setViewTitle:(NSString*)title;

- (UILabel*) titleLabelWithText:(NSString*)text y:(int)y;
- (UIImage *) imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

- (void) addTitleField;
- (void) addDescriptionField;
- (void) addTypeField;
- (void) addPictureField;

- (NSString*) title;
- (NSString*) description;
- (BOOL) isDescriptionEmpty;
- (HotspotType) type;
- (BOOL) isTypeSelected;
- (UIImage*) image;

- (void) setTitle:(NSString*)title;
- (void) setDescription:(NSString*)title;
- (void) setType:(HotspotType)type;
- (void) setImage:(UIImage*)image;

@end
