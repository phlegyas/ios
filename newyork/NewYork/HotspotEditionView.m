//
//  HotspotEditionView.m
//  NewYork
//
//  Created by Tim Autin on 05/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SystemVersion.h"
#import <QuartzCore/QuartzCore.h>

#import "HotspotEditionView.h"
#import "Colors.h"
#import "UserHotspotsDBHandler.h"

#define NEW_HOTSPOT_LABEL_HEIGHT 40
#define HORIZONTAL_MARGIN 10
#define BACKGROUND_VIEW_PADDING 5
#define BACKGROUND_VIEW_MARGIN_BOTTOM 10
#define INFORMATIONS_LABEL_HEIGHT 0
#define LABELS_HEIGHT 30
#define TEXTFIELDS_HEIGHT 45
#define HOTSPOT_IMAGE_WIDTH 250.0
#define HOTSPOT_IMAGE_HEIGHT 130.0
#define HOTSPOT_TYPE_SEGMENTED_CONTROL_IMAGE_WIDTH 30
#define HOTSPOT_TYPE_SEGMENTED_CONTROL_IMAGE_HEIGHT 30
#define HOTSPOT_TYPE_SEGMENTED_CONTROL_PADDING 4

#define IS_RETINA ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale == 2.0))

@implementation HotspotEditionView

@synthesize delegate = _delegate;
@synthesize wrapperScrollView = _wrapperScrollView;
@synthesize backgroundView = _backgroundView;
@synthesize activeView = _activeView;



// ----------------------------------------------------------------------------------------------------
#pragma mark - Init function

- (id) initWithFrame:(CGRect)frame usableFrame:(CGRect)usableFrame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _usableFrame = usableFrame;
        _textViewPlaceHolder = NSLocalizedString(@"new_york_tab_edit_hotspot_view_description_placeholder", @"Enter the description");
        
        [self loadView];
    }
    return self;
}

- (void) setViewTitle:(NSString*)title
{
    _viewTitleLabel.text = title;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) loadView
{
    // Set the background color :
    self.backgroundColor = [Colors backgroundColor];
    
    // Init the wrapper scroll view :
    _wrapperScrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
    _wrapperScrollView.contentSize = _usableFrame.size;
    _wrapperScrollView.backgroundColor = [Colors backgroundColor];
    
    // Create the new travel label :
    _viewTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, NEW_HOTSPOT_LABEL_HEIGHT)];
    _viewTitleLabel.text = @"-";
    _viewTitleLabel.backgroundColor = _wrapperScrollView.backgroundColor;
    _viewTitleLabel.textAlignment = UITextAlignmentCenter;
    _viewTitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    
    if(SYSTEM_VERSION < 7.0)
    {
        _viewTitleLabel.textColor = [Colors lightYellow];
        _viewTitleLabel.shadowColor = [UIColor darkGrayColor];
        _viewTitleLabel.shadowOffset = CGSizeMake(1, 1);
    }
    else
    {
        _viewTitleLabel.textColor = [Colors darkTextColor];
    }
    
    // Add a background view :
    _backgroundView = [[UIView alloc] initWithFrame:CGRectMake(HORIZONTAL_MARGIN, NEW_HOTSPOT_LABEL_HEIGHT+BACKGROUND_VIEW_PADDING, self.frame.size.width - 2*HORIZONTAL_MARGIN, _usableFrame.size.height - NEW_HOTSPOT_LABEL_HEIGHT - BACKGROUND_VIEW_PADDING - BACKGROUND_VIEW_MARGIN_BOTTOM)];
    
    _backgroundView.backgroundColor = [UIColor whiteColor];
    
    _backgroundView.layer.cornerRadius = 5;
    _backgroundView.layer.borderWidth = 2;
    _backgroundView.layer.borderColor = [[Colors lightYellow] CGColor];
    
    _backgroundView.layer.shadowColor = [[UIColor blackColor] CGColor];
    _backgroundView.layer.shadowOpacity = 0.8;
    _backgroundView.layer.shadowRadius = 3.0;
    _backgroundView.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    
    // Add the views :
    [_wrapperScrollView addSubview: _viewTitleLabel];
    [_wrapperScrollView addSubview: _backgroundView];
    [self addSubview:_wrapperScrollView];
    
    // Add the fields :
    [self addTitleField];
    [self addDescriptionField];
    [self addTypeField];
    [self addPictureField];
}

- (UILabel*) titleLabelWithText:(NSString*)text y:(int)y
{
    // Create a label :
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(HORIZONTAL_MARGIN, y, _backgroundView.frame.size.width, LABELS_HEIGHT)];
    label.tag = 3;
    label.text = text;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor darkGrayColor];
    
    return label;
}

- (UIImage *) imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();    
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void) addTitleField
{
    // Set a value for the y position :
    int y = 0;
    
    // Create a label :
    UILabel* label = [self titleLabelWithText:NSLocalizedString(@"new_york_tab_edit_hotspot_view_title_label", @"Title") y:y];
    
    int pictureFieldWidth = TEXTFIELDS_HEIGHT * HOTSPOT_IMAGE_WIDTH/HOTSPOT_IMAGE_HEIGHT;
    
    // Create a textfield (wrapped in a dummy scrollview in order to disable the autoscroll) :
    CGRect dummyScrollViewFrame = CGRectMake(2*HORIZONTAL_MARGIN, y + label.frame.size.height, _backgroundView.frame.size.width - 5*HORIZONTAL_MARGIN - pictureFieldWidth, TEXTFIELDS_HEIGHT);
    
    CGRect textFieldFrame = dummyScrollViewFrame;
    textFieldFrame.origin = CGPointZero;
    
    _titleTextField = [[UITextField alloc] initWithFrame:textFieldFrame];
    _titleTextField.delegate = self;
    _titleTextField.text = @"";
    _titleTextField.borderStyle = UITextBorderStyleRoundedRect;
    _titleTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _titleTextField.delegate = self;
    _titleTextField.font = [UIFont fontWithName:@"Helvetica" size:15.0f];
    _titleTextField.placeholder = NSLocalizedString(@"new_york_tab_edit_hotspot_view_title_placeholder", @"Enter the title");
    
    // Create the dummy scrollview :
    UIScrollView* dummyScrollView = [[UIScrollView alloc] initWithFrame:dummyScrollViewFrame];
    
    // Add the items :
    [_backgroundView addSubview: label];
    [dummyScrollView addSubview:_titleTextField];
    [_backgroundView addSubview: dummyScrollView];
}

- (void) addPictureField
{
    // Set a value for the y position :
    int y = 0;
    
    // Create a label :
    UILabel* label = [self titleLabelWithText:NSLocalizedString(@"new_york_tab_edit_hotspot_view_picture_label", @"Picture") y:y];
    int x = _titleTextField.frame.size.width + 3*HORIZONTAL_MARGIN;
    label.frame = CGRectMake(x, label.frame.origin.y, label.frame.size.width, label.frame.size.height);
    
    // Create the image field frame :
    CGRect hotspotImageFrame = CGRectMake(x + HORIZONTAL_MARGIN, y + label.frame.size.height, TEXTFIELDS_HEIGHT * HOTSPOT_IMAGE_WIDTH/HOTSPOT_IMAGE_HEIGHT, TEXTFIELDS_HEIGHT);
    
    // Create a textfield used as background :
    _hotspotImageTextField = [[UITextField alloc] initWithFrame:hotspotImageFrame];
    _hotspotImageTextField.borderStyle = UITextBorderStyleRoundedRect;
    _hotspotImageTextField.userInteractionEnabled = NO;
    _hotspotImageTextField.text = NSLocalizedString(@"new_york_tab_edit_hotspot_view_picture_placeholder", @"Choose");
    _hotspotImageTextField.textColor = [UIColor darkGrayColor];
    _hotspotImageTextField.backgroundColor = [UIColor clearColor];
    _hotspotImageTextField.textAlignment = UITextAlignmentCenter;
    _hotspotImageTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    // Create an empty image view that will contain the hotspot image :
    _hotspotImageImageView = [[UIImageView alloc] initWithFrame:hotspotImageFrame];
    _hotspotImageImageView.clipsToBounds = YES;
    _hotspotImageTextField.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.1];
    _hotspotImageImageView.userInteractionEnabled = YES;
    _hotspotImageImageView.layer.cornerRadius = 5;
    
    UITapGestureRecognizer* recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(getPictureButtonClicked)];
    [_hotspotImageImageView addGestureRecognizer:recognizer];
    
    // Add the items :
    [_backgroundView addSubview:label];
    [_backgroundView addSubview:_hotspotImageImageView];
    [_backgroundView addSubview:_hotspotImageTextField];
}

- (void) addDescriptionField
{
    // Set a value for the y position :
    int y = LABELS_HEIGHT + TEXTFIELDS_HEIGHT;
    
    // Create a label :
    UILabel* label = [self titleLabelWithText:NSLocalizedString(@"new_york_tab_edit_hotspot_view_description_label", @"Choose") y:y];
    
    // Create a textfield (wrapped in a dummy scrollview in order to disable the autoscroll) :
    CGRect dummyScrollViewFrame = CGRectMake(2*HORIZONTAL_MARGIN, y + label.frame.size.height, _backgroundView.frame.size.width - 3*HORIZONTAL_MARGIN, 2*TEXTFIELDS_HEIGHT + LABELS_HEIGHT);
    
    CGRect descriptionTextViewFrame = dummyScrollViewFrame;
    descriptionTextViewFrame.origin = CGPointZero;
    
    // Create the activity description textviews :
    _descriptionTextView = [[UITextView alloc] initWithFrame:descriptionTextViewFrame];
    _descriptionTextView.delegate = self;
    _descriptionTextView.text = _textViewPlaceHolder;
    _descriptionTextView.backgroundColor = [UIColor clearColor];
    _descriptionTextView.font = [UIFont fontWithName:@"Helvetica" size:14.0f];
    _descriptionTextView.clipsToBounds = YES;
    _descriptionTextView.delegate = self;
    _descriptionTextView.textColor = [UIColor lightGrayColor];
    
    // Create a toolbar with a centered button :
    CGRect inputAccessoryViewFrame = CGRectMake(0, 0, _descriptionTextView.inputView.frame.size.width, 50);
    UIToolbar* toolBar = [[UIToolbar alloc] initWithFrame:inputAccessoryViewFrame];
    UIBarButtonItem* flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"new_york_tab_edit_hotspot_view_keyboard_ok_button", @"OK") style:UIBarButtonItemStyleDone target:_descriptionTextView action:@selector(resignFirstResponder)];
    [toolBar setItems:[NSArray arrayWithObjects:flexibleSpace, doneButton, flexibleSpace, nil]];
    _descriptionTextView.inputAccessoryView = toolBar;
    
    // Create a textfield that will be used as background in editing mode (dirty trick :/ ) :
    UITextField* textField = [[UITextField alloc] initWithFrame:_descriptionTextView.frame];
    textField.tag = 5;
    textField.userInteractionEnabled = NO;
    textField.backgroundColor = [UIColor clearColor];
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.alpha = 1.0;
    
    // Create the dummy scrollview :
    UIScrollView* dummyScrollView = [[UIScrollView alloc] initWithFrame:dummyScrollViewFrame];    
    
    // Add the items :
    [_backgroundView addSubview:label];
    [dummyScrollView addSubview:textField];
    [dummyScrollView addSubview:_descriptionTextView];
    [_backgroundView addSubview:dummyScrollView];
}

- (void) addTypeField
{
    // Set a value for the y position :
    int y = 3*(LABELS_HEIGHT + TEXTFIELDS_HEIGHT);
    
    // Create a label :
    UILabel* label = [self titleLabelWithText:NSLocalizedString(@"new_york_tab_edit_hotspot_view_type_label", @"Type") y:y];
    
    // Create a segmented control :
    UIImage* imageFeeding;
    UIImage* imageHousing;
    UIImage* imageShopping;
    UIImage* imageCulture;
    UIImage* imagePicture;
    UIImage* imageLeisure;
    UIImage* imagePastry;
    
    // iOS 7 :
    if ([UIImage instancesRespondToSelector:@selector(imageWithRenderingMode:)])
    {
        imageFeeding = [[UIImage imageNamed:@"hotspot_feeding_off_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        imageHousing = [[UIImage imageNamed:@"hotspot_housing_off_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        imageShopping = [[UIImage imageNamed:@"hotspot_shopping_off_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        imageCulture = [[UIImage imageNamed:@"hotspot_culture_off_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        imagePicture = [[UIImage imageNamed:@"hotspot_picture_off_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        imageLeisure = [[UIImage imageNamed:@"hotspot_leisure_off_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        imagePastry = [[UIImage imageNamed:@"hotspot_pastry_off_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    // Previous :
    else
    {
        imageFeeding = [UIImage imageNamed:@"hotspot_feeding_off_35"];
        imageHousing = [UIImage imageNamed:@"hotspot_housing_off_35"];
        imageShopping = [UIImage imageNamed:@"hotspot_shopping_off_35"];
        imageCulture = [UIImage imageNamed:@"hotspot_culture_off_35"];
        imagePicture = [UIImage imageNamed:@"hotspot_picture_off_35"];
        imageLeisure = [UIImage imageNamed:@"hotspot_leisure_off_35"];
        imagePastry = [UIImage imageNamed:@"hotspot_pastry_off_35"];
    }
    
    NSArray *items = [NSArray arrayWithObjects: imageFeeding, imageHousing, imageShopping, imageCulture, imagePicture, imageLeisure, imagePastry, nil];
    
    _hotspotTypeSegmentedControl = [[UISegmentedControl alloc] initWithItems:items];
    _hotspotTypeSegmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    _hotspotTypeSegmentedControl.frame = CGRectMake(2*HORIZONTAL_MARGIN, y + label.frame.size.height, _backgroundView.frame.size.width - 3*HORIZONTAL_MARGIN, TEXTFIELDS_HEIGHT);
    
    [_hotspotTypeSegmentedControl addTarget:self action:@selector(hotspotTypeSegmentedControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    // Add the items :
    [_backgroundView addSubview:label];
    [_backgroundView addSubview:_hotspotTypeSegmentedControl];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Getters & setters

- (NSString*) title
{
    return _titleTextField.text;
}

- (NSString*) description
{
    return _descriptionTextView.text;
}

- (BOOL) isDescriptionEmpty
{
    if( [_descriptionTextView.textColor isEqual:[UIColor lightGrayColor]] || [self.description isEqualToString:@""])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (HotspotType) type
{
    // Get the hotspot type from the segmented control :
    HotspotType type;
    switch (_hotspotTypeSegmentedControl.selectedSegmentIndex)
    {
        case 0 : type = FEEDING; break;
        case 1 : type = HOUSING; break;
        case 2 : type = SHOPPING; break;
        case 3 : type = CULTURE; break;
        case 4 : type = PICTURE; break;
        case 5 : type = LEISURE; break;
        case 6 : type = PASTRY; break;
    }
    
    return type;
}

- (BOOL) isTypeSelected
{
    if(_hotspotTypeSegmentedControl.selectedSegmentIndex == -1) return NO;
    else return YES;
}

- (UIImage*) image
{
    return _hotspotImageImageView.image;
}

- (void) setTitle:(NSString*)title
{
    _titleTextField.text = title;
}

- (void) setDescription:(NSString*)description
{
    _descriptionTextView.text = description;
    if(![_descriptionTextView.text isEqualToString:@""])
    {
        _descriptionTextView.textColor = [UIColor blackColor];
    }
}

- (void) setType:(HotspotType)type
{
    // Get the hotspot type from the segmented control :
    int selectedIndex = -1;
    switch (type)
    {
        case FEEDING : selectedIndex = 0; break;
        case HOUSING : selectedIndex = 1; break;
        case SHOPPING : selectedIndex = 2; break;
        case CULTURE : selectedIndex = 3; break;
        case PICTURE : selectedIndex = 4; break;
        case LEISURE : selectedIndex = 5; break;
        case PASTRY : selectedIndex = 6; break;
        default : break;
    }
    
    [_hotspotTypeSegmentedControl setSelectedSegmentIndex:selectedIndex];
    [_hotspotTypeSegmentedControl sendActionsForControlEvents:UIControlEventValueChanged];
}

- (void) setImage:(UIImage*)image
{
    if(image != nil)
    {
        _hotspotImageImageView.image = [self imageWithImage:image scaledToSize:CGSizeMake(HOTSPOT_IMAGE_WIDTH, HOTSPOT_IMAGE_HEIGHT)];
        _hotspotImageTextField.text = @"";
    }
    else
    {
        _hotspotImageImageView.image = nil;
        _hotspotImageTextField.text = NSLocalizedString(@"new_york_tab_edit_hotspot_view_picture_placeholder", @"Choose");
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - UITextField delegate

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField
{
    _activeView = textField;
    
    return YES;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return NO;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - TextView delegate

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    _activeView = textView;
    
    if([textView.textColor isEqual:[UIColor lightGrayColor]])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    
    return YES;
}

- (BOOL) textViewShouldEndEditing:(UITextView *)textView
{
    if(textView.text.length == 0)
    {
        textView.textColor = [UIColor lightGrayColor];
        textView.text = _textViewPlaceHolder;
    }
    
    return YES;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Hotspot type segmented control events

- (void) hotspotTypeSegmentedControlValueChanged:(id)sender
{
    // Get the segmented control :
    UISegmentedControl* typeSegmentedControl = (UISegmentedControl*)sender;
    
    // iOS 7 :
    if ([UIImage instancesRespondToSelector:@selector(imageWithRenderingMode:)])
    {
        // Set all the images to off :
        [typeSegmentedControl setImage:[[UIImage imageNamed:@"hotspot_feeding_off_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:0];
        [typeSegmentedControl setImage:[[UIImage imageNamed:@"hotspot_housing_off_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:1];
        [typeSegmentedControl setImage:[[UIImage imageNamed:@"hotspot_shopping_off_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:2];
        [typeSegmentedControl setImage:[[UIImage imageNamed:@"hotspot_culture_off_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:3];
        [typeSegmentedControl setImage:[[UIImage imageNamed:@"hotspot_picture_off_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:4];
        [typeSegmentedControl setImage:[[UIImage imageNamed:@"hotspot_leisure_off_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:5];
        [typeSegmentedControl setImage:[[UIImage imageNamed:@"hotspot_pastry_off_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:6];
        
        // Set the selected image to on :
        switch (typeSegmentedControl.selectedSegmentIndex)
        {
            case 0 : [typeSegmentedControl setImage:[[UIImage imageNamed:@"hotspot_feeding_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:0]; break;
            case 1 : [typeSegmentedControl setImage:[[UIImage imageNamed:@"hotspot_housing_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:1]; break;
            case 2 : [typeSegmentedControl setImage:[[UIImage imageNamed:@"hotspot_shopping_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:2]; break;
            case 3 : [typeSegmentedControl setImage:[[UIImage imageNamed:@"hotspot_culture_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:3]; break;
            case 4 : [typeSegmentedControl setImage:[[UIImage imageNamed:@"hotspot_picture_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:4]; break;
            case 5 : [typeSegmentedControl setImage:[[UIImage imageNamed:@"hotspot_leisure_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:5]; break;
            case 6 : [typeSegmentedControl setImage:[[UIImage imageNamed:@"hotspot_pastry_35"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:6]; break;
            default : break;
        }
    }
    // Previous :
    else
    {
        // Set all the images to off :
        [typeSegmentedControl setImage:[UIImage imageNamed:@"hotspot_feeding_off_35"] forSegmentAtIndex:0];
        [typeSegmentedControl setImage:[UIImage imageNamed:@"hotspot_housing_off_35"] forSegmentAtIndex:1];
        [typeSegmentedControl setImage:[UIImage imageNamed:@"hotspot_shopping_off_35"] forSegmentAtIndex:2];
        [typeSegmentedControl setImage:[UIImage imageNamed:@"hotspot_culture_off_35"] forSegmentAtIndex:3];
        [typeSegmentedControl setImage:[UIImage imageNamed:@"hotspot_picture_off_35"] forSegmentAtIndex:4];
        [typeSegmentedControl setImage:[UIImage imageNamed:@"hotspot_leisure_off_35"] forSegmentAtIndex:5];
        [typeSegmentedControl setImage:[UIImage imageNamed:@"hotspot_pastry_off_35"] forSegmentAtIndex:6];
        
        // Set the selected image to on :
        switch (typeSegmentedControl.selectedSegmentIndex)
        {
            case 0 : [typeSegmentedControl setImage:[UIImage imageNamed:@"hotspot_feeding_35"] forSegmentAtIndex:0]; break;
            case 1 : [typeSegmentedControl setImage:[UIImage imageNamed:@"hotspot_housing_35"] forSegmentAtIndex:1]; break;
            case 2 : [typeSegmentedControl setImage:[UIImage imageNamed:@"hotspot_shopping_35"] forSegmentAtIndex:2]; break;
            case 3 : [typeSegmentedControl setImage:[UIImage imageNamed:@"hotspot_culture_35"] forSegmentAtIndex:3]; break;
            case 4 : [typeSegmentedControl setImage:[UIImage imageNamed:@"hotspot_picture_35"] forSegmentAtIndex:4]; break;
            case 5 : [typeSegmentedControl setImage:[UIImage imageNamed:@"hotspot_leisure_35"] forSegmentAtIndex:5]; break;
            case 6 : [typeSegmentedControl setImage:[UIImage imageNamed:@"hotspot_pastry_35"] forSegmentAtIndex:6]; break;
            default : break;
        }
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Get picture button clicked

- (void) getPictureButtonClicked
{
    if([self.delegate respondsToSelector:@selector(hotspotEditionViewGetPictureButtonClicked)])
    {
        [self.delegate hotspotEditionViewGetPictureButtonClicked];
    }
}
// ----------------------------------------------------------------------------------------------------


@end



