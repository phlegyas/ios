//
//  HotspotMarker.h
//  NewYork
//
//  Created by Tim Autin on 06/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Hotspot.h"
#import "RMMarker.h"

@interface HotspotMarker : RMMarker
{
    Hotspot* _hotspot;
    BOOL _isUserHotspot;
}

@property (strong) Hotspot* hotspot;
@property (assign) BOOL isUserHotspot;

- (id) initWithUIImage:(UIImage *)image hotspot:(Hotspot*)hotspot isUserHotspot:(BOOL)isUserHotspot;

@end
