//
//  HotspotMarker.m
//  NewYork
//
//  Created by Tim Autin on 06/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HotspotMarker.h"

@implementation HotspotMarker

@synthesize hotspot = _hotspot;
@synthesize isUserHotspot = _isUserHotspot;

- (id) initWithUIImage:(UIImage *)image hotspot:(Hotspot*)hotspot isUserHotspot:(BOOL)isUserHotspot
{
    self = [super initWithUIImage:image];
    if(self)
    {
        _hotspot = hotspot;
        _isUserHotspot = isUserHotspot;
    }
    
    return self;
}
@end
