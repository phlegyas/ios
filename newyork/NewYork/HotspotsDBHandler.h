//
//  HotspotsDBHandler.h
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

#import "DBHandler.h"
#import "Hotspot.h"


@interface HotspotsDBHandler : DBHandler
{
    //NSString* _tableName;
}

+ (HotspotsDBHandler*) instance;

+ (HotspotType) stringToHotspotType:(NSString*)string;
+ (NSString*) hotspotTypeToString:(HotspotType)hotspotType;

- (NSArray *) hotspots;
- (NSArray *) hotspotsWithSearchedWords:(NSArray*)searchedWords;
- (Hotspot*) hotspotWithId:(int)hotspotId;
- (BOOL) deleteHotspotWithId:(int)hotspotId;
- (BOOL) deleteAllHotspots;
- (Hotspot*) addHotspotWithTitle:(NSString*)title description:(NSString*)description longitude:(float)longitude latitude:(float)latitude type:(HotspotType)type;
- (Hotspot*) addHotspotWithId:(int)uniqueId Title:(NSString*)title description:(NSString*)description longitude:(float)longitude latitude:(float)latitude type:(HotspotType)type;
- (BOOL) updateHotspotWithId:(int)hotspotId setTitle:(NSString*)title description:(NSString*)description longitude:(float)longitude latitude:(float)latitude type:(HotspotType)type;

- (UIImage*) imageOfHotspot:(Hotspot*)hotspot;
- (BOOL) saveImage:(UIImage*)image forHotspot:(Hotspot*)hotspot;
- (BOOL) deleteImageOfHotspot:(Hotspot*)hotspot;

- (BOOL) setHotspot:(Hotspot*)hotspot isFavorite:(BOOL)isFavorite;
- (BOOL) isHotspotFavorite:(Hotspot*)hotspot;

- (BOOL) execute:(NSString*)query;

@end


