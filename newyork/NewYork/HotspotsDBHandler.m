//
//  HotspotsDBHandler.m
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HotspotsDBHandler.h"
#import "Logger.h"

@implementation HotspotsDBHandler

static HotspotsDBHandler *_instance;


// ----------------------------------------------------------------------------------------------------
#pragma mark - Get a static instance of the database

+ (HotspotsDBHandler*) instance
{
    if (_instance == nil)
    {
        _instance = [[HotspotsDBHandler alloc] init];
    }
    
    return _instance;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Hotspot to/from string conversion

+ (HotspotType) stringToHotspotType:(NSString*)string
{
    if([string isEqualToString:@"CULTURE"]) return CULTURE;
    else if([string isEqualToString:@"FEEDING"]) return FEEDING;
    else if([string isEqualToString:@"HOUSING"]) return HOUSING;
    else if([string isEqualToString:@"PICTURE"]) return PICTURE;
    else if([string isEqualToString:@"SHOPPING"]) return SHOPPING;
    else if([string isEqualToString:@"LEISURE"]) return LEISURE;
    else if([string isEqualToString:@"PASTRY"]) return PASTRY;
    
    else
    {
        [HotspotsDBHandler.instance displayErrorMessageFrom:@"hotspotTypeToString" message:@"Unknown hotspotType"];
        
        return -1;
    }
}

+ (NSString*) hotspotTypeToString:(HotspotType)hotspotType
{
    if(hotspotType == CULTURE) return @"CULTURE";
    else if(hotspotType == FEEDING) return @"FEEDING";
    else if(hotspotType == HOUSING) return @"HOUSING";
    else if(hotspotType == PICTURE) return @"PICTURE";
    else if(hotspotType == SHOPPING) return @"SHOPPING";
    else if(hotspotType == LEISURE) return @"LEISURE";
    else if(hotspotType == PASTRY) return @"PASTRY";
    
    else
    {
        [HotspotsDBHandler.instance displayErrorMessageFrom:@"hotspotTypeToString" message:@"Unknown hotspotType"];
        return nil;
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Hotspots

- (NSArray *) hotspots
{
    NSMutableArray *hotspots = [[NSMutableArray alloc] init];
    sqlite3_stmt *statement;
    NSString *query = @"SELECT id, title, description, longitude, latitude, type FROM HOTSPOTS";
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            char* titleChar = (char*) sqlite3_column_text(statement, 1);
            char* descriptionChar = (char*) sqlite3_column_text(statement, 2);
            char* longitudeChar = (char*) sqlite3_column_text(statement, 3);
            char* latitudeChar = (char*) sqlite3_column_text(statement, 4);
            char* typeChar = (char*) sqlite3_column_text(statement, 5);
            
            NSString* title = [[NSString alloc] initWithUTF8String:titleChar];
            NSString* description = [[NSString alloc] initWithUTF8String:descriptionChar];
            float longitude = [[[NSString alloc] initWithUTF8String:longitudeChar] floatValue];
            float latitude = [[[NSString alloc] initWithUTF8String:latitudeChar] floatValue];
            HotspotType type = [HotspotsDBHandler stringToHotspotType:[[NSString alloc] initWithUTF8String:typeChar]];
            
            Hotspot* hotspot = [[Hotspot alloc] initWithUniqueId:uniqueId
                                                           title:title
                                                     description:description
                                                       longitude:longitude
                                                        latitude:latitude
                                                            type:type];
            
            [hotspots addObject:hotspot];
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"hotspots" message:sqlErrorMsg];
    }
    
    return hotspots;
}

- (NSArray *) hotspotsWithSearchedWords:(NSArray*)searchedWords
{
    // Compute the WHERE clause :
    NSString* whereClause = @"";
    for(NSString* s in searchedWords)
    {
        whereClause = [whereClause stringByAppendingString:[NSString stringWithFormat:@"title || ' ' || description LIKE '%%%@%%' AND ", s]];
    }
    whereClause = [whereClause substringToIndex:[whereClause length] - 5];
    
    NSMutableArray *hotspots = [[NSMutableArray alloc] init];
    NSString *query = [NSString stringWithFormat:@"SELECT id, title, description, longitude, latitude, type FROM HOTSPOTS WHERE %@", whereClause];
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            char* titleChar = (char*) sqlite3_column_text(statement, 1);
            char* descriptionChar = (char*) sqlite3_column_text(statement, 2);
            char* longitudeChar = (char*) sqlite3_column_text(statement, 3);
            char* latitudeChar = (char*) sqlite3_column_text(statement, 4);
            char* typeChar = (char*) sqlite3_column_text(statement, 5);
            
            NSString* title = [[NSString alloc] initWithUTF8String:titleChar];
            NSString* description = [[NSString alloc] initWithUTF8String:descriptionChar];
            float longitude = [[[NSString alloc] initWithUTF8String:longitudeChar] floatValue];
            float latitude = [[[NSString alloc] initWithUTF8String:latitudeChar] floatValue];
            HotspotType type = [HotspotsDBHandler stringToHotspotType:[[NSString alloc] initWithUTF8String:typeChar]];
            
            Hotspot* hotspot = [[Hotspot alloc] initWithUniqueId:uniqueId
                                                           title:title
                                                     description:description
                                                       longitude:longitude
                                                        latitude:latitude
                                                            type:type];
            
            [hotspots addObject:hotspot];
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"hotspots" message:sqlErrorMsg];
    }
    
    return hotspots;
}

- (Hotspot*) hotspotWithId:(int)hotspotId
{
    Hotspot* hotspot = nil;
    NSString* query = [NSString stringWithFormat:@"SELECT id, title, description, longitude, latitude, type FROM HOTSPOTS WHERE id='%d'", hotspotId];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            char* titleChar = (char*) sqlite3_column_text(statement, 1);
            char* descriptionChar = (char*) sqlite3_column_text(statement, 2);
            char* longitudeChar = (char*) sqlite3_column_text(statement, 3);
            char* latitudeChar = (char*) sqlite3_column_text(statement, 4);
            char* typeChar = (char*) sqlite3_column_text(statement, 5);
            
            NSString* title = [[NSString alloc] initWithUTF8String:titleChar];
            NSString* description = [[NSString alloc] initWithUTF8String:descriptionChar];
            float longitude = [[[NSString alloc] initWithUTF8String:longitudeChar] floatValue];
            float latitude = [[[NSString alloc] initWithUTF8String:latitudeChar] floatValue];
            HotspotType type = [HotspotsDBHandler stringToHotspotType:[[NSString alloc] initWithUTF8String:typeChar]];
            
            hotspot = [[Hotspot alloc] initWithUniqueId:uniqueId
                                                  title:title
                                            description:description
                                              longitude:longitude
                                               latitude:latitude
                                                   type:type];
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"hotspotWithId" message:sqlErrorMsg];
    }
    
    return hotspot;
}

- (BOOL) deleteHotspotWithId:(int)hotspotId
{
    sqlite3_stmt* statement;
    NSString* query = [NSString stringWithFormat:@"DELETE FROM HOTSPOTS where id=%d", hotspotId];
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            
            [self notifyDataChanged];
            
            return YES;
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"deleteHotspotWithId" message:sqlErrorMsg];
            return NO;
        }
    }
    else
    {
        sqlite3_reset(statement);
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"deleteHotspotWithId" message:sqlErrorMsg];
        return NO;
    }
}

- (BOOL) deleteAllHotspots
{
    // Set the autoincrement to 0 and delete all the rows  :
    sqlite3_stmt* statement;
    NSString* query = @"DELETE FROM sqlite_sequence";
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            sqlite3_finalize(statement);
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"deleteAllHotspots" message:sqlErrorMsg];
            return NO;
        }
    }
    else
    {
        sqlite3_reset(statement);
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"deleteAllHotspots" message:sqlErrorMsg];
        return NO;
    }
    
    query = @"DELETE FROM HOTSPOTS";
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            
            [self notifyDataChanged];
            
            return YES;
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"deleteAllHotspots" message:sqlErrorMsg];
            return NO;
        }
    }
    else
    {
        sqlite3_reset(statement);
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"deleteAllHotspots" message:sqlErrorMsg];
        return NO;
    }
}

- (Hotspot*) addHotspotWithTitle:(NSString*)title description:(NSString*)description longitude:(float)longitude latitude:(float)latitude type:(HotspotType)type
{
    // Prepare the statement :
    sqlite3_stmt* statement;
    NSString *query = @"INSERT INTO HOTSPOTS (id, title, description, longitude, latitude, type) VALUES (NULL, ?, ?, ?, ?, ?)";
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        sqlite3_bind_text(statement, 1, [title UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 2, [description UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 3, [[NSString stringWithFormat:@"%f", longitude] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 4, [[NSString stringWithFormat:@"%f", latitude] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 5, [[HotspotsDBHandler hotspotTypeToString:type] UTF8String], -1, SQLITE_TRANSIENT);
        
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            
            [self notifyDataChanged];
            
            // Return the hotspot (in order to add its image) :
            int lastId = (int) sqlite3_last_insert_rowid(_db);
            return [self hotspotWithId:lastId];
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"addHotspotWithTitle" message:sqlErrorMsg];
            return nil;
        }
    }
    else
    {
        sqlite3_reset(statement);
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"addHotspotWithTitle" message:sqlErrorMsg];
        return nil;
    }
}

- (Hotspot*) addHotspotWithId:(int)uniqueId Title:(NSString*)title description:(NSString*)description longitude:(float)longitude latitude:(float)latitude type:(HotspotType)type
{
    // Prepare the statement :
    sqlite3_stmt* statement;
    NSString *query = @"INSERT INTO HOTSPOTS (id, title, description, longitude, latitude, type) VALUES (?, ?, ?, ?, ?, ?)";
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        sqlite3_bind_int(statement, 1, uniqueId);
        sqlite3_bind_text(statement, 2, [title UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 3, [description UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 4, [[NSString stringWithFormat:@"%f", longitude] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 5, [[NSString stringWithFormat:@"%f", latitude] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 6, [[HotspotsDBHandler hotspotTypeToString:type] UTF8String], -1, SQLITE_TRANSIENT);
        
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            
            [self notifyDataChanged];
            
            // Return the hotspot :
            return [self hotspotWithId:uniqueId];
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"addHotspotWithId" message:sqlErrorMsg];
            return nil;
        }
    }
    else
    {
        sqlite3_reset(statement);
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"addHotspotWithId" message:sqlErrorMsg];
        return nil;
    }
}

- (BOOL) updateHotspotWithId:(int)hotspotId setTitle:(NSString*)title description:(NSString*)description longitude:(float)longitude latitude:(float)latitude type:(HotspotType)type
{
    // Prepare the statement :
    NSString* query = [NSString stringWithFormat:@"UPDATE HOTSPOTS SET title=?, description=?, longitude=?, latitude=?, type=? where id=%d", hotspotId];
    
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        sqlite3_bind_text(statement, 1, [title UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 2, [description UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 3, [[NSString stringWithFormat:@"%f", longitude] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 4, [[NSString stringWithFormat:@"%f", latitude] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 5, [ [HotspotsDBHandler hotspotTypeToString:type] UTF8String], -1, SQLITE_TRANSIENT);
        
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            
            [self notifyDataChanged];
            
            return YES;
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"updateHotspotWithId" message:sqlErrorMsg];
            return NO;
        }
    }
    else
    {
        sqlite3_reset(statement);
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"updateHotspotWithId" message:sqlErrorMsg];
        return NO;
    }
}

- (UIImage*) imageOfHotspot:(Hotspot*)hotspot
{
    // Get the path :
    NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* imageName = [NSString stringWithFormat:@"hotspot_%d.png", hotspot.uniqueId];
    NSString* imagePath = [NSString stringWithFormat:@"%@/new_york/app_hotspots/images/%@", documentsDirectory, imageName];
    if([_dbPath isEqualToString:@"/new_york/user_hotspots/database/user_hotspots.sqlite3"])
    {
        imagePath = [NSString stringWithFormat:@"%@/new_york/user_hotspots/images/%@", documentsDirectory, imageName];
    }
    
    // Init and return the image :
    return [[UIImage alloc] initWithContentsOfFile:imagePath];
}

- (BOOL) saveImage:(UIImage*)image forHotspot:(Hotspot*)hotspot
{
    // Get the path :
    NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* imageName = [NSString stringWithFormat:@"hotspot_%d.png", hotspot.uniqueId];
    NSString* imagePath = [NSString stringWithFormat:@"%@/new_york/app_hotspots/images/%@", documentsDirectory, imageName];
    if([_dbPath isEqualToString:@"/new_york/user_hotspots/database/user_hotspots.sqlite3"])
    {
        imagePath = [NSString stringWithFormat:@"%@/new_york/user_hotspots/images/%@", documentsDirectory, imageName];
    }
    
    // Check if the image existed before or if it is an update of its data :
    BOOL existedBefore = NO;
    if([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) existedBefore = YES;
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    if([imageData writeToFile:imagePath atomically:YES])
    {
        if(existedBefore) [self notifyImageChanged:imageName];
        else [self notifyImageCreated:imageName];
        
        return YES;
    }
    else
    {
        Log(self.class, @"Error writing file : %@", imagePath);
        return NO;
    }
}

- (BOOL) deleteImageOfHotspot:(Hotspot*)hotspot
{
    // Get the path :
    NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* imageName = [NSString stringWithFormat:@"hotspot_%d.png", hotspot.uniqueId];
    NSString* imagePath = [NSString stringWithFormat:@"%@/new_york/app_hotspots/images/%@", documentsDirectory, imageName];
    if([_dbPath isEqualToString:@"/new_york/user_hotspots/database/user_hotspots.sqlite3"])
    {
        imagePath = [NSString stringWithFormat:@"%@/new_york/user_hotspots/images/%@", documentsDirectory, imageName];
    }
    
    NSError *error;
    // Remove the image :
    if ([[NSFileManager defaultManager] removeItemAtPath:imagePath error:&error] != YES)
    {
        Log(self.class, @"Unable to delete file : %@", [error localizedDescription]);
        return NO;
    }
    else
    {
        [self notifyImageDeleted:imageName];
        return YES;
    }
}
// ----------------------------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------------------------
#pragma mark - Favorite hotspots

- (BOOL) setHotspot:(Hotspot*)hotspot isFavorite:(BOOL)isFavorite
{
    if(isFavorite)
    {
        // Prepare the statement :
        sqlite3_stmt* statement;
        NSString *query = @"INSERT OR IGNORE INTO FAVORITE_HOTSPOTS (id, hotspot) VALUES (NULL, ?)";
        
        if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            sqlite3_bind_int(statement, 1, hotspot.uniqueId);
            
            if(sqlite3_step(statement)==SQLITE_DONE)
            {
                sqlite3_finalize(statement);
                [self notifyDataChanged];
                
                return YES;
            }
            else
            {
                sqlite3_reset(statement);
                NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
                [self displayErrorMessageFrom:@"setHotspot:(Hotspot*)hotspot isFavorite:(BOOL)isFavorite" message:sqlErrorMsg];
                
                return NO;
            }
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"setHotspot:(Hotspot*)hotspot isFavorite:(BOOL)isFavorite" message:sqlErrorMsg];
            
            return NO;
        }
    }
    else
    {
        // Prepare the statement :
        sqlite3_stmt* statement;
        NSString *query = @"DELETE FROM FAVORITE_HOTSPOTS WHERE hotspot = ?";
        
        if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            sqlite3_bind_int(statement, 1, hotspot.uniqueId);
            
            if(sqlite3_step(statement) == SQLITE_DONE)
            {
                sqlite3_finalize(statement);
                [self notifyDataChanged];
                
                return YES;
            }
            else
            {
                sqlite3_reset(statement);
                NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
                [self displayErrorMessageFrom:@"setHotspot:(Hotspot*)hotspot isFavorite:(BOOL)isFavorite" message:sqlErrorMsg];
                
                return NO;
            }
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"setHotspot:(Hotspot*)hotspot isFavorite:(BOOL)isFavorite" message:sqlErrorMsg];
            
            return NO;
        }
    }
    
    return NO;
}

- (BOOL) isHotspotFavorite:(Hotspot*)hotspot {
    
    sqlite3_stmt *statement;
    NSString *query = @"SELECT count(*) FROM FAVORITE_HOTSPOTS WHERE hotspot = ?";
    
    if(sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        sqlite3_bind_int(statement, 1, hotspot.uniqueId);
        
        if(sqlite3_step(statement) == SQLITE_ROW)
        {
            int isFavorite = sqlite3_column_int(statement, 0);
            
            sqlite3_finalize(statement);
            [self notifyDataChanged];
            
            return isFavorite > 0 ? YES : NO;
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"setHotspot:(Hotspot*)hotspot isFavorite:(BOOL)isFavorite" message:sqlErrorMsg];
            
            return NO;
        }
    }
    else
    {
        sqlite3_reset(statement);
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"setHotspot:(Hotspot*)hotspot isFavorite:(BOOL)isFavorite" message:sqlErrorMsg];
        
        return NO;
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Excecute a query

- (BOOL) execute:(NSString*)query
{
    sqlite3_stmt* statement;
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            return YES;
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"execute:(NSString*)query" message:sqlErrorMsg];
            return NO;
        }
    }
    else
    {
        sqlite3_reset(statement);
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"execute:(NSString*)query" message:sqlErrorMsg];
        return NO;
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
- (void) notifyDataChanged {}
- (void) notifyImageCreated:(NSString*)imageName {}
- (void) notifyImageChanged:(NSString*)imageName {}
- (void) notifyImageDeleted:(NSString*)imageName {}
// ----------------------------------------------------------------------------------------------------

@end




