//
//  HotspotUpdateController.h
//  NewYork
//
//  Created by Tim Autin on 10/04/13.
//
//

#import <UIKit/UIKit.h>

#import "HotspotsUpdateHandler.h"

@interface HotspotsUpdateController : UIViewController
{
    CGRect _usableFrame;
    HotspotsUpdateHandler* _hotspotsUpdateHandler;
    UIProgressView* _progressView;
    UILabel* _progressViewIndicator;
}

- (id) initWithHandler:(HotspotsUpdateHandler*)hotspotsUpdateHandler;
- (void) setProgress:(float)progress withDescription:(NSString*)description;

@end
