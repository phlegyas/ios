//
//  HotspotsUpdateController.m
//  NewYork
//
//  Created by Tim Autin on 10/04/13.
//
//

#import <QuartzCore/QuartzCore.h>
#import "HotspotsUpdateController.h"
#import "Colors.h"
#import "SystemVersion.h"

#define TITLE_MARGIN_TOP 75
#define TITLE_HEIGHT 50
#define DESCRIPTION_MARGIN_TOP 50
#define DESCRIPTION_HORIZONTAL_MARGIN 10
#define DESCRIPTION_HEIGHT 200
#define PROGRESS_VIEW_INDICATOR_MARGIN_TOP 0
#define PROGRESS_VIEW_INDICATOR_HEIGHT 25
#define PROGRESS_VIEW_MARGIN_TOP 5
#define PROGRESS_VIEW_HEIGHT 10
#define PROGRESS_VIEW_HORIZONTAL_MARGIN 20
#define TOOLBAR_HEIGHT 44

@implementation HotspotsUpdateController

// ----------------------------------------------------------------------------------------------------
#pragma mark - Init functions

- (id) initWithHandler:(HotspotsUpdateHandler*)hotspotsUpdateHandler
{
    self = [super init];
    if (self)
    {
        _hotspotsUpdateHandler = hotspotsUpdateHandler;
    }
    
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    // Compute the usable frame :
    int toolBarHeight = self.navigationController.toolbar.frame.size.height;
    int tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    _usableFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - tabBarHeight - toolBarHeight);
    
    // Set the view's background collor :
    self.view.backgroundColor = [Colors backgroundColor];
    
    // Create the title textview :
    UILabel* title = [[UILabel alloc] initWithFrame:CGRectMake(0, TITLE_MARGIN_TOP, _usableFrame.size.width, TITLE_HEIGHT)];
    title.text = NSLocalizedString(@"hotspots_update_controller_title", "Updating");
    title.numberOfLines = 0;
    title.textAlignment = UITextAlignmentCenter;
    title.backgroundColor = [UIColor clearColor];
    title.font = [UIFont boldSystemFontOfSize:18.0];
    
    if(SYSTEM_VERSION < 7.0)
    {
        title.textColor = [Colors lightTextColor];
        title.layer.shadowColor   = [[UIColor blackColor] CGColor];
        title.layer.shadowOffset  = CGSizeMake(0.0f, -1.0f);
        title.layer.shadowOpacity = 1.0f;
        title.layer.shadowRadius  = 0.5f;
    }
    else
    {
        title.textColor = [Colors darkTextColor];
    }
    
    // Create the description textview :
    UILabel* description = [[UILabel alloc] initWithFrame:CGRectMake(DESCRIPTION_HORIZONTAL_MARGIN, TITLE_MARGIN_TOP + DESCRIPTION_MARGIN_TOP, _usableFrame.size.width - 2*DESCRIPTION_HORIZONTAL_MARGIN, DESCRIPTION_HEIGHT)];
    description.text = NSLocalizedString(@"hotspots_update_controller_message", "...");
    description.numberOfLines = 0;
    description.textAlignment = UITextAlignmentCenter;
    description.backgroundColor = [UIColor clearColor];
    description.font = [UIFont boldSystemFontOfSize:14.0];
    
    if(SYSTEM_VERSION < 7.0)
    {
        description.textColor = [Colors lightTextColor];
        description.layer.shadowColor   = [[UIColor blackColor] CGColor];
        description.layer.shadowOffset  = CGSizeMake(0.0f, -1.0f);
        description.layer.shadowOpacity = 1.0f;
        description.layer.shadowRadius  = 0.5f;
    }
    else
    {
        description.textColor = [Colors darkTextColor];
    }
    
    // Create the progress indicator textview :
    _progressViewIndicator = [[UILabel alloc] initWithFrame:CGRectMake(0, PROGRESS_VIEW_INDICATOR_MARGIN_TOP, _usableFrame.size.width, PROGRESS_VIEW_INDICATOR_HEIGHT)];
    _progressViewIndicator.text = @"0 %";
    _progressViewIndicator.textAlignment = UITextAlignmentCenter;
    _progressViewIndicator.backgroundColor = [UIColor clearColor];
    _progressViewIndicator.textColor = [UIColor whiteColor];
    _progressViewIndicator.font = [UIFont boldSystemFontOfSize:12.0];
    
    _progressViewIndicator.layer.shadowColor   = [[UIColor blackColor] CGColor];
    _progressViewIndicator.layer.shadowOffset  = CGSizeMake(0.0f, -1.0f);
    _progressViewIndicator.layer.shadowOpacity = 1.0f;
    _progressViewIndicator.layer.shadowRadius  = 0.5f;
    
    // Create the progress view :
    _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(PROGRESS_VIEW_HORIZONTAL_MARGIN, PROGRESS_VIEW_INDICATOR_MARGIN_TOP + PROGRESS_VIEW_INDICATOR_HEIGHT + PROGRESS_VIEW_MARGIN_TOP, _usableFrame.size.width - 2 * PROGRESS_VIEW_HORIZONTAL_MARGIN, PROGRESS_VIEW_HEIGHT)];

    [self setProgress:0.01f withDescription:NSLocalizedString(@"hotspots_update_controller_progress_message_initialising", "Initialising...")];
    
    // Create toolbar :
    UIToolbar* toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, _usableFrame.size.height - TOOLBAR_HEIGHT, _usableFrame.size.width, TOOLBAR_HEIGHT)];
    [toolbar setBarStyle:UIBarStyleBlackTranslucent];
    
    // Construct the IHM :
    [self.view addSubview:title];
    [self.view addSubview:description];
    [toolbar addSubview:_progressViewIndicator];
    [toolbar addSubview:_progressView];
    [self.view addSubview:toolbar];
    
    // Start updating the hotspots :
    [_hotspotsUpdateHandler updateHotspots];
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - Progress update

- (void) setProgress:(float)progress withDescription:(NSString*)description
{
    [_progressView setProgress:progress];
    _progressViewIndicator.text = [NSString stringWithFormat:@"%i %% - %@", (int) (progress * 100), description];
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - Screen autorotate

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
// ----------------------------------------------------------------------------------------------------

@end
