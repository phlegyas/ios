//
//  HotspotsUpdateHandler.h
//  NewYork
//
//  Created by Tim Autin on 11/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


#define UPDATE_FAILURE_WHILE_DOWLOADING_IMAGES 0
#define UPDATE_FAILURE_WHILE_DELETING_HOTSPOTS 1
#define UPDATE_FAILURE_WHILE_UPDATING_DATABASE 2


@protocol HotspotsUpdateHandlerDelegate <NSObject>
@optional

- (void) hotspotsUpdateHandlerDidFindUpdate;
- (void) hotspotsUpdateHandlerDidProgress:(float)progress description:(NSString*)description;
- (void) hotspotsUpdateHandlerDidFailWithError:(int)error;
- (void) hotspotsUpdateHandlerDidSucceed;

@end


@interface HotspotsUpdateHandler : NSObject <NSURLConnectionDelegate>
{
    id <HotspotsUpdateHandlerDelegate> _delegate;
    
    NSMutableData* _responseData;
    NSDictionary* _JSONFileContent;
    
    float _localHotspotsFileVersion;
    float _onlineHotspotsFileVersion;
}

@property(nonatomic, strong) id <HotspotsUpdateHandlerDelegate> delegate;

+ (NSURL*) hotspotsJSONFileUrl;

- (void) checkForUpdate;

- (void) updateHotspots;
- (BOOL) downloadImages;
- (void) updateHotspotsDatabase;

@end
