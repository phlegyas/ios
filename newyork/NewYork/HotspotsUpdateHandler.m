//
//  HotspotsUpdateHandler.m
//  NewYork
//
//  Created by Tim Autin on 11/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HotspotsUpdateHandler.h"
#import "HotspotsUpdateController.h"
#import "SBJson.h"
#import "AppHotspotsDBHandler.h"
#import "Logger.h"
#import "AppDelegate.h"

@implementation HotspotsUpdateHandler

@synthesize delegate = _delegate;

// ----------------------------------------------------------------------------------------------------
#pragma mark - Init function

- (id) init
{
    self = [super init];
    if (self)
    {
        // Get the local hotspots file version :
        _localHotspotsFileVersion = [[NSUserDefaults standardUserDefaults] floatForKey:@"hotspotsFileVersion"];
        _onlineHotspotsFileVersion = _localHotspotsFileVersion;
    }
    
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Class functions

+ (NSURL*) hotspotsJSONFileUrl
{
    return [NSURL URLWithString:@"http://bonsplansnewyork.herokuapp.com/hotspots.json"];
    //return [NSURL URLWithString:@"http://ny-french-connection.com/interface_applis/json/pointsMap_mini.json"];
    //return [NSURL URLWithString:@"http://www.nyphel.com/~neoz/bonsPlansVoyageNYC_WebInterfaceV2/json/pointsMap_mini.json"];
}

+ (NSURL*) hotspotsVersionJSONFileUrl
{
    return [NSURL URLWithString:@"http://bonsplansnewyork.herokuapp.com/version"];
    //return [NSURL URLWithString:@"http://ny-french-connection.com/interface_applis/json/pointsMap_mini.json"];
    //return [NSURL URLWithString:@"http://www.nyphel.com/~neoz/bonsPlansVoyageNYC_WebInterfaceV2/json/pointsMap_mini.json"];
}
/*
+ (NSURL*) hotspotsImagesFolderUrl
{
    return [NSURL URLWithString:@"http://www.nyphel.com/~neoz/bonsPlansVoyageNYC_WebInterfaceV2/img/"];
}*/
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Convert the data

- (void) checkForUpdate
{
    Log(self.class, @"Starts downloading Hotspots version JSON file...");
    
	_responseData = [NSMutableData data];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[HotspotsUpdateHandler hotspotsVersionJSONFileUrl] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:4.0];
    NSURLConnection* connection = nil;
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) updateHotspots
{
    Log(self.class, @"Starts updating hotspots...");
    
    // Asynchronously download the images :
    dispatch_queue_t queue = dispatch_queue_create("com.vende.NewYork", NULL);
    dispatch_async(queue, ^
    {
        BOOL succeed = [self downloadImages];
        
        dispatch_async(dispatch_get_main_queue(), ^
        {
            if(succeed)
            {
                Log(self.class, @"All hotspots images have successfully been downloaded and updated");
                
                [self updateHotspotsDatabase];
            }
            else
            {
                Log(self.class, @"An error occured while downloading hotspots images");
                
                // Inform the delegate to inform the user :
                if([self.delegate respondsToSelector:@selector(hotspotsUpdateHandlerDidFailWithError:)])
                {
                    [self.delegate hotspotsUpdateHandlerDidFailWithError:UPDATE_FAILURE_WHILE_DOWLOADING_IMAGES];
                }
            }
        });
    });
}

- (BOOL) downloadImages
{
    Log(self.class, @"Starts downloading hotspots images...");
    
    // Get the hotspots :
    NSArray* hotspots = [_JSONFileContent objectForKey:@"hotspots"];
    
    // Create a temp folder for downloading the images :
    NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* tempImagesFolder = [NSString stringWithFormat:@"%@/new_york/app_hotspots/temp_images/", documentsDirectory];
    
    // If it already exists, delete it to delete any old image :
    if([[NSFileManager defaultManager] fileExistsAtPath:tempImagesFolder])
    {
        [[NSFileManager defaultManager] removeItemAtPath:tempImagesFolder error:nil];
    }
    
    // Create the folder :
    [[NSFileManager defaultManager] createDirectoryAtPath:tempImagesFolder withIntermediateDirectories:YES attributes:nil error:nil];
    
    // Loop through the hotspots :
    BOOL errorOccured = NO;
    int count = 0;
    for(NSDictionary* hotspot in hotspots)
    {
        // Increment the id :
        count++;
        int hotspotId = [[hotspot objectForKey:@"id"] intValue];
        
        // Inform the delegate to inform the user :
        if([self.delegate respondsToSelector:@selector(hotspotsUpdateHandlerDidProgress:description:)])
        {
            float progress = ((count * 97) / hotspots.count) / 100.0f;
            NSString* description = [NSString stringWithFormat:NSLocalizedString(@"hotspots_update_handler_progress_message_image_x_of_y", @"Downloading image %d of %d ..."), count, (int) hotspots.count];
            [self.delegate hotspotsUpdateHandlerDidProgress:progress description:description];
        }
        
        NSString* urlString = [[[hotspot objectForKey:@"url_image"] componentsSeparatedByString:@"?"] objectAtIndex:0];
        
        // If this hotspot have no image, ignore it :
        if(urlString == nil || [urlString isEqual:@"null"] || [urlString isEqual:@""])
        {
            Log(self.class, @"Hotspot %d has no image", hotspotId);
            continue;
        }
        
        // Get the image from the url :
        NSURL* url = [NSURL URLWithString:urlString];
        NSData* urlData = [NSData dataWithContentsOfURL:url];
        
        if (urlData)
        {
            //imageName = [[imageName componentsSeparatedByString:@"/"] objectAtIndex:1];
            NSString* newImageName = [NSString stringWithFormat:@"hotspot_%d.png", hotspotId];
            NSString* newFilePath = [NSString stringWithFormat:@"%@%@", tempImagesFolder, newImageName];
            
            if([urlData writeToFile:newFilePath atomically:YES])
            {
                // Add the skip backup flag :
                [AppDelegate addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:newFilePath]];
                
                Log(self.class, @"%@ : OK", newImageName);
            }
            else
            {
                errorOccured = YES;
                Log(self.class, @"%@ : error while writing %@ to %@", newImageName, urlString, newFilePath);
            }
        }
        else
        {
            errorOccured = YES;
            Log(self.class, @"Error downloading file : %@", urlString);
        }
        
        // Stop downloading if an error occured :
        if(errorOccured) break;
    }
    
    // If there were no error, replace the old images by the new ones :
    if(!errorOccured)
    {
        NSString* imagesFolder = [NSString stringWithFormat:@"%@/new_york/app_hotspots/images/", documentsDirectory];
        NSString* toDeleteImagesFolder = [NSString stringWithFormat:@"%@/new_york/app_hotspots/images_to_delete/", documentsDirectory];
        
        // Rename "images" to "images_to_delete" :
        if([[NSFileManager defaultManager] moveItemAtPath:imagesFolder toPath:toDeleteImagesFolder error:nil])
        {
            // Rename "temp_images" to "images" :
            if([[NSFileManager defaultManager] moveItemAtPath:tempImagesFolder toPath:imagesFolder error:nil])
            {
                // Remove "images_to_delete" :
                if([[NSFileManager defaultManager] removeItemAtPath:toDeleteImagesFolder error:nil])
                {
                    // All images have been downladed and updated. The function will return YES.
                }
                else
                {
                    Log(self.class, @"The hotspots images have successfully been downloaded and updated. The app will update its hotspots and use this version. Nevertheless, an error occured while trying to delete a temporary folder. This error may break the futures updates. This should never happens.");
                }
            }
            else
            {
                // Try to cancel : rename "images_to_delete" to "images" :
                if([[NSFileManager defaultManager] moveItemAtPath:toDeleteImagesFolder toPath:imagesFolder error:nil])
                {
                    errorOccured = YES;
                    
                    Log(self.class, @"The hotspots images have successfully been downloaded, but an error occured while replacing the old images by the new ones. The app will keep the old images and ignore this update.");
                }
                else
                {
                    errorOccured = YES;
                    
                    Log(self.class, @"The hotspots images have successfully been downloaded, but an error occured while replacing the old images by the new ones. Moreover, the file manager failed to rename the \"temp_images\" folder, so the hotspots will have no image. This should never happens.");
                }
            }
        }
        else
        {
            errorOccured = YES;
            
            Log(self.class, @"The hotspots images have successfully been downloaded, but an error occured while replacing the old images by the new ones. The app will keep the old images and ignore this update.");
        }
    }
    
    return ! errorOccured;
}

- (void) updateHotspotsDatabase
{
    Log(self.class, @"Starts updating hotspots database...");
    
    // Inform the delegate to inform the user :
    if([self.delegate respondsToSelector:@selector(hotspotsUpdateHandlerDidProgress:description:)])
    {
        [self.delegate hotspotsUpdateHandlerDidProgress:0.98f description:NSLocalizedString(@"hotspots_update_handler_progress_message_updating_database", @"Updating database...")];
    }
    
    // Delete all hotspots :
    if([[AppHotspotsDBHandler instanceWithLanguage:@"fr"] deleteAllHotspots] && [[AppHotspotsDBHandler instanceWithLanguage:@"en"] deleteAllHotspots] && [[AppHotspotsDBHandler instanceWithLanguage:@"es"] deleteAllHotspots])
    {
        NSArray* hotspots = [_JSONFileContent objectForKey:@"hotspots"];
        //int hotspotId = 0;
        BOOL errorOccured = NO;
        for(NSDictionary* hotspot in hotspots)
        {
            //hotspotId++;
            
            // Get the data :
            int hotspotId = [[hotspot objectForKey:@"id"] intValue];
            float longitude = [[hotspot objectForKey:@"longitude"] floatValue];
            float latitude = [[hotspot objectForKey:@"latitude"] floatValue];
            
            HotspotType type = FEEDING;
            NSString* typeString = [hotspot objectForKey:@"hotspot_type"];
            if([typeString isEqualToString:@"Feeding"]) type = FEEDING;
            else if([typeString isEqualToString:@"Housing"]) type = HOUSING;
            else if([typeString isEqualToString:@"Shopping"]) type = SHOPPING;
            else if([typeString isEqualToString:@"Culture"]) type = CULTURE;
            else if([typeString isEqualToString:@"Picture"]) type = PICTURE;
            else if([typeString isEqualToString:@"Leisure"]) type = LEISURE;
            else if([typeString isEqualToString:@"Pastry"]) type = PASTRY;
            
            NSArray* languages = [hotspot objectForKey:@"languages"];
            
            for(NSDictionary* language in languages)
            {
                // Get the data :
                NSString* locale = [language objectForKey:@"language"];
                NSString* title = [language objectForKey:@"title"];
                NSString* description = [language objectForKey:@"description"];
                
                if([locale isEqual:@"French"]) { locale = @"fr"; }
                else if([locale isEqual:@"English"]) { locale = @"en"; }
                else if([locale isEqual:@"Spanish"]) { locale = @"es"; }
                
                // Add the hotspot :
                [[AppHotspotsDBHandler instanceWithLanguage:locale] deleteHotspotWithId:hotspotId];
                Hotspot* hotspot = [[AppHotspotsDBHandler instanceWithLanguage:locale] addHotspotWithId:hotspotId Title:title description:description longitude:longitude latitude:latitude type:type];
                
                if(hotspot == nil) errorOccured = YES;
            }
        }
        
        if(!errorOccured)
        {
            Log(self.class, @"Hotspots database updated");
            
            [self updateHotspotsFileVersion];
        }
        else
        {
            Log(self.class, @"Error while updating hotspots database");
            
            // Inform the delegate to inform the user :
            if([self.delegate respondsToSelector:@selector(hotspotsUpdateHandlerDidFailWithError:)])
            {
                [self.delegate hotspotsUpdateHandlerDidFailWithError:UPDATE_FAILURE_WHILE_UPDATING_DATABASE];
            }
        }
    }
    else
    {
        Log(self.class, @"Error while deleting application hotspots");
        
        // Inform the delegate to inform the user :
        if([self.delegate respondsToSelector:@selector(hotspotsUpdateHandlerDidFailWithError:)])
        {
            [self.delegate hotspotsUpdateHandlerDidFailWithError:UPDATE_FAILURE_WHILE_DELETING_HOTSPOTS];
        }
    }
}

- (void) updateHotspotsFileVersion
{
    // Inform the delegate to inform the user :
    if([self.delegate respondsToSelector:@selector(hotspotsUpdateHandlerDidProgress:description:)])
    {
        [self.delegate hotspotsUpdateHandlerDidProgress:0.99f description:@"Mise à jour du numéro de version..."];
    }
    
    Log(self.class, @"Starts updating local hotspots file version...");
    
    // Update the local hotspots file version :
    _localHotspotsFileVersion = _onlineHotspotsFileVersion;
    [[NSUserDefaults standardUserDefaults] setFloat:_onlineHotspotsFileVersion forKey:@"hotspotsFileVersion"];
    
    Log(self.class, @"Local hotspots file version updated");
    Log(self.class, @"Hotspots update succeed !");
    
    // Inform the delegate to inform the user :
    if([self.delegate respondsToSelector:@selector(hotspotsUpdateHandlerDidProgress:description:)])
    {
        [self.delegate hotspotsUpdateHandlerDidProgress:1.0f description:NSLocalizedString(@"hotspots_update_handler_progress_message_update_succeed", "Update succeed !")];
    }
    
    // Inform the delegate to inform the user :
    if([self.delegate respondsToSelector:@selector(hotspotsUpdateHandlerDidSucceed)])
    {
        [self.delegate hotspotsUpdateHandlerDidSucceed];
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - NSURLConnectionDelegate

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [_responseData setLength:0];
}

- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	_responseData = nil;
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_responseData appendData:data];
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Convert the data in a NSDictionnary :
    NSString* responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
    _JSONFileContent = (NSDictionary*)[responseString JSONValue];
    _responseData = nil;
    
    // Get the URL :
    NSURL* url = [[connection currentRequest] URL];
    
    if([url isEqual:[HotspotsUpdateHandler hotspotsVersionJSONFileUrl]])
    {
        // Get the online hotspots file version :
        _onlineHotspotsFileVersion = [[_JSONFileContent objectForKey:@"version"] floatValue];
        
        if(_onlineHotspotsFileVersion > _localHotspotsFileVersion)
        {
            Log(self.class, @"Update available (local version is %f, online version is %f), downloading new version...", _localHotspotsFileVersion, _onlineHotspotsFileVersion);
            
            _responseData = [NSMutableData data];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:[HotspotsUpdateHandler hotspotsJSONFileUrl] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:4.0];
            NSURLConnection* connection = nil;
            connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        }
        else
        {
            Log(self.class, @"Local hotspots are up to date (current version is %f)", _localHotspotsFileVersion);
        }
        
    }
    else if([url isEqual:[HotspotsUpdateHandler hotspotsJSONFileUrl]])
    {
        Log(self.class, @"Hotspots JSON file received");
        
        if ( [_delegate respondsToSelector:@selector(hotspotsUpdateHandlerDidFindUpdate)] )
        {
            [_delegate hotspotsUpdateHandlerDidFindUpdate];
        }
    }
}
// ----------------------------------------------------------------------------------------------------



@end


