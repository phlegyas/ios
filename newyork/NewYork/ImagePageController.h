//
//  ImagePageController.h
//  NewYork
//
//  Created by Tim Autin on 30/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIZoomableImageView.h"

@interface ImagePageController : UIViewController
{
    CGRect _usableFrame;
    NSString* _fileName;
    UIZoomableImageView* _zoomableImageView;
}

- (id) initWithContenOfFile:(NSString*)fileName;
- (id) initWithPageId:(int)pageId;

@end
