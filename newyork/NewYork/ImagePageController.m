//
//  ImagePageController.m
//  NewYork
//
//  Created by Tim Autin on 30/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SystemVersion.h"
#import "ImagePageController.h"
#import "Colors.h"

@implementation ImagePageController

// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (id) initWithContenOfFile:(NSString*)fileName
{
    self = [super init];
    if (self)
    {
        _fileName = fileName;
    }
    return self;
}

- (id) initWithPageId:(int)pageId
{
    self = [super init];
    if (self)
    {
        // Get the image file path :
        NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        //NSString* language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
        _fileName = [NSString stringWithFormat:@"%@/good_tips/images/%d.png", documentsDirectory, pageId];
    }
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) loadView
{
    // Call super :
    [super loadView];
    
    // Set the background color :
    self.view.backgroundColor = [Colors backgroundColor];
    
    // Compute the usable frame :
    int toolBarHeight = self.navigationController.toolbar.frame.size.height;
    int tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    
    if(SYSTEM_VERSION < 7.0)
    {
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
        _usableFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - toolBarHeight - tabBarHeight);
    }
    else
    {
        _usableFrame = self.view.frame;
    }
    
    // Get the image file path :
    _zoomableImageView = [[UIZoomableImageView alloc] initWithFrame:_usableFrame contentOfFile:_fileName maximumZoomFactor:3.5];
    
    // Add the view :
    [self.view addSubview:_zoomableImageView];
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - Screen autorotate

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
// ----------------------------------------------------------------------------------------------------

@end
