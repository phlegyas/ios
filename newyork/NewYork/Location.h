//
//  FailedBankInfo.h
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Location : NSObject
{
    int _uniqueId;
    int _location;
    int _position;
    NSString *_title;
}

@property (nonatomic, assign) int uniqueId;
@property (nonatomic, assign) int location;
@property (nonatomic, assign) int position;
@property (nonatomic, copy) NSString *title;

- (id) initWithUniqueId:(int)uniqueId location:(int)location position:(int)position title:(NSString *)title;
- (NSString*) toString;

@end
