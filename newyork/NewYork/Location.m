//
//  FailedBankInfo.m
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Location.h"

@implementation Location

@synthesize uniqueId = _uniqueId;
@synthesize location = _location;
@synthesize position = _position;
@synthesize title = _title;

- (id)initWithUniqueId:(int)uniqueId location:(int)location position:(int)position title:(NSString *)title
{
    if ((self = [super init]))
    {
        self.uniqueId = uniqueId;
        self.location = location;
        self.position = position;
        self.title = title;
    }
    
    return self;
}

- (void) dealloc
{
    self.title = nil;
}

- (NSString*) toString
{
    return [NSString stringWithFormat:@"LOCATION : %d, %d, %d, %@", self.uniqueId, self.location, self.position, self.title];
}

@end

