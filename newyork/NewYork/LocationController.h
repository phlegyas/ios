//
//  LocationController.h
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GAITrackedViewController.h"

@interface LocationController : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate>
{
    int _locationId;
    
    CGRect _usableFrame;
    UITableView* _tableView;
    
    NSArray* _displayedLocations;
    NSArray* _cellsBackgroundColors;
}

- (id)init;
- (id)initWithLocationId:(int)locationId;

@end
