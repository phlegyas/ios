//
//  LocationController.m
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SystemVersion.h"
#import "LocationController.h"
#import "GoodTipsDBHandler.h"
#import "Location.h"
#import "Page.h"
#import "Colors.h"
#import "HTMLPageController.h"
#import "PlanningPageController.h"
#import "ChecklistPageController.h"
#import "ImagePageController.h"
#import "FullScreenImagePageController.h"
#import "CurrencyConverterPageController.h"
#import "TranslationsPageController.h"
#import "TipsComputerPageController.h"


@implementation LocationController


// ----------------------------------------------------------------------------------------------------
#pragma mark - Init functions

- (id) init
{
    self = [super init];
    
    // Init the table at the location 0 :
    if (self) _locationId = 0;
    
    return self;
}

- (id) initWithLocationId:(int)locationId
{
    self = [super init];
    
    // Init the table at the requested location :
    if (self) _locationId = locationId;
    
    return self;
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) loadView
{
    // Call super :
    [super loadView];

    if (_locationId == 1)
        self.screenName = @"A savoir avant de partir";
    else if (_locationId == 2)
        self.screenName = @"A savoir sur place";
    else if (_locationId == 3)
        self.screenName = @"Les plannings types";
    else if (_locationId == 4)
        self.screenName = @"Check-list avant de partir";
    else if (_locationId == 5)
        self.screenName = @"Guide de Conversation";
    
    // Set the back button title (for descendants controllers) :
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"good_tips_tab_location_controller_back_button", @"Back") style:UIBarButtonItemStylePlain target:nil action:nil];
    
    // Compute the usable frame :
    int toolBarHeight = self.navigationController.toolbar.frame.size.height;
    int tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    
    if(SYSTEM_VERSION < 7.0)
    {
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
        _usableFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - toolBarHeight - tabBarHeight);
    }
    else
    {
        _usableFrame = self.view.frame;
    }
    
    // Get the locations at the requested location :
    _displayedLocations = [[GoodTipsDBHandler instance] locationsAtLocation:_locationId];
    
    // Save the colors :
    _cellsBackgroundColors = [[NSArray alloc] initWithObjects:
                              [Colors darkYellow],
                              [Colors lightYellow ],
                              nil];
    
    // Set the view's background color :
    self.view.backgroundColor = [Colors backgroundColor];
    
    // Create the tableView :
    _tableView = [[UITableView alloc] initWithFrame:_usableFrame style:UITableViewStyleGrouped];
    _tableView.backgroundView = nil;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.01f, 0.01f)];
    
    [self.view addSubview:_tableView];
    
    // Set the window's title :
    NSString* locationTitle = [[GoodTipsDBHandler instance] locationWithId:_locationId].title;
    locationTitle = [locationTitle stringByReplacingOccurrencesOfString:@"\\n" withString:@" "];
    
    self.title = locationTitle;
}

- (void) viewWillAppear:(BOOL)inAnimated
{
    NSIndexPath *selected = [_tableView indexPathForSelectedRow];
    if (selected) [_tableView deselectRowAtIndexPath:selected animated:YES];
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - Screen autorotate

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - Table view data source

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_displayedLocations count];
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get the cell :
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    // Get the Location :
    Location* location = [_displayedLocations objectAtIndex:indexPath.row];
    
    // Set its accessoryType and selectionStyle :
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    // Set the cell's label text and color :
    cell.textLabel.text = location.title;
    cell.textLabel.textColor = [UIColor blackColor];
    
    // Set the cell's background color :
    cell.backgroundColor = [_cellsBackgroundColors objectAtIndex:(indexPath.row % _cellsBackgroundColors.count)];
    
    return cell;
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - Table view delegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get the id of the location clicked :
    id object = [_displayedLocations objectAtIndex:indexPath.row];
    int locationClicked = ((Location*) object).uniqueId;
    
    // If the location clicked contains a page :
    if([[GoodTipsDBHandler instance] locationContainsPage:locationClicked])
    {
        // Get the page :
        Page* page = [[GoodTipsDBHandler instance] pageAtLocation:locationClicked];
        
        switch (page.type)
        {
            case HTML :
            {
                // Create and push a HTMLPageController :
                HTMLPageController* HTMLPage = [[HTMLPageController alloc] initWithPageId:page.uniqueId];
                
                // Display it :
                [self.navigationController pushViewController:HTMLPage animated:YES];
                
                break;
            }
            case PLANNING :
            {
                // Create and push a PlanningPageController :
                PlanningPageController* planningPage = [[PlanningPageController alloc] initWithPageId:page.uniqueId];
                
                // Display it :
                [self.navigationController pushViewController:planningPage animated:YES];
                
                break;
            }
            case CHECKLIST :
            {
                // Create and push a ChecklistPageController :
                ChecklistPageController* checklistPage = [[ChecklistPageController alloc] initWithPageId:page.uniqueId];
                
                // Display it :
                [self.navigationController pushViewController:checklistPage animated:YES];
                
                break;
            }
            case IMAGE :
            {
                // Create and push an ImagePageController :
                ImagePageController* checklistPage = [[ImagePageController alloc] initWithPageId:page.uniqueId];
                
                // Display it :
                [self.navigationController pushViewController:checklistPage animated:YES];
                
                break;
            }
            case FULLSCREEN_IMAGE :
            {
                // Create and push a ImagePageController :
                FullScreenImagePageController* fullscreenImagePage = [[FullScreenImagePageController alloc] initWithPageId:page.uniqueId];
                
                // Display it :
                [self.navigationController presentModalViewController:fullscreenImagePage animated:YES];
                
                break;
            }
            case CURRENCY_CONVERTER :
            {
                // Create and push an ImagePageController :
                CurrencyConverterPageController* currencyConverterPage = [[CurrencyConverterPageController alloc] init];
                
                // Display it :
                [self.navigationController pushViewController:currencyConverterPage animated:YES];
                
                break;
            }
            case TRANSLATION :
            {
                // Create and push an ImagePageController :
                TranslationsPageController* translationsPage = [[TranslationsPageController alloc] initWithPageId:page.uniqueId];
                
                // Display it :
                [self.navigationController pushViewController:translationsPage animated:YES];
                
                break;
            }
            case TIPS_COMPUTER :
            {
                // Create and push an TipsComputerPageController :
                TipsComputerPageController* tipsComputerPage = [self.storyboard instantiateViewControllerWithIdentifier:@"TipsComputerPageController"];
                
                // Display it :
                [self.navigationController pushViewController:tipsComputerPage animated:YES];
                
                break;
            }
            default:
            {
                NSLog(@"default page");
                break;
            }
        }
    }
    else
    {
        // Create and push another LocationController :
        LocationController *locationClickedContent = [[LocationController alloc] initWithLocationId:locationClicked];
        [self.navigationController pushViewController:locationClickedContent animated:YES];
    }
}
// ----------------------------------------------------------------------------------------------------

@end




