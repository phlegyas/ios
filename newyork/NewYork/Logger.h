//
//  Logger.h
//  NewYork
//
//  Created by Tim Autin on 08/08/13.
//
//


#import "HotspotsUpdateHandler.h"
#import "ConversionRateUpdateHandler.h"


static BOOL isLoggingEnableForClass(Class sender)
{
    //if(sender == [AppDelegate class]) return NO;
    //if(sender == [HotspotsUpdateHandler class]) return NO;
    if(sender == [ConversionRateUpdateHandler class]) return NO;
    
    return YES;
}

static void Log(Class sender, NSString* format, ...)
{
    if(isLoggingEnableForClass(sender))
    {
        format = [NSString stringWithFormat:@"%@ -> %@", NSStringFromClass(sender), format];
        
        va_list args;
        va_start(args, format);
        NSLogv(format, args);
        va_end(args);
    }
}