//
//  MetroBusController.h
//  NewYork
//
//  Created by Tim Autin on 30/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIZoomableImageView.h"
#import "GAITrackedViewController.h"

@interface MetroBusController : GAITrackedViewController
{
    CGRect _usableFrame;
    UISegmentedControl* _segmentedControl;
    
    UIZoomableImageView* _metroImageView;
    UIZoomableImageView* _busImageView;
}

@end
