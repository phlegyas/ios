//
//  MetroBusController.m
//  NewYork
//
//  Created by Tim Autin on 30/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SystemVersion.h"
#import "MetroBusController.h"
#import "Colors.h"

@implementation MetroBusController

// ----------------------------------------------------------------------------------------------------
#pragma mark - Init function

- (id) initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        
    }
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) viewDidLoad
{
    // Call super :
    [super viewDidLoad];
    self.screenName = @"Métro/Bus";
    
    // Set title :
    self.navigationItem.title = NSLocalizedString(@"metro_bus_tab_metro_tab_title", @"Metro");
    self.view.backgroundColor = [Colors backgroundColor];
    
    // Compute the usable frame :
    int statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    int toolBarHeight = self.navigationController.toolbar.frame.size.height;
    int tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    
    if(SYSTEM_VERSION < 7.0)
    {
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
        _usableFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - toolBarHeight - tabBarHeight);
    }
    else
    {
        _usableFrame = self.view.frame;
    }
    
    // Create the segmented control :
    //NSArray *items = [NSArray arrayWithObjects: @"Métro", @"Bus", nil];
    NSArray *items = [NSArray arrayWithObjects: [UIImage imageNamed:@"metro.png"], [UIImage imageNamed:@"bus.png"], nil];
    _segmentedControl = [[UISegmentedControl alloc] initWithItems:items];
    _segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    _segmentedControl.selectedSegmentIndex = 0;
    _segmentedControl.frame = CGRectMake(0, 0, 80, 30);
    if(SYSTEM_VERSION < 7.0) _segmentedControl.tintColor = [Colors lightOrange];
    [_segmentedControl addTarget:self action:@selector(segmentedControlValueChanged) forControlEvents:UIControlEventValueChanged];
    
    // Create the metro image view :
    _metroImageView = [[UIZoomableImageView alloc] initWithFrame:_usableFrame imageNamed:@"map_metro.png" maximumZoomFactor:8.0];
    _busImageView = [[UIZoomableImageView alloc] initWithFrame:_usableFrame imageNamed:@"map_bus.png" maximumZoomFactor:5.0];
    
    _metroImageView.backgroundColor = [Colors backgroundColor];
    _busImageView.backgroundColor = [Colors backgroundColor];
    
    if(SYSTEM_VERSION >= 7.0)
    {
        _busImageView.scrollView.contentInset = UIEdgeInsetsMake(statusBarHeight + toolBarHeight, 0, tabBarHeight, 0);
        _busImageView.scrollView.contentOffset = CGPointMake(0, (statusBarHeight + toolBarHeight) * -1);
    }
    
    // Add the views :
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_segmentedControl];
    [self.view addSubview:_metroImageView];
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - Screen autorotate

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Segmented control event

- (void) segmentedControlValueChanged
{
    if(_segmentedControl.selectedSegmentIndex == 0)
    {
        // Change the map :
        [UIView transitionFromView:_busImageView toView:_metroImageView duration:0.75 options:UIViewAnimationOptionTransitionFlipFromLeft completion:nil];
        
        // Set title :
        self.navigationItem.title = NSLocalizedString(@"metro_bus_tab_metro_tab_title", @"Metro");
    }
    else
    {
        // Change the map :
        [UIView transitionFromView:_metroImageView toView:_busImageView duration:0.75 options:UIViewAnimationOptionTransitionFlipFromRight completion:nil];
        
        // Set title :
        self.navigationItem.title = NSLocalizedString(@"metro_bus_tab_bus_tab_title", @"Bus");
    }
}
// ----------------------------------------------------------------------------------------------------






@end
