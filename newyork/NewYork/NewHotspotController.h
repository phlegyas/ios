//
//  NewHotspotController.h
//  NewYork
//
//  Created by Tim Autin on 05/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HotspotEditionController.h"


@class NewHotspotController;
@protocol NewHotspotControllerDelegate <NSObject>
@optional
- (void) newHotspotController:(NewHotspotController *)newHotspotController didFinishAddingHotspot:(Hotspot*)hotspot;
@end


@interface NewHotspotController : HotspotEditionController
{
    __weak id <NewHotspotControllerDelegate> _delegate;
}

@property (nonatomic, weak) id <NewHotspotControllerDelegate> delegate;

@end
