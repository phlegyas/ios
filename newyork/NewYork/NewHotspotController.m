//
//  NewHotspotController.m
//  NewYork
//
//  Created by Tim Autin on 05/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NewHotspotController.h"
#import "UserHotspotsDBHandler.h"

@implementation NewHotspotController

@synthesize delegate = _delegate;


// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) loadView
{
    // Call super :
    [super loadView];
    
    // Set the view's title :
    [_hotspotEditionView setViewTitle:NSLocalizedString(@"new_york_tab_new_hotspot_controller_title", @"New hotspot")];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Screen autorotate

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Save the hotspot

- (void) saveHotspot:(Hotspot *)hotspot image:(UIImage *)image mustSaveImage:(BOOL)mustSaveImage
{
    // Save the hotspot in the DB :
    _newHotspot = [[UserHotspotsDBHandler instance] addHotspotWithTitle:hotspot.title description:hotspot.description longitude:hotspot.longitude latitude:hotspot.latitude type:hotspot.type];
    
    // Save the image (if any) :
    if(mustSaveImage && image != nil) [[UserHotspotsDBHandler instance] saveImage:image forHotspot:_newHotspot];
    
    // Open an alert to inform the user :
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"new_york_tab_new_hotspot_controller_hotspot_saved_confirmation_popup_title", @"Confirmation") message:NSLocalizedString(@"new_york_tab_new_hotspot_controller_hotspot_saved_confirmation_popup_message", @"Your hotspot has successfully been saved") delegate:self cancelButtonTitle:NSLocalizedString(@"new_york_tab_new_hotspot_controller_hotspot_saved_confirmation_popup_ok_button", @"OK") otherButtonTitles: nil];
    alert.tag = 3;
    [alert show];
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - UIAlertView delegate

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [super alertView:alertView didDismissWithButtonIndex:buttonIndex];
    
    if(alertView.tag == 3)
    {
        // Notify delegate :
        if([self.delegate respondsToSelector:@selector(newHotspotController:didFinishAddingHotspot:)])
        {
            [self.delegate newHotspotController:self didFinishAddingHotspot:_newHotspot];
        }
    }
}
// ----------------------------------------------------------------------------------------------------



@end



