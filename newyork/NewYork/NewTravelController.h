//
//  NewTravelController.h
//  NewYork
//
//  Created by Tim Autin on 22/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YourTravelsController.h"
#import"GAITrackedViewController.h"

@interface NewTravelController : GAITrackedViewController <UITextFieldDelegate>
{
    CGRect _usableFrame;
    UIScrollView* _wrapperScrollView;
    UIView* _backgroundView;
    UITextField* _activeTextField;
    UIEdgeInsets _scrollViewInsets;
}

@end
