//
//  NewTravelController.m
//  NewYork
//
//  Created by Tim Autin on 22/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SystemVersion.h"
#import <QuartzCore/QuartzCore.h>

#import "NewTravelController.h"
#import "Colors.h"
#import "UIDatePickerTextField.h"
#import "YourTravelsDBHandler.h"
#import "YourTravelsController.h"

#define NEW_TRAVEL_LABEL_HEIGHT 40
#define HORIZONTAL_MARGIN 10
#define VERTICAL_PADDING 5
#define BACKGROUND_VIEW_MARGIN_BOTTOM 40
#define INFORMATIONS_LABEL_HEIGHT 40
#define LABELS_HEIGHT 30
#define TEXTFIELDS_HEIGHT 45
#define SCROLL_TO_ACTIVE_VIEW_MARGIN_BOTTOM 5

@implementation NewTravelController

// ----------------------------------------------------------------------------------------------------
#pragma mark - Init functions

- (id) init
{
    self = [super init];

    if (self)
    {
        _activeTextField = nil;
    }
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) loadView
{
    // Call super :
    [super loadView];
    self.screenName = @"Nouveau Voyage";
    
    // Compute the usable frame :
    int statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    int toolBarHeight = self.navigationController.toolbar.frame.size.height;
    int tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    
    if(SYSTEM_VERSION < 7.0)
    {
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
        _usableFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - toolBarHeight - tabBarHeight);
    }
    else
    {
        _usableFrame = CGRectMake(0, statusBarHeight + toolBarHeight, self.view.frame.size.width, self.view.frame.size.height - statusBarHeight - toolBarHeight - tabBarHeight);
    }
    
    // Set the background color : :
    self.view.backgroundColor = [Colors backgroundColor];
    
    // Init the wrapper scroll view :
    if(SYSTEM_VERSION < 7.0) _wrapperScrollView = [[UIScrollView alloc] initWithFrame:_usableFrame];
    else _wrapperScrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    _wrapperScrollView.contentSize = _usableFrame.size;
    _wrapperScrollView.backgroundColor = [UIColor clearColor];
    
    // Create the new travel label :
    UILabel* newTravelLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _usableFrame.size.width, NEW_TRAVEL_LABEL_HEIGHT)];
    newTravelLabel.text = NSLocalizedString(@"your_travels_tab_new_travel_controller_new_travel_title_label", @"New Travel");
    newTravelLabel.backgroundColor = _wrapperScrollView.backgroundColor;
    newTravelLabel.textAlignment = UITextAlignmentCenter;
    newTravelLabel.font = [UIFont boldSystemFontOfSize:16.0];
    
    if(SYSTEM_VERSION < 7.0)
    {
        newTravelLabel.textColor = [Colors lightYellow];
        newTravelLabel.shadowColor = [UIColor darkGrayColor];
        newTravelLabel.shadowOffset = CGSizeMake(1, 1);
    }
    else
    {
        newTravelLabel.textColor = [Colors darkTextColor];
    }
    
    // Add a background view :
    _backgroundView = [[UIView alloc] initWithFrame:CGRectMake(HORIZONTAL_MARGIN, NEW_TRAVEL_LABEL_HEIGHT+VERTICAL_PADDING, _usableFrame.size.width - 2*HORIZONTAL_MARGIN, _usableFrame.size.height - 2*VERTICAL_PADDING - NEW_TRAVEL_LABEL_HEIGHT - BACKGROUND_VIEW_MARGIN_BOTTOM)];
    
    _backgroundView.backgroundColor = [UIColor whiteColor];
    
    _backgroundView.layer.cornerRadius = 5;
    _backgroundView.layer.borderWidth = 2;
    _backgroundView.layer.borderColor = [[Colors lightYellow] CGColor];
    
    _backgroundView.layer.shadowColor = [[UIColor blackColor] CGColor];
    _backgroundView.layer.shadowOpacity = 0.8;
    _backgroundView.layer.shadowRadius = 3.0;
    _backgroundView.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    
    
    // Create informations label :
    UILabel* informationsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _backgroundView.frame.size.width, INFORMATIONS_LABEL_HEIGHT)];
    informationsLabel.text = NSLocalizedString(@"your_travels_tab_new_travel_controller_new_travel_informations_label", @"Informations");
    informationsLabel.textAlignment = UITextAlignmentCenter;
    informationsLabel.backgroundColor = [UIColor clearColor];
    
    
    // Add the views :
    [self.view addSubview:_wrapperScrollView];
    [_wrapperScrollView addSubview: newTravelLabel];
    [_wrapperScrollView addSubview: _backgroundView];
    [_backgroundView addSubview: informationsLabel];
    
    
    // Create a date formater :
    NSDateFormatter* _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"dd/MM/YYYY"];
    NSDate* today = [NSDate date];
    NSDate* tomorrow = [today dateByAddingTimeInterval:60*60*24];
    NSString* labelTitle = @"";
    NSString* textFieldValue = @"";
    NSDate* textFieldDate = nil;
    NSString* textFieldPlaceholder = @"";
    
    // Create 3 labels / textfields :
    for(int i=0; i<3; i++)
    {
        switch (i)
        {
            case 0 :
            {
                labelTitle = NSLocalizedString(@"your_travels_tab_new_travel_controller_new_travel_travel_title_label", @"Title");
                textFieldValue = @"";
                textFieldPlaceholder = NSLocalizedString(@"your_travels_tab_new_travel_controller_new_travel_travel_title_label_placeholder", @"Please enter the title");
                break;
            }
            case 1 :
            {
                labelTitle = NSLocalizedString(@"your_travels_tab_new_travel_controller_new_travel_travel_begin_date_label", @"Begin date");
                textFieldDate = today;
                break;
            }
            case 2 :
            {
                labelTitle = NSLocalizedString(@"your_travels_tab_new_travel_controller_new_travel_travel_end_date_label", @"End date");
                textFieldDate = tomorrow;
                break;
            }
            default : break;
        }
        
        // Create a label :
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(HORIZONTAL_MARGIN, INFORMATIONS_LABEL_HEIGHT + i*(LABELS_HEIGHT + TEXTFIELDS_HEIGHT), _backgroundView.frame.size.width, LABELS_HEIGHT)];
        label.tag = 2*i + 1;
        label.text = labelTitle;
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor darkGrayColor];
        
        // Create a textfield (wrapped in a dummy scrollview in order to disable the autoscroll) :
        CGRect dummyScrollFrame = CGRectMake(2*HORIZONTAL_MARGIN, INFORMATIONS_LABEL_HEIGHT + LABELS_HEIGHT + i*(LABELS_HEIGHT + TEXTFIELDS_HEIGHT), _backgroundView.frame.size.width - 3*HORIZONTAL_MARGIN, TEXTFIELDS_HEIGHT);
        
        CGRect textFieldFrame = dummyScrollFrame;
        textFieldFrame.origin = CGPointZero;
        
        UITextField* textField;
        if(i == 0)
        {
            textField = [[UITextField alloc] initWithFrame:textFieldFrame];
            textField.text = textFieldValue;
        }
        else if(i == 1 || i == 2)
        {
            textField = [[UIDatePickerTextField alloc] initWithFrame:textFieldFrame];
            [(UIDatePickerTextField*)textField setDatePickerValueAndText:textFieldDate];
            [(UIDatePickerTextField*)textField setDoneButtonTitle:NSLocalizedString(@"your_travels_tab_new_travel_controller_datepicker_ok_button", @"OK")];
        }
        
        textField.tag = 2*i + 2;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        textField.delegate = self;
        textField.placeholder = textFieldPlaceholder;
        
        // Create the dummy scrollview :
        UIScrollView* dummyScrollView = [[UIScrollView alloc] initWithFrame:dummyScrollFrame];
        
        // Add the items :
        [_backgroundView addSubview: label];
        [dummyScrollView addSubview:textField];
        [_backgroundView addSubview: dummyScrollView];
    }
    
    // Create the save button :
    UIBarButtonItem* saveButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"your_travels_tab_new_travel_controller_save_button", @"Save") style:UIBarButtonItemStyleDone target:self action:@selector(saveButtonClicked)];
    if(SYSTEM_VERSION < 7.0) saveButton.tintColor = [Colors lightGreen];
    self.navigationItem.rightBarButtonItem = saveButton;
    
    
    // Add a notification for the keyboard showing :
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardDidShow:) name: UIKeyboardDidShowNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardWillHide:) name: UIKeyboardWillHideNotification object: nil];
}

- (void) viewDidAppear:(BOOL)animated
{
    _scrollViewInsets = _wrapperScrollView.contentInset;
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - Screen autorotate

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - UITextField delegate

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    _activeTextField = textField;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    _activeTextField = nil;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return NO;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Keyboard events

- (void) keyboardDidShow:(NSNotification*)notification
{
    // Get the keyboard size :
    NSDictionary* info = [notification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    // Get the tabbar height :
    int tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    
    // Add an inset under the content :
    UIEdgeInsets newInsets = _scrollViewInsets;
    newInsets.bottom = keyboardSize.height; if(SYSTEM_VERSION < 7.0) newInsets.bottom -= tabBarHeight;
    _wrapperScrollView.contentInset = newInsets;
    _wrapperScrollView.scrollIndicatorInsets = newInsets;
    
    // Compute the visible rect :
    CGRect visibleRect = _usableFrame;
    visibleRect.size.height -= (keyboardSize.height - tabBarHeight);
    
    // Compute the y axis of the active textfield :
    float y =   _backgroundView.frame.origin.y +
    _activeTextField.superview.frame.origin.y +
    _activeTextField.frame.origin.y +
    _activeTextField.frame.size.height +
    SCROLL_TO_ACTIVE_VIEW_MARGIN_BOTTOM;
    
    // Compute the offset :
    CGPoint pointToShow = CGPointMake(0, y - _wrapperScrollView.contentOffset.y);
    CGPoint offsetToShow = CGPointMake(0, y - visibleRect.origin.y - visibleRect.size.height );
    
    // Animate to the offset :
    if (!CGRectContainsPoint(visibleRect, pointToShow) )
    {
        [_wrapperScrollView setContentOffset:offsetToShow animated:YES];
    }
}

- (void) keyboardWillHide:(NSNotification*)notification
{
    // Remove the inset under the content :
    [UIView animateWithDuration:0.3 animations:^
    {
        _wrapperScrollView.contentInset = _scrollViewInsets;
        _wrapperScrollView.scrollIndicatorInsets = _scrollViewInsets;
    }];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Saving the travel

- (void) saveButtonClicked
{
    // Get the data :
    NSString* title = ((UITextField*)[self.view viewWithTag:2]).text;
    NSDate* beginDate = ((UIDatePickerTextField*)[self.view viewWithTag:4]).date;
    NSDate* endDate = ((UIDatePickerTextField*)[self.view viewWithTag:6]).date;
    
    if([title isEqualToString:@""])
    {
        // Open an alert view to inform the user that he must enter a title :
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"your_travels_tab_new_travel_no_title_error_popup_title", @"Error") message:NSLocalizedString(@"your_travels_tab_new_travel_no_title_error_popup_message", @"Please enter the travel's title") delegate:nil cancelButtonTitle:NSLocalizedString(@"your_travels_tab_new_travel_no_title_error_popup_ok_button", @"OK") otherButtonTitles:nil];
        [alert show];
    }
    else if([beginDate compare:endDate] == NSOrderedDescending)
    {
        // Open an alert view to inform the user that the end date must be after the begin date :
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"your_travels_tab_new_travel_wrong_dates_error_popup_title", @"Error") message:NSLocalizedString(@"your_travels_tab_new_travel_wrong_dates_error_popup_message", @"The end date must follow the begin date") delegate:nil cancelButtonTitle:NSLocalizedString(@"The end date must follow the begin date", @"OK") otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        NSDateFormatter* df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"dd/MM/YYYY"];

        if([[YourTravelsDBHandler instance] addTravelWithTitle:title beginDate:beginDate endDate:endDate isCurrentTravel:YES])
        {
            // Open an alert with two custom buttons
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"your_travels_tab_new_travel_confirmation_popup_title", @"Confirmation") message:NSLocalizedString(@"your_travels_tab_new_travel_confirmation_popup_message", @"Your travel has been saved") delegate:self cancelButtonTitle:NSLocalizedString(@"your_travels_tab_new_travel_confirmation_popup_ok_button", @"OK") otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            // Open an alert with two custom buttons
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"your_travels_tab_new_travel_unknown_error_popup_title", @"Error") message:NSLocalizedString(@"your_travels_tab_new_travel_unknown_error_popup_message", @"Failed to create the travel") delegate:nil cancelButtonTitle:NSLocalizedString(@"your_travels_tab_new_travel_unknown_error_popup_ok_button", @"OK") otherButtonTitles:nil];
            [alert show];
        }
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - UIAlertView delegate

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    // Create a new YourTravelsController :
    YourTravelsController* controller = [[YourTravelsController alloc] initWithTravel:[[YourTravelsDBHandler instance] currentTravel] coder:nil];
    
    // Replace the last one by the new :
    NSMutableArray *controllers = [self.navigationController.viewControllers mutableCopy];
    [controllers replaceObjectAtIndex:controllers.count-2 withObject:controller];
    self.navigationController.viewControllers = controllers;
    
    // Display the new :
    [self.navigationController popViewControllerAnimated:YES];
}
// ----------------------------------------------------------------------------------------------------


@end







