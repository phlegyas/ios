//
//  NewYorkController.h
//  NewYork
//
//  Created by Tim Autin on 05/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RMMapView.h"
#import "NewHotspotController.h"
#import "EditHotspotController.h"
#import "HotspotMarker.h"
#import "UICustomAlertView.h"
#import "CompassView.h"

typedef enum {APP_HOTSPOTS_ONLY, USER_HOTSPOTS_ONLY, FAVORITE_HOTSPOTS_ONLY, ALL_HOTSPOTS} HotspotsDisplaying;

@interface NewYorkController : GAITrackedViewController <RMMapViewDelegate, UIGestureRecognizerDelegate, NewHotspotControllerDelegate, EditHotspotControllerDelegate, UISearchBarDelegate, UICustomAlertViewDelegate, CLLocationManagerDelegate>
{
    CGRect _usableFrame;
	RMMapView *_mapView;
    
    CLLocationCoordinate2D _initialLocation;
    CLLocationCoordinate2D _topLeftOfCoverage;
    CLLocationCoordinate2D _bottomRightOfCoverage;
    
    CLLocationCoordinate2D _newHotspotLocation;
    HotspotMarker* _selectedMarker;
    HotspotMarker* _displayedMarker;
    
    CLLocationManager* _locationManager;
    
    UIButton* _hotspotsDisplayingButton;
    
    UIToolbar* _searchBarContainer;
    UISearchBar* _searchBar;
    UIView *tuto;
    UIView *first;
    UIView *last;
    NSMutableDictionary *label;
    int step;
}

//tutoriel
- (void) firstlaunch;

- (void) initToolBar;
- (void) initHotspotButtonsToolBar;
- (void) initMapView;
- (void) setDBMapSource;
- (void) initHotspots;
- (void) initAboutButton;
- (void) resetHotspots;

- (void) addHotspotMarker:(Hotspot*)hotspot isUserHotspot:(BOOL)isUserHotspot;
- (void) updateHotspotMarker:(HotspotMarker*)marker setType:(HotspotType)type;
//- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

- (void) centerMapOnInitialLocationButtonClicked;
- (void) trackUserPositionButtonClicked:(UIBarButtonItem*)sender;

- (HotspotsDisplaying) hotspotsDisplaying;
- (void) setHotspotsDisplaying:(HotspotsDisplaying) hotspotsDisplaying;
- (BOOL) hotspotTypeIsVisible:(HotspotType)hotspotType;
- (void) setHotspotType:(HotspotType)hotspotType isVisible:(BOOL)visible;

- (void) hotspotButtonClicked:(UIButton*)sender;
- (void) hotspotsDisplayingButtonClicked:(UIButton*)sender;

- (void) deleteHotspot:(Hotspot*)hotspot;

- (BOOL) userInMapCoverage;
- (BOOL) isTracking;
- (void) startTracking;
- (void) stopTracking;

@end
