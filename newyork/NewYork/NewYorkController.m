//
//  NewYorkController.m
//  NewYork
//
//  Created by Tim Autin on 05/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SystemVersion.h"
#import "NewYorkController.h"


#import "RMDBMapSource.h"
#import "RMAnnotation.h"
#import "RMUserLocation.h"

#import "RMOpenStreetMapSource.h"
#import "RMOpenSeaMapLayer.h"
#import "RMMapView.h"
#import "RMMarker.h"
#import "RMCircle.h"
#import "RMProjection.h"
#import "RMAnnotation.h"
#import "RMQuadTree.h"
#import "RMCoordinateGridSource.h"
#import "RMOpenCycleMapSource.h"

/*
 TODO
#import "RMMBTilesTileSource.h"
#import "RMMarkerManager.h"
#import "RMLayerCollection.h"
*/

#import "HotspotMarker.h"
#import "CODialog.h"
#import "AboutPageController.h"

#import "AppHotspotsDBHandler.h"
#import "UserHotspotsDBHandler.h"

#import "Colors.h"

#import "PCToastMessage.h"
#import "Logger.h"

#define HOTSPOTS_TOOLBAR_BUTTONS_WIDTH 35
#define HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT 35
#define HOTSPOTS_TOOLBAR_SEPARATOR_WIDTH 5
#define HOTSPOTS_TOOLBAR_SEPARATOR_HEIGHT 35
#define HOTSPOTS_TOOLBAR_HOTSPOTS_DISPLAYING_BUTTON_WIDTH 35
#define HOTSPOTS_TOOLBAR_HOTSPOTS_DISPLAYING_BUTTON_HEIGHT 35
#define HOTSPOTS_TOOLBAR_HEIGHT 45

#define SEARCHBBAR_CONTAINER_HEIGHT 45

#define MAP_HOTSPOT_IMAGE_WIDTH 25
#define MAP_HOTSPOT_IMAGE_HEIGHT 25



@implementation NewYorkController


// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) viewDidLoad
{
    // Call super :
    
    [super viewDidLoad];
    
    // Compute the usable frame :
    int statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    int toolBarHeight = self.navigationController.toolbar.frame.size.height;
    int tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    
    if(SYSTEM_VERSION < 7.0)
    {
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
        _usableFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - toolBarHeight - tabBarHeight);
    }
    else
    {
        _usableFrame = CGRectMake(0, statusBarHeight + toolBarHeight, self.view.frame.size.width, self.view.frame.size.height - statusBarHeight - toolBarHeight - tabBarHeight);
    }
    
    // Init elements of the controller :
    [self initToolBar];
    [self initHotspotButtonsToolBar];
    [self initSearchBar];
    [self initShowSearchBarButton];
    [self initMapView];
    [self initHotspots];
    [self initLocationManager];
    [self initAboutButton];
    
    CompassView *compass;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        compass = [[CompassView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width - 120, _usableFrame.origin.y + SEARCHBBAR_CONTAINER_HEIGHT * 2, 120, 120)];
    else
        compass = [[CompassView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width - 80, _usableFrame.origin.y + SEARCHBBAR_CONTAINER_HEIGHT * 2, 80, 80)];
    
    [self.view addSubview:compass];
    
    if ( ! [[NSUserDefaults standardUserDefaults]boolForKey:@"firstlaunch"] ) {
        [self firstlaunch];
       [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"firstlaunch"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"New York";
    if ( ! [[NSUserDefaults standardUserDefaults]boolForKey:@"firstlaunch"] ) {
        if (!tuto)
            [self firstlaunch];
        else
            [self relaunch_tuto];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"firstlaunch"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void) relaunch_tuto
{
    UILabel *object;
    step = 1;
    first.frame = CGRectMake(0,  0, self.view.bounds.size.width, self.view.bounds.size.height);
    tuto.frame = CGRectMake(0,  self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    last.frame = CGRectMake(0,  self.view.bounds.size.height * 2, self.view.bounds.size.width, self.view.bounds.size.height);
    object = [label objectForKey:@"marker"];
    object.frame = CGRectMake((self.view.bounds.size.width / 2) - 100, object.frame.origin.y, object.frame.size.width, object.frame.size.height);
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        object = [label objectForKey:@"cadre"];
        object.frame = CGRectMake((self.view.bounds.size.width / 2) - 115, (self.view.bounds.size.height / 2) - 15, 250, 120);
        object = [label objectForKey:@"decadre"];
        object.frame = CGRectMake(self.view.bounds.size.width - 10, - HOTSPOTS_TOOLBAR_HEIGHT, 22, 0);
        object = [label objectForKey:@"decadresys"];
        object.frame = CGRectMake(- 6, - HOTSPOTS_TOOLBAR_HEIGHT, 22, 0);
    }
}

- (void) firstlaunch
{
    UIImageView *cadre;
    UILabel *object;
    UIImageView *touch;
    UIButton *start;


    label = [[NSMutableDictionary alloc] initWithCapacity:21];
    
    [label setObject: [UIButton buttonWithType:UIButtonTypeRoundedRect] forKey:@"welcom"];
    
    start = [label objectForKey:@"welcom"];
    start.frame = CGRectMake((self.view.bounds.size.width / 2) - 72, self.view.bounds.size.height / 2 - 100, 150, 150);
    [start setTitle:@"play" forState:UIControlStateNormal];
    [start addTarget:self action:@selector(removetuto:) forControlEvents:UIControlEventTouchUpInside];

    [start setImage: [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"play" ofType:@"png"]] forState:UIControlStateNormal];
    start.tintColor = [UIColor colorWithRed:(149/255.f) green:(165/255.f) blue:(166/255.f) alpha:1.0];                                                                                                                                                                                                                                                                                                                                                                          
   
    
    [label setObject: [UIButton buttonWithType:UIButtonTypeRoundedRect ]forKey:@"start"];
    start = [label objectForKey:@"start"];
    start.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    start.titleLabel.textAlignment = NSTextAlignmentCenter;
    [start setTitle: NSLocalizedString(@"new_york_tab_new_york_controller_touch_tuto", @"Touch here to start") forState:UIControlStateNormal];
    start.frame = CGRectMake((self.view.bounds.size.width / 2) - 110, (self.view.bounds.size.height / 2) + SEARCHBBAR_CONTAINER_HEIGHT * 2, 220, SEARCHBBAR_CONTAINER_HEIGHT * 2);
    start.titleLabel.font = [UIFont fontWithName:@"Futura-CondensedExtraBold" size:22];
    start.tintColor = [UIColor colorWithRed:(236/255.f) green:(240/255.f) blue:(241/255.f) alpha:1.0];
    
    float horizontalHotspotButtonsMargin = (_usableFrame.size.width - 7 * HOTSPOTS_TOOLBAR_BUTTONS_WIDTH - HOTSPOTS_TOOLBAR_SEPARATOR_WIDTH - HOTSPOTS_TOOLBAR_HOTSPOTS_DISPLAYING_BUTTON_WIDTH) / 10.0;

    int x = horizontalHotspotButtonsMargin;

    int size ;
    int marge;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        x = x - 14;
        size = 12;
        marge = 37;
    }
    else
    {
        size = 7;
        marge = 2;
    }
    [label setObject: [[UILabel alloc] initWithFrame:CGRectMake(x - marge, HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT + 7 , HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + marge * 2, HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT)] forKey:@"restaurant"];
     object = [label objectForKey:@"restaurant"];
    [object setFont:[UIFont fontWithName:@"Futura-CondensedExtraBold" size: size]];
    object.lineBreakMode = NSLineBreakByWordWrapping;
    object.numberOfLines = 0;
    object.text = NSLocalizedString(@"new_york_tab_new_york_controller_retaurant_tuto", @"Restaurant");
    object.textColor = [UIColor colorWithRed:(246/255.f) green:(250/255.f) blue:(251/255.f) alpha:1.0];
    [object setNumberOfLines:0];
    [object sizeToFit];
    object.frame = CGRectMake(x - marge,  HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT * 1.3, HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + marge * 2, object.frame.size.height);
    object.textAlignment = NSTextAlignmentCenter;
    x += HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + horizontalHotspotButtonsMargin;
    
    [label setObject: [[UILabel alloc] initWithFrame:CGRectMake(x - marge, HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT, HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + marge * 2, HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT)] forKey:@"hotel"];
    object = [label objectForKey:@"hotel"];
     [object setFont:[UIFont fontWithName:@"Futura-CondensedExtraBold" size:size]];
    object.lineBreakMode = NSLineBreakByWordWrapping;
    object.text = NSLocalizedString(@"new_york_tab_new_york_controller_hotel_tuto", @"Hotel");
    object.textColor = [UIColor colorWithRed:(236/255.f) green:(240/255.f) blue:(241/255.f) alpha:1.0];
    [object setNumberOfLines:0];
    [object sizeToFit];
    object.frame = CGRectMake(x - marge,  HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT * 1.3, HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + marge * 2, object.frame.size.height);
    object.textAlignment = NSTextAlignmentCenter;
    x += HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + horizontalHotspotButtonsMargin;
    
    [label setObject: [[UILabel alloc] initWithFrame:CGRectMake(x - marge, HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT , HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + marge * 2, HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT)] forKey:@"shopping"];
    object = [label objectForKey:@"shopping"];
     [object setFont:[UIFont fontWithName:@"Futura-CondensedExtraBold" size:size]];
    object.text = NSLocalizedString(@"new_york_tab_new_york_controller_shopping_tuto", @"Shopping");
    object.textColor = [UIColor colorWithRed:(236/255.f) green:(240/255.f) blue:(241/255.f) alpha:1.0];
    [object setNumberOfLines:0];
    [object sizeToFit];
    object.frame = CGRectMake(x - marge,  HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT * 1.3, HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + marge * 2, object.frame.size.height);
    object.textAlignment = NSTextAlignmentCenter;
    x += HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + horizontalHotspotButtonsMargin;
    
    [label setObject: [[UILabel alloc] initWithFrame:CGRectMake(x - marge, HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT + 3, HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + marge * 2, HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT)] forKey:@"monument"];
    object = [label objectForKey:@"monument"];
     [object setFont:[UIFont fontWithName:@"Futura-CondensedExtraBold" size:size]];
    object.lineBreakMode = NSLineBreakByWordWrapping;
    object.numberOfLines = 0;
    object.text = NSLocalizedString(@"new_york_tab_new_york_controller_monument_tuto", @"Monument");
    object.textColor = [UIColor colorWithRed:(236/255.f) green:(240/255.f) blue:(241/255.f) alpha:1.0];
    [object setNumberOfLines:0];
    [object sizeToFit];
    object.frame = CGRectMake(x - marge,  HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT * 1.3, HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + marge * 2, object.frame.size.height);
    object.textAlignment = NSTextAlignmentCenter;
    x += HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + horizontalHotspotButtonsMargin;

    [label setObject: [[UILabel alloc] initWithFrame:CGRectMake(x - marge, HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT, HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + marge * 2, HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT)] forKey:@"place"];
    object = [label objectForKey:@"place"];
     [object setFont:[UIFont fontWithName:@"Futura-CondensedExtraBold" size:size]];
    object.text = NSLocalizedString(@"new_york_tab_new_york_controller_place_tuto", @"Place");
    object.textColor = [UIColor colorWithRed:(236/255.f) green:(240/255.f) blue:(241/255.f) alpha:1.0];
    [object setNumberOfLines:0];
    [object sizeToFit];
    object.frame = CGRectMake(x - marge,  HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT * 1.3, HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + marge * 2, object.frame.size.height);
    object.textAlignment = NSTextAlignmentCenter;
    x += HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + horizontalHotspotButtonsMargin;
    
    [label setObject: [[UILabel alloc] initWithFrame:CGRectMake(x - marge, HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT , HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + marge * 2, HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT)] forKey:@"loisir"];
    object = [label objectForKey:@"loisir"];
     [object setFont:[UIFont fontWithName:@"Futura-CondensedExtraBold" size:size]];
    object.text = NSLocalizedString(@"new_york_tab_new_york_controller_sport_tuto", @"Loisir");
    object.textColor = [UIColor colorWithRed:(236/255.f) green:(240/255.f) blue:(241/255.f) alpha:1.0];
    [object setNumberOfLines:0];
    [object sizeToFit];
    object.frame = CGRectMake(x - marge,  HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT * 1.3, HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + marge * 2, object.frame.size.height);
    object.textAlignment = NSTextAlignmentCenter;
    x += HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + horizontalHotspotButtonsMargin;
    
    [label setObject: [[UILabel alloc] initWithFrame:CGRectMake(x - marge, HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT , HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + marge * 2, HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT)] forKey:@"patisserie"];
    object = [label objectForKey:@"patisserie"];
     [object setFont:[UIFont fontWithName:@"Futura-CondensedExtraBold" size:size]];
    object.text = NSLocalizedString(@"new_york_tab_new_york_controller_patisserie_tuto", @"Pastry shop");
    object.textColor = [UIColor colorWithRed:(236/255.f) green:(240/255.f) blue:(241/255.f) alpha:1.0];
    [object setNumberOfLines:0];
    [object sizeToFit];
    object.frame = CGRectMake(x - marge,  HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT * 1.3, HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + marge * 2, object.frame.size.height);
    object.textAlignment = NSTextAlignmentCenter;
    x += HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + horizontalHotspotButtonsMargin;


    [label setObject: [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] forKey:@"hotspot"];
    object = [label objectForKey:@"hotspot"];
    object.text = NSLocalizedString(@"new_york_tab_new_york_controller_hotspot_tuto", @"hotspot's filter");
    object.font = [UIFont fontWithName:@"Futura-CondensedExtraBold" size:15];
    object.lineBreakMode = NSLineBreakByWordWrapping;
    object.textAlignment = NSTextAlignmentCenter;
    object.numberOfLines = 0;
    object.textColor = [UIColor colorWithRed:(236/255.f) green:(240/255.f) blue:(241/255.f) alpha:1.0];

    [label setObject: [[UILabel alloc] initWithFrame:CGRectMake(- self.view.bounds.size.width, - HOTSPOTS_TOOLBAR_HEIGHT, self.view.bounds.size.width, 0)] forKey:@"hslayer"];
    object = [label objectForKey:@"hslayer"];
    object.backgroundColor = [UIColor colorWithRed:(52/255.f) green:(73/255.f) blue:(94/255.f) alpha:0.4];
    object.layer.cornerRadius = 8;
    object.layer.borderWidth = 0.1;

    [label setObject: [[UILabel alloc] initWithFrame:CGRectMake(self.view.bounds.size.width, 8, 200, 50)] forKey:@"localize"];
    object = [label objectForKey:@"localize"];
    object.text = NSLocalizedString(@"new_york_tab_new_york_controller_localize_tuto", @"location");
    object.font = [UIFont fontWithName:@"Futura-CondensedExtraBold" size:15];
    object.textColor = [UIColor colorWithRed:(236/255.f) green:(240/255.f) blue:(241/255.f) alpha:1.0];
    
    [label setObject: [[UILabel alloc] initWithFrame:CGRectMake(- 200, 8, 200, 50)] forKey:@"mapcenter"];
    object = [label objectForKey:@"mapcenter"];
    object.text =  NSLocalizedString(@"new_york_tab_new_york_controller_mapcenter_tuto", @"center the map");
    object.font = [UIFont fontWithName:@"Futura-CondensedExtraBold" size:15];
    object.textColor = [UIColor colorWithRed:(236/255.f) green:(240/255.f) blue:(241/255.f) alpha:1.0];
    
    [label setObject: [[UILabel alloc] initWithFrame:CGRectMake((self.view.bounds.size.width / 2) - 100, self.view.bounds.size.height / 2, 220, SEARCHBBAR_CONTAINER_HEIGHT * 2)] forKey:@"marker"];
    object = [label objectForKey:@"marker"];
    object.text =  NSLocalizedString(@"new_york_tab_new_york_controller_marker_tuto", @"To create your own hotspot: hold your finger on a location on the map");
    object.font = [UIFont fontWithName:@"Futura-CondensedExtraBold" size:15];
    object.textColor = [UIColor colorWithRed:(236/255.f) green:(240/255.f) blue:(241/255.f) alpha:1.0];
    object.lineBreakMode = NSLineBreakByWordWrapping;
    object.backgroundColor = [UIColor colorWithRed:(52/255.f) green:(73/255.f) blue:(94/255.f) alpha:0.4];
    object.numberOfLines = 0;
    object.textAlignment = NSTextAlignmentCenter;
    
    [label setObject: [[UILabel alloc] initWithFrame:CGRectMake(-200, _usableFrame.size.height + 10, 200, 50)] forKey:@"search"];
    object = [label objectForKey:@"search"];
    object.text = NSLocalizedString(@"new_york_tab_new_york_controller_search_tuto", @"search button");
    object.font = [UIFont fontWithName:@"Futura-CondensedExtraBold" size:15];
    object.textColor = [UIColor colorWithRed:(236/255.f) green:(240/255.f) blue:(241/255.f) alpha:1.0];
   
    [label setObject: [[UILabel alloc] initWithFrame:CGRectMake(_usableFrame.size.width, _usableFrame.size.height + 10, 100, 50)] forKey:@"info"];
    object = [label objectForKey:@"info"];
    object.text = NSLocalizedString(@"new_york_tab_new_york_controller_about_tuto", @"about...");
    object.font = [UIFont fontWithName:@"Futura-CondensedExtraBold" size:15];
    object.textColor = [UIColor colorWithRed:(236/255.f) green:(240/255.f) blue:(241/255.f) alpha:1.0];
    
    size = size + 8;
    marge = 125;
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        marge = 200;
    [label setObject: [[UILabel alloc] initWithFrame:CGRectMake((self.view.bounds.size.width / 2) - marge, self.view.bounds.size.height / 2, marge * 2, SEARCHBBAR_CONTAINER_HEIGHT * 3)] forKey:@"end"];
    object = [label objectForKey:@"end"];
    object.text =  NSLocalizedString(@"new_york_tab_new_york_controller_end_tuto", @"Thank you for following this tutorial you can find under 'About'. Feel free to mail me if you have questions.");
    object.font = [UIFont fontWithName:@"Futura-CondensedExtraBold" size:size];
    object.textColor = [UIColor colorWithRed:(236/255.f) green:(240/255.f) blue:(241/255.f) alpha:1.0];
    object.lineBreakMode = NSLineBreakByWordWrapping;
    object.numberOfLines = 0;
    object.textAlignment = NSTextAlignmentCenter;
    
    
    [label setObject: [[UIImageView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width - 10, - HOTSPOTS_TOOLBAR_HEIGHT, 22, 0)] forKey:@"decadre"];
    cadre = [label objectForKey:@"decadre"];
    cadre.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"demi_encadrement" ofType:@"png"]];
    
    [label setObject: [[UIImageView alloc] initWithFrame:CGRectMake(- 6, - HOTSPOTS_TOOLBAR_HEIGHT, 22, 0)] forKey:@"decadresys"];
    cadre = [label objectForKey:@"decadresys"];
    cadre.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"demi_encadrement" ofType:@"png"]];
    
    cadre.image = [UIImage imageWithCGImage:cadre.image.CGImage
                                      scale:cadre.image.scale
                                orientation:UIImageOrientationUpMirrored];
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [label setObject: [[UIImageView alloc] initWithFrame:CGRectMake((self.view.bounds.size.width / 2) - 115, (self.view.bounds.size.height / 2) - 15, 250, 120)] forKey:@"cadre"];
        cadre = [label objectForKey:@"cadre"];
        cadre.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"encadrement" ofType:@"png"]];
    }
   
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        [label setObject: [[UIImageView alloc] initWithFrame:CGRectMake((self.view.bounds.size.width / 2) - 30, (self.view.bounds.size.height / 2)  + 120, 100, 117)] forKey:@"touch"];
    } else {
        [label setObject: [[UIImageView alloc] initWithFrame:CGRectMake((self.view.bounds.size.width / 2) - 20, (self.view.bounds.size.height / 2)  + 100, 50, 65)] forKey:@"touch"];
    }
    touch = [label objectForKey:@"touch"];
    touch.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"touch" ofType:@"png"]];
 
    
    tuto = [[UIView alloc] initWithFrame:self.view.bounds];
    tuto.frame = CGRectMake(0,  self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    
    tuto.backgroundColor = [[UIColor colorWithRed:(52/255.f) green:(73/255.f) blue:(94/255.f) alpha:1.0] colorWithAlphaComponent:.6];
    [[[UIApplication sharedApplication] keyWindow] addSubview:tuto];
    
    UITapGestureRecognizer *touchlayer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removetuto:)];
    UITapGestureRecognizer *touchlast = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removetuto:)];
    
    [tuto addSubview:[label objectForKey:@"hslayer"]];
    [tuto addSubview:[label objectForKey:@"hotspot"]];
    [[label objectForKey:@"hslayer"]  addSubview:[label objectForKey:@"hotel"]];
    [[label objectForKey:@"hslayer"]  addSubview:[label objectForKey:@"shopping"]];
    [[label objectForKey:@"hslayer"] addSubview:[label objectForKey:@"restaurant"]];
    [[label objectForKey:@"hslayer"]  addSubview:[label objectForKey:@"monument"]];
    [[label objectForKey:@"hslayer"]  addSubview:[label objectForKey:@"place"]];
    [[label objectForKey:@"hslayer"]  addSubview:[label objectForKey:@"patisserie"]];
    [[label objectForKey:@"hslayer"]  addSubview:[label objectForKey:@"loisir"]];
    [tuto addSubview:[label objectForKey:@"localize"]];
    [tuto addSubview:[label objectForKey:@"mapcenter"]];
    [tuto addSubview:[label objectForKey:@"info"]];
    [tuto addSubview:[label objectForKey:@"search"]];
    [tuto addSubview:[label objectForKey:@"decadre"]];
    [tuto addSubview:[label objectForKey:@"decadresys"]];
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    [tuto addSubview:cadre];
    [tuto addSubview:touch];
    [tuto addSubview:[label objectForKey:@"marker"]];
   
    
    step = 1;

    first = [[UIView alloc] initWithFrame:CGRectMake(0,  self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height)];
    last = [[UIView alloc] initWithFrame:CGRectMake(0,  self.view.bounds.size.height * 2, self.view.bounds.size.width, self.view.bounds.size.height)];
    last.backgroundColor = [[UIColor colorWithRed:(52/255.f) green:(73/255.f) blue:(94/255.f) alpha:1.0] colorWithAlphaComponent:.6];
    
    first.backgroundColor = [[UIColor colorWithRed:(52/255.f) green:(73/255.f) blue:(94/255.f) alpha:1.0] colorWithAlphaComponent:.6];
    [self.view addSubview:first];
    [[[UIApplication sharedApplication] keyWindow] addSubview:last];
    [first addSubview:[label objectForKey:@"start"]];
    [first addSubview:[label objectForKey:@"welcom"]];
    [[label objectForKey:@"start"] addTarget:self action:@selector(removetuto:) forControlEvents:UIControlEventTouchUpInside];

    [tuto addGestureRecognizer:touchlayer];
    [last addGestureRecognizer:touchlast];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:3.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    
    first.frame = CGRectMake(0,  0, self.view.bounds.size.width, self.view.bounds.size.height);

   
    [UIView commitAnimations];
}

- (IBAction)        removetuto :(id) sender
{
    UILabel         *object;
    UIImageView     *touch;
    UIImageView     *cadre;


    touch = [label objectForKey:@"touch"];
    cadre = [label objectForKeyedSubscript:@"cadre"];

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    switch (step) {
        case 1:
        {
            first.frame = CGRectMake(0, - self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
            tuto.frame = CGRectMake(0, tuto.frame.origin.y - self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
            last.frame = CGRectMake(0, last.frame.origin.y - self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
            step = 2;
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone )
            {
                object = [label objectForKey:@"marker"];
                object.layer.borderWidth = 1.0;
                object.layer.cornerRadius = 0;
                object.layer.borderColor = [UIColor colorWithRed:(189/255.f) green:(195/255.f) blue:(199/255.f) alpha:1.0].CGColor;
            }
            break;
        }
        case 2:
        {
            object = [label objectForKey:@"hotspot"];
            object.frame = CGRectMake(0, _usableFrame.origin.y + 38, self.view.bounds.size.width, SEARCHBBAR_CONTAINER_HEIGHT * 2);
            object = [label objectForKey:@"hslayer"];
            object.frame = CGRectMake(14, _usableFrame.origin.y - 5, self.view.bounds.size.width - 28, HOTSPOTS_TOOLBAR_HEIGHT + 10);
            object.layer.borderColor = [UIColor colorWithRed:(189/255.f) green:(195/255.f) blue:(199/255.f) alpha:1.0].CGColor;
            
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
            {
                object = [label objectForKey:@"hslayer"];
                object.frame = CGRectMake(14, _usableFrame.origin.y - 15, self.view.bounds.size.width - 28, HOTSPOTS_TOOLBAR_HEIGHT + 30);
                touch.frame = CGRectMake(touch.frame.origin.x, _usableFrame.origin.y + HOTSPOTS_TOOLBAR_HEIGHT + 59, touch.frame.size.width, touch.frame.size.height);
                object = [label objectForKey:@"decadre"];
                object.frame = CGRectMake(self.view.bounds.size.width - 29, _usableFrame.origin.y - 30, 29, 105);
                object = [label objectForKey:@"decadresys"];
                object.frame = CGRectMake(0, _usableFrame.origin.y - 30, 29, 105);
            }
            else
            {
                object = [label objectForKey:@"hslayer"];
                object.frame = CGRectMake(0, _usableFrame.origin.y - 15, self.view.bounds.size.width, HOTSPOTS_TOOLBAR_HEIGHT + 30);
                object.layer.borderWidth = 1.0;
                object.layer.cornerRadius = 0;
                touch.frame = CGRectMake(touch.frame.origin.x, _usableFrame.origin.y + HOTSPOTS_TOOLBAR_HEIGHT + 65, touch.frame.size.width, touch.frame.size.height);
            }
            object = [label objectForKey:@"marker"];
            object.frame = CGRectMake(- 700, object.frame.origin.y, object.frame.size.width, object.frame.size.height);
            cadre.frame = CGRectMake(- 700, cadre.frame.origin.y, cadre.frame.size.width, cadre.frame.size.height);
            step = 3;
            break;
        }
        case 3:
        {
            object = [label objectForKey:@"localize"];
            object.frame = CGRectMake(self.view.bounds.size.width - 119, -2, 200, 50);
            object = [label objectForKey:@"hotspot"];
            object.frame = CGRectMake(0, 0, 0, 0);
            object = [label objectForKey:@"hslayer"];
            object.frame = CGRectMake(- self.view.bounds.size.width, - HOTSPOTS_TOOLBAR_HEIGHT, object.frame.size.width, object.frame.size.height);
            object = [label objectForKey:@"decadre"];
            object.frame = CGRectMake(- 22 , - HOTSPOTS_TOOLBAR_HEIGHT, 22, 65);
            object = [label objectForKey:@"decadresys"];
            object.frame = CGRectMake(- self.view.bounds.size.width, - HOTSPOTS_TOOLBAR_HEIGHT, 22, 65);
            
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
                touch.frame = CGRectMake(self.view.bounds.size.width - 88, 60, touch.frame.size.width, touch.frame.size.height);
            else
                touch.frame = CGRectMake(self.view.bounds.size.width - 68, 56, touch.frame.size.width, touch.frame.size.height);
            touch.image = [UIImage imageWithCGImage:touch.image.CGImage
                                              scale:touch.image.scale
                                        orientation:UIImageOrientationUpMirrored];
            step = 4;
            break;
        }
        case 4:
        {
            object = [label objectForKey:@"mapcenter"];
            object.frame = CGRectMake(0, -2 , 200, 50);
            object = [label objectForKey:@"localize"];
            object.frame = CGRectMake(self.view.bounds.size.width, 8, 200, 50);
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
            touch.frame = CGRectMake(1, touch.frame.origin.y, touch.frame.size.width, touch.frame.size.height);
            }
            else
            {
                touch.frame = CGRectMake(1, touch.frame.origin.y, touch.frame.size.width, touch.frame.size.height);
            }
            touch.image = [UIImage imageWithCGImage:touch.image.CGImage
                                              scale:touch.image.scale
                                        orientation:UIImageOrientationUp];
            step = 5;
            break;
        }
        case 5:
        {
            object = [label objectForKey:@"search"];
            object.frame = CGRectMake(0, _usableFrame.size.height + 5, 200, 50);
            object = [label objectForKey:@"mapcenter"];
            object.frame = CGRectMake(- 200, 8, 200, 50);
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
            touch.frame = CGRectMake(touch.frame.origin.x, _usableFrame.size.height - 105, touch.frame.size.width, touch.frame.size.height);
            }
            else
            {
                touch.frame = CGRectMake(touch.frame.origin.x, _usableFrame.size.height - 53, touch.frame.size.width, touch.frame.size.height);
            }
            touch.image = [UIImage imageWithCGImage:touch.image.CGImage
                                scale:touch.image.scale
                          orientation:UIImageOrientationDownMirrored];
            
            step = 6;
            break;
        }
        case 6:
        {
            object = [label objectForKey:@"info"];
            object.frame = CGRectMake(_usableFrame.size.width - 100, _usableFrame.size.height + 5, 100, 50);
            object = [label objectForKey:@"search"];
            object.frame = CGRectMake(-200, _usableFrame.size.height + 10, 200, 50);
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
           touch.frame = CGRectMake(_usableFrame.size.width - 76, touch.frame.origin.y, touch.frame.size.width, touch.frame.size.height);
            }
            else
            {
                touch.frame = CGRectMake(_usableFrame.size.width - 56, touch.frame.origin.y, touch.frame.size.width, touch.frame.size.height);
            }
            touch.image = [UIImage imageWithCGImage:touch.image.CGImage
                                              scale:touch.image.scale
                                        orientation:UIImageOrientationDown];
            step = 7;
            break;
        }
        case 7:
        {
            object = [label objectForKey:@"info"];
            object.frame = CGRectMake(_usableFrame.size.width, _usableFrame.size.height + 10, 100, 50);
            if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
                touch.frame = CGRectMake((self.view.bounds.size.width / 2) - 30, (self.view.bounds.size.height / 2)  + 120, touch.frame.size.width, touch.frame.size.height);
            } else {
                touch.frame = CGRectMake((self.view.bounds.size.width / 2) - 20, (self.view.bounds.size.height / 2)  + 100, touch.frame.size.width, touch.frame.size.height);
            }
            
            touch.image = [UIImage imageWithCGImage:touch.image.CGImage
                                              scale:touch.image.scale
                                        orientation:UIImageOrientationUp];
            first.frame = CGRectMake(0, - self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
            tuto.frame = CGRectMake(0, tuto.frame.origin.y - self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
            last.frame = CGRectMake(0, last.frame.origin.y - self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
             [last addSubview:[label objectForKey:@"end"]];
            step = 8;
            break;
        }
        case 8:
        {
            last.frame = CGRectMake(0, last.frame.origin.y - self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    
        }
        default: break;
    }
    
   
    [UIView commitAnimations];
}

- (void) initToolBar
{
    // Add a button to center the map on NY :
    UIBarButtonItem* centerMapOnInitialLocationButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"target_new_york.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(centerMapOnInitialLocationButtonClicked)];
    self.navigationItem.leftBarButtonItem = centerMapOnInitialLocationButtonItem;
    
    // Add a button to track the user position :
    UIBarButtonItem* trackUserPositionButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"target_user.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(trackUserPositionButtonClicked:)];
    self.navigationItem.rightBarButtonItem = trackUserPositionButtonItem;
}

- (void) initHotspotButtonsToolBar
{
    // Create a toolbar which will contain the buttons :
    CGRect hotspotButtonsToolBarFrame = _usableFrame;
    hotspotButtonsToolBarFrame.size.height = HOTSPOTS_TOOLBAR_HEIGHT;
    
    UIToolbar* hotspotButtonsToolBar = [[UIToolbar alloc] initWithFrame:hotspotButtonsToolBarFrame];
    hotspotButtonsToolBar.barStyle = UIBarStyleDefault;
    
    if(SYSTEM_VERSION < 7.0)
    {
        [hotspotButtonsToolBar setBackgroundImage:[[UIImage alloc] init] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
        UIView* background = [[UIView alloc] initWithFrame:hotspotButtonsToolBar.bounds];
        background.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        [hotspotButtonsToolBar insertSubview:background atIndex:0];
    }
    
    // Add the toolbar :
	[self.view addSubview:hotspotButtonsToolBar];
    
    // Compute margins :
    float horizontalHotspotButtonsMargin = (hotspotButtonsToolBarFrame.size.width - 7*HOTSPOTS_TOOLBAR_BUTTONS_WIDTH - HOTSPOTS_TOOLBAR_SEPARATOR_WIDTH - HOTSPOTS_TOOLBAR_HOTSPOTS_DISPLAYING_BUTTON_WIDTH) / 10.0;
    float verticalHotspotButtonsMargin = (hotspotButtonsToolBarFrame.size.height - HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT) / 2.0;
    
    // Add the places types buttons :
    int x = horizontalHotspotButtonsMargin;
    for(int i=0; i<7; i++)
    {
        // Get the button image name :
        NSString* buttonImageNameSelected;
        NSString* buttonImageNameNormal;
        
        // Visibility of the hotspots :
        BOOL showHotspots = YES;
        
        switch (i)
        {
            case 0 :
            {
                buttonImageNameSelected = @"hotspot_feeding_55.png";
                buttonImageNameNormal = @"hotspot_feeding_off_55.png";
                showHotspots = [[NSUserDefaults standardUserDefaults] boolForKey:@"showFeedingHotspots"];
                break;
            }
            case 1 :
            {
                buttonImageNameSelected = @"hotspot_housing_55.png";
                buttonImageNameNormal = @"hotspot_housing_off_55.png";
                showHotspots = [[NSUserDefaults standardUserDefaults] boolForKey:@"showHousingHotspots"];
                break;
            }
            case 2 :
            {
                buttonImageNameSelected = @"hotspot_shopping_55.png";
                buttonImageNameNormal = @"hotspot_shopping_off_55.png";
                showHotspots = [[NSUserDefaults standardUserDefaults] boolForKey:@"showShoppingHotspots"];
                break;
            }
            case 3 :
            {
                buttonImageNameSelected = @"hotspot_culture_55.png";
                buttonImageNameNormal = @"hotspot_culture_off_55.png";
                showHotspots = [[NSUserDefaults standardUserDefaults] boolForKey:@"showCultureHotspots"];
                break;
            }
            case 4 :
            {
                buttonImageNameSelected = @"hotspot_picture_55.png";
                buttonImageNameNormal = @"hotspot_picture_off_55.png";
                showHotspots = [[NSUserDefaults standardUserDefaults] boolForKey:@"showPictureHotspots"];
                break;
            }
            case 5 :
            {
                buttonImageNameSelected = @"hotspot_leisure_55.png";
                buttonImageNameNormal = @"hotspot_leisure_off_55.png";
                showHotspots = [[NSUserDefaults standardUserDefaults] boolForKey:@"showLeisureHotspots"];
                break;
            }
            case 6 :
            {
                buttonImageNameSelected = @"hotspot_pastry_55.png";
                buttonImageNameNormal = @"hotspot_pastry_off_55.png";
                showHotspots = [[NSUserDefaults standardUserDefaults] boolForKey:@"showPastryHotspots"];
                break;
            }
            default : break;
        }
        
        // Create the button :
        UIButton* button = [[UIButton alloc] initWithFrame:CGRectMake(x, verticalHotspotButtonsMargin, HOTSPOTS_TOOLBAR_BUTTONS_WIDTH, HOTSPOTS_TOOLBAR_BUTTONS_HEIGHT)];
        button.selected = showHotspots;
        button.tag = i;
        //button.backgroundColor = i%2 == 0 ? [UIColor redColor] : [UIColor blueColor];
        
        // Set its images :
        [button setImage:[UIImage imageNamed:buttonImageNameSelected] forState:UIControlStateSelected];
        [button setImage:[UIImage imageNamed:buttonImageNameNormal] forState:UIControlStateNormal];
        
        // Set the button's target :
        [button addTarget:self action:@selector(hotspotButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        // Add the button to the hotspotButtonsToolBar :
        [hotspotButtonsToolBar addSubview:button];
        
        // Increment x :
        x += HOTSPOTS_TOOLBAR_BUTTONS_WIDTH + horizontalHotspotButtonsMargin;
    }
    
    // Create a separator :
    UIImageView* separator = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"separator.png"]];
    separator.frame = CGRectMake(x, verticalHotspotButtonsMargin, HOTSPOTS_TOOLBAR_SEPARATOR_WIDTH, HOTSPOTS_TOOLBAR_SEPARATOR_HEIGHT);
    x += HOTSPOTS_TOOLBAR_SEPARATOR_WIDTH + horizontalHotspotButtonsMargin;
    
    [hotspotButtonsToolBar addSubview:separator];
    
    
    // Create the hotspots displaying button :
    _hotspotsDisplayingButton = [[UIButton alloc] initWithFrame:CGRectMake(x, verticalHotspotButtonsMargin, HOTSPOTS_TOOLBAR_HOTSPOTS_DISPLAYING_BUTTON_WIDTH, HOTSPOTS_TOOLBAR_HOTSPOTS_DISPLAYING_BUTTON_HEIGHT)];
    
    // Set its images  and target :
    [_hotspotsDisplayingButton setImage:[UIImage imageNamed:@"hotspots_displaying_app_hotspots_only_55.png"] forState:UIControlStateNormal];
    [_hotspotsDisplayingButton addTarget:self action:@selector(hotspotsDisplayingButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [hotspotButtonsToolBar addSubview:_hotspotsDisplayingButton];
}

- (void) initSearchBar
{
    // Init the toolBar's frame :
    CGRect searchBarContainerFrame = _usableFrame;
    searchBarContainerFrame.size.height = SEARCHBBAR_CONTAINER_HEIGHT;
    searchBarContainerFrame.origin.y = _usableFrame.origin.y;
    
    // Init the search bar container :
    _searchBarContainer = [[UIToolbar alloc] initWithFrame:searchBarContainerFrame];
    _searchBarContainer.barStyle = UIBarStyleDefault;
    _searchBarContainer.alpha = 0.0f;
    
    // Init the search bar's frame :
    CGRect searchBarFrame = CGRectMake(0, 0, self.view.frame.size.width - 100, SEARCHBBAR_CONTAINER_HEIGHT);
    
    // Init the search bar :
    _searchBar = [[UISearchBar alloc] initWithFrame:searchBarFrame];
    _searchBar.translucent = YES;
    _searchBar.backgroundImage = [[UIImage alloc] init];
    _searchBar.delegate = self;
    _searchBar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    
    if(SYSTEM_VERSION < 7.0)
    {
        // Remove the background :
        [_searchBarContainer setBackgroundImage:[[UIImage alloc] init] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
        
        // Create a background view :
        UIView* background = [[UIView alloc] initWithFrame:_searchBarContainer.bounds];
        background.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        background.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        
        // Add the background at index 0 :
        [_searchBarContainer insertSubview:background atIndex:0];
    }
    
    UIButton* cancelButton;
    if(SYSTEM_VERSION < 7.0) { cancelButton = [UIButton buttonWithType:UIButtonTypeCustom]; }
    else { cancelButton = [UIButton buttonWithType:UIButtonTypeRoundedRect]; }
    
    cancelButton.frame = CGRectMake(_searchBar.frame.size.width, 0, 100, SEARCHBBAR_CONTAINER_HEIGHT);
    cancelButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [cancelButton setTitle:NSLocalizedString(@"new_york_tab_new_york_controller_search_bar_cancel_button", @"Cancel") forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(searchBarCancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [_searchBarContainer addSubview:_searchBar];
    [_searchBarContainer addSubview:cancelButton];
    
	[self.view addSubview:_searchBarContainer];
}

- (void) initShowSearchBarButton
{
    // Get the image :
    UIImage* searchButtonImage = [UIImage imageNamed:@"search.png"];
    
    // Create the button and assign the image :
    UIButton* searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchButton setImage:searchButtonImage forState:UIControlStateNormal];
    
    // Set the frame :
    CGRect searchButtonFrame = CGRectMake(15, _usableFrame.origin.y + _usableFrame.size.height - 33, 29, 29);
    searchButton.frame = searchButtonFrame;
    
    // Add a target :
    [searchButton addTarget:self action:@selector(showSearchBar) forControlEvents:UIControlEventTouchUpInside];
    
    // Add it :
    [self.view addSubview:searchButton];
}

- (void) initMapView
{
    // Set the initial location :
    _initialLocation = CLLocationCoordinate2DMake(40.746737,-73.989315);
    
    // Init the map view :
    _mapView = [[RMMapView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [_mapView setCenterCoordinate:_initialLocation];
    _mapView.delegate = self;
    _mapView.backgroundColor = [UIColor blackColor];
    _mapView.showLogoBug = NO;
    [_mapView setHideAttribution:YES];
    
    // Init the map content :
    [self setDBMapSource];
    _mapView.zoom = 11;
    
    _mapView.centerCoordinate = _initialLocation;
    // Increase max zoom level :
    _mapView.maxZoom = 17;
    
    // Add the map view :
    [[self view] addSubview:_mapView];
    [[self view] sendSubviewToBack:_mapView];
    
    // If the app must track the user, do it :
    if([self isTracking])
    {
        if([self userInMapCoverage])
        {
            NSLog(@"startTracking");
            [self startTracking];
        }
        else
        {
            NSLog(@"stopTracking");
            [self stopTracking];
        }
    }
}

- (void) initHotspots
{
    // Add the application hotspots :
    NSArray* appHotspots = [[AppHotspotsDBHandler instance] hotspots];
    
    for(Hotspot* hotspot in appHotspots)
    {
        if([hotspot isKindOfClass:[Hotspot class]])
        {
            [self addHotspotMarker:hotspot isUserHotspot:NO];
        }
    }
    
    // Add the user hotspots :
    NSArray* userHotspots = [[UserHotspotsDBHandler instance] hotspots];
    
    for(Hotspot* hotspot in userHotspots)
    {
        if([hotspot isKindOfClass:[Hotspot class]])
        {
            [self addHotspotMarker:hotspot isUserHotspot:YES];
        }
    }
    
    [self setHotspotsDisplaying:self.hotspotsDisplaying];
}

- (void) initLocationManager
{
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
     if([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) [_locationManager requestWhenInUseAuthorization];
    
    [_locationManager startUpdatingLocation];
}

- (void) initAboutButton
{
    // Get the image :
    UIImage* aboutButtonImage = [UIImage imageNamed:@"info.png"];
    
    // Create the button and assign the image :
    UIButton* aboutButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [aboutButton setImage:aboutButtonImage forState:UIControlStateNormal];
    
    // Set the frame :
    CGRect aboutButtonFrame = CGRectMake(_usableFrame.size.width - 33, _usableFrame.origin.y + _usableFrame.size.height - 33, 29, 29);
    aboutButton.frame = aboutButtonFrame;
    
    // Add a target :
    [aboutButton addTarget:self action:@selector(showAboutPage) forControlEvents:UIControlEventTouchUpInside];
    
    // Add it :
    [self.view addSubview:aboutButton];
}

- (void) resetHotspots
{
    // Remove all the markers :
    [_mapView removeAllAnnotations];
    
    // Reload the markers :
    [self initHotspots];
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - Screen autorotate

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Add a hotspot marker on the map

- (void) addHotspotMarker:(Hotspot*)hotspot isUserHotspot:(BOOL)isUserHotspot
{
    // Get coordinates :
    CLLocationCoordinate2D coordinates = CLLocationCoordinate2DMake(hotspot.latitude, hotspot.longitude);
    
    BOOL showHotspot = YES;
    
    // Get image name :
    NSString* imageName = @"";
    switch (hotspot.type)
    {
        case FEEDING :
        {
            imageName = @"hotspot_feeding_55.png";
            showHotspot = [[NSUserDefaults standardUserDefaults] boolForKey:@"showFeedingHotspots"];
            break;
        }
        case HOUSING :
        {
            imageName = @"hotspot_housing_55.png";
            showHotspot = [[NSUserDefaults standardUserDefaults] boolForKey:@"showHousingHotspots"];
            break;
        }
        case SHOPPING :
        {
            imageName = @"hotspot_shopping_55.png";
            showHotspot = [[NSUserDefaults standardUserDefaults] boolForKey:@"showShoppingHotspots"];
            break;
        }
        case CULTURE :
        {
            imageName = @"hotspot_culture_55.png";
            showHotspot = [[NSUserDefaults standardUserDefaults] boolForKey:@"showCultureHotspots"];
            break;
        }
        case PICTURE :
        {
            imageName = @"hotspot_picture_55.png";
            showHotspot = [[NSUserDefaults standardUserDefaults] boolForKey:@"showPictureHotspots"];
            break;
        }
        case LEISURE :
        {
            imageName = @"hotspot_leisure_55.png";
            showHotspot = [[NSUserDefaults standardUserDefaults] boolForKey:@"showLeisureHotspots"];
            break;
        }
        case PASTRY :
        {
            imageName = @"hotspot_pastry_55.png";
            showHotspot = [[NSUserDefaults standardUserDefaults] boolForKey:@"showPastryHotspots"];
            break;
        }
        default : break;
    }
    
    if([self hotspotsDisplaying] == USER_HOTSPOTS_ONLY && !isUserHotspot) showHotspot = NO;
    else if([self hotspotsDisplaying] == APP_HOTSPOTS_ONLY && isUserHotspot) showHotspot = NO;
    
    if([self hotspotsDisplaying] == FAVORITE_HOTSPOTS_ONLY)
    {
        if(isUserHotspot)
        {
            if(![[UserHotspotsDBHandler instance] isHotspotFavorite:hotspot]) showHotspot = NO;
        }
        else
        {
            if(![[AppHotspotsDBHandler instance] isHotspotFavorite:hotspot]) showHotspot = NO;
        }
    }
    
    // Create and add a marker :
    HotspotMarker* marker = [[HotspotMarker alloc] initWithUIImage:[UIImage imageNamed:imageName] hotspot:hotspot isUserHotspot:isUserHotspot];
    [marker setBounds:CGRectMake(0, 0, MAP_HOTSPOT_IMAGE_WIDTH, MAP_HOTSPOT_IMAGE_HEIGHT)];
    marker.opacity = (float) showHotspot;
    
    RMAnnotation *annotation = [RMAnnotation annotationWithMapView:_mapView coordinate:coordinates andTitle:@"test"];
    annotation.userInfo = marker;
    annotation.layer.bounds = CGRectMake(0, 0, MAP_HOTSPOT_IMAGE_WIDTH, MAP_HOTSPOT_IMAGE_HEIGHT);
    annotation.anchorPoint = CGPointMake(0.5, 1.0);
    
    [_mapView addAnnotation:annotation];
}

- (void) updateHotspotMarker:(HotspotMarker*)marker setType:(HotspotType)type
{
    // Get image name :
    NSString* imageName = @"";
    switch (type)
    {
        case FEEDING : { imageName = @"hotspot_feeding_55.png"; break; }
        case HOUSING : { imageName = @"hotspot_housing_55.png"; break; }
        case SHOPPING : { imageName = @"hotspot_shopping_55.png"; break; }
        case CULTURE : { imageName = @"hotspot_culture_55.png"; break; }
        case PICTURE : { imageName = @"hotspot_picture_55.png"; break; }
        case LEISURE : { imageName = @"hotspot_leisure_55.png"; break; }
        case PASTRY : { imageName = @"hotspot_pastry_55.png"; break; }
        default : break;
    }
    
    // Update the marker image :
    CGRect bounds = marker.bounds;
    [marker replaceUIImage:[UIImage imageNamed:imageName]];
    [marker setBounds:bounds];
}

- (RMMapLayer*)mapView:(RMMapView *)mapView layerForAnnotation:(RMAnnotation *)annotation
{
    if(annotation.isUserLocationAnnotation) return nil;
    return annotation.userInfo;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Map source

- (void) setDBMapSource
{
    // Create the map source :
    NSString *databasePath = [[NSBundle mainBundle] pathForResource:@"NYMap" ofType:@"sqlite"];
   // NSString *databasePath = [NSString stringWithFormat:@"/new_york/NYMap.sqlite"];
    RMDBMapSource* tileSource = [[RMDBMapSource alloc] initWithPath:databasePath];
    
    // Set the map tile source :
    [_mapView setTileSource:(tileSource)];
    
    // Save the coverage :
    _topLeftOfCoverage = tileSource.topLeftOfCoverage;
    _bottomRightOfCoverage = tileSource.bottomRightOfCoverage;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Toolbar and hotspot toolbar interraction

- (void) centerMapOnInitialLocationButtonClicked
{
    [self stopTracking];
    [_mapView setCenterCoordinate:_initialLocation];
}

- (void) trackUserPositionButtonClicked:(UIBarButtonItem*)sender
{
    if([self userInMapCoverage])
    {
        if(_mapView.userTrackingMode == RMUserTrackingModeNone) [self startTracking];
        else [self stopTracking];
    }
    else
    {
        // Show an alert view :
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"new_york_tab_new_york_controller_geolocation_failed_popup_title", @"Geolocation failed") message:NSLocalizedString(@"new_york_tab_new_york_controller_geolocation_failed_popup_message_you_seem_to_be_outside_of_new_york", @"You seem to be outside of New York") delegate:self cancelButtonTitle:NSLocalizedString(@"new_york_tab_new_york_controller_geolocation_failed_popup_message_ok_button", @"OK") otherButtonTitles: nil];
        [alertView show];
    }
}

- (void) hotspotButtonClicked:(UIButton*)sender
{
    float zomlvl = _mapView.zoom;
    float lat = _mapView.centerCoordinate.latitude;
    float lon = _mapView.centerCoordinate.longitude;
    
    [_mapView setZoom:12 atCoordinate:_mapView.centerCoordinate animated:NO];
    UIButton* button = (UIButton*) sender;
    button.selected = ! button.selected;
    
    HotspotType typeToToggle = -1;
    switch (button.tag)
    {
        case 0 : typeToToggle = FEEDING; break;
        case 1 : typeToToggle = HOUSING; break;
        case 2 : typeToToggle = SHOPPING; break;
        case 3 : typeToToggle = CULTURE; break;
        case 4 : typeToToggle = PICTURE; break;
        case 5 : typeToToggle = LEISURE; break;
        case 6 : typeToToggle = PASTRY; break;
        default : break;
    }
    
    [self setHotspotType:typeToToggle isVisible:button.selected];
    
    for(RMAnnotation* annotation in _mapView.annotations)
    {
        if([annotation.layer isKindOfClass:[HotspotMarker class]])
        {
            // Cast the hotspot :
            HotspotMarker* hotspotMarker = (HotspotMarker*) annotation.layer;
            
            // If the hotspot style is the one to toggle :
            if(hotspotMarker.hotspot.type == typeToToggle)
            {
                switch ([self hotspotsDisplaying])
                {
                    case APP_HOTSPOTS_ONLY:
                    {
                        if(!hotspotMarker.isUserHotspot)
                        {
                            hotspotMarker.opacity = (float) button.selected;
                        }
                        break;
                    }
                    case USER_HOTSPOTS_ONLY:
                    {
                        if(hotspotMarker.isUserHotspot)
                        {
                            hotspotMarker.opacity = (float) button.selected;
                        }
                        break;
                    }
                    case FAVORITE_HOTSPOTS_ONLY:
                    {
                        // Check if this hotspot is a favorite :
                        if(hotspotMarker.isUserHotspot)
                        {
                            if([[UserHotspotsDBHandler instance] isHotspotFavorite:hotspotMarker.hotspot]) hotspotMarker.opacity = (float) button.selected;
                            else hotspotMarker.opacity = 0.0;
                        }
                        else
                        {
                            if([[AppHotspotsDBHandler instance] isHotspotFavorite:hotspotMarker.hotspot]) hotspotMarker.opacity = (float) button.selected;
                            else hotspotMarker.opacity = 0.0;
                        }
                        
                        break;
                    }
                    case ALL_HOTSPOTS:
                    {
                        hotspotMarker.opacity = (float) button.selected;
                        break;
                    }
                        
                    default : break;
                }
            }
        }
    }
    [_mapView setZoom:zomlvl atCoordinate:_mapView.centerCoordinate animated:NO];
    _mapView.centerCoordinate = CLLocationCoordinate2DMake(lat, lon);
}

- (void) hotspotsDisplayingButtonClicked:(UIButton*)sender
{
    float zomlvl = _mapView.zoom;
    float lat = _mapView.centerCoordinate.latitude;
    float lon = _mapView.centerCoordinate.longitude;
    
    [_mapView setZoom:12 atCoordinate:_mapView.centerCoordinate animated:NO];
    
    if([self hotspotsDisplaying] == ALL_HOTSPOTS)
    {
        [self setHotspotsDisplaying:USER_HOTSPOTS_ONLY];
        [PCToastMessage toastWithDuration:2.0 andText:NSLocalizedString(@"new_york_tab_new_york_controller_hotspot_displaying_user_hotspots_only", @"My hotspots only")];
    }
    
    else if([self hotspotsDisplaying] == USER_HOTSPOTS_ONLY)
    {
        [self setHotspotsDisplaying:FAVORITE_HOTSPOTS_ONLY];
        [PCToastMessage toastWithDuration:2.0 andText:NSLocalizedString(@"new_york_tab_new_york_controller_hotspot_displaying_favorite_hotspots_only", @"Favorite hotspots only")];
    }
    
    else if([self hotspotsDisplaying] == FAVORITE_HOTSPOTS_ONLY)
    {
        //[self setHotspotsDisplaying:APP_HOTSPOTS_ONLY];
        //[PCToastMessage toastWithDuration:1.0 andText:@"Hotspots de l'app seulement"];
        
        [self setHotspotsDisplaying:ALL_HOTSPOTS];
        [PCToastMessage toastWithDuration:2.0 andText:NSLocalizedString(@"new_york_tab_new_york_controller_hotspot_displaying_all_hotspots", @"All hotspots")];
        
    }
    
    else if([self hotspotsDisplaying] == APP_HOTSPOTS_ONLY)
    {
        [self setHotspotsDisplaying:ALL_HOTSPOTS];
        [PCToastMessage toastWithDuration:2.0 andText:NSLocalizedString(@"new_york_tab_new_york_controller_hotspot_displaying_all_hotspots", @"All hotspots")];
    }
    [_mapView setZoom:zomlvl atCoordinate:_mapView.centerCoordinate animated:NO];
    _mapView.centerCoordinate = CLLocationCoordinate2DMake(lat, lon);
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Hotspot displaying

- (HotspotsDisplaying) hotspotsDisplaying
{
    NSString* displaying = [[NSUserDefaults standardUserDefaults] stringForKey:@"hotspotsDisplaying"];
    
    if([displaying isEqualToString:@"APP_HOTSPOTS_ONLY"]) return APP_HOTSPOTS_ONLY;
    if([displaying isEqualToString:@"ALL_HOTSPOTS"]) return ALL_HOTSPOTS;
    if([displaying isEqualToString:@"USER_HOTSPOTS_ONLY"]) return USER_HOTSPOTS_ONLY;
    if([displaying isEqualToString:@"FAVORITE_HOTSPOTS_ONLY"]) return FAVORITE_HOTSPOTS_ONLY;
    
    return ALL_HOTSPOTS;
}

- (void) setHotspotsDisplaying:(HotspotsDisplaying) hotspotsDisplaying
{
    switch (hotspotsDisplaying)
    {
        case APP_HOTSPOTS_ONLY :
        {
            [[NSUserDefaults standardUserDefaults] setValue:@"APP_HOTSPOTS_ONLY" forKey:@"hotspotsDisplaying"];
            [_hotspotsDisplayingButton setImage:[UIImage imageNamed:@"hotspots_displaying_app_hotspots_only_55.png"] forState:UIControlStateNormal];
            
            for(RMAnnotation* annotation in _mapView.annotations)
            {
                if([annotation.layer isKindOfClass:[HotspotMarker class]])
                {
                    // Get the hotspot marker and the hotspot :
                    HotspotMarker* hotspotMarker = (HotspotMarker*) annotation.layer;
                    Hotspot* hotspot = hotspotMarker.hotspot;
                    
                    // By default show the hotspot :
                    annotation.layer.opacity = 1.0;
                    
                    // If the marker hotspot type is hidden, don't show the marker :
                    if(![self hotspotTypeIsVisible:hotspot.type]) hotspotMarker.opacity = 0.0;
                    
                    // If the marker hotspot is a user hotspot, don't show the marker :
                    if(hotspotMarker.isUserHotspot) hotspotMarker.opacity = 0.0;
                }
            }
            
            break;
        }
        case ALL_HOTSPOTS :
        {
            [[NSUserDefaults standardUserDefaults] setValue:@"ALL_HOTSPOTS" forKey:@"hotspotsDisplaying"];
            [_hotspotsDisplayingButton setImage:[UIImage imageNamed:@"hotspots_displaying_all_hotspots_55.png"] forState:UIControlStateNormal];
            
            for(RMAnnotation* annotation in _mapView.annotations)
            {
                if([annotation.layer isKindOfClass:[HotspotMarker class]])
                {
                    // Get the hotspot marker and the hotspot :
                    HotspotMarker* hotspotMarker = (HotspotMarker*) annotation.layer;
                    Hotspot* hotspot = hotspotMarker.hotspot;
                    
                    // By default show the hotspot :
                    annotation.layer.opacity = 1.0;
                    
                    // If the marker hotspot type is hidden, don't show the marker :
                    if(![self hotspotTypeIsVisible:hotspot.type]) annotation.layer.opacity = 0.0;
                }
            }
            
            break;
        }
        case USER_HOTSPOTS_ONLY :
        {
            [[NSUserDefaults standardUserDefaults] setValue:@"USER_HOTSPOTS_ONLY" forKey:@"hotspotsDisplaying"];
            [_hotspotsDisplayingButton setImage:[UIImage imageNamed:@"hotspots_displaying_user_hotspots_only_55.png"] forState:UIControlStateNormal];
            
            for(RMAnnotation* annotation in _mapView.annotations)
            {
                if([annotation.layer isKindOfClass:[HotspotMarker class]])
                {
                    // Get the hotspot marker and the hotspot :
                    HotspotMarker* hotspotMarker = (HotspotMarker*) annotation.layer;
                    Hotspot* hotspot = hotspotMarker.hotspot;
                    
                    // By default show the hotspot :
                    annotation.layer.opacity = 1.0;
                    
                    // If the marker hotspot type is hidden, don't show the marker :
                    if(![self hotspotTypeIsVisible:hotspot.type]) annotation.layer.opacity = 0.0;
                    
                    // If the marker hotspot is an app hotspot, don't show the marker :
                    if(!hotspotMarker.isUserHotspot) annotation.layer.opacity = 0.0;
                }
            }
            
            break;
        }
        case FAVORITE_HOTSPOTS_ONLY :
        {
            [[NSUserDefaults standardUserDefaults] setValue:@"FAVORITE_HOTSPOTS_ONLY" forKey:@"hotspotsDisplaying"];
            [_hotspotsDisplayingButton setImage:[UIImage imageNamed:@"hotspots_displaying_favorite_hotspots_only_55.png"] forState:UIControlStateNormal];
            
            for(RMAnnotation* annotation in _mapView.annotations)
            {
                if([annotation.layer isKindOfClass:[HotspotMarker class]])
                {
                    // Get the hotspot marker and the hotspot :
                    HotspotMarker* hotspotMarker = (HotspotMarker*) annotation.layer;
                    Hotspot* hotspot = hotspotMarker.hotspot;
                    
                    // By default show the hotspot :
                    annotation.layer.opacity = 1.0;
                    
                    // If the marker hotspot type is hidden, don't show the marker :
                    if(![self hotspotTypeIsVisible:hotspot.type]) hotspotMarker.opacity = 0.0;
                    
                    // Check if this hotspot is a favorite :
                    if(hotspotMarker.isUserHotspot)
                    {
                        if(![[UserHotspotsDBHandler instance] isHotspotFavorite:hotspot]) hotspotMarker.opacity = 0.0;
                    }
                    else
                    {
                        if(![[AppHotspotsDBHandler instance] isHotspotFavorite:hotspot]) hotspotMarker.opacity = 0.0;
                    }
                }
            }
            
            break;
        }
        default : break;
    }
}

- (BOOL) hotspotTypeIsVisible:(HotspotType)hotspotType
{
    switch (hotspotType)
    {
        case FEEDING : return [[NSUserDefaults standardUserDefaults] boolForKey:@"showFeedingHotspots"];
        case HOUSING : return [[NSUserDefaults standardUserDefaults] boolForKey:@"showHousingHotspots"];
        case SHOPPING : return [[NSUserDefaults standardUserDefaults] boolForKey:@"showShoppingHotspots"];
        case CULTURE : return [[NSUserDefaults standardUserDefaults] boolForKey:@"showCultureHotspots"];
        case PICTURE : return [[NSUserDefaults standardUserDefaults] boolForKey:@"showPictureHotspots"];
        case LEISURE : return [[NSUserDefaults standardUserDefaults] boolForKey:@"showLeisureHotspots"];
        case PASTRY : return [[NSUserDefaults standardUserDefaults] boolForKey:@"showPastryHotspots"];
        default : return NO;
    }
}

- (void) setHotspotType:(HotspotType)hotspotType isVisible:(BOOL)visible
{
    switch (hotspotType)
    {
        case FEEDING : [[NSUserDefaults standardUserDefaults] setBool:visible forKey:@"showFeedingHotspots"]; break;
        case HOUSING : [[NSUserDefaults standardUserDefaults] setBool:visible forKey:@"showHousingHotspots"]; break;
        case SHOPPING : [[NSUserDefaults standardUserDefaults] setBool:visible forKey:@"showShoppingHotspots"]; break;
        case CULTURE : [[NSUserDefaults standardUserDefaults] setBool:visible forKey:@"showCultureHotspots"]; break;
        case PICTURE : [[NSUserDefaults standardUserDefaults] setBool:visible forKey:@"showPictureHotspots"]; break;
        case LEISURE : [[NSUserDefaults standardUserDefaults] setBool:visible forKey:@"showLeisureHotspots"]; break;
        case PASTRY : [[NSUserDefaults standardUserDefaults] setBool:visible forKey:@"showPastryHotspots"]; break;
        default : break;
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Map interraction & delegate

- (void) tapOnAnnotation:(RMAnnotation *)annotation onMap:(RMMapView *)map
{
    if([annotation.layer isKindOfClass:[HotspotMarker class]])
    {
        // Get the marker and the hotspot :
        HotspotMarker* hotspotMarker = (HotspotMarker*) annotation.layer;
        Hotspot* hotspot = hotspotMarker.hotspot;
        
        // Remember the displayed marker :
        _displayedMarker = hotspotMarker;
        
        // Get the hotspot's image :
        UIImage *image = nil;
        if(hotspotMarker.isUserHotspot) image = [[UserHotspotsDBHandler instance] imageOfHotspot:hotspot];
        else image = [[AppHotspotsDBHandler instance] imageOfHotspot:hotspot];
        
        // Check if the hotspot is a favorite or not :
        BOOL isFavorite;
        if(hotspotMarker.isUserHotspot) isFavorite = [[UserHotspotsDBHandler instance] isHotspotFavorite:hotspot];
        else isFavorite = [[AppHotspotsDBHandler instance] isHotspotFavorite:hotspot];
        
        // Create a custom alert :
        UICustomAlertView* alert = [[UICustomAlertView alloc] initWithTitle:hotspot.title message:hotspot.description image:image checked:isFavorite positiveButtonTitle:NSLocalizedString(@"new_york_tab_new_york_controller_hotspot_popup_ok_button", @"OK")];
        alert.delegate = self;
        [alert show];
        
        /*
        // Create an imageView :
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 100, 250, 130)];
        [imageView setImage:image];
        
        // Create and show an alert view :
        UIImageAlertView* alertView = [[UIImageAlertView alloc] initWithTitle:hotspot.title description:hotspot.description Image:image delegate:self];
        alertView.tag = 1;
        [alertView show];*/
        
        
        /*
        if(SYSTEM_VERSION < 7.0)
        {
            // Get some sizes :
            float layoutWidth = self.view.frame.size.width - 72; // CODialog leave 36px on each side of the popup
            
            // Create the dialog :
            CODialog* dialog = [CODialog dialogWithWindow:self.view.window];
            dialog.dialogStyle = CODialogStyleCustomView;
            [dialog addButtonWithTitle:@"OK" target:dialog selector:@selector(hideAnimated:) highlighted:YES];
            
            // Create the scrollView :
            UIScrollView* scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, layoutWidth, 300)];
            
            // Create the title :
            UILabel* title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, layoutWidth, 20)];
            title.text = hotspot.title;
            title.font = [UIFont boldSystemFontOfSize:18.0];
            title.lineBreakMode = NSLineBreakByTruncatingTail;
            title.textAlignment = NSTextAlignmentCenter;
            title.textColor = [UIColor whiteColor];
            title.backgroundColor = [UIColor clearColor];
            title.layer.shadowColor   = [[UIColor blackColor] CGColor];
            title.layer.shadowOffset  = CGSizeMake(0.0f, -1.0f);
            title.layer.shadowOpacity = 1.0f;
            title.layer.shadowRadius  = 0.5f;
            
            // Create the imageView :
            
            // Create an imageView :
            UIImageView* imageView = [[UIImageView alloc] initWithImage:image];
            imageView.frame = CGRectMake((layoutWidth - 250)/2, 30, 250, 130);
            
            // Create the textView :
            UILabel* message = [[UILabel alloc] init];
            message.font = [UIFont systemFontOfSize:14.0];
            message.textColor = [UIColor whiteColor];
            message.backgroundColor = [UIColor clearColor];
            message.lineBreakMode = NSLineBreakByWordWrapping;
            message.numberOfLines = 0;
            message.layer.shadowColor   = [[UIColor blackColor] CGColor];
            message.layer.shadowOffset  = CGSizeMake(0.0f, -1.0f);
            message.layer.shadowOpacity = 1.0f;
            message.layer.shadowRadius  = 0.5f;
            
            message.text = hotspot.description;
            CGSize size = [hotspot.description sizeWithFont:message.font constrainedToSize:CGSizeMake(layoutWidth, 2000) lineBreakMode:message.lineBreakMode];
            
            message.frame = CGRectMake(0, 180, layoutWidth, size.height);
            [message sizeToFit];
            
            scrollView.contentSize = CGSizeMake(layoutWidth, 190 + size.height);
            
            // Add the widgets to the scrollView :
            [scrollView addSubview:title];
            [scrollView addSubview:imageView];
            [scrollView addSubview:message];
            
            // Set the scrollView as the dialog's customView :
            dialog.customView = scrollView;
            
            // Show the dialog :
            [dialog showOrUpdateAnimated:YES];
        }
        else
        {
            UICustomAlertView* alert = [[UICustomAlertView alloc] initWithTitle:hotspot.title message:hotspot.description image:image positiveButtonTitle:@"Ok"];
            
            [alert show];
        }*/
    }
}

- (void) longPressOnAnnotation:(RMAnnotation *)annotation onMap:(RMMapView *)map
{
    if([annotation.layer isKindOfClass:HotspotMarker.class])
    {
        HotspotMarker* hotspotMarker = (HotspotMarker*) annotation.layer;
        
        if(hotspotMarker.isUserHotspot)
        {
            // Save the marker :
            _selectedMarker = hotspotMarker;
    //        _selectedMarker.enableDragging = YES;
            
            // Get the hotspot :
            Hotspot* hotspot = _selectedMarker.hotspot;
            
            // Show an alert view :
            NSString* message = [NSString stringWithFormat:@"\"%@\"", hotspot.title];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"new_york_tab_new_york_controller_long_press_on_marker_popup_title", @"What do you want to do ?") message:message delegate:self cancelButtonTitle:NSLocalizedString(@"new_york_tab_new_york_controller_long_press_on_marker_popup_cancel_button", @"Cancel") otherButtonTitles:NSLocalizedString(@"new_york_tab_new_york_controller_long_press_on_marker_popup_update_hotspot_button", @"Update this hotspot"), NSLocalizedString(@"new_york_tab_new_york_controller_long_press_on_marker_popup_delete_hotspot_button", @"Delete this hotspot"), nil];
            alertView.tag = 2;
            [alertView show];
        }
    }
}

- (void) longPressOnMap:(RMMapView *)map at:(CGPoint)point
{
    // Get the coordinates from the touched point :
    _newHotspotLocation = [_mapView pixelToCoordinate:point];

    // Show an alert view :
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"new_york_tab_new_york_controller_long_press_on_map_popup_title", @"Do you want to add a hotspot here ?") message:@"" delegate:self cancelButtonTitle:NSLocalizedString(@"new_york_tab_new_york_controller_long_press_on_map_popup_cancel_button", @"Cancel") otherButtonTitles:NSLocalizedString(@"new_york_tab_new_york_controller_long_press_on_map_popup_add_button", @"Add"), nil];
    alertView.tag = 3;
    [alertView show];
}

- (void) mapView:(RMMapView *)mapView didChangeUserTrackingMode:(RMUserTrackingMode)mode animated:(BOOL)animated
{
    if(_mapView.userTrackingMode == RMUserTrackingModeNone)
    {
        self.navigationItem.rightBarButtonItem.tintColor = [Colors lightOrange];
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - NewHotspotController & EditHotspotController delegates

- (void) newHotspotController:(NewHotspotController *)newHotspotController didFinishAddingHotspot:(Hotspot*)hotspot
{
    // Add a hotspot marker :
    [self addHotspotMarker:hotspot isUserHotspot:YES];
    
    // Pop back :
    [self.navigationController popViewControllerAnimated:YES];
    
    // If the user hotposts weren't visible, show them :
    if(self.hotspotsDisplaying == APP_HOTSPOTS_ONLY)
    {
        [self setHotspotsDisplaying:ALL_HOTSPOTS];
    }
}

- (void) editHotspotController:(EditHotspotController *)editHotspotController didFinishEditingHotspot:(Hotspot *)hotspot
{
    // Update the marker image :
    [self updateHotspotMarker:_selectedMarker setType:hotspot.type];
    
    // Pop back :
    [self.navigationController popViewControllerAnimated:YES];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Hotspot delete

- (void) deleteHotspot:(Hotspot*)hotspot
{
    // Delete the hotspot from the database :
    if([[UserHotspotsDBHandler instance] deleteHotspotWithId:hotspot.uniqueId])
    {
        // Remove the image :
        [[UserHotspotsDBHandler instance] deleteImageOfHotspot:hotspot];
        
        // Remove the marker :
        [_selectedMarker removeFromSuperlayer];
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - CLLocationManagerDelegate and user tracking handling

- (BOOL) userInMapCoverage
{
    // Get the last user location :
    CLLocationCoordinate2D userLocation = _locationManager.location.coordinate;
    
    // Create a rect with the coverage :
    CGRect coverage = CGRectMake(_topLeftOfCoverage.longitude,
                                 _topLeftOfCoverage.latitude,
                                 _bottomRightOfCoverage.longitude - _topLeftOfCoverage.longitude,
                                 _bottomRightOfCoverage.latitude - _topLeftOfCoverage.latitude);
    
    // Create a point with the location :
    CGPoint userLocationPoint = CGPointMake(userLocation.longitude, userLocation.latitude);
    

    // Return the test result :
    return CGRectContainsPoint(coverage, userLocationPoint);
}

- (BOOL) isTracking
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"trackUserPosition"];
}

- (void) startTracking
{

    if(_locationManager == nil)
    {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        if([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) [_locationManager requestWhenInUseAuthorization];
    }
   
    _mapView.showsUserLocation = YES;
     [_mapView setUserTrackingMode:RMUserTrackingModeFollowWithHeading animated:YES];
   // _mapView.userTrackingMode = RMUserTrackingModeFollowWithHeading;
    self.navigationItem.rightBarButtonItem.tintColor = [Colors darkOrange];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"trackUserPosition"];
}

- (void) stopTracking
{
    _mapView.showsUserLocation = NO;
    _mapView.userTrackingMode = RMUserTrackingModeNone;
    self.navigationItem.rightBarButtonItem.tintColor = nil;
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"trackUserPosition"];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - UIAlertView delegate

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag)
    {
        // Tap on marker alert view :
        case 1 : break;
            
        // Long press on marker alert view :
        case 2 :
        {
            // Edit marker :
            if(buttonIndex == 1)
            {
                // Create a new EditHotspotController :
                EditHotspotController* controller = [[EditHotspotController alloc] initWithHospot:_selectedMarker.hotspot];
                controller.delegate = self;
                
                // Display it :
                [self.navigationController pushViewController:controller animated:YES];
            }
            
            // Delete marker :
            else if(buttonIndex == 2)
            {
                // Ask confirmation :
                NSString* message = [NSString stringWithFormat:NSLocalizedString(@"new_york_tab_new_york_controller_delete_hotspot_confirmation_popup_message", @"Do you really want to drop the hotspot \"%@\" ?"), _selectedMarker.hotspot.title];
                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"new_york_tab_new_york_controller_delete_hotspot_confirmation_popup_title", @"Confirmation") message:message delegate:self cancelButtonTitle:NSLocalizedString(@"new_york_tab_new_york_controller_delete_hotspot_confirmation_popup_cancel_button", @"Cancel") otherButtonTitles:NSLocalizedString(@"new_york_tab_new_york_controller_delete_hotspot_confirmation_popup_delete_button", @"Delete"), nil];
                alertView.tag = 4;
                [alertView show];
            }
            
            break;
        }
            
        // Long press on map alert view :
        case 3 :
        {
            if(buttonIndex == 1)
            {
                // Create a new NewHotspotController :
                NewHotspotController* controller = [[NewHotspotController alloc] initWithLocation:_newHotspotLocation];
                controller.delegate = self;
                
                // Display it :
                [self.navigationController pushViewController:controller animated:YES];
            }
            
            break;
        }
            
            // Hotspot suppression confirmation :
        case 4 :
        {
            if(buttonIndex == 1)
            {
                // Delete the hotspot :
                [self deleteHotspot:_selectedMarker.hotspot];
            }
            
            break;
        }
            
        default : break;
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - UICustomAlertView delegate

- (void) positiveButtonClicked
{
    [self setHotspotsDisplaying:self.hotspotsDisplaying];
    _displayedMarker = nil;
}

- (void) checkedChanged:(BOOL)checked
{
    if(_displayedMarker != nil)
    {
        if(_displayedMarker.isUserHotspot) [[UserHotspotsDBHandler instance] setHotspot:((HotspotMarker*)_displayedMarker).hotspot isFavorite:checked];
        else [[AppHotspotsDBHandler instance] setHotspot:((HotspotMarker*)_displayedMarker).hotspot isFavorite:checked];
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Show the "About" page

- (void) showAboutPage
{
    AboutPageController* controller = [[AboutPageController alloc] init];
    controller.modalTransitionStyle = UIModalTransitionStylePartialCurl;
    [self.navigationController presentModalViewController:controller animated:YES];
}

// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - USearchBar delegate

- (void) showSearchBar
{
    // Compute the new frames :
    CGRect newSearchBarContainerFrame = _searchBarContainer.frame;
    newSearchBarContainerFrame.origin.y = _usableFrame.origin.y + SEARCHBBAR_CONTAINER_HEIGHT;
    
    // Give the focus to the search bar :
    [_searchBar becomeFirstResponder];
    
    // Animate to the new frame :
    [UIView animateWithDuration:0.3 animations:^
    {
        _searchBarContainer.alpha = 1.0;
        _searchBarContainer.frame = newSearchBarContainerFrame;
    }];
}

- (void) searchBarCancelButtonClicked
{
    // Compute the new frames :
    CGRect newSearchBarContainerFrame = _searchBarContainer.frame;
    newSearchBarContainerFrame.origin.y = _usableFrame.origin.y;
    
    // Remove the focus from the search bar :
    [_searchBar resignFirstResponder];
    
    // Animate to the new frame :
    [UIView animateWithDuration:0.3 animations:^
    {
        _searchBarContainer.alpha = 0.0;
        _searchBarContainer.frame = newSearchBarContainerFrame;
    }];
    
    // Remove all the hotspots :
    [_mapView removeAllAnnotations];
    
    // Reload the hotspots :
    [self initHotspots];
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    // Remove the focus from the search bar :
    [searchBar resignFirstResponder];
    
    // Get the searched text :
    NSArray* searchedWords = [searchBar.text componentsSeparatedByString:@" "];
    
    // Remove all the hotspots :
    [_mapView removeAllAnnotations];
    
    // Search in the application hotspots :
    NSArray* appHotspots = [[AppHotspotsDBHandler instance] hotspotsWithSearchedWords:searchedWords];
    
    for(Hotspot* hotspot in appHotspots)
    {
        if([hotspot isKindOfClass:[Hotspot class]])
        {
            [self addHotspotMarker:hotspot isUserHotspot:NO];
        }
    }
    
    // Search in the user hotspots :
    NSArray* userHotspots = [[UserHotspotsDBHandler instance] hotspotsWithSearchedWords:searchedWords];
    
    for(Hotspot* hotspot in userHotspots)
    {
        if([hotspot isKindOfClass:[Hotspot class]])
        {
            [self addHotspotMarker:hotspot isUserHotspot:YES];
        }
    }
}
// ----------------------------------------------------------------------------------------------------


@end







