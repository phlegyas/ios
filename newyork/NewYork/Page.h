//
//  Page.h
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {HTML, PLANNING, CHECKLIST, IMAGE, FULLSCREEN_IMAGE, CURRENCY_CONVERTER, TRANSLATION, TIPS_COMPUTER} PageType;

@interface Page : NSObject
{
    int _uniqueId;
    int _location;
    int _position;
    PageType _type;
    NSString* _content;
}

@property (nonatomic, assign) int uniqueId;
@property (nonatomic, assign) int location;
@property (nonatomic, assign) PageType type;
@property (nonatomic, copy) NSString *content;

- (id)initWithUniqueId:(int)uniqueId location:(int)location type:(PageType)type content:(NSString *)content;
- (NSString*) toString;

@end
