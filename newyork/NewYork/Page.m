//
//  Page.m
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Page.h"
#import "GoodTipsDBHandler.h"

@implementation Page

@synthesize uniqueId = _uniqueId;
@synthesize location = _location;
@synthesize type = _type;
@synthesize content = _content;

- (id)initWithUniqueId:(int)uniqueId location:(int)location type:(PageType)type content:(NSString *)content
{
    if ((self = [super init]))
    {
        self.uniqueId = uniqueId;
        self.location = location;
        self.type = type;
        self.content = content;
    }
    return self;
}

- (void) dealloc
{
    self.content = nil;
}

- (NSString*) toString
{
    return [NSString stringWithFormat:@"PAGE : %d, %d, %@", self.uniqueId, self.location, [GoodTipsDBHandler pageTypeToString:self.type]];
}

@end

