//
//  Planning.h
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Planning : NSObject
{
    int _uniqueId;
    int _page;
    int _position;
    NSString* _title;
}

@property (nonatomic, assign) int uniqueId;
@property (nonatomic, assign) int page;
@property (nonatomic, assign) int position;
@property (nonatomic, copy) NSString *title;

- (id)initWithUniqueId:(int)uniqueId page:(int)page position:(int)position title:(NSString *)title;
- (NSString*) toString;

@end
