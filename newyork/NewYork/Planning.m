//
//  Planning.m
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Planning.h"

@implementation Planning

@synthesize uniqueId = _uniqueId;
@synthesize page = _page;
@synthesize position = _position;
@synthesize title = _title;

- (id)initWithUniqueId:(int)uniqueId page:(int)page position:(int)position title:(NSString *)title
{
    if ((self = [super init]))
    {
        self.uniqueId = uniqueId;
        self.page = page;
        self.position = position;
        self.title = title;
    }
    return self;
}

- (void) dealloc
{
    self.title = nil;
}

- (NSString*) toString
{
    return [NSString stringWithFormat:@"PLANNING : %d, %d, %d, %@", self.uniqueId, self.page, self.position, self.title];
}

@end

