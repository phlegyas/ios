//
//  PlanningPageController.h
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UITopView.h"


@interface PlanningPageController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    CGRect _usableFrame;
    
    int _pageId;
    NSArray* _plannings;
    UIWebView* _webView;
    UITopView* _topView;
    NSArray* _cellsBackgroundColors;
}

- (id)initWithPageId:(int)pageId;

@end
