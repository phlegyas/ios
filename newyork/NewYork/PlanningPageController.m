//
//  BonsPlansController.m
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SystemVersion.h"
#import <QuartzCore/QuartzCore.h>

#import "PlanningPageController.h"
#import "GoodTipsDBHandler.h"
#import "Planning.h"
#import "ImagePageController.h"
#import "Colors.h"

@implementation PlanningPageController


// ----------------------------------------------------------------------------------------------------
#pragma mark - Init functions

- (id) init
{
    self = [super init];
    
    // Init the table at the location 0 :
    if (self) _pageId = 0;
    
    return self;
}

- (id) initWithPageId:(int)pageId
{
    self = [super init];
    
    // Init the table at the requested location :
    if (self) _pageId = pageId;
    
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) loadView
{
    // Call super :
    [super loadView];
    
    // Set the colors :
    UIColor* buttonsTintColor = [Colors lightOrange];
    _cellsBackgroundColors = [[NSArray alloc] initWithObjects:
                              [Colors darkYellow],
                              [Colors lightYellow ],
                              nil];
    
    // Get the content of the requested page :
    Page* page = [[GoodTipsDBHandler instance] pageWithId:_pageId];
    NSString* pageContent = page.content;
    
    // Set the window's title :
    self.title = [[GoodTipsDBHandler instance] locationWithId:page.location].title;
    self.view.backgroundColor = [Colors backgroundColor];
    
    // Get the plannings at the requested page :
    _plannings = [[GoodTipsDBHandler instance] planningsWithPageId:_pageId];
    
    // Create a webView :
    _webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    [_webView loadHTMLString:pageContent baseURL:nil];
    _webView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    _webView.backgroundColor = [UIColor clearColor];
    
    // Handle the scroll speed (to slow by default) :
    UIScrollView *scroll = [_webView.subviews lastObject];
    if ([scroll isKindOfClass:[UIScrollView class]])
    {
        scroll.decelerationRate = UIScrollViewDecelerationRateNormal;
    }
    
    // Add the web views :
    [self.view addSubview:_webView];

    // Add a button to display the plannings list, if any :
    if(_plannings.count > 0)
    {
        // Compute the usable frame :
        int statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
        int toolBarHeight = self.navigationController.toolbar.frame.size.height;
        int tabBarHeight = self.tabBarController.tabBar.frame.size.height;
        
        if(SYSTEM_VERSION < 7.0)
        {
            self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
            _usableFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - toolBarHeight - tabBarHeight);
        }
        else
        {
            _usableFrame = CGRectMake(0, statusBarHeight + toolBarHeight, self.view.frame.size.width, self.view.frame.size.height - statusBarHeight - toolBarHeight - tabBarHeight);
        }
        
        
        // Create the _topView :
        _topView = [[UITopView alloc] initWithHeight:270 superview:self.view marginTop:_usableFrame.origin.y toolBarStyle:UITopViewToolbarStyleOk];
        [_topView setOkButtonTitle:NSLocalizedString(@"good_tips_plannings_page_controller_top_view_close_button", @"Close")];
        _topView.okButton.tintColor = buttonsTintColor;
        _topView.view.backgroundColor = [Colors backgroundColor];
        
        // Create the tableView :
        UITableView* tableView = [[UITableView alloc] initWithFrame:_topView.view.bounds style:UITableViewStyleGrouped];
        tableView.backgroundView = nil;
        tableView.backgroundColor = [UIColor clearColor];
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.01f, 0.01f)];
        
        // Create the button :
        UIBarButtonItem* planningsButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"calendar.png"] style:UIBarButtonItemStyleBordered target:_topView action:@selector(toggleVisibility)];
        if(SYSTEM_VERSION < 7.0) planningsButtonItem.tintColor = buttonsTintColor;
        
        // Add the views :
        self.navigationItem.rightBarButtonItem = planningsButtonItem;
        [_topView.view addSubview:tableView];
        [self.view addSubview:_topView];
    }
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - Screen autorotate

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Table view data source

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_plannings count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get the cell :
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    // Get the planning :
    Planning* planning = [_plannings objectAtIndex:indexPath.row];
    
    // Set the cell's label text, background color, selection style and accessory type :
    cell.textLabel.text = planning.title;
    cell.backgroundColor = [_cellsBackgroundColors objectAtIndex:(indexPath.row % _cellsBackgroundColors.count)];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Table view delegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get the id of the location clicked :
    Planning* planning = (Planning*)[_plannings objectAtIndex:indexPath.row];
    
    // Get the image file path :
    NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSString* planningImgFileName = [NSString stringWithFormat:@"%@/good_tips/plannings/%d_%@.jpg", documentsDirectory, planning.uniqueId, language];
    
    // Create a new ZoomableImageViewController :
    ImagePageController* controller = [[ImagePageController alloc] initWithContenOfFile:planningImgFileName];
    
    // Deselect the row, hide the topView and open the planning image :
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [_topView hide];
    [self.navigationController pushViewController:controller animated:YES];
}
// ----------------------------------------------------------------------------------------------------


@end















