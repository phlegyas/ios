//
//  SynchronisationViewController.h
//  NewYork
//
//  Created by Tim Autin on 07/08/13.
//
//

#import <UIKit/UIKit.h>

@interface SynchronisationViewController : UIViewController
{
    
}

@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UITextView* notLinkedTextView;
@property (weak, nonatomic) IBOutlet UIButton* notLinkedButton;

@property (weak, nonatomic) IBOutlet UITextView* linkedTextView;
@property (weak, nonatomic) IBOutlet UIButton* linkedButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

- (void) didStartConnectingToDropBox;
- (void) didConnectToDropBox;

@end
