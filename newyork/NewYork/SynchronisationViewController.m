//
//  SynchronisationViewController.m
//  NewYork
//
//  Created by Tim Autin on 07/08/13.
//
//

#import <QuartzCore/QuartzCore.h>
#import <Dropbox/Dropbox.h>

#import "SynchronisationViewController.h"
#import "Colors.h"
#import "AppDelegate.h"
#import "Logger.h"


@implementation SynchronisationViewController


// ----------------------------------------------------------------------------------------------------
#pragma mark - Init function

- (id) initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        
    }
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) viewDidLoad
{
    // Call super :
    [super viewDidLoad];
    
    self.containerView.layer.cornerRadius = 5;
    self.containerView.layer.borderWidth = 2;
    self.containerView.layer.borderColor = [[Colors lightYellow] CGColor];
    
    self.containerView.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.containerView.layer.shadowOpacity = 0.8;
    self.containerView.layer.shadowRadius = 3.0;
    self.containerView.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    
    if([AppDelegate sharedDelegate].dropBoxHandler.connectedToDropBox)
    {
        self.linkedTextView.hidden = NO;
        self.linkedButton.hidden = NO;
        
        self.notLinkedTextView.hidden = YES;
        self.notLinkedButton.hidden = YES;
    }
    else
    {
        self.linkedTextView.hidden = YES;
        self.linkedButton.hidden = YES;
        
        self.notLinkedTextView.hidden = NO;
        self.notLinkedButton.hidden = NO;
    }
    
    self.activityIndicator.hidden = YES;
    self.view.backgroundColor = [Colors backgroundColor];
}

- (void) viewDidUnload
{
    [self setContainerView:nil];
    [self setNotLinkedTextView:nil];
    [self setNotLinkedButton:nil];
    [self setActivityIndicator:nil];
    [super viewDidUnload];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - User interraction

- (IBAction) linkToDropBoxButtonClicked
{
    UIAlertView* alert = [[UIAlertView alloc]
                          initWithTitle:@"Quelle version ?"
                          message:@"Si vous avez déjà synchronisé d'autres appareils, souhaitez vous récupérer la version en ligne, ou bien envoyer la version locale de ce téléphone ?"
                          delegate:self
                          cancelButtonTitle:@"C'est ma 1ère synchro"
                          otherButtonTitles:@"Envoyer la version locale", @"Récupérer la version en ligne",nil];
    [alert show];
}

- (IBAction) unlinkFromDropBoxButtonClicked
{
    [[AppDelegate sharedDelegate].dropBoxHandler unlinkFromDropBox];
    
    self.linkedTextView.hidden = YES;
    self.linkedButton.hidden = YES;
    
    self.notLinkedTextView.hidden = NO;
    self.notLinkedButton.hidden = NO;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - UIAlertView delegate

- (void) alertView:(UIAlertView*)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0 : [AppDelegate sharedDelegate].dropBoxHandler.fileVersionToUse = LOCAL; break; // First sync, use the local version
        case 1 : [AppDelegate sharedDelegate].dropBoxHandler.fileVersionToUse = LOCAL; break; // Use the local version
        case 2 : [AppDelegate sharedDelegate].dropBoxHandler.fileVersionToUse = REMOTE; break;// Use the remote version
        default: break;
    }
    
    [[DBAccountManager sharedManager] linkFromController:self];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Update the UI when the DropBoxHandler did connect to DropBox

- (void) didStartConnectingToDropBox
{
    self.linkedTextView.hidden = YES;
    self.linkedButton.hidden = YES;
    
    self.notLinkedTextView.hidden = YES;
    self.notLinkedButton.hidden = YES;
    
    self.activityIndicator.hidden = NO;
    [self.activityIndicator startAnimating];
}

- (void) didConnectToDropBox
{
    Log(self.class, @"didConnectToDropBox");
    
    self.linkedTextView.hidden = NO;
    self.linkedButton.hidden = NO;
    
    self.notLinkedTextView.hidden = YES;
    self.notLinkedButton.hidden = YES;
    
    self.activityIndicator.hidden = YES;
    [self.activityIndicator stopAnimating];
}
// ----------------------------------------------------------------------------------------------------

@end




