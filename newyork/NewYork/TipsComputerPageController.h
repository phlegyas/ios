//
//  TipsComputerPageController.h
//  NewYork
//
//  Created by Tim Autin on 20/08/13.
//
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface TipsComputerPageController : GAITrackedViewController <UITextFieldDelegate>
{
    NSNumberFormatter* _currencyFormatter;
    NSNumberFormatter* _twoDecimalsFormatter;
    CGRect _usableFrame;
    
    float _price;
}
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITextField *tipsTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *taxTipsRateSegmentedControl;

@property (weak, nonatomic) IBOutlet UILabel *taxTipsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxTipsValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
/*
- (IBAction) taxInfosButtonClicked:(id)sender;
- (IBAction) tipsInfosButtonClicked:(id)sender;
*/

- (IBAction)taxInfosButtonClicked:(UIButton *)sender;
- (IBAction)tipsInfosButtonClicked:(UIButton *)sender;

@end
