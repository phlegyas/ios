//
//  TipsComputerPageController.m
//  NewYork
//
//  Created by Tim Autin on 20/08/13.
//
//

#import "SystemVersion.h"
#import <QuartzCore/QuartzCore.h>

#import "TipsComputerPageController.h"
#import "Colors.h"

#define HORIZONTAL_MARGIN 20
#define VERTICAL_MARGIN 20
#define TEXTFIELD_HEIGHT 30


@implementation TipsComputerPageController


// ----------------------------------------------------------------------------------------------------
#pragma mark - Init function

- (id) initWithNibName:(NSString*)name bundle:(NSBundle*)bundle
{
    self = [super initWithNibName:(NSString*)name bundle:bundle];
    if(self != nil) [self customInit];
    return self;
}

- (id) initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    if(self != nil) [self customInit];
    return self;
}

- (id) init
{
    self = [super init];
    if(self != nil) [self customInit];
    return self;
}

- (void) customInit
{
    _price = -1;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) viewDidLoad
{
    // Call super :
    [super viewDidLoad];
    self.screenName = @"Taxes & Pourboires";
    
    // Compute the usable frame :
    int statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    int toolBarHeight = self.navigationController.toolbar.frame.size.height;
    int tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    
    if(SYSTEM_VERSION < 7.0)
    {
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
        _usableFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - toolBarHeight - tabBarHeight);
    }
    else
    {
        _usableFrame = CGRectMake(0, statusBarHeight + toolBarHeight, self.view.frame.size.width, self.view.frame.size.height - statusBarHeight - toolBarHeight - tabBarHeight);
    }
    
    // Init the currency formatter :
    _currencyFormatter = [[NSNumberFormatter alloc] init];
    [_currencyFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    _twoDecimalsFormatter = [[NSNumberFormatter alloc] init];
    [_twoDecimalsFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"fr_FR"]];
    [_twoDecimalsFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [_twoDecimalsFormatter setMaximumFractionDigits:2];
    [_twoDecimalsFormatter setMinimumFractionDigits:2];
    
    // Set the background color :
    self.view.backgroundColor = [Colors backgroundColor];
    
    // Init the tips textfield :
    self.tipsTextField.delegate = self;
    self.tipsTextField.keyboardType = UIKeyboardTypeDecimalPad;
    
    // Create a toolbar with a centered button :
    CGRect inputAccessoryViewFrame = CGRectMake(0, 0, self.tipsTextField.inputView.frame.size.width, 50);
    UIToolbar* toolBar = [[UIToolbar alloc] initWithFrame:inputAccessoryViewFrame];
    UIBarButtonItem* flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"good_tips_tab_tips_computer_page_controller_keyboard_ok_button", @"OK") style:UIBarButtonItemStyleDone target:self action:@selector(resignKeyboardAndGetPrice)];
    [toolBar setItems:[NSArray arrayWithObjects:flexibleSpace, doneButton, flexibleSpace, nil]];
    self.tipsTextField.inputAccessoryView = toolBar;
    
    if(SYSTEM_VERSION >= 7.0)
    {
        // Set the containerView's frame :
        CGRect frame = _usableFrame;
        frame.origin.x += 20;
        frame.origin.y += 20;
        frame.size.width -= 40;
        frame.size.height -= 40;
        self.containerView.frame = frame;
    }
    
    // Add a border and radius to the containerView :
    self.containerView.layer.cornerRadius = 5;
    self.containerView.layer.borderWidth = 2;
    self.containerView.layer.borderColor = [[Colors lightYellow] CGColor];
    
    self.containerView.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.containerView.layer.shadowOpacity = 0.8;
    self.containerView.layer.shadowRadius = 3.0;
    self.containerView.layer.shadowOffset = CGSizeMake(2.0, 2.0);
    
    // Init the tips segmented control :
    if(SYSTEM_VERSION < 7.0)
    {
        [self.taxTipsRateSegmentedControl setTitle:@"15% \ue416" forSegmentAtIndex:1];
        [self.taxTipsRateSegmentedControl setTitle:@"18% \ue056" forSegmentAtIndex:2];
        [self.taxTipsRateSegmentedControl setTitle:@"20% \ue057" forSegmentAtIndex:3];
    }
    else
    {
        UIFont* font = [UIFont systemFontOfSize:12.0];
        [self.taxTipsRateSegmentedControl setImage:[[self imageFromText:NSLocalizedString(@"good_tips_tab_tips_computer_page_controller_tax_choice", @"Tax 8.875%") font:font] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:0];
        [self.taxTipsRateSegmentedControl setImage:[[self imageFromText:@"15% \ue416" font:font] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:1];
        [self.taxTipsRateSegmentedControl setImage:[[self imageFromText:@"18% \ue056" font:font] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:2];
        [self.taxTipsRateSegmentedControl setImage:[[self imageFromText:@"20% \ue057" font:font] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forSegmentAtIndex:3];
    }
    
    self.taxTipsRateSegmentedControl.selectedSegmentIndex = 2;
    [self.taxTipsRateSegmentedControl addTarget:self action:@selector(taxTipsRateSegmentedControlValueChanged) forControlEvents:UIControlEventValueChanged];
    [self.taxTipsRateSegmentedControl setWidth:80 forSegmentAtIndex:0];
    //[self.taxTipsRateSegmentedControl sizeToFit];
    
    // Add a border bottom to the quantityLabel :
    CALayer* additionBar = [CALayer layer];
    additionBar.frame = CGRectMake(self.taxTipsValueLabel.frame.origin.x, self.taxTipsValueLabel.frame.origin.y + self.taxTipsValueLabel.frame.size.height + 5, self.taxTipsValueLabel.frame.size.width, 1.0f);
    additionBar.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
    [((UIView*)self.containerView.subviews[0]).layer addSublayer:additionBar];

    // Dismiss the keyboard on click outside (only on iOS6+ because in iOS5- it will prevent the buttons IBActions to be triggered...) :
    if(SYSTEM_VERSION >= 6.0)
    {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignKeyboardAndGetPrice)];
        [self.view addGestureRecognizer:tap];
    }
}

- (void) viewDidUnload
{
    [self setTipsTextField:nil];
    [self setTaxTipsRateSegmentedControl:nil];
    [self setTotalLabel:nil];
    [self setContainerView:nil];
    [self setTaxTipsTitleLabel:nil];
    [self setTaxTipsValueLabel:nil];
    [super viewDidUnload];
}

-(UIImage *) imageFromText:(NSString *)text font:(UIFont*)font
{
    // set the size
    CGSize size  = [text sizeWithFont:font];
    
    // check if UIGraphicsBeginImageContextWithOptions is available (iOS is 4.0+)
    if (UIGraphicsBeginImageContextWithOptions != NULL)
        UIGraphicsBeginImageContextWithOptions(size,NO,0.0);
    
    // iOS is < 4.0 :
    else UIGraphicsBeginImageContext(size);
    
    // optional: add a shadow, to avoid clipping the shadow you should make the context size bigger
    //
    // CGContextRef ctx = UIGraphicsGetCurrentContext();
    // CGContextSetShadowWithColor(ctx, CGSizeMake(1.0, 1.0), 5.0, [[UIColor grayColor] CGColor]);
    
    // draw in context, you can use also drawInRect:withFont:
    [text drawAtPoint:CGPointMake(0.0, 0.0) withFont:font];
    
    // transfer image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Update the values :

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    // Set the textfield's text as the _bidValue (without the €) :
    if(![self.tipsTextField.text isEqualToString:@""])
    {
        self.tipsTextField.text = [_twoDecimalsFormatter stringFromNumber:[NSNumber numberWithFloat:_price]];
    }
}

- (void) taxTipsRateSegmentedControlValueChanged
{
    switch (self.taxTipsRateSegmentedControl.selectedSegmentIndex)
    {
        case 0 :
        {
            self.taxTipsTitleLabel.text = NSLocalizedString(@"good_tips_tab_tips_computer_page_controller_tax_tips_title_label_tax", @"Tax :");
            break;
        }
        case 1 :
        case 2 :
        {
            self.taxTipsTitleLabel.text = NSLocalizedString(@"good_tips_tab_tips_computer_page_controller_tax_tips_title_label_tips", @"Tips :");
            break;
        }
    }
    
    // If the keyboard is not show, update the values :
    if(!self.tipsTextField.isFirstResponder && _price != -1) [self updateValues];
}

- (void) resignKeyboardAndGetPrice
{
    // If the keyboard is not show, return :
    if(!self.tipsTextField.isFirstResponder) return;
    
    // Close the keyboard :
    [self.tipsTextField resignFirstResponder];
    
    // Ensure that the text field is not empty :
    if([self.tipsTextField.text isEqualToString:@""])
    {
        _price = -1;
    }
    else
    {
        // Try to get the text as float :
        _price = [[self.tipsTextField.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
        _price = [[_twoDecimalsFormatter numberFromString:[_twoDecimalsFormatter stringFromNumber:[NSNumber numberWithFloat:_price]]] floatValue];
    }
    
    // Update the GUI :
    [self updateValues];
}

- (void) updateValues
{
    // Ensure that the text field is not empty :
    if(_price == -1)
    {
        self.taxTipsValueLabel.text = @"-";
        self.totalLabel.text = @"-";
        return;
    }
    
    // Set the textfield's text :
    self.tipsTextField.text = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:_price]];
    
    // Update the tips label value :
    float taxTipsRate = 0.08875f;
    if(self.taxTipsRateSegmentedControl.selectedSegmentIndex == 1) taxTipsRate = 0.15f;
    else if(self.taxTipsRateSegmentedControl.selectedSegmentIndex == 2) taxTipsRate = 0.18f;
    else if(self.taxTipsRateSegmentedControl.selectedSegmentIndex == 3) taxTipsRate = 0.2f;
    
    // Update the tax / tips label value :
    float taxTipsValue = _price * taxTipsRate;
    self.taxTipsValueLabel.text = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:taxTipsValue]];
    
    // Update the total label value :
    float total = _price + taxTipsValue;
    self.totalLabel.text = [_currencyFormatter stringFromNumber:[NSNumber numberWithFloat:total]];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Button clicked :

- (IBAction) taxInfosButtonClicked:(id)sender
{
    // Show an alert view :
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"good_tips_tab_tips_computer_page_controller_taxes_infos_popup_title", @"Taxes infos") message:NSLocalizedString(@"good_tips_tab_tips_computer_page_controller_taxes_infos_popup_message", @"...") delegate:self cancelButtonTitle:NSLocalizedString(@"good_tips_tab_tips_computer_page_controller_infos_popup_ok_button", @"OK") otherButtonTitles: nil];
    [alertView show];
}

- (IBAction) tipsInfosButtonClicked:(id)sender
{
    // Show an alert view :
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"good_tips_tab_tips_computer_page_controller_tips_infos_popup_title", @"Tips infos") message:NSLocalizedString(@"good_tips_tab_tips_computer_page_controller_tips_infos_popup_message", @"...") delegate:self cancelButtonTitle:NSLocalizedString(@"good_tips_tab_tips_computer_page_controller_infos_popup_ok_button", @"OK") otherButtonTitles: nil];
    [alertView show];
}
// ----------------------------------------------------------------------------------------------------

@end




