//
//  Translation.h
//  NewYork
//
//  Created by Tim Autin on 20/08/13.
//
//

#import <Foundation/Foundation.h>

@interface Translation : NSObject
{
    
}

@property (nonatomic, assign) int uniqueId;
@property (nonatomic, assign) int page;
@property (nonatomic, assign) int position;
@property (nonatomic, copy) NSString* frenchSentence;
@property (nonatomic, copy) NSString* englishSentence;

- (id) initWithUniqueId:(int)uniqueId page:(int)page position:(int)position frenchSentence:(NSString *)frenchSentence englishSentence:(NSString *)englishSentence;
- (NSString*) toString;

@end
