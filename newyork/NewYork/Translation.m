//
//  Translation.m
//  NewYork
//
//  Created by Tim Autin on 20/08/13.
//
//

#import "Translation.h"

@implementation Translation

- (id) initWithUniqueId:(int)uniqueId page:(int)page position:(int)position frenchSentence:(NSString *)frenchSentence englishSentence:(NSString *)englishSentence;
{
    if ((self = [super init]))
    {
        self.uniqueId = uniqueId;
        self.page = page;
        self.position = position;
        self.frenchSentence = frenchSentence;
        self.englishSentence = englishSentence;
    }
    
    return self;
}

- (void) dealloc
{
    self.frenchSentence = nil;
    self.englishSentence = nil;
}

- (NSString*) toString
{
    return [NSString stringWithFormat:@"TRANSLATION : %d, %d, %d, %@, %@", self.uniqueId, self.page, self.position, self.frenchSentence, self.englishSentence];
}

@end
