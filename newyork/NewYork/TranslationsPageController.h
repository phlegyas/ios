//
//  TranslationsPageController.h
//  NewYork
//
//  Created by Tim Autin on 20/08/13.
//
//

#import <UIKit/UIKit.h>

@interface TranslationsPageController : UIViewController
{
    int _pageId;
    CGRect _usableFrame;
}

- (id) initWithPageId:(int)pageId;

@end
