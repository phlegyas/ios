//
//  TranslationsPageController.m
//  NewYork
//
//  Created by Tim Autin on 20/08/13.
//
//

#import "SystemVersion.h"
#import <QuartzCore/QuartzCore.h>

#import "TranslationsPageController.h"
#import "GoodTipsDBHandler.h"
#import "Colors.h"

#define HORIZONTAL_MARGIN 20
#define VERTICAL_MARGIN 20

@implementation TranslationsPageController


// ----------------------------------------------------------------------------------------------------
#pragma mark - Init function

- (id) initWithPageId:(int)pageId
{
    self = [super init];
    
    // Init the table at the requested location :
    if (self)
    {
        _pageId = pageId;
    }
    
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) loadView
{
    // Call super :
    [super loadView];
    
    // Compute the usable frame :
    int toolBarHeight = self.navigationController.toolbar.frame.size.height;
    int tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    
    if(SYSTEM_VERSION < 7.0)
    {
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
        _usableFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - toolBarHeight - tabBarHeight);
    }
    else
    {
        _usableFrame = self.view.frame;
    }
    
    // Set the background color :
    self.view.backgroundColor = [Colors backgroundColor];
    
    // Get the content of the requested page :
    Page* page = [[GoodTipsDBHandler instance] pageWithId:_pageId];
    
    // Set the window's title :
    NSString* locationTitle = [[GoodTipsDBHandler instance] locationWithId:page.location].title;
    
    // Set the view's title :
    self.title = locationTitle;
    
    // Init the containers :
    UIScrollView* scrollView = [[UIScrollView alloc] initWithFrame:_usableFrame];
    
    // Get this page's translations :
    NSArray* translations = [[GoodTipsDBHandler instance] translationsWithPageId:_pageId];
    int y = 20;
    for(Translation* translation in translations)
    {
        // French sentence :
        UIView* frenchSentenceBackground = [[UIView alloc] initWithFrame:CGRectMake(40, y, _usableFrame.size.width - 60, 0)];
        frenchSentenceBackground.backgroundColor =  [UIColor lightGrayColor];
        
        UITextView* frenchSentence = [[UITextView alloc] initWithFrame:CGRectMake(20, 0, frenchSentenceBackground.frame.size.width - 20, 0)];
        frenchSentence.text = translation.frenchSentence;
        frenchSentence.textColor = [UIColor whiteColor];
        frenchSentence.font = [UIFont boldSystemFontOfSize:14];
        frenchSentence.backgroundColor = [UIColor lightGrayColor];
        frenchSentence.userInteractionEnabled = NO;
        frenchSentence.scrollEnabled = NO;
        frenchSentence.contentOffset = CGPointMake(30, 0);
        
        [frenchSentence sizeToFit];
        [frenchSentence layoutIfNeeded];

        if(SYSTEM_VERSION < 7)
        {
            frenchSentence.frame = CGRectMake(frenchSentence.frame.origin.x, frenchSentence.frame.origin.y, frenchSentence.frame.size.width, frenchSentence.contentSize.height);
        }
        
        CGRect frenchSentenceBackgroundFrame = frenchSentenceBackground.frame;
        frenchSentenceBackgroundFrame.size.height = frenchSentence.frame.size.height;
        frenchSentenceBackground.frame = frenchSentenceBackgroundFrame;
        
        [frenchSentenceBackground addSubview:frenchSentence];
        [scrollView addSubview:frenchSentenceBackground];
        
        // English sentence :
        UIView* englishSentenceBackground = [[UIView alloc] initWithFrame:CGRectMake(40, y + frenchSentenceBackground.frame.size.height + 5, _usableFrame.size.width - 60, 0)];
        englishSentenceBackground.backgroundColor =  [Colors darkYellow];
        
        UITextView* englishSentence = [[UITextView alloc] initWithFrame:CGRectMake(20, 0, frenchSentenceBackground.frame.size.width - 20, 0)];
        englishSentence.text = translation.englishSentence;
        englishSentence.textColor = [UIColor whiteColor];
        englishSentence.font = [UIFont boldSystemFontOfSize:14];
        englishSentence.backgroundColor = [Colors darkYellow];
        englishSentence.userInteractionEnabled = NO;
        englishSentence.scrollEnabled = NO;
        englishSentence.contentOffset = CGPointMake(30, 0);
        
        [englishSentence sizeToFit];
        [englishSentence layoutIfNeeded];
            
        if(SYSTEM_VERSION < 7)
        {
            englishSentence.frame = CGRectMake(englishSentence.frame.origin.x, englishSentence.frame.origin.y, englishSentence.frame.size.width, englishSentence.contentSize.height);
        }
        
        CGRect englishSentenceBackgroundFrame = englishSentenceBackground.frame;
        englishSentenceBackgroundFrame.size.height = englishSentence.frame.size.height;
        englishSentenceBackground.frame = englishSentenceBackgroundFrame;
        
        [englishSentenceBackground addSubview:englishSentence];
        [scrollView addSubview:englishSentenceBackground];
        
        // Flags :
        float flagsSize = 38.0f;
        
        // local flag :
        NSString* language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
        NSString *namefile = [NSString stringWithFormat: @"%@_flag",[language uppercaseString]];
        UIImageView* localFlag = [[UIImageView alloc] initWithImage:[UIImage imageNamed:namefile]];
        localFlag.frame = CGRectMake(40 - flagsSize/2, y - (flagsSize - 33)/2, flagsSize, flagsSize);
        [scrollView addSubview:localFlag];
        
        // USA flag :
        UIImageView* usaFlag = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"USA_flag"]];
        usaFlag.frame = CGRectMake(40 - flagsSize/2, englishSentenceBackground.frame.origin.y - (flagsSize - 33)/2, flagsSize, flagsSize);
        
        [scrollView addSubview:usaFlag];
        
        // Increment y :
        y += frenchSentenceBackground.frame.size.height + 5 + englishSentenceBackground.frame.size.height + 20;
    }
    
    scrollView.contentSize = CGSizeMake(_usableFrame.size.width - 60, y);
    
    [self.view addSubview:scrollView];
}
// ----------------------------------------------------------------------------------------------------

@end



