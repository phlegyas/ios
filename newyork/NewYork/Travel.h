//
//  Travel.h
//  NewYork
//
//  Created by Tim Autin on 20/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Travel : NSObject
{
    int _uniqueId;
    NSString* _title;
    NSDate* _beginDate;
    NSDate* _endDate;
    BOOL _isCurrentTravel;
}

@property (nonatomic, assign) int uniqueId;
@property (nonatomic, copy) NSString* title;
@property (nonatomic, copy) NSDate* beginDate;
@property (nonatomic, copy) NSDate* endDate;
@property (nonatomic, assign) BOOL isCurrentTravel;

- (id)initWithUniqueId:(int)uniqueId title:(NSString*)title beginDate:(NSDate*)beginDate endDate:(NSDate *)endDate isCurrentTravel:(BOOL)isCurrentTravel;
- (NSString*) toString;

@end
