//
//  Travel.m
//  NewYork
//
//  Created by Tim Autin on 20/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Travel.h"

@implementation Travel

@synthesize uniqueId = _uniqueId;
@synthesize title = _title;
@synthesize beginDate = _beginDate;
@synthesize endDate = _endDate;
@synthesize isCurrentTravel = _isCurrentTravel;

- (id)initWithUniqueId:(int)uniqueId title:(NSString*)title beginDate:(NSDate*)beginDate endDate:(NSDate *)endDate isCurrentTravel:(BOOL)isCurrentTravel
{
    if ((self = [super init]))
    {
        self.uniqueId = uniqueId;
        self.title = title;
        self.beginDate = beginDate;
        self.endDate = endDate;
        self.isCurrentTravel = isCurrentTravel;
    }
    return self;
}

- (void) dealloc
{
    self.title = nil;
    self.beginDate = nil;
    self.beginDate = nil;
    self.endDate = nil;
}

- (NSString*) toString
{
    return [NSString stringWithFormat:@"TRAVEL : %d, %@, %@, %@, %d", self.uniqueId, self.title, self.beginDate, self.endDate, self.isCurrentTravel];
}

@end
