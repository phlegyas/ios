//
//  UICheckBox.h
//  NewYork
//
//  Created by Tim Autin on 18/01/2014.
//
//

#import <UIKit/UIKit.h>

@protocol UICheckBoxDelegate <NSObject>
@optional
- (void) checkedChanged:(BOOL)checked;
@end

@interface UICheckBox : UIView
{
    UIImageView* _imageView;
}

@property (nonatomic, weak) id <UICheckBoxDelegate> delegate;
@property (nonatomic, assign) BOOL checked;

@end
