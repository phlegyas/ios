//
//  UICheckBox.m
//  NewYork
//
//  Created by Tim Autin on 18/01/2014.
//
//

#import "UICheckBox.h"

@implementation UICheckBox

- (id) initWithFrame:(CGRect)frame
{
    return [self initWithFrame:frame checked:NO];
}

- (id) initWithFrame:(CGRect)frame checked:(BOOL)checked
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touch:)];
        singleTap.numberOfTapsRequired = 1;
        singleTap.numberOfTouchesRequired = 1;
        [_imageView addGestureRecognizer:singleTap];
        [_imageView setUserInteractionEnabled:YES];
        
        [self addSubview:_imageView];
        
        self.checked = checked;
    }
    return self;
}

- (void) touch:(UIGestureRecognizer *)gestureRecognizer {
    
    self.checked = !self.checked;
    
    if([self.delegate respondsToSelector:@selector(checkedChanged:)]) [self.delegate checkedChanged:self.checked];
}

- (void) setChecked:(BOOL)checked
{
    _checked = checked;
    self.checked ? [_imageView setImage:[UIImage imageNamed:@"star_checked"]] : [_imageView setImage:[UIImage imageNamed:@"star_unchecked"]];
}


@end




