//
//  UICustomAlertView.h
//  UICustomAlertView
//
//  Created by Tim Autin on 30/09/13.
//  Copyright (c) 2013 Tim Autin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UICheckBox.h"

@protocol UICustomAlertViewDelegate <NSObject>
@optional
- (void) positiveButtonClicked;
- (void) negativeButtonClicked;
- (void) checkedChanged:(BOOL)checked;
@end


@interface UICustomAlertView : UIView <UICheckBoxDelegate>
{
    UIView* _backgroundLayer;
    
    UIButton* _positiveButton;
    UIButton* _negativeButton;
}

@property (nonatomic, weak) id <UICustomAlertViewDelegate> delegate;

- (id) initWithContentView:(UIView*)contentView positiveButtonTitle:(NSString*)positiveButtonTitle;
- (id) initWithContentView:(UIView*)contentView positiveButtonTitle:(NSString*)positiveButtonTitle negativeButtonTitle:(NSString*)negativeButtonTitle;

- (id) initWithTitle:(NSString *)title message:(NSString *)message positiveButtonTitle:(NSString*)positiveButtonTitle;
- (id) initWithTitle:(NSString *)title message:(NSString *)message positiveButtonTitle:(NSString*)positiveButtonTitle negativeButtonTitle:(NSString*)negativeButtonTitle;

- (id) initWithTitle:(NSString *)title message:(NSString *)message image:(UIImage*)image positiveButtonTitle:(NSString *)positiveButtonTitle;
- (id) initWithTitle:(NSString *)title message:(NSString *)message image:(UIImage*)image positiveButtonTitle:(NSString *)positiveButtonTitle negativeButtonTitle:(NSString*)negativeButtonTitle;

- (id) initWithTitle:(NSString *)title message:(NSString *)message image:(UIImage*)image checked:(BOOL)checked positiveButtonTitle:(NSString *)positiveButtonTitle;
- (id) initWithTitle:(NSString *)title message:(NSString *)message image:(UIImage*)image checked:(BOOL)checked positiveButtonTitle:(NSString *)positiveButtonTitle negativeButtonTitle:(NSString*)negativeButtonTitle;

- (void) show;

+ (CGRect) sizeOfText:(NSString*)text withFont:(UIFont*)font andWidth:(float)width;

@end
