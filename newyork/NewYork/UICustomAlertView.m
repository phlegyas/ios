//
//  UICustomAlertView.m
//  UICustomAlertView
//
//  Created by Tim Autin on 30/09/13.
//  Copyright (c) 2013 Tim Autin. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "UICustomAlertView.h"
#import "UICheckBox.h"

// Left and right margins :
#define UI_CUSTOM_ALERT_VIEW_WIDTH 280

// Left and right padding :
#define UI_CUSTOM_ALERT_VIEW_HORIZONTAL_PADDING 10
#define UI_CUSTOM_ALERT_VIEW_VERTICAL_PADDING 10

// Vertical margin (between widgets) :
#define UI_CUSTOM_ALERT_VIEW_VERTICAL_MARGIN 10

// Widgets sizes :
#define UI_CUSTOM_ALERT_VIEW_TITLE_LABEL_HEIGHT 25
#define UI_CUSTOM_ALERT_VIEW_BUTTONS_HEIGHT 40
#define UI_CUSTOM_ALERT_VIEW_CONTENT_VIEW_MAX_HEIGHT ([[UIScreen mainScreen] bounds].size.height - 120)

@implementation UICustomAlertView


// ----------------------------------------------------------------------------------------------------
// Init methods :



// Custom content view alert init methods :
- (id) initWithContentView:(UIView*)contentView positiveButtonTitle:(NSString*)positiveButtonTitle
{
    return [self initWithContentView:contentView positiveButtonTitle:positiveButtonTitle negativeButtonTitle:nil];
}

- (id) initWithContentView:(UIView*)contentView positiveButtonTitle:(NSString*)positiveButtonTitle negativeButtonTitle:(NSString*)negativeButtonTitle
{
    // Init the background layer :
    _backgroundLayer = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    _backgroundLayer.opaque = NO;
    _backgroundLayer.alpha = 0.0f;
    _backgroundLayer.backgroundColor = [UIColor blackColor];
    
    // Check if the content view height is not to high :
    if(contentView.frame.size.height > UI_CUSTOM_ALERT_VIEW_CONTENT_VIEW_MAX_HEIGHT)
    {
        // Create a scrollview :
        CGRect scrollViewFrame = contentView.frame;
        scrollViewFrame.origin.x = UI_CUSTOM_ALERT_VIEW_HORIZONTAL_PADDING;
        scrollViewFrame.origin.y = UI_CUSTOM_ALERT_VIEW_VERTICAL_PADDING;
        scrollViewFrame.size.height = UI_CUSTOM_ALERT_VIEW_CONTENT_VIEW_MAX_HEIGHT;
        UIScrollView* scrollView = [[UIScrollView alloc] initWithFrame:scrollViewFrame];
        scrollView.contentSize = contentView.frame.size;
        
        // Add the content view to the scrollview :
        [scrollView addSubview:contentView];
        
        contentView = scrollView;
    }
    else
    {
        // Add padding to the content view and add it :
        CGRect frame = contentView.frame;
        frame.origin.x = UI_CUSTOM_ALERT_VIEW_HORIZONTAL_PADDING;
        frame.origin.y = UI_CUSTOM_ALERT_VIEW_VERTICAL_PADDING;
        contentView.frame = frame;
    }
    
    // Compute the alert frame :
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    CGRect alertViewFrame = CGRectZero;
    alertViewFrame.size.width = UI_CUSTOM_ALERT_VIEW_WIDTH;
    alertViewFrame.size.height = UI_CUSTOM_ALERT_VIEW_VERTICAL_PADDING + contentView.frame.size.height + UI_CUSTOM_ALERT_VIEW_VERTICAL_MARGIN + UI_CUSTOM_ALERT_VIEW_BUTTONS_HEIGHT;
    alertViewFrame.origin.x = screenBounds.size.width/2 - UI_CUSTOM_ALERT_VIEW_WIDTH/2;
    alertViewFrame.origin.y = screenBounds.size.height/2 - alertViewFrame.size.height/2;
    
    // Init:
    self = [super initWithFrame:alertViewFrame];
    if (self)
    {
        // Add the content view:
        [self addSubview:contentView];
        
        // Set some style :
        self.backgroundColor = [UIColor whiteColor];
        self.alpha = 0.95f;
        
        self.layer.cornerRadius = 5.0f;
        
        self.layer.shadowColor = [[UIColor blackColor] CGColor];
        self.layer.shadowOpacity = 0.8;
        self.layer.shadowRadius = 10.0;
        self.layer.shadowOffset = CGSizeMake(2.0, 2.0);
        
        // Add a separator line :
        CALayer* separator = [CALayer layer];
        separator.frame = CGRectMake(UI_CUSTOM_ALERT_VIEW_HORIZONTAL_PADDING, contentView.frame.origin.y + contentView.frame.size.height + UI_CUSTOM_ALERT_VIEW_VERTICAL_MARGIN, alertViewFrame.size.width - 2*UI_CUSTOM_ALERT_VIEW_HORIZONTAL_PADDING, 1);
        separator.backgroundColor = [UIColor lightGrayColor].CGColor;
        [self.layer addSublayer:separator];
        
        // Add the button(s) :
        float buttonWidth = negativeButtonTitle == nil ? self.frame.size.width - 2*UI_CUSTOM_ALERT_VIEW_HORIZONTAL_PADDING : (self.frame.size.width - 2*UI_CUSTOM_ALERT_VIEW_HORIZONTAL_PADDING) / 2;
        UIColor* buttonsTitleColor = [UIColor colorWithRed:0/255.0 green:122/255.0 blue:255/255.0 alpha:1.0];
        
        _positiveButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _positiveButton.frame = CGRectMake(UI_CUSTOM_ALERT_VIEW_HORIZONTAL_PADDING, contentView.frame.origin.y + contentView.frame.size.height + UI_CUSTOM_ALERT_VIEW_VERTICAL_MARGIN, buttonWidth, UI_CUSTOM_ALERT_VIEW_BUTTONS_HEIGHT);
        [_positiveButton setTitle:positiveButtonTitle forState:UIControlStateNormal];
        [_positiveButton setTitleColor:buttonsTitleColor forState:UIControlStateNormal];
        [_positiveButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
        [_positiveButton addTarget:self action:@selector(positiveButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:_positiveButton];
        
        if(negativeButtonTitle != nil)
        {
            // Add a separator line :
            CALayer* separator = [CALayer layer];
            separator.frame = CGRectMake(_positiveButton.frame.origin.x + _positiveButton.frame.size.width, contentView.frame.origin.y + contentView.frame.size.height + UI_CUSTOM_ALERT_VIEW_VERTICAL_MARGIN, 1, UI_CUSTOM_ALERT_VIEW_BUTTONS_HEIGHT - UI_CUSTOM_ALERT_VIEW_VERTICAL_MARGIN);
            separator.backgroundColor = [UIColor lightGrayColor].CGColor;
            [self.layer addSublayer:separator];
            
            // Add the button :
            _negativeButton = [UIButton buttonWithType:UIButtonTypeCustom];
            _negativeButton.frame = CGRectMake(UI_CUSTOM_ALERT_VIEW_HORIZONTAL_PADDING + buttonWidth, contentView.frame.origin.y + contentView.frame.size.height + UI_CUSTOM_ALERT_VIEW_VERTICAL_MARGIN, buttonWidth, UI_CUSTOM_ALERT_VIEW_BUTTONS_HEIGHT);
            [_negativeButton setTitle:negativeButtonTitle forState:UIControlStateNormal];
            [_negativeButton setTitleColor:buttonsTitleColor forState:UIControlStateNormal];
            [_negativeButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
            [_negativeButton addTarget:self action:@selector(negativeButtonClicked) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:_negativeButton];
        }
    }
    return self;
}



// Title + Message alert init methods :
- (id) initWithTitle:(NSString *)title message:(NSString *)message positiveButtonTitle:(NSString*)positiveButtonTitle
{
    return [self initWithTitle:title message:message positiveButtonTitle:positiveButtonTitle negativeButtonTitle:nil];
}

- (id) initWithTitle:(NSString *)title message:(NSString *)message positiveButtonTitle:(NSString*)positiveButtonTitle negativeButtonTitle:(NSString*)negativeButtonTitle
{
    // Compute the widgets width :
    float width = UI_CUSTOM_ALERT_VIEW_WIDTH - 2*UI_CUSTOM_ALERT_VIEW_HORIZONTAL_PADDING;
    
    // Create the title label :
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, UI_CUSTOM_ALERT_VIEW_TITLE_LABEL_HEIGHT)];
    titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    titleLabel.text = title;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.backgroundColor = [UIColor clearColor];
    
    // Create the message label :
    UILabel* messageLabel = [[UILabel alloc] init];
    messageLabel.text = message;
    messageLabel.backgroundColor = [UIColor clearColor];
    messageLabel.userInteractionEnabled = NO;
    messageLabel.numberOfLines = 0;
    messageLabel.font = [UIFont systemFontOfSize:14.0f];
    
    CGRect frame = [UICustomAlertView sizeOfText:message withFont:messageLabel.font andWidth:width];
    frame.origin.y = titleLabel.frame.size.height + UI_CUSTOM_ALERT_VIEW_VERTICAL_MARGIN;
    messageLabel.frame = frame;
    
    // Create a container view :
    UIView* container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, messageLabel.frame.origin.y + messageLabel.frame.size.height)];
    [container addSubview:titleLabel];
    [container addSubview:messageLabel];
    
    return [self initWithContentView:container positiveButtonTitle:positiveButtonTitle negativeButtonTitle:negativeButtonTitle];
}



// Title + Message + Image alert init methods :
- (id) initWithTitle:(NSString *)title message:(NSString *)message image:(UIImage*)image positiveButtonTitle:(NSString *)positiveButtonTitle
{
    return [self initWithTitle:title message:message image:image positiveButtonTitle:positiveButtonTitle negativeButtonTitle:nil];
}

- (id) initWithTitle:(NSString *)title message:(NSString *)message image:(UIImage*)image positiveButtonTitle:(NSString *)positiveButtonTitle negativeButtonTitle:(NSString*)negativeButtonTitle
{
    // Compute the widgets width :
    float width = UI_CUSTOM_ALERT_VIEW_WIDTH - 2*UI_CUSTOM_ALERT_VIEW_HORIZONTAL_PADDING;
    
    // Create the title label :
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, UI_CUSTOM_ALERT_VIEW_TITLE_LABEL_HEIGHT)];
    titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    titleLabel.text = title;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.backgroundColor = [UIColor clearColor];
    
    // Create an image view :
    UIImageView* imageView = [[UIImageView alloc] initWithImage:image];
    CGRect frame = imageView.frame;
    
    if(image.size.width > width)
    {
        // Scale the image :
        float ratio = image.size.width / image.size.height;
        frame.origin.x = 0;
        frame.origin.y = titleLabel.frame.size.height + UI_CUSTOM_ALERT_VIEW_VERTICAL_MARGIN;
        frame.size.width = width;
        frame.size.height = width / ratio;
        imageView.frame = frame;
    }
    else
    {
        // Center the image view :
        frame.origin.x = width/2 - image.size.width/2;
        frame.origin.y = titleLabel.frame.size.height + UI_CUSTOM_ALERT_VIEW_VERTICAL_MARGIN;
        imageView.frame = frame;
    }
    
    // Create the message label :
    UILabel* messageLabel = [[UILabel alloc] init];
    messageLabel.text = message;
    messageLabel.backgroundColor = [UIColor clearColor];
    messageLabel.userInteractionEnabled = NO;
    messageLabel.numberOfLines = 0;
    messageLabel.font = [UIFont systemFontOfSize:14.0f];
    
    frame = [UICustomAlertView sizeOfText:message withFont:messageLabel.font andWidth:width];
    frame.origin.y = imageView.frame.origin.y + imageView.frame.size.height + UI_CUSTOM_ALERT_VIEW_VERTICAL_MARGIN;
    messageLabel.frame = frame;
    
    // Create a container view :
    UIView* container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, messageLabel.frame.origin.y + messageLabel.frame.size.height)];
    [container addSubview:titleLabel];
    [container addSubview:imageView];
    [container addSubview:messageLabel];
    
    return [self initWithContentView:container positiveButtonTitle:positiveButtonTitle negativeButtonTitle:negativeButtonTitle];
}



// Title + Message + Image + CheckBox alert init methods :
- (id) initWithTitle:(NSString *)title message:(NSString *)message image:(UIImage*)image checked:(BOOL)checked positiveButtonTitle:(NSString *)positiveButtonTitle
{
    return [self initWithTitle:title message:message image:image checked:checked positiveButtonTitle:positiveButtonTitle negativeButtonTitle:nil];
}

- (id) initWithTitle:(NSString *)title message:(NSString *)message image:(UIImage*)image checked:(BOOL)checked positiveButtonTitle:(NSString *)positiveButtonTitle negativeButtonTitle:(NSString*)negativeButtonTitle
{
    // Compute the widgets width :
    float width = UI_CUSTOM_ALERT_VIEW_WIDTH - 2*UI_CUSTOM_ALERT_VIEW_HORIZONTAL_PADDING;
    
    // Create the title label :
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width -25, UI_CUSTOM_ALERT_VIEW_TITLE_LABEL_HEIGHT)];
    titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    titleLabel.text = title;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.backgroundColor = [UIColor clearColor];
    
    // Create the checkbox :
    UICheckBox* checkbox = [[UICheckBox alloc] initWithFrame:CGRectMake(width -25, 0, 25, 25)];
    checkbox.checked = checked;
    checkbox.delegate = self;
    
    // Create an image view :
    UIImageView* imageView = [[UIImageView alloc] initWithImage:image];
    CGRect frame = imageView.frame;
    
    if(image.size.width > width)
    {
        // Scale the image :
        float ratio = image.size.width / image.size.height;
        frame.origin.x = 0;
        frame.origin.y = titleLabel.frame.size.height + UI_CUSTOM_ALERT_VIEW_VERTICAL_MARGIN;
        frame.size.width = width;
        frame.size.height = width / ratio;
        imageView.frame = frame;
    }
    else
    {
        // Center the image view :
        frame.origin.x = width/2 - image.size.width/2;
        frame.origin.y = titleLabel.frame.size.height + UI_CUSTOM_ALERT_VIEW_VERTICAL_MARGIN;
        imageView.frame = frame;
    }
    
    // Create the message label :
    UILabel* messageLabel = [[UILabel alloc] init];
    messageLabel.text = message;
    messageLabel.backgroundColor = [UIColor clearColor];
    messageLabel.userInteractionEnabled = NO;
    messageLabel.numberOfLines = 0;
    messageLabel.font = [UIFont systemFontOfSize:14.0f];
    
    frame = [UICustomAlertView sizeOfText:message withFont:messageLabel.font andWidth:width];
    frame.origin.y = imageView.frame.origin.y + imageView.frame.size.height + UI_CUSTOM_ALERT_VIEW_VERTICAL_MARGIN;
    messageLabel.frame = frame;
    
    // Create a container view :
    UIView* container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, messageLabel.frame.origin.y + messageLabel.frame.size.height)];
    [container addSubview:titleLabel];
    [container addSubview:checkbox];
    [container addSubview:imageView];
    [container addSubview:messageLabel];
    
    return [self initWithContentView:container positiveButtonTitle:positiveButtonTitle negativeButtonTitle:negativeButtonTitle];
}


// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
// Setters :

- (void) setButtonsTitleColor:(UIColor*)color forState:(UIControlState)state
{
    [_positiveButton setTitleColor:color forState:state];
    if(_negativeButton != nil) [_negativeButton setTitleColor:color forState:state];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
// Show / hide methods :

- (void) show
{
    // Scale down and set alpha 0 :
    self.transform = CGAffineTransformMakeScale(0.6, 0.6);
    self.alpha = 0.0f;
    
    // Add self and the background layer to the keyWindow :
    [[[UIApplication sharedApplication] keyWindow] addSubview:_backgroundLayer];
    [[[UIApplication sharedApplication] keyWindow] addSubview:self];
    
    
    // Animate to 100% scale :
    [UIView animateWithDuration:0.05 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^
     {
         _backgroundLayer.alpha = 0.4f;
     }
                     completion:^(BOOL finished)
     {
         // Start animating to a scale a little bigger than 100% :
         [UIView animateWithDuration:0.15 delay:0.01 options:UIViewAnimationOptionCurveEaseInOut animations:^
          {
              self.transform = CGAffineTransformMakeScale(1.05, 1.05);
              self.alpha = 1.0f;
          }
                          completion:^(BOOL finished)
          {
              // Animate to 100% scale :
              [UIView animateWithDuration:0.15 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^
               {
                   self.transform = CGAffineTransformMakeScale(0.90, 0.90);
               }
                               completion:^(BOOL finished)
               {
                   // Animate to 100% scale :
                   [UIView animateWithDuration:0.10 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^
                    {
                        self.transform = CGAffineTransformIdentity;
                    }
                                    completion:^(BOOL finished) { }];
               }];
          }];
     }];
}

- (void) hide
{
    // Fade out the alert :
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        self.alpha = 0.0f;
    }
                     completion:^(BOOL finished)
     {
         // Fade out the layer :
         [UIView animateWithDuration:0.05 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^
          {
              _backgroundLayer.alpha = 0.0f;
          }
                          completion:^(BOOL finished)
          {
              // Remove the elements :
              [self removeFromSuperview];
              [_backgroundLayer removeFromSuperview];
          }];
     }];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
// Buttons click :

- (void) positiveButtonClicked
{
    [self hide];
    if([self.delegate respondsToSelector:@selector(positiveButtonClicked)]) [self.delegate positiveButtonClicked];
}

- (void) negativeButtonClicked
{
    [self hide];
    if([self.delegate respondsToSelector:@selector(negativeButtonClicked)]) [self.delegate negativeButtonClicked];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
// Utils :

+ (CGRect) sizeOfText:(NSString*)text withFont:(UIFont*)font andWidth:(float)width
{
    NSDictionary* attributes = @{NSFontAttributeName:font};
    NSAttributedString* attributedText = [[NSAttributedString alloc] initWithString:text attributes:attributes];
    CGRect frame = [attributedText boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    frame.size.height = ceil(frame.size.height);
    frame.size.width = width;
    
    return frame;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
// Checkbox delegate :

- (void) checkedChanged:(BOOL)checked
{
    if([self.delegate respondsToSelector:@selector(checkedChanged:)]) [self.delegate checkedChanged:checked];
}
// ----------------------------------------------------------------------------------------------------

@end
















