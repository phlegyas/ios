//
//  UIDatePickerTextField.h
//  NewYork
//
//  Created by Tim Autin on 23/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDatePickerTextField : UITextField 
{
    UIDatePicker* _datePicker;
    UIDatePicker* _minimumDatePicker;
    UIDatePicker* _maximumDatePicker;
    UIBarButtonItem* _doneButton;
    NSDateFormatter* _dateFormatter;
}

@property(strong) UIDatePicker* datePicker;

- (void) setDoneButtonTitle:(NSString*)title;
- (void) setDoneButtonTitle:(NSString*)title target:(id)target action:(SEL)action;
- (void) setMinimumDatePicker:(UIDatePicker*)datePicker;
- (void) setMaximumDatePicker:(UIDatePicker*)datePicker;
- (NSDate*) date;
- (void) setDatePickerValueAndText:(NSDate*)date;

@end
