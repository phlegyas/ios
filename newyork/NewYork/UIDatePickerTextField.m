//
//  UIDatePickerTextField.m
//  NewYork
//
//  Created by Tim Autin on 23/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIDatePickerTextField.h"

@implementation UIDatePickerTextField

@synthesize datePicker = _datePicker;

// ----------------------------------------------------------------------------------------------------
#pragma mark - Init function

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    if(self)
    {
        // Create a date picker
        _datePicker = [[UIDatePicker alloc] initWithFrame:self.inputView.frame];
        _datePicker.datePickerMode = UIDatePickerModeDate;
        [_datePicker addTarget:self action:@selector(datePickerValueChanged) forControlEvents:UIControlEventValueChanged];
        
        // Init the min and max datepickers to nil :
        _minimumDatePicker = nil;
        _maximumDatePicker = nil;
        
        // Set the datePicker as the textfield's inputView :
        self.inputView = _datePicker;
        
        // Create a toolbar with a centered button :
        CGRect inputAccessoryViewFrame = CGRectMake(0, 0, self.inputView.frame.size.width, 50);
        UIToolbar* toolBar = [[UIToolbar alloc] initWithFrame:inputAccessoryViewFrame];
        
        UIBarButtonItem* flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        
        _doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(resignFirstResponder)];
        
        [toolBar setItems:[NSArray arrayWithObjects:flexibleSpace, _doneButton, flexibleSpace, nil]];
        
        // Set the toolbar as the inputAccexxoryView of the textfield :
        self.inputAccessoryView = toolBar;
        
        // Init the date formatter :
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateFormat:@"dd/MM/YYYY"];
    }
    
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Setters

- (void) setDoneButtonTitle:(NSString*)title
{
    _doneButton.title = title;
}

- (void) setDoneButtonTitle:(NSString*)title target:(id)target action:(SEL)action
{
    _doneButton.title = title;
    _doneButton.target = target;
    _doneButton.action = action;
}

- (void) setMinimumDatePicker:(UIDatePicker *)datePicker
{
    _minimumDatePicker = datePicker;
    _datePicker.minimumDate = _minimumDatePicker.date;
    [_minimumDatePicker addTarget:self action:@selector(minimumDatePickerValueChanged) forControlEvents:UIControlEventValueChanged];
}

- (void) setMaximumDatePicker:(UIDatePicker *)datePicker
{
    _maximumDatePicker = datePicker;
    _datePicker.maximumDate = _maximumDatePicker.date;
    [_maximumDatePicker addTarget:self action:@selector(maximumDatePickerValueChanged) forControlEvents:UIControlEventValueChanged];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Event handling

- (void) datePickerValueChanged
{
    self.text = [_dateFormatter stringFromDate:_datePicker.date];
}

- (void) minimumDatePickerValueChanged
{
    _datePicker.minimumDate = _minimumDatePicker.date;
}

- (void) maximumDatePickerValueChanged
{
    _datePicker.maximumDate = _maximumDatePicker.date;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Getter & Setter

- (NSDate*) date
{
    return _datePicker.date;
}

- (void) setDatePickerValueAndText:(NSDate*)date
{
    _datePicker.date = date;
    self.text = [_dateFormatter stringFromDate:date];
}
// ----------------------------------------------------------------------------------------------------

@end
