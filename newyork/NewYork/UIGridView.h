//
//  UIGridView.h
//  NewYork
//
//  Created by Tim Autin on 14/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIGridViewCell.h"

@protocol UIGridViewDelegate <NSObject>
@optional
- (void) cellTouched:(UIGridViewCell*)cell;
@end



@interface UIGridView : UIView <UIScrollViewDelegate, UIGestureRecognizerDelegate>
{
    __weak id <UIGridViewDelegate> _delegate;
    
    int _columnCount;
    int _rowHeight;
    
    NSMutableArray* _cells;
    
    UIScrollView* _scrollView;
    UIView* _contentView;
    
    UILongPressGestureRecognizer* _tap;
    CGPoint _touchedPoint;
}

@property (nonatomic, weak) id <UIGridViewDelegate> delegate;

- (id)initWithFrame:(CGRect)frame columnCount:(int)columnCount rowHeight:(int)rowHeight;

- (UIGridViewCell*) addEmptyCell;
- (void) addNEmptyCells:(int)n;

- (UIGridViewCell*) cellNumber:(int)number;
- (UIGridViewCell*) cellAtRow:(int)row column:(int)column;

- (void) fadeAllViewsBackgroundColorToClearColorWithDuration:(double)duration delay:(double)delay;

@end
