//
//  UIGridView.m
//  NewYork
//
//  Created by Tim Autin on 14/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIGridView.h"


@implementation UIGridView

@synthesize delegate = _delegate;

// ----------------------------------------------------------------------------------------------------
#pragma mark - Init function

- (id)initWithFrame:(CGRect)frame columnCount:(int)columnCount rowHeight:(int)rowHeight
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        // Save variables :
        _columnCount = columnCount;
        _rowHeight = rowHeight;
        
        // Init the _views as an empty array :
        _cells = [[NSMutableArray alloc] initWithObjects: nil];
        
        // Init the _scrollView :
        _scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
        _scrollView.backgroundColor = [UIColor clearColor];
        
        // Add the scrollview :
        [self addSubview:_scrollView];
    }
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - View management

- (UIGridViewCell*) addEmptyCell
{
    // Compute row and column numbers :
    int row = (int) _cells.count / _columnCount;
    int column = _cells.count % _columnCount;
        
    // Compute the cell's frame :
    CGRect frame = CGRectZero;
    frame.size.width = ((double)_scrollView.bounds.size.width) / ((double)_columnCount);
    frame.size.height = _rowHeight;
    frame.origin.x = column * ((double)_scrollView.bounds.size.width) / ((double)_columnCount);
    frame.origin.y = row * _rowHeight;
    
    // Create the cell :
    UIGridViewCell* cell = [[UIGridViewCell alloc] initWithFrame:frame number:(int)_cells.count row:row column:column];
    
    // Add gesture recognizers to the cell :
    _tap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    _tap.delegate = self;
    _tap.minimumPressDuration = 0.0;
    [cell addGestureRecognizer:_tap];
    
    // Update the scroll view's content size :
    _scrollView.contentSize = CGSizeMake(_scrollView.bounds.size.width, _rowHeight * (row +1) );
    
    // Insert the cell :
    [_cells addObject:cell];
    [_scrollView addSubview:cell];
    
    // Return the cell :
    return cell;
}

- (void) addNEmptyCells:(int)n
{
    // Insert the views :
    for(int i=0; i<n; i++)
    {
        // Create and insert a view :
        [self addEmptyCell];
    }
}

- (UIGridViewCell*) cellNumber:(int)number
{
    if(number < _cells.count)
    {
        return [_cells objectAtIndex:number];
    }
    else return nil;
}

- (UIGridViewCell*) cellAtRow:(int)row column:(int)column
{
    int cellNumber = row * _columnCount + column;

    if(cellNumber < _cells.count)
    {
        return [_cells objectAtIndex:cellNumber];
    }
    else return nil;
}

- (void) fadeAllViewsBackgroundColorToClearColorWithDuration:(double)duration delay:(double)delay
{
    // For each view, fade the background color to clearColor :
    for(int i=0; i<_cells.count; i++)
    {
        [UIView beginAnimations:@"fade out" context:nil];
        [UIView setAnimationDelay:delay];
        [UIView setAnimationDuration:duration];
        ((UIGridViewCell*)[self cellNumber:i]).backgroundColor = [UIColor clearColor];
        [UIView commitAnimations];
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark Tap detecting delegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)handleTap:(UIGestureRecognizer *)sender
{
    switch (sender.state)
    {
        case UIGestureRecognizerStateBegan :
        {
            // Save the position of the touched point :
            _touchedPoint = [sender locationInView:self];
            
            // Fade the background color :
            [UIView beginAnimations:@"fade in" context:nil];
            [UIView setAnimationDuration:0.2];
            sender.view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.2];
            [UIView commitAnimations];
            
            break;
        }
        case UIGestureRecognizerStateChanged :
        {
            // Compute the distance between the point the user touched at the begninning of the gesture and the point touched now :
            CGPoint newTouchedPoint = [sender locationInView:self];
            int distance = (_touchedPoint.x - newTouchedPoint.x)*(_touchedPoint.x - newTouchedPoint.x) + (_touchedPoint.y - newTouchedPoint.y)*(_touchedPoint.y - newTouchedPoint.y);
            
            // If the user has moved enough his fingers :
            if(distance > 300)
            {
                // Cancel the GestureRecognizer :
                sender.enabled = NO;
                sender.enabled = YES;
                
                // For each view, fade the background color to clearColor :
                [self fadeAllViewsBackgroundColorToClearColorWithDuration:0.2 delay:0.1];
            }
            
            break;
        }
        case UIGestureRecognizerStateEnded :
        {
            if (_delegate != nil && [_delegate respondsToSelector:@selector(cellTouched:)])
            {
                [_delegate cellTouched:(UIGridViewCell*)sender.view];
            }
        }
        default: break;
    }
}
// ----------------------------------------------------------------------------------------------------


@end
