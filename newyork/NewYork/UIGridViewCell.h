//
//  UIGridViewCell.h
//  NewYork
//
//  Created by Tim Autin on 30/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIGridViewCell : UIView
{
    int _number;
    int _row;
    int _column;
}

@property (nonatomic, strong) id contentTag;
@property (assign) int number;
@property (assign) int row;
@property (assign) int column;

- (id) initWithFrame:(CGRect)frame number:(int)number row:(int)row column:(int)column;

@end
