//
//  UIGridViewCell.m
//  NewYork
//
//  Created by Tim Autin on 30/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIGridViewCell.h"

@implementation UIGridViewCell

@synthesize number = _number;
@synthesize row = _row;
@synthesize column = _column;


- (id) initWithFrame:(CGRect)frame number:(int)number row:(int)row column:(int)column
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _number = number;
        _row = row;
        _column = column;
    }
    return self;
}

@end
