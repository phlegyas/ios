//
//  UIImageCropController.h
//  NewYork
//
//  Created by Tim Autin on 05/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIZoomableImageView.h"

@class UIImageCropController;

@protocol UIImageCropControllerDelegate <NSObject>
@optional
- (void) imageCropController:(UIImageCropController *)cropController didFinishCroppingImage:(UIImage*)image;
@end

@interface UIImageCropController : UIViewController
{
    __weak id <UIImageCropControllerDelegate> _delegate;
    
    CGRect _usableFrame;
    float _usableFrameRatio;
    CGRect _cropFrame;
    UIImage* _originalImage;
    UIImage* _croppedImage;
    float _ratio;
    UIZoomableImageView* _zoomableImageView;
}

@property (nonatomic, weak) id <UIImageCropControllerDelegate> delegate;
@property (strong) UIBarButtonItem* doneButton;

- (id) initWithImage:(UIImage*)image ratio:(float)ratio;
- (void) addCropFrameLayers;

@end
