//
//  UIImageCropController.m
//  NewYork
//
//  Created by Tim Autin on 05/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SystemVersion.h"
#import "UIImageCropController.h"
#import "Colors.h"


@implementation UIImageCropController

@synthesize delegate = _delegate;
@synthesize doneButton = _doneButton;


// ----------------------------------------------------------------------------------------------------
#pragma mark - Init function

- (id) initWithImage:(UIImage*)image ratio:(float)ratio
{
    self = [super init];
    if (self)
    {
        _originalImage = [self fixOrientation:image];
        _ratio = ratio;
    }
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) viewDidLoad
{
    // Call super :
    [super viewDidLoad];
    
    // Set the title :
    self.navigationItem.title = NSLocalizedString(@"ui_image_crop_controller_title", "Reshape");
    
    // Compute the usable frame :
    /*
    int toolBarHeight = self.navigationController.toolbar.frame.size.height;
    int tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    _usableFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - tabBarHeight - toolBarHeight);
    _usableFrameRatio = _usableFrame.size.width / _usableFrame.size.height;
    */
    
    //int statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    int toolBarHeight = self.navigationController.toolbar.frame.size.height;
    int tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    
    if(SYSTEM_VERSION < 7.0)
    {
        _usableFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - toolBarHeight - tabBarHeight);
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = NO;
        _usableFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - tabBarHeight);
    }
    
    _usableFrameRatio = _usableFrame.size.width / _usableFrame.size.height;
    
    // Compute the crop frame :
    if(_ratio > _usableFrameRatio)
    {
        float y = _usableFrame.origin.y + (_usableFrame.size.height - _usableFrame.size.width / _ratio) / 2.0;
        _cropFrame = CGRectMake(0, y, _usableFrame.size.width, _usableFrame.size.width / _ratio);
    }
    else
    {
        float x = _usableFrame.origin.x + (_usableFrame.size.width - _usableFrame.size.height * _ratio) / 2.0;
        _cropFrame = CGRectMake(x, 0, _usableFrame.size.height * _ratio, _usableFrame.size.height);
    }
    
    /*
    UIView* test = [[UIView alloc] initWithFrame:_usableFrame];
    test.backgroundColor = [UIColor redColor];
    [self.view addSubview:test];
    
    UIView* test2 = [[UIView alloc] initWithFrame:_cropFrame];
    test2.backgroundColor = [UIColor yellowColor];
    [self.view addSubview:test2];
     */
    // Add a background for the crop frame :
    UIView* cropFrameBackground = [[UIView alloc] initWithFrame:_cropFrame];
    cropFrameBackground.backgroundColor = [UIColor darkGrayColor];
    [self.view addSubview:cropFrameBackground];
    
    // Set the background color :
    self.view.backgroundColor = [UIColor blackColor];
    
    // Hide the back button :
    self.navigationItem.hidesBackButton = YES;
    if(SYSTEM_VERSION < 7.0) self.navigationController.navigationBar.translucent = NO;
    
    // Add a "Done" button to the toolbar :
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"ui_image_crop_controller_ok_button", @"OK") style:UIBarButtonItemStyleBordered target:self action:@selector(doneButtonClicked)];
    if(SYSTEM_VERSION < 7.0) doneButton.tintColor = [Colors lightOrange];
    self.navigationItem.rightBarButtonItem = doneButton;
    
    // Add the zoomable image view :
    _zoomableImageView = [[UIZoomableImageView alloc] initWithFrame:_usableFrame image:_originalImage maximumZoomFactor:4.0];
    _zoomableImageView.scrollView.contentInset = UIEdgeInsetsMake( (_usableFrame.size.height - _cropFrame.size.height)/2, (_usableFrame.size.width - _cropFrame.size.width)/2, (_usableFrame.size.height - _cropFrame.size.height)/2, (_usableFrame.size.width - _cropFrame.size.width)/2);
    
    // Center the zoomable image view image :
    CGPoint offset = CGPointMake(0, - _usableFrame.size.height/2.0 + (_zoomableImageView.imageView.image.size.height/2.0)*_zoomableImageView.scrollView.zoomScale );
    _zoomableImageView.scrollView.contentOffset = offset;
    
    [self.view addSubview:_zoomableImageView];
    
    // Add the crop frame layer :
    [self addCropFrameLayers];
}

- (void) addCropFrameLayers
{
    // Create the 2 layers :
    UIView* firstLayer = [[UIView alloc] initWithFrame:CGRectZero];
    firstLayer.backgroundColor = [UIColor blackColor];
    firstLayer.alpha = 0.5;
    firstLayer.opaque = NO;
    firstLayer.userInteractionEnabled = NO;
    
    UIView* secondLayer = [[UIView alloc] initWithFrame:CGRectZero];
    secondLayer.backgroundColor = [UIColor blackColor];
    secondLayer.alpha = 0.5;
    secondLayer.opaque = NO;
    secondLayer.userInteractionEnabled = NO;
    
    // Compute the layers frame :
    if(_ratio > _usableFrameRatio)
    {
        CGSize layersSize = CGSizeMake(_usableFrame.size.width, (_usableFrame.size.height - _cropFrame.size.height)/2.0);
        
        firstLayer.frame = CGRectMake(_usableFrame.origin.x, _usableFrame.origin.y, layersSize.width, layersSize.height);
        secondLayer.frame = CGRectMake(_usableFrame.origin.x, _usableFrame.origin.y + layersSize.height + _cropFrame.size.height, layersSize.width, layersSize.height);
    }
    else
    {
        CGSize layersSize = CGSizeMake((_usableFrame.size.width - _cropFrame.size.width)/2.0, _usableFrame.size.height);
        
        firstLayer.frame = CGRectMake(_usableFrame.origin.x, _usableFrame.origin.y, layersSize.width, layersSize.height);
        secondLayer.frame = CGRectMake(_usableFrame.origin.x + layersSize.width + _cropFrame.size.width, _usableFrame.origin.y, layersSize.width, layersSize.height);
    }
    
    // Add the layers :
    [self.view addSubview:firstLayer];
    [self.view addSubview:secondLayer];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Done button clicked

- (void) doneButtonClicked
{
    CGRect rect = _cropFrame;
    rect.origin.x += _zoomableImageView.scrollView.contentOffset.x;
    rect.origin.y += _zoomableImageView.scrollView.contentOffset.y;
    
    rect.origin.x /= _zoomableImageView.scrollView.zoomScale;
    rect.origin.y /= _zoomableImageView.scrollView.zoomScale;
    rect.size.width /= _zoomableImageView.scrollView.zoomScale;
    rect.size.height /= _zoomableImageView.scrollView.zoomScale;
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([_originalImage CGImage], rect);
    _croppedImage = [UIImage imageWithCGImage:imageRef];
    
    //[self dismissViewControllerAnimated:YES completion:NULL];
    //[self.navigationController popViewControllerAnimated:YES];
    
    if([_delegate respondsToSelector:@selector(imageCropController:didFinishCroppingImage:)])
    {
        [_delegate imageCropController:self didFinishCroppingImage:_croppedImage];
    }
}

- (UIImage *) fixOrientation:(UIImage*)image {
    
    // No-op if the orientation is already correct
    if (image.imageOrientation == UIImageOrientationUp) return image;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (image.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, image.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (image.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, image.size.width, image.size.height,
                                             CGImageGetBitsPerComponent(image.CGImage), 0,
                                             CGImageGetColorSpace(image.CGImage),
                                             CGImageGetBitmapInfo(image.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (image.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.height,image.size.width), image.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.width,image.size.height), image.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    
    return img;
}
// ----------------------------------------------------------------------------------------------------

@end



