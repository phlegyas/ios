//
//  ChecklistItemView.h
//  NewYork
//
//  Created by Tim Autin on 12/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum {UITopViewToolbarStyleNone, UITopViewToolbarStyleSaveCancel, UITopViewToolbarStyleOk} UITopViewToolbarStyle;

@interface UITopView : UIView
{
    UIView* _layerView;
    UIView* _view;
    UIToolbar* _toolbar;
    
    int _height;
    BOOL _isVisible;
    
    UIBarButtonItem* _cancelButton;
    UIBarButtonItem* _saveButton;
    UIBarButtonItem* _okButton;
}

@property (strong) UIView* view;
@property (strong) UIView* toolbar;

@property (strong) UIBarButtonItem* cancelButton;
@property (strong) UIBarButtonItem* saveButton;
@property (strong) UIBarButtonItem* okButton;

- (id) initWithHeight:(int)height superview:(UIView*)superview;
- (id) initWithHeight:(int)height superview:(UIView*)superview toolBarStyle:(UITopViewToolbarStyle)toolbarStyle;
- (id)initWithHeight:(int)height superview:(UIView*)superview marginTop:(int)marginTop toolBarStyle:(UITopViewToolbarStyle)toolbarStyle;

- (void) setSaveButtonTitle:(NSString*)title target:(id)target action:(SEL)action;
- (void) setCancelButtonTitle:(NSString*)title target:(id)target action:(SEL)action;
- (void) setOkButtonTitle:(NSString*)title target:(id)target action:(SEL)action;

- (void) setSaveButtonTitle:(NSString*)title;
- (void) setCancelButtonTitle:(NSString*)title;
- (void) setOkButtonTitle:(NSString*)title;

- (void) show;
- (void) hide;
- (void) toggleVisibility;
- (BOOL) isVisible;

@end
