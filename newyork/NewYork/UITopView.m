//
//  UITopView.m
//  NewYork
//
//  Created by Tim Autin on 12/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "UITopView.h"

#define TOOLBAR_HEIGHT 44

@implementation UITopView


@synthesize view = _view;
@synthesize toolbar = _toolbar;

@synthesize cancelButton = _cancelButton;
@synthesize saveButton = _saveButton;
@synthesize okButton = _okButton;


// ----------------------------------------------------------------------------------------------------
#pragma mark - Init functions

- (id)initWithHeight:(int)height superview:(UIView*)superview
{
    return [self initWithHeight:height superview:superview toolBarStyle:UITopViewToolbarStyleNone];
}


- (id)initWithHeight:(int)height superview:(UIView*)superview toolBarStyle:(UITopViewToolbarStyle)toolbarStyle
{
    return [self initWithHeight:height superview:superview marginTop:0 toolBarStyle:UITopViewToolbarStyleNone];
}


- (id)initWithHeight:(int)height superview:(UIView*)superview marginTop:(int)marginTop toolBarStyle:(UITopViewToolbarStyle)toolbarStyle
{
    // Init the main view :
    CGRect frame = superview.bounds;
    frame.origin.y = marginTop;
    self = [super initWithFrame:frame];

    if (self)
    {
        // Save the height :
        _height = height;
        
        // Set the main view's alpha :
        self.alpha = 0.0;
        
        // Init the _layerView :
        _layerView = [[UIView alloc] initWithFrame:self.bounds];
        _layerView.backgroundColor = [UIColor darkGrayColor];
        _layerView.alpha = 0.0;
        
        // Init the _view :
        _view = [[UIView alloc] initWithFrame:CGRectMake(0, -1 * height, self.bounds.size.width, height)];
        _view.backgroundColor = [UIColor darkGrayColor];
        _view.layer.shadowColor = [UIColor blackColor].CGColor;
        _view.layer.shadowOpacity = 0.0;
        _view.layer.shadowRadius = 5.0;
        _view.layer.shadowOffset = CGSizeMake(0, 3);
        _view.clipsToBounds = NO;
        _view.alpha = 0.0;
        
        // Add the _view to the main view :
        [self addSubview:_layerView];
        [self addSubview:_view];
        
        
        if(toolbarStyle != UITopViewToolbarStyleNone)
        {
            CGRect bounds = _view.bounds;
            bounds.size.height -= TOOLBAR_HEIGHT;
            _view.bounds = bounds;
            
            // Buttons and toolbar caracteristics :
            UIColor* darkGreenColor = [UIColor colorWithRed:0 green:200.0/255.0 blue:0 alpha:2];
            
            // Create a toolbar :
            _toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, -1 * TOOLBAR_HEIGHT, self.frame.size.width, TOOLBAR_HEIGHT)];
            
            // Create an array which will contains the UIBarButtonItems :
            NSMutableArray *items = [[NSMutableArray alloc] init];
            
            // Create a flexible space
            UIBarButtonItem* flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
            
            switch (toolbarStyle)
            {
                case UITopViewToolbarStyleSaveCancel :
                {
                    // Create a cancel button :
                    _cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(toggleVisibility)];
                    _cancelButton.tintColor = [UIColor redColor];
                    
                    // Create a save button :
                    _saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(toggleVisibility)];
                    _saveButton.tintColor = darkGreenColor;
                    
                    // Add the items tp the array :
                    [items addObject:flexibleSpace];
                    [items addObject:_cancelButton];
                    [items addObject:flexibleSpace];
                    [items addObject:_saveButton];
                    [items addObject:flexibleSpace];
                    
                    break;
                }
                case UITopViewToolbarStyleOk :
                {
                    // Create a ok button :
                    _okButton = [[UIBarButtonItem alloc] initWithTitle:@"OK" style:UIBarButtonItemStyleDone target:self action:@selector(toggleVisibility)];
                    
                    // Add the items tp the array :
                    [items addObject:flexibleSpace];
                    [items addObject:_okButton];
                    [items addObject:flexibleSpace];
                    
                    break;
                }
                default:
                {
                    break;
                }
            }
            
            // Set the toolbar's items :
            [_toolbar setItems:items animated:NO];
            [_toolbar setBarStyle:UIBarStyleBlackTranslucent];
            
            // Add the toolbar :
            [self addSubview:_toolbar];
        }
    }
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Buttons setters

- (void)setSaveButtonTitle:(NSString*)title target:(id)target action:(SEL)action
{
    if(_saveButton != nil)
    {
        _saveButton.title = title;
        _saveButton.target = target;
        _saveButton.action = action;
    }
}


- (void)setCancelButtonTitle:(NSString*)title target:(id)target action:(SEL)action
{
    if(_cancelButton != nil)
    {
        _cancelButton.title = title;
        _cancelButton.target = target;
        _cancelButton.action = action;
    }
}


- (void)setOkButtonTitle:(NSString*)title target:(id)target action:(SEL)action
{
    if(_okButton != nil)
    {
        _okButton.title = title;
        _okButton.target = target;
        _okButton.action = action;
    }
}


- (void)setSaveButtonTitle:(NSString*)title
{
    if(_saveButton != nil) _saveButton.title = title;
}


- (void)setCancelButtonTitle:(NSString*)title
{
    if(_cancelButton != nil) _cancelButton.title = title;
}


- (void)setOkButtonTitle:(NSString*)title
{
    if(_okButton != nil) _okButton.title = title;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Visibility management

- (void)show
{
    self.alpha = 1.0;
    _view.alpha = 1.0;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:0.4];
    _view.frame = CGRectMake(0, 0, _view.bounds.size.width, _height);
    _toolbar.frame = CGRectMake(0, _height - TOOLBAR_HEIGHT, self.frame.size.width, TOOLBAR_HEIGHT);
    
    _view.layer.shadowOpacity = 1.0;
    _layerView.alpha = 0.5;
    [UIView commitAnimations];
}


- (void)hide
{
    [UIView animateWithDuration:0.4 delay:0.0 options:UIViewAnimationOptionCurveEaseOut
    animations:^
    {
        _view.frame = CGRectMake(0, -1 * _height, _view.bounds.size.width, _height);
        _toolbar.frame = CGRectMake(0, -1 * TOOLBAR_HEIGHT, self.frame.size.width, TOOLBAR_HEIGHT);
        _view.layer.shadowOpacity = 0.0;
        _layerView.alpha = 0.0;
    }
    completion:^(BOOL finished)
    {
        _view.alpha = 0.0;
        self.alpha = 0.0;
    }];
}


- (void)toggleVisibility
{
    if([self isVisible]) [self hide];
    else [self show];
}


- (BOOL)isVisible
{
     return CGRectEqualToRect(_view.frame, CGRectMake(0, 0, _view.bounds.size.width, _height));
}
// ----------------------------------------------------------------------------------------------------


@end
