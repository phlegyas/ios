//
//  UIZoomableImageView.h
//  NewYork
//
//  Created by Tim Autin on 06/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIZoomableImageView : UIView <UIScrollViewDelegate>
{
    float _maximumZoomFactor;
    
    float _minimumZoomScale;
    float _maximumZoomScale;
    
    UIScrollView* _scrollView;
    UIImageView* _imageView;
    
    UITapGestureRecognizer* _doubleTap;
}

@property (strong) UIScrollView* scrollView;
@property (strong) UIImageView* imageView;

- (id)initWithFrame:(CGRect)frame image:(UIImage*)image maximumZoomFactor:(float)maximumZoomFactor;
- (id)initWithFrame:(CGRect)frame imageNamed:(NSString *)imageNamed maximumZoomFactor:(float)maximumZoomFactor;
- (id)initWithFrame:(CGRect)frame contentOfFile:(NSString *)fileName maximumZoomFactor:(float)maximumZoomFactor;

- (void) initContent;

- (void) handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer;

@end
