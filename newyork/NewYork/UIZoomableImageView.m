//
//  UIZoomableImageView.m
//  NewYork
//
//  Created by Tim Autin on 06/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIZoomableImageView.h"

@implementation UIZoomableImageView


@synthesize scrollView = _scrollView;
@synthesize imageView = _imageView;


// ----------------------------------------------------------------------------------------------------
#pragma mark - Init functions

- (id) initWithFrame:(CGRect)frame image:(UIImage*)image maximumZoomFactor:(float)maximumZoomFactor
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _maximumZoomFactor = maximumZoomFactor;
        _imageView = [[UIImageView alloc]initWithImage:image];
        
        [self initContent];
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame imageNamed:(NSString *)imageNamed maximumZoomFactor:(float)maximumZoomFactor
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _maximumZoomFactor = maximumZoomFactor;
        _imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imageNamed]];
        
        [self initContent];
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame contentOfFile:(NSString *)fileName maximumZoomFactor:(float)maximumZoomFactor
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _maximumZoomFactor = maximumZoomFactor;
        _imageView = [[UIImageView alloc]initWithImage:[UIImage imageWithContentsOfFile:fileName]];
        
        [self initContent];
    }
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) initContent
{
    // Init the view :
    self.autoresizesSubviews = YES;
    
    // Create imageView :
    _imageView.contentMode = UIViewContentModeScaleToFill;
    _imageView.userInteractionEnabled = YES;
    _imageView.backgroundColor = [UIColor clearColor];
    
    // Create scrollView :
    _scrollView = [[UIScrollView alloc]initWithFrame:self.bounds];
    _scrollView.bouncesZoom = YES;
    _scrollView.delegate = self;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.contentSize = CGSizeMake(_imageView.image.size.width, _imageView.image.size.height);
    _scrollView.backgroundColor = [UIColor clearColor];
    _scrollView.alwaysBounceHorizontal = YES;
    _scrollView.alwaysBounceVertical = YES;
    
    // Compute min / max zoom scale to perfectly fit image width, and begin at that scale :
    _minimumZoomScale = _scrollView.bounds.size.width  / _imageView.bounds.size.width;
    _maximumZoomScale = _minimumZoomScale * _maximumZoomFactor;
    _scrollView.minimumZoomScale = _minimumZoomScale;
    _scrollView.maximumZoomScale = _maximumZoomScale;
    _scrollView.zoomScale = _minimumZoomScale;
    
    // Add gesture recognizers to the image view
    _doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    [_doubleTap setNumberOfTapsRequired:2];
    [_imageView addGestureRecognizer:_doubleTap];
    
    // Add the views :
    [_scrollView addSubview:_imageView];
    [self addSubview:_scrollView];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark UIScrollViewDelegate methods

- (UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _imageView;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark TapDetectingImageViewDelegate methods

- (void) handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer
{
    // Get the touch point :
    CGPoint touchPoint = [gestureRecognizer locationInView:gestureRecognizer.view];
    
    // Compute the new zoom scale :
    float newZoomscale;
    
    // If the scrollView is zoomed to the max zoom to the min, and vice versa :
    if(_scrollView.zoomScale == _maximumZoomScale) newZoomscale = _minimumZoomScale;
    else newZoomscale = _maximumZoomScale;
    
    // Compute the size and origin of the rect :
    float w = _scrollView.bounds.size.width / newZoomscale;
    float h = _scrollView.bounds.size.height / newZoomscale;
    float x = touchPoint.x - (w / 2.0);
    float y = touchPoint.y - (h / 2.0);
    
    // Create the rect :
    CGRect rectToZoom = CGRectMake(x, y, w, h);
    
    // Zoom to the rect :
    [_scrollView zoomToRect:rectToZoom animated:YES];
    _scrollView.zoomScale = newZoomscale;
}
// ----------------------------------------------------------------------------------------------------


@end




