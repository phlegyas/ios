//
//  UserHotspotsDBHandler.m
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserHotspotsDBHandler.h"
#import "AppDelegate.h"

@implementation UserHotspotsDBHandler

static UserHotspotsDBHandler *_instance;


// ----------------------------------------------------------------------------------------------------
#pragma mark - Get a static instance of the database

+ (UserHotspotsDBHandler*) instance
{
    if (_instance == nil)
    {
        _instance = [[UserHotspotsDBHandler alloc] init];
    }
    
    return _instance;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Handler lifecycle

- (id) init
{
    if ((self = [super initWithPath:@"/new_york/user_hotspots/database/user_hotspots.sqlite3"]))
    {
        //_tableName = @"HOTSPOTS";
    }
    
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
- (void) notifyDataChanged
{
    // Ask the dropboxHandler to update the user_hotspots file :
    DBPath* path = [[DBPath root] childPath:@"/new_york/user_hotspots/database/user_hotspots.sqlite3"];
    [[AppDelegate sharedDelegate].dropBoxHandler updateRemoteFileAtPath:path];
}

- (void) notifyImageCreated:(NSString*)imageName
{
    // Get the documents directory path :
    NSString* documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    // Ask the dropboxHandler to link the file :
    NSString* localPath = [[documentsDirectory stringByAppendingPathComponent:@"/new_york/user_hotspots/images/"] stringByAppendingPathComponent:imageName];
    DBPath* remotePath = [[DBPath root] childPath:[@"/new_york/user_hotspots/images/" stringByAppendingPathComponent:imageName]];
    
    [[AppDelegate sharedDelegate].dropBoxHandler linkFileAtLocalPath:localPath toFileAtRemotePath:remotePath];
}

- (void) notifyImageChanged:(NSString*)imageName
{
    // Ask the dropboxHandler to update the file :
    DBPath* path = [[DBPath root] childPath:[@"/new_york/user_hotspots/images/" stringByAppendingPathComponent:imageName]];
    [[AppDelegate sharedDelegate].dropBoxHandler updateRemoteFileAtPath:path];
}

- (void) notifyImageDeleted:(NSString*)imageName
{
    // Ask the dropboxHandler to delete the file :
    DBPath* path = [[DBPath root] childPath:[@"/new_york/user_hotspots/images/" stringByAppendingPathComponent:imageName]];
    [[AppDelegate sharedDelegate].dropBoxHandler deleteRemoteFileAtPath:path];
}
// ----------------------------------------------------------------------------------------------------

@end




