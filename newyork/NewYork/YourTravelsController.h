//
//  YourTravelsControllerw.h
//  NewYork
//
//  Created by Tim Autin on 07/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Activity.h"
#import "DayView.h"
#import "UITopView.h"
#import "Travel.h"
#import "GAITrackedViewController.h"

@interface YourTravelsController : GAITrackedViewController <UIScrollViewDelegate, DayViewDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>
{
    Travel* _travel;
    NSArray* _days;
    CGRect _usableFrame;
    UILabel* _travelTitleLabel;
    UIScrollView* _wrapperScrollView;
    UIScrollView* _scrollView;
    UIPageControl* _pageControl;
    DayView* _editingDayView;
    UIEdgeInsets _scrollViewInsets;
    
    NSArray* _travels;
    UITopView* _topView;
    UITableView* _tableView;
    NSArray* _cellsBackgroundColors;
    Travel* _travelClicked;
    
    CGSize _keyboardSize;
    
    CGRect _scrollViewSavedFrame;
    CGSize _wrapperScrollViewSavedContentSize;
    CGRect _pageControlSavedFrame;
}

- (id) initWithTravel:(Travel*)displayedTravel coder:(NSCoder*)coder;
- (void) pageControlInteracted:(UIPageControl *)sender;

- (void) resetTravel;

@end
