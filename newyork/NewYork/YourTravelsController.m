//
//  YourTravelsController.m
//  NewYork
//
//  Created by Tim Autin on 07/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SystemVersion.h"
#import <QuartzCore/QuartzCore.h>

#import "YourTravelsController.h"
#import "YourTravelsDBHandler.h"
#import "Colors.h"
#import "NewTravelController.h"

@implementation YourTravelsController

#define TRAVEL_TITLE_LABEL_HEIGHT 40
#define PAGE_CONTROL_HEIGHT 40
#define SCROLL_TO_ACTIVE_VIEW_MARGIN_BOTTOM 5

#define HORIZONTAL_MARGIN 20
#define VERTICAL_MARGIN 10
#define VERTICAL_PADDING 5
#define BACKGROUND_VIEW_MARGIN_BOTTOM 40
#define NO_TRAVEL_LABEL_HEIGHT 40
#define NO_TRAVEL_DESCRIPTION_LABEL_HEIGHT 60
#define NEW_TRAVEL_BUTTON_WIDTH 150
#define NEW_TRAVEL_BUTTON_HEIGHT 44

// ----------------------------------------------------------------------------------------------------
#pragma mark - Init functions

- (id) initWithCoder:(NSCoder*)coder
{
    return [self initWithTravel:[[YourTravelsDBHandler instance] currentTravel] coder:coder];
}

- (id) initWithTravel:(Travel*)travel coder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    
    if (self)
    {
        _travel = travel;
        _travels = [[YourTravelsDBHandler instance] travels];
        _editingDayView = nil;
        _travelClicked = nil;
    }
    
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - View lifecycle

- (void) viewDidLoad
{
    // Call super :
    [super viewDidLoad];
    self.screenName = @"Vos Voyages";
    
    // Remove eventual view controllers in the stack :
    self.navigationController.viewControllers = [NSArray arrayWithObject:self];
    
    // Init the navigation item :
    self.navigationItem.hidesBackButton = YES;  
    self.navigationItem.title = NSLocalizedString(@"your_travels_tab_your_travels_controller_title", @"Your Travels");
    
    // Set the colors :
    _cellsBackgroundColors = [[NSArray alloc] initWithObjects:
                              [Colors darkYellow],
                              [Colors lightYellow ],
                              nil];
    
    // Compute the usable frame :
    int statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
    int toolBarHeight = self.navigationController.toolbar.frame.size.height;
    int tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    
    if(SYSTEM_VERSION < 7.0)
    {
        self.navigationController.navigationBar.barStyle = UIBarStyleBlackOpaque;
        _usableFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - toolBarHeight - tabBarHeight);
    }
    else
    {
        _usableFrame = CGRectMake(0, statusBarHeight + toolBarHeight, self.view.frame.size.width, self.view.frame.size.height - statusBarHeight - toolBarHeight - tabBarHeight);
    }
    
    // Init the view :
    self.view.backgroundColor = [Colors backgroundColor];
    
    // If there is no travel :
    if(_travels.count == 0)
    {
        // Display a label and a button to add a new travel :
        UILabel* noTravelLabel = [[UILabel alloc] initWithFrame:CGRectMake(_usableFrame.origin.x, _usableFrame.origin.y, _usableFrame.size.width, NO_TRAVEL_LABEL_HEIGHT)];
        noTravelLabel.text = NSLocalizedString(@"your_travels_tab_your_travels_controller_no_travel_title_label", @"No travel");
        noTravelLabel.backgroundColor = self.view.backgroundColor;
        noTravelLabel.textAlignment = UITextAlignmentCenter;
        noTravelLabel.font = [UIFont boldSystemFontOfSize:16.0];
        noTravelLabel.textColor = [Colors lightYellow];
        noTravelLabel.shadowColor = [UIColor darkGrayColor];
        noTravelLabel.shadowOffset = CGSizeMake(1, 1);
        
        // Add a background view :
        UIView* backgroundView = [[UIView alloc] initWithFrame:CGRectMake(_usableFrame.origin.x + HORIZONTAL_MARGIN, _usableFrame.origin.y + NO_TRAVEL_LABEL_HEIGHT + VERTICAL_PADDING, _usableFrame.size.width - 2*HORIZONTAL_MARGIN, _usableFrame.size.height - 2*VERTICAL_PADDING - NO_TRAVEL_LABEL_HEIGHT - BACKGROUND_VIEW_MARGIN_BOTTOM)];
        
        backgroundView.backgroundColor = [UIColor whiteColor];
        
        backgroundView.layer.cornerRadius = 5;
        backgroundView.layer.borderWidth = 2;
        backgroundView.layer.borderColor = [[Colors lightYellow] CGColor];
        
        backgroundView.layer.shadowColor= [[UIColor blackColor] CGColor];
        backgroundView.layer.shadowOpacity = 0.8;
        backgroundView.layer.shadowRadius = 3.0;
        backgroundView.layer.shadowOffset = CGSizeMake(2.0, 2.0);
        
        // Add a label to the background view :
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(HORIZONTAL_MARGIN, 2 * VERTICAL_MARGIN, backgroundView.frame.size.width - 2*HORIZONTAL_MARGIN, NO_TRAVEL_DESCRIPTION_LABEL_HEIGHT)];
        label.text = NSLocalizedString(@"your_travels_tab_your_travels_controller_no_travel_message_label", @"There are no travels saved on your phone.");
        label.numberOfLines = 2;
        label.textAlignment = UITextAlignmentCenter;
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor darkGrayColor];
        
        // Add a buton to create a new travel :
        UIButton* button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.frame = CGRectMake(backgroundView.frame.size.width/2.0 - NEW_TRAVEL_BUTTON_WIDTH/2.0, 4 * VERTICAL_MARGIN + NO_TRAVEL_DESCRIPTION_LABEL_HEIGHT, NEW_TRAVEL_BUTTON_WIDTH, NEW_TRAVEL_BUTTON_HEIGHT);
        [button setTitle:NSLocalizedString(@"your_travels_tab_your_travels_controller_add_travel_message_label", @"Add a new one") forState:UIControlStateNormal];
        [button addTarget:self action:@selector(createNewTravel) forControlEvents:UIControlEventTouchUpInside];
        
        [backgroundView addSubview:label];
        [backgroundView addSubview:button];
        [self.view addSubview:noTravelLabel];
        [self.view addSubview:backgroundView];
    }
    
    // If there are travels :
    else
    {
        // Create the _topView :
        int marginTop = (SYSTEM_VERSION < 7.0) ? 0 : statusBarHeight + toolBarHeight;
        _topView = [[UITopView alloc] initWithHeight:250 superview:self.view marginTop:marginTop toolBarStyle:UITopViewToolbarStyleSaveCancel];
        [_topView setCancelButtonTitle:NSLocalizedString(@"your_travels_tab_your_travels_controller_top_view_cancel_button_title", @"Close")];
        [_topView setSaveButtonTitle:NSLocalizedString(@"your_travels_tab_your_travels_controller_top_view_save_button_title", @"New") target:self action:@selector(createNewTravel)];
        _topView.cancelButton.tintColor = [Colors lightOrange];
        _topView.saveButton.tintColor = [Colors lightGreen];
        _tableView.backgroundView = nil;
        _topView.view.backgroundColor = [Colors backgroundColor];
        
        // Create the tableView :
        _tableView = [[UITableView alloc] initWithFrame:_topView.view.bounds style:UITableViewStyleGrouped];
        _tableView.backgroundView = nil;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.01f, 0.01f)];
        
        // Handle gestures on the table view :
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGestureOnTravelsTableView:)];
        tapRecognizer.numberOfTapsRequired = 1;
        tapRecognizer.numberOfTouchesRequired = 1;
        [_tableView addGestureRecognizer:tapRecognizer];
        
        // Create the button :
        UIBarButtonItem* travelsListButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"list.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(toggleTopViewVisibility)];
        if(SYSTEM_VERSION < 7.0) travelsListButtonItem.tintColor = [Colors lightOrange];
        
        
        // Create the wrapper view :
        if(SYSTEM_VERSION < 7.0) _wrapperScrollView = [[UIScrollView alloc] initWithFrame:_usableFrame];
        else _wrapperScrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
        _wrapperScrollView.contentSize = _usableFrame.size;
        _wrapperScrollView.backgroundColor = [UIColor clearColor];
        
        // Get the requested travel and it's days :
        _days = [[YourTravelsDBHandler instance] daysOfTravel:_travel.uniqueId];
        
        // Create the travel title label :
        _travelTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _usableFrame.size.width, TRAVEL_TITLE_LABEL_HEIGHT)];
        _travelTitleLabel.text = _travel.title;
        _travelTitleLabel.backgroundColor = _wrapperScrollView.backgroundColor;
        _travelTitleLabel.textAlignment = UITextAlignmentCenter;
        _travelTitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        
        if(SYSTEM_VERSION < 7.0)
        {
            _travelTitleLabel.textColor = [Colors lightYellow];
            _travelTitleLabel.shadowColor = [UIColor darkGrayColor];
            _travelTitleLabel.shadowOffset = CGSizeMake(1, 1);
        }
        else
        {
            _travelTitleLabel.textColor = [Colors darkTextColor];
        }
        
        // Create the scroll view :
        int x = 0;
        int y = TRAVEL_TITLE_LABEL_HEIGHT;
        int width = _usableFrame.size.width;
        int height = _usableFrame.size.height - TRAVEL_TITLE_LABEL_HEIGHT - PAGE_CONTROL_HEIGHT ;
        
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(x, y, width, height)];
        _scrollView.contentSize = CGSizeMake(_days.count * _scrollView.frame.size.width, _scrollView.frame.size.height);
        _scrollView.pagingEnabled = YES;
        _scrollView.delegate = self;
        
        // Create the day views :
        for (int i=0; i<_days.count; i++)
        {
            // Frame of the view :
            int x = i*_usableFrame.size.width + HORIZONTAL_MARGIN;
            int y  = 0;
            int width = _usableFrame.size.width - 2*HORIZONTAL_MARGIN;
            int height = _scrollView.frame.size.height;
            
            // Day of the view :
            Day* day = [_days objectAtIndex:i];
            
            // Create the view :
            DayView* dayView = [[DayView alloc] initWithFrame:CGRectMake(x, y, width, height) day:day];
            dayView.delegate = self;
            
            // Add the day view to the scroll view :
            [_scrollView addSubview: dayView];
        }
        
        if(_days.count <= 20)
        {
            // Hide the horizontal scroll indicator :
            _scrollView.showsHorizontalScrollIndicator = NO;
            
            // Create a page control :
            _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 0, _usableFrame.size.width, PAGE_CONTROL_HEIGHT)];
            _pageControl.center = CGPointMake(_usableFrame.size.width/2, _usableFrame.size.height - PAGE_CONTROL_HEIGHT / 2);
            _pageControl.numberOfPages = _days.count;
            _pageControl.currentPage = 0;
            if(SYSTEM_VERSION >= 7.0) _pageControl.pageIndicatorTintColor = [Colors darkBlue];
            [_pageControl addTarget:self action:@selector(pageControlInteracted:) forControlEvents:UIControlEventValueChanged];
            [_wrapperScrollView addSubview: _pageControl];
        }
        else
        {
            CGRect frame = _scrollView.frame;
            frame.size.height = _usableFrame.size.height - TRAVEL_TITLE_LABEL_HEIGHT;
            _scrollView.frame = frame;
        }
        
        // Add the views :
        [_wrapperScrollView addSubview: _travelTitleLabel];
        [_wrapperScrollView addSubview: _scrollView];
        [self.view addSubview:_wrapperScrollView];
        
        // Add the top view and the button to display it :
        self.navigationItem.rightBarButtonItem = travelsListButtonItem;
        [_topView.view addSubview:_tableView];
        [self.view addSubview:_topView];
    }
    
    // Save the frames :
    _scrollViewSavedFrame = _scrollView.frame;
    _wrapperScrollViewSavedContentSize = _wrapperScrollView.contentSize;
    _pageControlSavedFrame = _pageControl.frame;
    
}

- (void) viewDidAppear:(BOOL)animated
{
    // Save the scrollView insets :
    _scrollViewInsets = _wrapperScrollView.contentInset;
    
    // Add a notification for the keyboard showing :
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object: nil];
}

- (void) viewWillDisappear:(BOOL)animated
{
    // Unsusribe from the keyboard notification (else it will be triggered from the NewTravelController) :
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
// ----------------------------------------------------------------------------------------------------




// ----------------------------------------------------------------------------------------------------
#pragma mark - Screen autorotate

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Scroll view delegate

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float roundedValue = round(scrollView.contentOffset.x / self.view.bounds.size.width);
    _pageControl.currentPage = roundedValue;    
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Page control handling

- (void) pageControlInteracted:(UIPageControl *)sender
{
    CGRect frame = _scrollView.bounds;
    frame.origin.x = _scrollView.bounds.size.width * sender.currentPage;
    [_scrollView scrollRectToVisible:frame animated:YES];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Keyboard events

- (void) keyboardDidShow:(NSNotification*)notification
{
    // Get the keyboard size :
    NSDictionary* info = [notification userInfo];
    _keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    [self scrollToActiveTextView:_keyboardSize animated:YES];
}

- (void) keyboardWillHide:(NSNotification*)notification
{
    // Close the editing day view :
    [_editingDayView stopEditingMode];
    
    // Remove the inset under the content :
    [UIView animateWithDuration:0.3 animations:^
     {
         _wrapperScrollView.contentInset = _scrollViewInsets;
         _wrapperScrollView.scrollIndicatorInsets = _scrollViewInsets;
     }];
}

- (void) scrollToActiveTextView:(CGSize)keyboardSize animated:(BOOL)animated
{
    // Get the tabbar height :
    int tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    
    // Add an inset under the content :
    UIEdgeInsets newInsets = _scrollViewInsets;
    newInsets.bottom = keyboardSize.height; if(SYSTEM_VERSION < 7.0) newInsets.bottom -= tabBarHeight;
    _wrapperScrollView.contentInset = newInsets;
    _wrapperScrollView.scrollIndicatorInsets = newInsets;
    
    // Scroll to the editing textview :
    CGRect visibleRect = _usableFrame;
    visibleRect.size.height -= (keyboardSize.height - tabBarHeight);
    
    float y =   TRAVEL_TITLE_LABEL_HEIGHT +
    _editingDayView.frame.origin.y +
    _editingDayView.scrollView.frame.origin.y +
    _editingDayView.activeTextView.superview.frame.origin.y +
    _editingDayView.activeTextView.frame.origin.y +
    _editingDayView.activeTextView.frame.size.height +
    SCROLL_TO_ACTIVE_VIEW_MARGIN_BOTTOM;
    
    // Compute the offset :
    CGPoint pointToShow = CGPointMake(0, y - _wrapperScrollView.contentOffset.y);
    CGPoint offsetToShow = CGPointMake(0, y - visibleRect.origin.y - visibleRect.size.height );
    
    // Animate to the offset :
    if (!CGRectContainsPoint(visibleRect, pointToShow) )
    {
        [_wrapperScrollView setContentOffset:offsetToShow animated:animated];
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - DayView delegate

- (void) dayViewWillStartEditingMode:(DayView*)dayView
{
    // Remember the day view :
    _editingDayView = dayView;
    
    // Disable the pagination :
    _scrollView.scrollEnabled = NO;
    _pageControl.userInteractionEnabled = NO;
    
    // Fade out the page control :
    [UIView animateWithDuration:0.2 animations:^
    {
        _pageControl.alpha = 0.3;
    }];
}

- (void) dayViewDidStartEditingMode:(DayView*)dayView
{
    // Update the scrollView's frame's height :
    CGRect frame = _scrollView.frame;
    frame.size.height = dayView.frame.size.height + TRAVEL_TITLE_LABEL_HEIGHT + PAGE_CONTROL_HEIGHT + VERTICAL_MARGIN;
    _scrollView.frame = frame;
    
    // Update the wrappeScrollView's contentSize height :
    _wrapperScrollView.contentSize = _scrollView.frame.size;
    
    // Update the page control position :
    frame = _pageControl.frame;
    frame.origin.y = _wrapperScrollView.contentSize.height - PAGE_CONTROL_HEIGHT;
    _pageControl.frame = frame;
}

- (void) dayViewWillStopEditingMode:(DayView*)dayView
{
    // There won't be any editing day view :
    _editingDayView = nil;
    
    // Enable the pagination :
    _scrollView.scrollEnabled = YES;
    _pageControl.userInteractionEnabled = YES;

    // Fade in the page control :
    [UIView animateWithDuration:0.2 animations:^
    {
        _pageControl.alpha = 1.0;
    }];
}

- (void) dayViewDidStopEditingMode:(DayView*)dayView
{
    _scrollView.frame = _scrollViewSavedFrame;
    _wrapperScrollView.contentSize = _wrapperScrollViewSavedContentSize;
    _pageControl.frame = _pageControlSavedFrame;
}

- (void) dayViewHeightDidChange:(DayView*)dayView
{
    // Update the scrollView's frame's height :
    CGRect frame = _scrollView.frame;
    frame.size.height = dayView.frame.size.height + TRAVEL_TITLE_LABEL_HEIGHT + PAGE_CONTROL_HEIGHT + VERTICAL_MARGIN;
    _scrollView.frame = frame;
    
    // Update the wrappeScrollView's contentSize height :
    _wrapperScrollView.contentSize = _scrollView.frame.size;
    
    // Update the page control position :
    frame = _pageControl.frame;
    frame.origin.y = _wrapperScrollView.contentSize.height - PAGE_CONTROL_HEIGHT;
    _pageControl.frame = frame;
    
    // Scroll :
    [self scrollToActiveTextView:_keyboardSize animated:NO];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - UITableView data source

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_travels count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Get the cell :
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    // Get the travel :
    Travel* travel = [_travels objectAtIndex:indexPath.row];
    
    cell.textLabel.text = travel.title;
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.backgroundColor = [_cellsBackgroundColors objectAtIndex:(indexPath.row % _cellsBackgroundColors.count)];
    if(travel.isCurrentTravel) cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}    
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - UITableView delegate

- (void) handleGestureOnTravelsTableView:(UIGestureRecognizer *)gestureRecognizer
{
    if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]])
    {
        // Get the row touched :
        CGPoint p = [gestureRecognizer locationInView:_tableView];
        NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:p];
        if (indexPath != nil)
        {
            // Get the travel clicked :
            _travelClicked = [_travels objectAtIndex:indexPath.row];
            
            // Open an alert view :
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"your_travels_tab_your_travels_controller_choose_action_popup_title", @"What do you want to do ?") message:_travelClicked.title delegate:self cancelButtonTitle:NSLocalizedString(@"your_travels_tab_your_travels_controller_choose_action_cancel_button", @"Cancel") otherButtonTitles:NSLocalizedString(@"your_travels_tab_your_travels_controller_choose_action_popup_set_as_main_travel_button", @"Set as main travel"), NSLocalizedString(@"your_travels_tab_your_travels_controller_delete_confirmation_popup_delete_button", @"Delete"), nil];
            alert.tag = 1;
            
            [alert show];
        }
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - UIAlertView delegate

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    // Main alert view :
    if(alertView.tag == 1)
    {
        // Set the travel as curent travel button :
        if (buttonIndex == 1) 
        {
            // Close the top view :
            [_topView hide];
            
            // Set the current travel :
            [[YourTravelsDBHandler instance] setCurrentTravel:_travelClicked.uniqueId];
            
            // Create a new YourTravelsController :
            YourTravelsController* controller = [[YourTravelsController alloc] initWithTravel:_travelClicked coder:nil];
            
            // Display the new :
            [self.navigationController pushViewController:controller animated:YES];
        }
        
        // Delete the travel button :
        else if (buttonIndex == 2) 
        {
            // Open an alert view :
            NSString* message = [NSString stringWithFormat:NSLocalizedString(@"your_travels_tab_your_travels_controller_delete_confirmation_popup_message", @"Do you really want to delete the travel \" %@\" ?"), _travelClicked.title];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"your_travels_tab_your_travels_controller_delete_confirmation_popup_title", @"Confirmation") message:message delegate:self cancelButtonTitle:NSLocalizedString(@"your_travels_tab_your_travels_controller_delete_confirmation_popup_cancel_button", @"Cancel") otherButtonTitles:NSLocalizedString(@"your_travels_tab_your_travels_controller_delete_confirmation_popup_delete_button", @"Delete"), nil];
            alert.tag = 2;
            
            [alert show];
        }
    }
    
    
    // Suppress travel confirmation alert view :
    else if(alertView.tag == 2)
    {
        // Delete the travel button :
        if (buttonIndex == 1) 
        {
            // Close the top view :
            [_topView hide];
            
            // Set the current travel :
            [[YourTravelsDBHandler instance] deleteTravelWithId:_travelClicked.uniqueId];
            
            // Create a new YourTravelsController :
            YourTravelsController* controller = [[YourTravelsController alloc] initWithTravel:[[YourTravelsDBHandler instance] currentTravel] coder:nil];
            
            // Display the new :
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - UITopView events

- (void) toggleTopViewVisibility
{
    if(_topView.isVisible)
    {
        [_topView hide];
    }
    else
    {
        if(_editingDayView.isEditing)
        {
            [_editingDayView stopEditingMode];
        }
        [_topView show];
    }
}

- (void) createNewTravel
{
    // Close the top view :
    [_topView hide];
    
    // Create and push an ImagePageController :
    NewTravelController *newTravel = [[NewTravelController alloc] init];
    
    // Display it :
    [self.navigationController pushViewController:newTravel animated:YES];
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Reset the travel (when updated by DropBox)

- (void) resetTravel
{
    // Create a new YourTravelsController :
    YourTravelsController* controller = [[YourTravelsController alloc] initWithTravel:[[YourTravelsDBHandler instance] currentTravel] coder:nil];
    
    // Display the new :
    [self.navigationController pushViewController:controller animated:NO];
}
// ----------------------------------------------------------------------------------------------------


@end



