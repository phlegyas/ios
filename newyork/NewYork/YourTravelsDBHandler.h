//
//  YourTravelsDBHandler.h
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

#import "DBHandler.h"

#import "Travel.h"
#import "Day.h"
#import "Activity.h"


@interface YourTravelsDBHandler : DBHandler
{
    NSDateFormatter* _dateFormatter;
}

+ (YourTravelsDBHandler*) instance;

+ (DayTimeType) stringToDayTimeType:(NSString*)string;
+ (NSString*) dayTimeTypeToString:(DayTimeType)dayTimeType;

- (NSArray*) travels;
- (Travel*) travelWithId:(int)travelId;
- (Travel*) currentTravel;
- (Day*) dayWithId:(int)dayId;
- (NSArray*) daysOfTravel:(int)travelId;
- (NSArray*) activitiesOfDay:(int)dayId;
- (Activity*) activityOfDay:(int)dayId dayTime:(DayTimeType)dayTime;
- (BOOL) updateActivityOfDay:(int)dayId dayTime:(DayTimeType)dayTime description:(NSString*)description;
- (BOOL) addTravelWithTitle:(NSString*)title beginDate:(NSDate*)beginDate endDate:(NSDate*)endDate isCurrentTravel:(BOOL)isCurrentTravel;
- (BOOL) addDayWithDate:(NSDate*)date travelId:(int)travelId;
- (BOOL) addActivityWithDescription:(NSString*)description dayTime:(DayTimeType)dayTime dayId:(int)dayId;
- (void) setCurrentTravel:(int)travelId;
- (void) deleteTravelWithId:(int)travelId;
- (void) deleteActivitiesOfTravel:(int)travelId;
- (void) deleteDaysOfTravel:(int)travelId;

@end


