//
//  YourTravelsDBHandler.m
//  NewYork
//
//  Created by Tim Autin on 08/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "YourTravelsDBHandler.h"
#import "AppDelegate.h"

@implementation YourTravelsDBHandler

static YourTravelsDBHandler *_instance;


// ----------------------------------------------------------------------------------------------------
#pragma mark - Get a static instance of the database

+ (YourTravelsDBHandler*) instance
{
    if (_instance == nil)
    {
        _instance = [[YourTravelsDBHandler alloc] init];
    }
    
    return _instance;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Handler lifecycle
- (id)init
{
    if ((self = [super initWithPath:@"/your_travels/database/your_travels.sqlite3"]))
    {
        // Init the date formater :
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateFormat:@"yyyy-MM-dd"];
    }
    
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Other class functions

+ (DayTimeType) stringToDayTimeType:(NSString*)string
{
    if([string isEqualToString:@"MORNING"]) return MORNING;
    else if([string isEqualToString:@"AFTERNOON"]) return AFTERNOON;
    else if([string isEqualToString:@"EVENING"]) return EVENING;
    
    else return -1;
}

+ (NSString*) dayTimeTypeToString:(DayTimeType)dayTimeType
{
    if(dayTimeType == MORNING) return @"MORNING";
    else if(dayTimeType == AFTERNOON) return @"AFTERNOON";
    else if(dayTimeType == EVENING) return @"EVENNING";
    
    else return nil;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Utils functions

- (int) daysBetween:(NSDate *)date1 and:(NSDate *)date2
{
    NSTimeInterval interval = [date1 timeIntervalSinceDate:date2];
    int intervalInDays = interval / (60.0 * 60.0 * 24.0);
    if(intervalInDays < 0) intervalInDays *= -1;
    
    intervalInDays += 1;
    
    return intervalInDays;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Travels, days & activities

- (NSArray*) travels
{    
    NSMutableArray *travels = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT id, title, beginDate, endDate, isCurrentTravel FROM TRAVELS";
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            char* titleChar = (char *) sqlite3_column_text(statement, 1);
            char* beginDateChar = (char*) sqlite3_column_text(statement, 2);
            char* endDateChar = (char*) sqlite3_column_text(statement, 3);
            BOOL isCurrentTravel = sqlite3_column_int(statement, 4);
            
            NSString *title = [[NSString alloc] initWithUTF8String:titleChar];
            
            NSString* beginDateString = [[NSString alloc] initWithUTF8String:beginDateChar];
            NSDate* beginDate = [_dateFormatter dateFromString:beginDateString];
            
            NSString* endDateString = [[NSString alloc] initWithUTF8String:endDateChar];
            NSDate* endDate = [_dateFormatter dateFromString:endDateString];
            
            Travel* travel = [[Travel alloc] initWithUniqueId:uniqueId title:title beginDate:beginDate endDate:endDate isCurrentTravel:isCurrentTravel];
            
            [travels addObject:travel];
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"travels" message:sqlErrorMsg];
    }
    
    return travels;
}

- (Travel*) travelWithId:(int)travelId
{
    Travel* travel = nil;
    NSString* query = [NSString stringWithFormat:@"SELECT id, title, beginDate, endDate, isCurrentTravel FROM TRAVELS WHERE id='%d'", travelId];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            char* titleChar = (char *) sqlite3_column_text(statement, 1);
            char* beginDateChar = (char*) sqlite3_column_text(statement, 2);
            char* endDateChar = (char*) sqlite3_column_text(statement, 3);
            BOOL isCurrentTravel = sqlite3_column_int(statement, 4);
            
            NSString *title = [[NSString alloc] initWithUTF8String:titleChar];
            
            NSString* beginDateString = [[NSString alloc] initWithUTF8String:beginDateChar];
            NSDate* beginDate = [_dateFormatter dateFromString:beginDateString];
            
            NSString* endDateString = [[NSString alloc] initWithUTF8String:endDateChar];
            NSDate* endDate = [_dateFormatter dateFromString:endDateString];
            
            travel = [[Travel alloc] initWithUniqueId:uniqueId title:title beginDate:beginDate endDate:endDate isCurrentTravel:isCurrentTravel];
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"travelWithId" message:sqlErrorMsg];
    }
    
    return travel;
}

- (Travel*) currentTravel
{
    Travel* travel = nil;
    NSString* query = [NSString stringWithFormat:@"SELECT id, title, beginDate, endDate, isCurrentTravel FROM TRAVELS WHERE isCurrentTravel=1"];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            char* titleChar = (char *) sqlite3_column_text(statement, 1);
            char* beginDateChar = (char*) sqlite3_column_text(statement, 2);
            char* endDateChar = (char*) sqlite3_column_text(statement, 3);
            BOOL isCurrentTravel = sqlite3_column_int(statement, 4);
            
            NSString *title = [[NSString alloc] initWithUTF8String:titleChar];
            
            NSString* beginDateString = [[NSString alloc] initWithUTF8String:beginDateChar];
            NSDate* beginDate = [_dateFormatter dateFromString:beginDateString];
            
            NSString* endDateString = [[NSString alloc] initWithUTF8String:endDateChar];
            NSDate* endDate = [_dateFormatter dateFromString:endDateString];
            
            travel = [[Travel alloc] initWithUniqueId:uniqueId title:title beginDate:beginDate endDate:endDate isCurrentTravel:isCurrentTravel];
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"currentTravel" message:sqlErrorMsg];
    }
    
    return travel;
}

- (Day*) dayWithId:(int)dayId
{
    Day* day = nil;
    NSString* query = [NSString stringWithFormat:@"SELECT id, date, travel FROM DAYS WHERE id='%d'", dayId];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            char* dateChar = (char*) sqlite3_column_text(statement, 1);
            int travel = sqlite3_column_int(statement, 2);
            
            NSString *dateString = [[NSString alloc] initWithUTF8String:dateChar];
            NSDate* date = [_dateFormatter dateFromString:dateString];
            
            day = [[Day alloc] initWithUniqueId:uniqueId travel:travel date:date];
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"travelWithId" message:sqlErrorMsg];
    }
    
    return day;
}

- (NSArray*) daysOfTravel:(int)travelId
{
    NSMutableArray* days = [NSMutableArray array];
    NSString* query = [NSString stringWithFormat:@"SELECT id, date, travel FROM DAYS WHERE travel='%d'", travelId];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            char* dateChar = (char*) sqlite3_column_text(statement, 1);
            int travel = sqlite3_column_int(statement, 2);
            
            NSString *dateString = [[NSString alloc] initWithUTF8String:dateChar];
            NSDate* date = [_dateFormatter dateFromString:dateString];
            
            Day* day = [[Day alloc] initWithUniqueId:uniqueId travel:travel date:date];
            [days addObject:day];
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"daysOfTravel" message:sqlErrorMsg];
    }
    
    return days;
}

- (NSArray*) activitiesOfDay:(int)dayId
{
    NSMutableArray* activities = [NSMutableArray array];
    NSString* query = [NSString stringWithFormat:@"SELECT id, description, dayTime, day FROM ACTIVITIES WHERE day='%d'", dayId];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            char* descriptionChar = (char *) sqlite3_column_text(statement, 1);
            char* dayTimeChar = (char *) sqlite3_column_text(statement, 2);
            int day = sqlite3_column_int(statement, 3);
            
            NSString *description = [[NSString alloc] initWithUTF8String:descriptionChar];
            NSString *dayTimeString = [[NSString alloc] initWithUTF8String:dayTimeChar];
            
            DayTimeType dayTime = [YourTravelsDBHandler stringToDayTimeType:dayTimeString];
            
            Activity* activity = [[Activity alloc] initWithUniqueId:uniqueId description:description dayTime:dayTime day:day];
            [activities addObject:activity];
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"activitiesOfDay" message:sqlErrorMsg];
    }
    
    return activities;
}

- (Activity*) activityOfDay:(int)dayId dayTime:(DayTimeType)dayTime
{
    Activity* activity = nil;
    NSString* query = [NSString stringWithFormat:@"SELECT id, description, dayTime, day FROM ACTIVITIES WHERE day='%d' and dayTime='%@'", dayId, [YourTravelsDBHandler dayTimeTypeToString:dayTime]];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            int uniqueId = sqlite3_column_int(statement, 0);
            char* descriptionChar = (char *) sqlite3_column_text(statement, 1);
            char* dayTimeChar = (char *) sqlite3_column_text(statement, 2);
            int day = sqlite3_column_int(statement, 3);
            
            NSString *description = [[NSString alloc] initWithUTF8String:descriptionChar];
            NSString *dayTimeString = [[NSString alloc] initWithUTF8String:dayTimeChar];
            
            DayTimeType dayTime = [YourTravelsDBHandler stringToDayTimeType:dayTimeString];
            
            activity = [[Activity alloc] initWithUniqueId:uniqueId description:description dayTime:dayTime day:day];
        }
        
        sqlite3_finalize(statement);
    }
    else
    {
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"activityOfDay" message:sqlErrorMsg];
    }
    
    return activity;
}

- (BOOL) updateActivityOfDay:(int)dayId dayTime:(DayTimeType)dayTime description:(NSString*)description
{
    Activity* activity = [self activityOfDay:dayId dayTime:dayTime];
    const char* descriptionChar = [description UTF8String];
    
    // Prepare the statement :
    const char *query = "UPDATE ACTIVITIES set description=? where id=?";
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(_db, query, -1, &statement, nil) == SQLITE_OK)
    {
        sqlite3_bind_text(statement, 1, descriptionChar, -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(statement, 2, activity.uniqueId);
        
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            
            [self notifyDataChanged];
            
            return YES;
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"updateActivityOfDay" message:sqlErrorMsg];
            return NO;
        }
    }
    else
    {
        sqlite3_reset(statement);
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"updateActivityOfDay" message:sqlErrorMsg];
        return NO;
    }
}

- (BOOL) addTravelWithTitle:(NSString*)title beginDate:(NSDate*)beginDate endDate:(NSDate*)endDate isCurrentTravel:(BOOL)isCurrentTravel
{
    // Format the dates :
    NSString *beginDateString = [_dateFormatter stringFromDate:beginDate];
    NSString *endDateString = [_dateFormatter stringFromDate:endDate];
    
    // Create the query and statement :
    const char* query;
    sqlite3_stmt* statement;
    
    // If this travel must be the current one, set the other's isCurrentTravel field to 0 :
    if(isCurrentTravel)
    {
        query = "UPDATE TRAVELS SET isCurrentTravel='0'";
        
        if (sqlite3_prepare_v2(_db, query, -1, &statement, nil) == SQLITE_OK)
        {
            if(sqlite3_step(statement)==SQLITE_DONE)
            {
                sqlite3_finalize(statement);
            }
            else
            {
                sqlite3_reset(statement);
                NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
                [self displayErrorMessageFrom:@"addTravelWithTitle" message:sqlErrorMsg];
                return NO;
            }
        }
    }
    
    
    // Prepare the statement :
    query = "INSERT INTO TRAVELS (id, title, beginDate, endDate, isCurrentTravel) VALUES (NULL, ?, ?, ?, ?)";
    if (sqlite3_prepare_v2(_db, query, -1, &statement, nil) == SQLITE_OK)
    {
        sqlite3_bind_text(statement, 1, [title UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 2, [beginDateString UTF8String] , -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 3, [endDateString UTF8String] , -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(statement, 4, isCurrentTravel);
        
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            
            int daysCount = [self daysBetween:beginDate and:endDate];
            int lastId = (int) sqlite3_last_insert_rowid(_db);
            NSDate *date = beginDate;
            
            for(int i=0; i<daysCount; i++)
            {
                [self addDayWithDate:date travelId:lastId];
                date = [date dateByAddingTimeInterval:60*60*24];
            }
            
            [self notifyDataChanged];
            
            return YES;
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"addTravelWithTitle" message:sqlErrorMsg];
            return NO;
        }
    }
    else
    {
        sqlite3_reset(statement);
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"addTravelWithTitle" message:sqlErrorMsg];
        return NO;
    }
}

- (BOOL) addDayWithDate:(NSDate*)date travelId:(int)travelId
{
    // Format the date :
    NSString *dateString = [_dateFormatter stringFromDate:date];
    
    // Prepare the statement :
    sqlite3_stmt* statement;
    const char* query = "INSERT INTO DAYS (id, date, travel) VALUES (NULL, ?, ?)";
    if (sqlite3_prepare_v2(_db, query, -1, &statement, nil) == SQLITE_OK)
    {
        sqlite3_bind_text(statement, 1, [dateString UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(statement, 2, travelId);
        
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            int lastId = (int) sqlite3_last_insert_rowid(_db);
            [self addActivityWithDescription:@"" dayTime:MORNING dayId:lastId];
            [self addActivityWithDescription:@"" dayTime:AFTERNOON dayId:lastId];
            [self addActivityWithDescription:@"" dayTime:EVENING dayId:lastId];
            
            //[self notifyDataChanged];
            
            return YES;
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"addDayWithDate" message:sqlErrorMsg];
            return NO;
        }
    }
    else
    {
        sqlite3_reset(statement);
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"addDayWithDate" message:sqlErrorMsg];
        return NO;
    }
}

- (BOOL) addActivityWithDescription:(NSString*)description dayTime:(DayTimeType)dayTime dayId:(int)dayId
{
    // Prepare the statement :
    sqlite3_stmt* statement;
    const char* query = "INSERT INTO ACTIVITIES (id, description, dayTime, day) VALUES (NULL, ?, ?, ?)";
    if (sqlite3_prepare_v2(_db, query, -1, &statement, nil) == SQLITE_OK)
    {
        sqlite3_bind_text(statement, 1, [description UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(statement, 2, [[YourTravelsDBHandler dayTimeTypeToString:dayTime] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(statement, 3, dayId);
        
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            
            //[self notifyDataChanged];
            
            return YES;
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"addActivityWithDescription" message:sqlErrorMsg];
            return NO;
        }
    }
    else
    {
        sqlite3_reset(statement);
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"addActivityWithDescription" message:sqlErrorMsg];
        return NO;
    }
}

- (void) setCurrentTravel:(int)travelId
{
    // Create the query and statement :
    const char* query;
    sqlite3_stmt* statement;
    
    query = "UPDATE TRAVELS SET isCurrentTravel='0'";
    
    if (sqlite3_prepare_v2(_db, query, -1, &statement, nil) == SQLITE_OK)
    {
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            sqlite3_finalize(statement);
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"setCurrentTravel" message:sqlErrorMsg];
        }
    }
    else
    {
        sqlite3_reset(statement);
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"setCurrentTravel" message:sqlErrorMsg];
    }
    
    
    query = "UPDATE TRAVELS SET isCurrentTravel='1' where id = ?";
    
    if (sqlite3_prepare_v2(_db, query, -1, &statement, nil) == SQLITE_OK)
    {
        sqlite3_bind_int(statement, 1, travelId);
        
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            
            [self notifyDataChanged];
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"setCurrentTravel" message:sqlErrorMsg];
        }
    }
    else
    {
        sqlite3_reset(statement);
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"setCurrentTravel" message:sqlErrorMsg];
    }
}

- (void) deleteTravelWithId:(int)travelId
{
    // Delete the activities of the travel :
    [self deleteActivitiesOfTravel:travelId];
    
    // Delete the days of the travel :
    [self deleteDaysOfTravel:travelId];
    
    // Delete the travel :
    const char* query = "DELETE FROM TRAVELS WHERE id = ?";
    sqlite3_stmt* statement;
    
    if (sqlite3_prepare_v2(_db, query, -1, &statement, nil) == SQLITE_OK)
    {
        sqlite3_bind_int(statement, 1, travelId);
        
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            sqlite3_finalize(statement);
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"deleteTravelWithId" message:sqlErrorMsg];
        }
    }
    else
    {
        sqlite3_reset(statement);
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"deleteTravelWithId" message:sqlErrorMsg];
    }
    
    // Set the last travel added as current travel :
    query = "SELECT MAX(id) FROM TRAVELS";
    
    if (sqlite3_prepare_v2(_db, query, -1, &statement, nil) == SQLITE_OK)
    {
        if(sqlite3_step(statement)==SQLITE_ROW)
        {
            int lastTravel = sqlite3_column_int(statement, 0);
            
            [self setCurrentTravel:lastTravel];
            sqlite3_finalize(statement);
            
            [self notifyDataChanged];
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"deleteTravelWithId" message:sqlErrorMsg];
        }
    }
    else
    {
        sqlite3_reset(statement);
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"deleteTravelWithId" message:sqlErrorMsg];
    }
}

- (void) deleteActivitiesOfTravel:(int)travelId
{
    // Delete the activities :
    const char* query = "DELETE FROM ACTIVITIES WHERE day IN (SELECT id from DAYS where travel = ?)";
    sqlite3_stmt* statement;
    
    if (sqlite3_prepare_v2(_db, query, -1, &statement, nil) == SQLITE_OK)
    {
        sqlite3_bind_int(statement, 1, travelId);
        
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            
            [self notifyDataChanged];
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"deleteActivitiesOfTravel" message:sqlErrorMsg];
        }
    }
    else
    {
        sqlite3_reset(statement);
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"deleteActivitiesOfTravel" message:sqlErrorMsg];
    }
}

- (void) deleteDaysOfTravel:(int)travelId
{
    // Delete the activities :
    const char* query = "DELETE FROM DAYS WHERE travel = ?";
    sqlite3_stmt* statement;
    
    if (sqlite3_prepare_v2(_db, query, -1, &statement, nil) == SQLITE_OK)
    {
        sqlite3_bind_int(statement, 1, travelId);
        
        if(sqlite3_step(statement)==SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            
            [self notifyDataChanged];
        }
        else
        {
            sqlite3_reset(statement);
            NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
            [self displayErrorMessageFrom:@"deleteDaysOfTravel" message:sqlErrorMsg];
        }
    }
    else
    {
        sqlite3_reset(statement);
        NSString* sqlErrorMsg = [[NSString alloc] initWithUTF8String:sqlite3_errmsg(_db)];
        [self displayErrorMessageFrom:@"deleteDaysOfTravel" message:sqlErrorMsg];
    }
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
- (void) notifyDataChanged
{
    // Ask the dropboxHandler to update the your_travels file :
    DBPath* path = [[DBPath root] childPath:@"/your_travels/database/your_travels.sqlite3"];
    [[AppDelegate sharedDelegate].dropBoxHandler updateRemoteFileAtPath:path];
}
// ----------------------------------------------------------------------------------------------------

@end




