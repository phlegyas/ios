//
//  UICustomAlertView.h
//  NewYork
//
//  Created by Tim Autin on 03/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICustomAlertView : UIAlertView
{
    UIScrollView* _scrollView;
}

@property(strong) UIScrollView* scrollView;

- (id) initWithDelegate:(id)delegate;

@end
