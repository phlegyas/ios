//
//  UICustomAlertView.m
//  NewYork
//
//  Created by Tim Autin on 03/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "UICustomAlertView.h"

#define ALERT_VIEW_HEIGHT 350
#define HORIZONTAL_MARGIN 10
#define ALERT_VIEW_PADDING_TOP 5
#define ALERT_VIEW_PADDING_BOTTOM 15


@implementation UICustomAlertView

@synthesize scrollView = _scrollView;


// ----------------------------------------------------------------------------------------------------
#pragma mark - Init function

- (id) initWithDelegate:(id)delegate
{
    self = [super initWithTitle:@"" message:@"" delegate:delegate cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    
    if(self)
    {
        
        
    }
    
    return self;
}
// ----------------------------------------------------------------------------------------------------


- (void) drawRect:(CGRect)rect
{
    // Call super :
    [super drawRect:rect];
    
    // Get the alert view button :
    UIButton* okButton = nil;
    for(UIView* view in self.subviews)
    {
        if([view isKindOfClass:[UIButton class]])
        {
            okButton = (UIButton*)view;
        }
        else if(![view isKindOfClass:[UIImageView class]])
        {
            [view removeFromSuperview];
        }
    }
    
    // Set the ok button y position :
    CGRect okButtonFrame = CGRectZero;
    if(okButton)
    {
        CGRect okButtonFrame = okButton.frame;
        okButtonFrame.origin.y = self.bounds.size.height - okButton.frame.size.height - ALERT_VIEW_PADDING_BOTTOM;
        okButton.frame = okButtonFrame;
    }
    
    
    // Create a scroll view :
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(HORIZONTAL_MARGIN, ALERT_VIEW_PADDING_TOP, self.bounds.size.width - 2*HORIZONTAL_MARGIN, ALERT_VIEW_HEIGHT - ALERT_VIEW_PADDING_TOP - ALERT_VIEW_PADDING_BOTTOM - okButtonFrame.size.height)];
    _scrollView.backgroundColor = [UIColor clearColor];
    [self addSubview:_scrollView];
    
    // Set the scroll view size :
    CGRect scrollViewFrame = _scrollView.frame;
    scrollViewFrame.size.height = self.bounds.size.height - okButton.bounds.size.height - ALERT_VIEW_PADDING_TOP - ALERT_VIEW_PADDING_BOTTOM;
    _scrollView.frame = scrollViewFrame;
}


// ----------------------------------------------------------------------------------------------------
#pragma mark - Show the alert view

- (void) show
{
    // Call super :
    [super show];
    
    // Set the alert view height (if we don't, the popup animation is weird) :
    self.bounds = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, ALERT_VIEW_HEIGHT);
}
// ----------------------------------------------------------------------------------------------------



@end
