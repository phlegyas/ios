//
//  UIImageAlertView.h
//  NewYork
//
//  Created by Tim Autin on 03/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UICustomAlertView.h"

@interface UIImageAlertView : UICustomAlertView
{
    NSString* _title;
    NSString* _description;
    UIImage* _image;
    
    UITextView* _titleTextView;
    UITextView* _descriptionTextView;
}

@property(strong) UITextView* titleTextView;
@property(strong) UITextView* descriptionTextView;

- (id) initWithTitle:(NSString*)title description:(NSString*)description Image:(UIImage*)image delegate:(id)delegate;

@end
