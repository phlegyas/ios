//
//  UIImageAlertView.m
//  NewYork
//
//  Created by Tim Autin on 03/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "UIImageAlertView.h"

#define ALERT_VIEW_HEIGHT 350
#define HORIZONTAL_MARGIN 10
#define ALERT_VIEW_PADDING_TOP 5
#define ALERT_VIEW_PADDING_BOTTOM 15


@implementation UIImageAlertView

@synthesize titleTextView = _titleTextView;
@synthesize descriptionTextView = _descriptionTextView;


// ----------------------------------------------------------------------------------------------------
#pragma mark - Init function

- (id) initWithTitle:(NSString*)title description:(NSString*)description Image:(UIImage*)image delegate:(id)delegate
{
    self = [super initWithTitle:@"" message:@"" delegate:delegate cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    
    if(self)
    {
        _image = image;
        _title = title;
        _description = description;
    }
    
    return self;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Draw the view

- (void) drawRect:(CGRect)rect
{
    // Call super :
    [super drawRect:rect];
    
    // Create the title text view :
    _titleTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, self.scrollView.bounds.size.width, 0)];
    _titleTextView.text = _title;
    _titleTextView.userInteractionEnabled = NO;
    _titleTextView.backgroundColor = [UIColor clearColor];
    _titleTextView.textColor = [UIColor whiteColor];
    _titleTextView.font = [UIFont boldSystemFontOfSize:18.0];
    _titleTextView.textAlignment = UITextAlignmentCenter;
    
    _titleTextView.layer.shadowColor   = [[UIColor blackColor] CGColor];
    _titleTextView.layer.shadowOffset  = CGSizeMake(0.0f, -1.0f);
    _titleTextView.layer.shadowOpacity = 1.0f;
    _titleTextView.layer.shadowRadius  = 0.5f;
    
    [self.scrollView addSubview:_titleTextView];
    
    // Set textview height = textview content height (must be called once the textview has been added to the scrollview) :
    CGRect titleTextViewFrame = _titleTextView.frame;
    titleTextViewFrame.size.height = _titleTextView.contentSize.height;
    _titleTextView.frame = titleTextViewFrame;
    
    
    
    // Create an image view :
    UIImageView* imageView = [[UIImageView alloc] initWithImage:_image];
    if(_image != nil)
    {
        float ratio = _image.size.width / _image.size.height;
        imageView.frame = CGRectMake(0, _titleTextView.bounds.size.height, self.scrollView.bounds.size.width, self.scrollView.bounds.size.width/ratio);
        [self.scrollView addSubview:imageView];
    }
    
    
    
    // Create the description text view :
    _descriptionTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, imageView.bounds.size.height + _titleTextView.bounds.size.height, self.scrollView.bounds.size.width, 0)];
    _descriptionTextView.text = _description;
    _descriptionTextView.userInteractionEnabled = NO;
    _descriptionTextView.backgroundColor = [UIColor clearColor];
    _descriptionTextView.textColor = [UIColor whiteColor];
    _descriptionTextView.font = [UIFont systemFontOfSize:14.0];
    
    _descriptionTextView.layer.shadowColor   = [[UIColor blackColor] CGColor];
    _descriptionTextView.layer.shadowOffset  = CGSizeMake(0.0f, -1.0f);
    _descriptionTextView.layer.shadowOpacity = 1.0f;
    _descriptionTextView.layer.shadowRadius  = 0.5f;
    
    [self.scrollView addSubview:_descriptionTextView];
    
    // Set textview height = textview content height (must be called once the textview has been added to the scrollview) :
    CGRect descriptionTextViewFrame = _descriptionTextView.frame;
    descriptionTextViewFrame.size.height = _descriptionTextView.contentSize.height;
    _descriptionTextView.frame = descriptionTextViewFrame;
    
    
    
    // Set the scroll view content size :
    CGSize contentSize = CGSizeMake(self.scrollView.contentSize.width,
                                        _titleTextView.bounds.size.height +
                                        _descriptionTextView.bounds.size.height +
                                        imageView.bounds.size.height);
    self.scrollView.contentSize = contentSize;
}
// ----------------------------------------------------------------------------------------------------



// ----------------------------------------------------------------------------------------------------
#pragma mark - Show the alert view

- (void) show
{
    // Call super :
    [super show];
    
    // Set the alert view height (if we don't, the popup animation is weird) :
    self.bounds = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, ALERT_VIEW_HEIGHT);
}
// ----------------------------------------------------------------------------------------------------



@end
