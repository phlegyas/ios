
-- ----------------------------------------------------------------------
-- DROP ALL TABLES :
DROP TABLE IF EXISTS LOCATIONS;
DROP TABLE IF EXISTS PAGES;
DROP TABLE IF EXISTS PLANNINGS;
DROP TABLE IF EXISTS CHECKLIST_ITEMS;
DROP TABLE IF EXISTS TRANSLATIONS;
-- ----------------------------------------------------------------------



-- ----------------------------------------------------------------------
-- CREATE TABLES :
CREATE TABLE LOCATIONS ( id INTEGER PRIMARY KEY AUTOINCREMENT, location INT NOT NULL, position INT NOT NULL, title VARCHAR(100) NOT NULL );
CREATE TABLE PAGES ( id INTEGER PRIMARY KEY AUTOINCREMENT, location INT NOT NULL, type VARCHAR NOT NULL, content TEXT );
CREATE TABLE PLANNINGS ( id INTEGER PRIMARY KEY AUTOINCREMENT, page INT NOT NULL, position INT NOT NULL, title VARCHAR NOT NULL );
CREATE TABLE CHECKLIST_ITEMS ( id INTEGER PRIMARY KEY AUTOINCREMENT, page INT NOT NULL, position INT NOT NULL, checked BOOLEAN NOT NULL DEFAULT 0, title VARCHAR NOT NULL, description TEXT );
CREATE TABLE TRANSLATIONS ( id INTEGER PRIMARY KEY AUTOINCREMENT, page INT NOT NULL, position INT NOT NULL, frenchSentence VARCHAR NOT NULL, englishSentence VARCHAR NOT NULL );
-- ----------------------------------------------------------------------



-- ----------------------------------------------------------------------
-- INSERT LOCATIONS (id, location, position, title) :

-- Location 0 (top level) :
INSERT INTO LOCATIONS VALUES ('1', '0', '0', "To know before\nyou leave");
INSERT INTO LOCATIONS VALUES ('2', '0', '2', "To know once\nyou're there");
INSERT INTO LOCATIONS VALUES ('3', '0', '3', "The typical plannings");
INSERT INTO LOCATIONS VALUES ('4', '0', '1', "To do before\nyou leave");
-- INSERT INTO LOCATIONS VALUES ('5', '0', '5', "Guide de\nconversation");
INSERT INTO LOCATIONS VALUES ('6', '0', '4', 'The poster\n"I \ue022 GTNY"');
INSERT INTO LOCATIONS VALUES ('7', '0', '7', "€ - $\nconverter");
INSERT INTO LOCATIONS VALUES ('8', '0', '6', "Taxes & Tips");
INSERT INTO LOCATIONS VALUES ('9', '0', '8', "Usefull numbers\nin New York");

-- Location 1 (L'essentiel avant de partir) :
INSERT INTO LOCATIONS VALUES ('10', '1', '0', "Before taking the plane");
INSERT INTO LOCATIONS VALUES ('11', '1', '1', "About the luggages");
INSERT INTO LOCATIONS VALUES ('12', '1', '2', "Exchanging dollars");
INSERT INTO LOCATIONS VALUES ('13', '1', '3', "Electronics in New York");
INSERT INTO LOCATIONS VALUES ('14', '1', '4', "Transports in New York");
INSERT INTO LOCATIONS VALUES ('15', '1', '5', "Taking pictures");

-- Location 2 (L'essentiel sur place) :
INSERT INTO LOCATIONS VALUES ('17', '2', '0', "Sending a post card");
-- INSERT INTO LOCATIONS VALUES ('18', '2', '1', "Enjoy your travel");
INSERT INTO LOCATIONS VALUES ('19', '2', '2', "Buying souvenirs");
INSERT INTO LOCATIONS VALUES ('20', '2', '3', "Exchanging dollars");
INSERT INTO LOCATIONS VALUES ('21', '2', '4', "More info about New York");

-- Location 3 (Les plannings) :
INSERT INTO LOCATIONS VALUES ('22', '3', '0', "1 day");
INSERT INTO LOCATIONS VALUES ('23', '3', '1', "3 days");
INSERT INTO LOCATIONS VALUES ('24', '3', '2', "1 week");

-- Location 4 (A faire et à voir avant de partir) :
INSERT INTO LOCATIONS VALUES ('25', '4', '0', "1 month before leaving");
INSERT INTO LOCATIONS VALUES ('26', '4', '1', "1 week before leaving");
INSERT INTO LOCATIONS VALUES ('27', '4', '2', "1h before, in the luggage");
INSERT INTO LOCATIONS VALUES ('28', '4', '3', "1h before, in the handbag");

/*
-- Location 5 (Guide de conversation) :
INSERT INTO LOCATIONS VALUES ('29', '5', '0', "Les bases");
INSERT INTO LOCATIONS VALUES ('30', '5', '1', "Les chiffres");
INSERT INTO LOCATIONS VALUES ('31', '5', '2', "Le temps");
INSERT INTO LOCATIONS VALUES ('32', '5', '6', "Discussion avec un américain");
INSERT INTO LOCATIONS VALUES ('33', '5', '7', "Dans la rue");
INSERT INTO LOCATIONS VALUES ('34', '5', '3', "La météo");
INSERT INTO LOCATIONS VALUES ('35', '5', '4', "Les lieux communs");
INSERT INTO LOCATIONS VALUES ('36', '5', '5', "En cas d'urgence");
INSERT INTO LOCATIONS VALUES ('37', '5', '8', "Dans un hôtel");
INSERT INTO LOCATIONS VALUES ('38', '5', '9', "Dans une pharmacie ou chez le médecin");
INSERT INTO LOCATIONS VALUES ('39', '5', '11', "Dans les transports en commun");
INSERT INTO LOCATIONS VALUES ('40', '5', '12', "Dans un taxi");
INSERT INTO LOCATIONS VALUES ('41', '5', '10', "Dans l'avion");
INSERT INTO LOCATIONS VALUES ('42', '5', '13', "Dans un magasin, bar ou restaurant");
INSERT INTO LOCATIONS VALUES ('43', '5', '14', "Dans un magasin");
INSERT INTO LOCATIONS VALUES ('44', '5', '15', "Dans un bar ou un restaurant");
INSERT INTO LOCATIONS VALUES ('45', '5', '16', "Dans une excursion");
INSERT INTO LOCATIONS VALUES ('46', '5', '17', "Dans un spectacle");
*/
-- ----------------------------------------------------------------------



-- ----------------------------------------------------------------------
-- INSERT PAGES (id, location, type, content) :

-- Location 10 (L'essentiel avant de partir -> Avant de prendre l'avion) :
INSERT INTO PAGES VALUES ('1', '10', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>Packing tips for your trip to New York City</h2><p>I want to share my reminder with you, useful when packing you luggage to make sure not to forget anything before heading to the airport.</p><p>First of all, I recommend that you review the baggage allowance on the weight, size and number of bags. It may vary from an airline to another.</p>Now let me tell you what you can’t forget to pack! Ready?</p><h3>What to pack in your carry-on luggage?</h3><ul><li>Passports: that''s the most important!</li><li>Either :</li><ul><li>A copy of your ESTA form if the US requires it</li><li>Your current visa (depending on your nationality)</li></ul><li>A copy of your electronic flight tickets,</li><li>Your vouchers: if you purchased tours or a pass like the NY City Pass, NY PASS ... the vouchers of your hotel or apartment,</li><li>Your dollars, do not put money in your checked baggage,</li><li>Your guidebook,</li><li>The address of your hotel or apartment, with phone number,</li><li>Your laptop,</li><li>Your camera,</li><li>The chargers of your electronic devices. I always have them in my carry-on luggage. If the company lose my checked bag, I can use my devices when arriving,</li><li>The plug adapter, in case you need it, </li><li>Your fragile or valuable personal effects (jewellery, sunglasses ...),</li><li>If you need medication, put them in your carry-on luggage, with the prescription of your doctor especially if it is a liquid medicine,</li><li>A toilet kit with your toothbrush, toothpaste, cream... You can purchase in-flight toiletries kit in most of grocery stores,</li><li>If you want to sleep on the plane, buy some earplugs and a sleep mask,</li><li>All your electronic equipment must be fully charged!! It is a new mandatory safety measure.</li></ul><h3>My best tip for your hand luggage:</h3><ul><li>A power strip: I already told you to purchase a power strip in another NYCTT post. It’s really a good idea because we now travel with a camera, a laptop, a mobile phone... If you want to charge them all at the same time, a power strip will be extremely useful! Moreover, you will need just one plug adapter and not one for each device,</li><li>Extra clothing: it’s useful if your checked baggage get lost at the airports. You can put underwear and a shirt in your hand luggage you may use until they find your luggage,</li><li>A scarf and warm clothing because the air conditioning is very powerful on the plane. It would be a shame to arrive to New York with a cold or a flu and go to the doctor.</li></ul><h3>Your checked baggage</h3><ul><li>A copy of your passport, airline tickets, vouchers... You never know,</li><li>Liquids of more than 100 ml,</li><li>Leave a space in your luggage, in case you want to bring back some purchases of NY, souvenirs or clothing,</li><li>Medicine that you may need (paracetamol, vitamins ...) and a small "first aid" box, with bandages, foot cream, sunscreen...,</li><li>Obviously pack everything you need to dress according to the season. If travelling during the winter, do not forget your scarves and gloves. Take an umbrella when travelling in autumn or spring. If you’re travelling in summer, take light clothing because the weather may be hot at this time,</li><li>For any season, my best advice is to wear sneakers or comfortable shoes.</li></ul><h3>My best tip for your checked baggage:</h3><p>If you’re travelling with another person, a good tip is to pack half of your clothes in a luggage and the other half in your companion’s luggage, particularly underwear and extra clothes. Why? If your bag get lost, you’ll have some clothes in your companion’s luggage!<h3>What to do with your identity cards, driving license ...?</h3><p>A few days before your departure, I recommend scanning your important papers: passport, credit card number, note down the number to call if you lose your credit card...<p>New York is a secure city, but you’d rather be cautious.<p>You can keep copies of these papers and documents on the Internet. I mean, there are now many "places" to save these copies, called Clouds. Google offers this service for example. The best thing is they are free!<p>For anything that might happen to you during your vacation, or even in your daily life, it is very convenient to have access to your personal documents whenever you need them!<p>In fact, there are applications for iPhone, Blackberry and Android: D<h3>My last best tip before you travel</h3><p>You can have up to two hand luggage!</p><p>How?</p><p>Almost all airline companies allow one carry-on for a device (laptop, camera), a purse...</p><p>So I recommend that you have a large hand luggage (as big as allowed by the airline company) and one for your laptop “filled up to the top”! :D</p><p>For example, in my laptop carrying case, I usually put my guidebooks, identifications, and in my other hand baggage I put my camera, basic clothes and underwear ...</p><h3>Conclusion</h3><p>I hope that thanks to this reminder you won’t forget anything for your trip to New York!</p><p>Now if you think I forgot something, feel free to tell me.</p></body></html>');

-- Location 11 (L'essentiel avant de partir -> Concernant les bagages) :
INSERT INTO PAGES VALUES ('2', '11', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>Luggage Allowance</h1><p>Many of you have asked questions about what can you take on a US bound flights regarding your carry-on or weight luggage regulations. </p><p>I''ve written a little reminder on everything you need to know about baggage allowance before flying to New York City. </p><p>For information, rules may differ with airline companies, so make sure to contact them prior travelling. </p><p>So here''s a small recap for some airline companies including: Air France, British Airways, Lufthansa, Continental Airlines (same as United) and Emirates. </p><p>Carry-on luggage:</p><ul><li>As a general rule, carry-on bags must not exceed the following size: approximately 22" x 14" x 9" or 56 x 35 x  23 cm including any handles and wheels. </li><li>Garment bags are considered standard baggage. </li><li>Handbags, computer laptops or cameras can be added to your carry-on luggage. </li><li>The maximum total weight (standard luggage + extra items) is between 5 and 23 kg or 11 and 50 lbs depending on airline companies (Air France 12 kg or 26 lbs, British Airways 23 kg or 50 lbs, Lufthansa 8 kg or 17 lbs, Continental Airlines 18 kg or 39 lbs and Emirates 12kg or 26 lbs). It''s unusual for the staff to weight your carry-on luggage unless it looks really obvious!!! So if you feel that it might be a little too heavy, try not to show it and just pretend everything is ok :) </li><li>Liquids are forbidden in carry-on luggage except in a 100 ml or 3.4 ounces container placed in a plastic ziplock bag. The bag must closed and all articles must fit in easily. One plastic bag per person is allowed. </li><li>Although some liquids are accepted without restrictions:<ul><li>Food and prescribed medicine - note: the security agent might ask for your doctor prescription.</li><li>Special diet food in case of allergies like lactose intolerant, gluten..</li><li>Baby food</li><li>Over the counter medicine (the agent might ask you to taste it)</li><li>Forbidden objects on the plane include: any sharp object (cutter, knives, scissors), firearms, chemical products, inflammable liquids...</li><li>The Best Tip : take a big hand bag that you can use (especially for the return) and computer case where you can store you guidebooks or papers...</li></ul></li></ul><p>Checked Baggage:</p><ul><li>On routes toward the US, most airline companies and especially New York allow a maximum weight of checked baggage of 20 to 23 kg or 44 to 50 lbs depending on the company. </li><li>Your baggage can''t exceed 157 cm or 62 inches when you total length+width+height. </li><li>If your baggage weights more than 23 kg or 50 lbs but below 32 kg or 50 lb, you''ll have to pay an additional fee which vary according to the airline company: Air France 55€ , British Airways $60, Lufthansa $300, Continental Airlines 35€. </li><li>If your baggage weights more than 32 kg or 70 lbs, then you''ll ave to pay a fee for each additional kg or lb. Air France for instance has a flat rate of 300€ or $300. </ul><p>The Good Tip with Air France and Emirates: if you''re a flying blue member (Silver, Gold or Platinum) or member of Skyteam Elite and Elite Plus, they offer the option of checking an additional baggage of 23 kg or 50 lbs!!!! </p></body></html>');

-- Location 12 (L'essentiel avant de partir -> Retirer des dollars) :
INSERT INTO PAGES VALUES ('3', '12', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>The best tips to change your currency before and during your stay in New York</h1><p>Before taking your flight to New York, you must have some dollars in your pockets!</p><p>Don''t worry, there are different ways to get dollars before leaving your country.</p><p>First of all and as I always recommend, it''s important to organize and prepare in advance a trip to New York. You''ll save a maximum of money if you book your visits, attractions, hotel, tours... before heading to NY.</p><p>Since I know that not everything can be purchased in advance online, you also need to come to NYC with a few dollars in your wallet.</p><p>Naturally, when leaving any airport you will have to pay the taxi or metro with cash, unless you already reserved and "prepaid" online your shuttle (<a href="http://http://www.new-york-city-travel-tips.com/summary-bookings-not-to-forget-before-trip/" target="_blank">see more in NYCTT</a>).</p><h3>How can you change dollars before going to New York?</h3><p>You have several options:</p><ul><li>Go to a bank</li><li>Go to a currency exchange office</li></ul><p>First of all, check online the exchange rate between dollars and your national currency.</p><p>If you decide to go to a bank, I recommend going to the bank where you have your savings, because as customers you can buy dollars at best price... But do not hesitate to go to several banks and ask what their exchange rate is, and if you have to pay any commission.</p><p>Indeed, some foreign banks have international agreements with US banks (Bank of America for example) offering the most economical rate. It''s the same with international banks like HSBC.</p><p>I have the same advice with currency exchange offices. If you have an office near your home, check if it has a website. They will have information on its exchange rate you can check from home. :)</p><p>However, you have to be very careful with the exchange offices, especially those in airports: the exchange rate they manage are not so competitive.</p><p>In fact, be very careful with the exchange offices in New York, because they are just waiting for tourists to sell dollars at premium prices...</p><h3>How can you get Travelers Cheques?</h3><p>The Travellers Cheques unfortunately exist anymore since late 2012. </p><p>Since it worked less and less over the credit card, American Express decided to stop selling travelers checks in several countries.</p><h3>Can you pay with your credit / debit card?</h3><p>It seems very practical to make payments with your credit card, as you daily do in your country. But for each payment, your bank may charge fees...</p><p>Now, if you have a Premier / Gold MasterCard or Visa, You won''t be charged commissions. Other banks have similar systems, depending on your home country.</p><p>In both cases, ask your bank for further information before heading to New York!</p><h3>Withdrawing money with your credit card in New York</h3><p>There are two types of ATM’s in the United States: an ATM belonging to a bank or a cash dispenser also called ATM (in stores for example).</p><ul><li>The cash dispenser: Those charge more commissions. Anywhere in New York you will find an ATM. They are very practical when you want to have some cash.<ul><li>An ATM charge $3 to $ 5 each time you withdraw money.</li><li>The ATM can be convenient in case of emergency but is better to withdraw from a bank ATM.</li></ul><li>Bank ATM: In banks, you can withdraw dollars but your bank will charge you the exchange rate.<ul><li>Another good tip is to go to your bank before departing to NY, ask if you have any agreement with an American bank. Such is the case if your bank is international as HSBC, UBS, Citigroup...</li><li>You can also check if there is a branch of your bank in New York. It might be helpful to have the information before going on vacation. In case you have a problem, it will be easier to go to your bank in NY.</li></ul></ul><h3>Conclusion</h3><p>These are the different ways to change your currency or withdraw dollars during your stay in New York. My order of preference to have dollars will be:</p><ul><li>Go to a bank that has an agreement with a US bank in your home country.</li><li>Go to an exchange office in your home country (in all cases verify rates, fees, extra fees charged).</li><li>Withdraw dollars from a US bank.</li><li>Withdrawing money from an ATM.</li><li>Change your currency at the airport.</li><li>Go to a tourist exchange office in New York.</li></ul><p>Before leaving, do not forget to ask your bank about ALL fees and costs they charge, avoid "surprises" coming home and reading your bank statement...</p><h3>Help me!</h3><p>I would appreciate if you can tell me how does it work on your country. These are general recommendations and it will be helpful if you let me know the procedure with your bank. My goal is to help you with my best advices, what if you have the best tips? :D</p><p>Mail NYCTT (nytraveltips@gmail.com) and I will update this article. ;)</p><p>In advance, thank you very much!</p></body></html>');

-- Location 13 (L'essentiel avant de partir -> L'electronique à New York) :
INSERT INTO PAGES VALUES ('4', '13', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>How do your electrical and electronic devices work in New York?</h1><p>Going on vacation means preparing your luggage... without forgetting your electric and electronic devices:</p><ul><li>Phone charger</li><li>Charger for your camera</li><li>Laptop</li><li>Electric shaver</li><li>Hair dryer...</li></ul><p>In addition, from July 8, it is mandatory to have your electronic devices charged and in good condition! </p><h3>The plugs in the United States</h3><p>It is very important to know that plugs in the United States may be different from those used in your home country. So you might need to buy an adapter for your devices for your trip to New York.</p><p>The adapters can be purchased at any special electronic store or grocery supermarkets.</p><p>If you travel a lot to several countries, I recommend that you buy a multi adapter like the one below. But if you only plan to travel to USA, you might purchase a simple adapter, they are even cheaper and they work very well ;)</p><h3>The voltage of your electrical and electronic devices</h3><p>Another important thing, the voltage in USA, 110V, may also be different from your country.</p><p>Before taking your electronic devices to NY, check if there''s a tag indicating "voltage 110/220". If not, your device will "burn" from the moment you plug it in and use it with an unsuitable voltage. There are no more remedies that throwing away your device...</p><p>Here''s my advice: even if your hair dryer indicates 110 / 220V, leave it at home... My wife burned several hair dryers! : D</p><p>Don''t worry, because most hotels provide hair dryers.</p><h3>Do you want another suggestion?</h3><p>Don''t buy an adapter for each electronic device you have. Purchase a power strip and one adapter, plug all your devices to the power strip and use only one adapter. Furthermore, some power strips already have surge protection.</p><p>It''s my best advice to travel safe, with no surprises and no need to buy replacement units in New York... that won''t work at home!</p></body></html>');

-- Location 14 (L'essentiel avant de partir -> Les transports à New York) :
INSERT INTO PAGES VALUES ('5', '14', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>Take a subway or bus ride in New York with the MetroCard</h1><p>The public transportation of New York is suitable if you stay for a week or more in the city.</p><p>I know the best way to see a city is by walking in the streets, but NY is very large, and you’ll get quickly tired :D</p><p>The subway and local buses will help you to go to different points of interest in New York and its boroughs.</p><h3>How is the subway system in New York?</h3><p>There are 22 subway lines in NYC:</p><p>As you can see the colors differ for the southern part of Manhattan.</p><p>The same subway lines with same colors have the same route in a large part of Manhattan, but are divided into sections for the boroughs of NYC (Queens, Bronx, Brooklyn, Staten Island and Harlem).</p><h3>What does "local" and "express" mean?</h3><p>Each subway line has "local" and "express" trains.</p><p>Local: means that the trains stop at every stations without exception. The ride will be a bit slower.</p><p>Express: trains stop every 2, 3, 4 or 5 stations depending on the area of NY.</p><p>The ride is faster since the train doesn’t have to stop to every stations.</p><p>You must take those trains if you want to go rapidly from the North to the South of Manhattan for example.</p><p>Make no mistake and read carefully if the line is an Express or a local one! You will get too far and need to come back… loosing time and money.</p><p>It''s going to happen at least once during your stay.</p><h3>What are the subway and buses fares in New York?</h3><p>The fare for a subway or local bus ride is $2.50, and $6 for an Express Bus ride.</p><h3>What is the "7-Day Unlimited MetroCard"?</h3><p>This MetroCard allows an unlimited number of subway and bus rides for a fixed price during 7 days. You can also choose a 30-Day or 7-Day Express Bus Plus MetroCard.</p><p>It is interesting because in New York, the subway runs 24 hours a day.</p><p>The card can only be used by one person.</p><h3>How much does the "7-Day Unlimited MetroCard" cost?</h3><p>This 7 day MetroCard costs $30. If you spend more days in New York, you can refill it at any MetroCard Vending Machine.</p><p>The 30-day pass is available 30 days, and costs $112.</p><p>The MetroCard costs 1USD. This cost applies since March 2013 to prevent the cards to be used just once and discarded.</p><p>By refilling and reusing your current MetroCard you avoid the new $1.00 “new card fee”.</p><h3>What is the Pay-Per-Ride (Regular) MetroCard?</h3><p>If you decide to purchase a Pay-Per-Ride MetroCard, you can buy as many rides as you want, from $5 to $100 (if you purchase it a Vendor Machine).</p><p>It can be used by up to 4 people at once for the same trip.</p><p>Your MetroCard can hold any combination of unlimited rides and dollar value!</p><p>The number of trips is limited to the amount of money you bought or refilled your MetroCard.</p><h3>Which MetroCard is more suitable?</h3><p>It depends on your stay in New York and your use of public transport!</p><p>For less than 13 rides in a week: - The Pay-Per-Ride costs 10 USD or $ 2.38 per ride.</p><p>For 13 ride per week: - The 7-Day Unlimited costs 30 USD, or $ 2.31 per ride.</p><p>For 20 ride per week: - The 7-Day Unlimited costs 30 USD, or $ 1.50 per ride.</p><p>For 25 ride per week: - The 7-Day Unlimited costs 30 USD, or $ 1.20 per ride.</p><p>Anyway, if you stay a week in New York, do not hesitate to buy an unlimited MetroCard. It’s more profitable than the Pay-Per-Ride after 12 subway or local bus rides.</p><p>If you take a local bus or subway more than 4 times in a day, it is profitable in just 3 days</p><p>The 30-day Unlimited MetroCard is profitable with 45 rides. So if you take 4 times a day the bus or subway, the card is profitable after 12 days.</p><h3>Where to buy the MetroCard?</h3><p>You can purchase it at a MetroCard Vending Machines. I recommend that you pay with cash, but you can also use a debit card.</p><p>You can select your language on the machines ;)</p><p>At a Subway Station Booth, you can purchase your MetroCard too, but they only accept cash.</p><p>In some subway stations you won’t find any Vending Machine or booth, look at the signs when going in the subway station!</p><p>You can purchase a MetroCard in a Bus and Vans. They are in different areas of the city by a defined time, depending on the day.</p><p>Finally, you can buy it in some shops or Neighborhood Merchants. </p><h3>How to purchase a MetroCard at a vending Machine?</h3><p>I you want to pay with cash, it’s not complicated. It’s like a dispenser machine.</p><p>If you want to use your debit card, not from a US Bank, the machine asks for a ZIP Code (a local zip code). For those living abroad you may indicate this code: 99999.</p><h3>What you need to know before taking the subway:</h3><ul><li>If you first used your Unlimited Metrocard, you can’t slide it back within 18 minutes. So when boarding a subway station, make no mistake.</li><li>Some stations are divided in 2 directions (one to Downtown and one Uptown). If you’re going to the wrong direction, you will have to wait 18 minutes before re-using your Metrocard.</li><li>If you have a baby stroller or a large bag that does not pass through the turnstile, take the the emergency exit, right next to the turnstiles. The doors do not open from outside so someone has to go through the turnstile with your MetroCard and then open the emergency exit door. If traveling alone, you can ask a New Yorker who will help you for sure.</li></ul><h3>Comparison between the bus and the subway in New York</h3><p>They are two different means of transport and they don’t offer the same possibilities.</p><ul><li>The Faster: the subway. Is faster than the bus because the latter stops at every block and traffic lights.</li><li>To travel east-west: the bus. Its rides are essentially east-west, where there is no subway.</li><li>The best way to discover New York: the bus. From the subway tunnels you won’t appreciate the cityscape  :D</li><li>The best way to meet New Yorkers: the bus. There are far fewer tourists on the bus.</li><li>The most comfortable: the subway, you always find where to sit, unless if you’re travelling at peak hours.</li><li>The most detailed: the bus because it has more stops than the subway.</li><li>The cheapest: tied because they have the same price ... except the Express Bus.</li></ul><h3>Comparison between the taxi and the subway in New York</h3><p>I often listen that taking rides in taxi is the best in NY.</p><p>I leave you my comparison and let you make up your own idea:</p><ul><li>The Faster: depends where you want to go and if there is traffic. Usually, you can save about 5-10 minutes with the taxi, but during peak hours the metro is much faster.</li><li>The best way to discover NY: The taxi, for the same reasons as the bus.</li><li>The best way to meet New Yorkers: In the taxi, is easy to talk to the driver.</li><li>The most comfortable: I would say the subway. Taxis are comfortable cars but most taxi drivers drive very fast and may even scare you.</li><li>If you’re tired: the taxi. They are everywhere, you can take a ride wherever you want and mostly if you don’t want to walk.</li><li>The more detailed: the taxi, for the same reasons as mentioned above.</li><li>The cheapest: the subway because about 10-15 minutes of a taxi ride costs 6-10 USD.</li></ul><h3>Conclusion</h3><p>You know, visiting New York by walking in the street is the best. However, the city is too big to see everything... The public transportation will be very useful during your vacation.</p><p>Now if you prefer to use a greener transportation, there is Citibike. I explain how it works in NYCTT.</p></body></html>');

-- Location 15 (L'essentiel avant de partir -> Prendre des photos) :
INSERT INTO PAGES VALUES ('6', '15', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>Where to take the best pictures of New York?</h1><p>For photography amateurs, New York is the perfect city to take pictures: thanks to its famous skyscrapers, the New York architecture, the different perspectives of NYC... </p><p>In this article I try to give my best tips to take great photos of New York from different places. I''m not able to indicate all of them but those which draw my attention ;) </p><h3>The view from the streets of New York</h3><p>It’s fun to play with the architecture and places of NYC that look like some movies. </p><p>A street with a good light, some buildings in perspective and some taxis and you have your scenery for a perfect picture of New York. </p><p>In fact I recommend that you take a picture from the ground, it will be as original as bellow, in this Meatpacking District photograph. </p><p>In the streets of New York, the best is to have a wide angle lens, especially when the buildings are very tall or for very wide streets.</p><h3>The view from a building or rooftop</h3><p>New York is a well-known city for its skyscrapers, so if you can take your photos from one of them, you will have some great pictures. </p><p>Take your photograph from a bar like the 230 Fifth Rooftop. </p><p>You can zoom in and take a picture of the Flatiron Building (the triangular building), Madison Square Park (left) and the 230 Fifth Rooftop (bottom right)</p><p>The view from the Top of the Rock is beautiful too, towards the south, you can see the Empire State Building and the Financial District</p><h3>Taking pictures of New York from a boat</h3><p>To be able to take very nice photos of NYC from a ferry, you must take Circle Line boat tour(the New York City Pass). You can also take the ferry to Staten Island, it’s for free.</p><p>It’s interesting how Manhattan "goes away" slowly, with a different sight every 30 seconds... look forward to taking many pictures. </p><p>The evening boat tours are very nice, but to take pictures is more complicated. Indeed, the boat is in motion and it is difficult to take a picture that is not blurry.</p><h3>The view from Central Park</h3><p>In Central Park, I suggest going to five places that have a nice view over the city. </p><p>First, go to the Belvedere Castle, where you see the Turtle Pond, the baseball fields, the park tracks ... </p><p>At the South-East part of the park, there is a rock called "The Roc" from which you can take photos. I love this photo of Gaëtan Habrand. </p><p>From The Pond, there is a small lake south-East of the park which is also a very nice place to take pictures. There is a stone bridge, it’s always beautiful in the winter with snow, in spring or summer with green trees, or in fall with changing colors. </p><p>The Reservoir is a perfect place to run... but also to take pictures! </p><p>The Lake is another spot to take photos, because you can see the boats and the buildings of the Upper West Side in the back. You can also have a break in this area and lunch at the Loeb Boathouse.</p><h3>The view from the High Line</h3><p>The High Line is a visit that I strongly recommend in NYCTT. It is about 10 meters high so you will have a good perspective on the streets of NY and the surrounding areas. </p><h3>The view from the TKTS steps in Times Square</h3><p>Times Square is a mythical place in New York. </p><p>The best is to sit on the red steps of TKTS, the company that sells tickets for Broadway musicals. </p><h3>The view from a helicopter</h3><p>Photographing New York from a helicopter is one of the most exciting ways to take pictures. The flight over the city itself is a great experience, but above all you’ll have an exceptional view for a picture of New York from the sky. It''s something I highly recommend. </p><p>For more information on the different flights, check NYCTT. </p><h3>Photographing the LOVE sculpture</h3><p>This sculpture was created by the American artist Robert Indiana, it’s nice and perfect for a photo, especially with your soul mate :) </p><p>The sculpture is close to the MoMA. </p><h3>The view from the Tudor City Bridge</h3><p>This bridge is located in Midtown, close to Grand Central. It offers a nice view of the street with the Chrysler Building at the bottom right and of Times Square in the back. </p><h3>Photographing NYC during the Manhattanhenge</h3><p>2 times per year, the sun sets right in the middle of the streets of New York. It is a phenomenon that cannot be missed because it looks amazing. </p><h3>The view from the Apple Store in Grand Central</h3><p>Grand Central is one of the must-see places in New York. To see the main hall in one photo, you have to go to the Apple Store. Then you can get the large windows, the American flag and people passing through the station in only one pic. </p><h3>The view from Brooklyn</h3><p>The place that most draw my attention is Brooklyn with the Brooklyn Brooklyn Heights Promenade: It''s a very nice walk of about 4.5 km with exceptional views of Manhattan, the Brooklyn Bridge (Brooklyn Bridge) and the Statue of Liberty. </p><p>That place is easily accessible from Manhattan by subway or walking. </p><p>You may go by night and by day, but if you have to choose, I recommend that you go at night: the illuminated panorama looks amazing. </p><p>From Williamsburg, you’d take nice pictures, for example near the Flea Market and the Water Way. </p><h3>The view from the Queens</h3><p>If you plan to visit the Queens, I recommend going to Gantry Plaza State Park. </p><p>It is a place that gives a nice view of the skyline and Midtown. </p><h3>The view from New Jersey </h3><p>The walk on the riverbanks of Hoboken and Weehawken is very nice. </p><p>In fact, I also have a good tip to dinner at a restaurant in New Jersey in NYCTT. </p><h3>The view from the crown of the Statue of Liberty</h3><p>It is one of the most famous landmarks in the world! You can visit the crown (see here) and take a picture like the one below. I also recommend having a wide angle lens if you want to visit the crown and take picture from there. </p><h3>The photo you can’t forget to take: New Yorkers taxis</h3><p>What is more typical than the yellow cabs of New York? </p><p>You can take a picture of them from several points of view and play with their speed. </p><h3>“Street Photo” in New York</h3><p>For those who want to get more original photos with the people of New York, you will see that you will be able to take many portraits. </p><p>However, I recommend you to be discreet because the guy in the picture did not look very happy :D </p><p>If you like these kind of photos, you can buy the book Humans of New York ;) </p><h3>Taking pictures at night in New York</h3><p>The tripod is mandatory to photograph New York at night. </p><p>You can return to the places you liked during the day or go to one of the places that I recommend above. </p><h3>New york black and white</h3><p>If you want to get original pictures, put your camera in black and white.</p><h3>Take photography lessons in New York</h3><p>If you want to learn how to take pictures with a professional, you could hire a photography lesson with Citifari. </p><p>You will find more information for the day tour and the night tour in NYCTT. </p><h3>The most important thing to know when taking pictures of New York</h3><p>Take all the photos you can, you will never regret because then you can delete the one you don’t like so much. </p><p>Try to get unique and original photos and avoid having the same photos that other tourists. </p><p>If you do not have all the necessary equipment, you can rent it at Adorama in NY. </p><h3>Conclusion</h3><p>I hope that now you know where to take your photos once in New york. </p><p>I invite you to share your photos with the NYCTT community on Facebook, Twitter, use the forum, especially if you find other spots or if you want to take your photo I Love NYCTT ;) </p></body></html>');

-- Location 17 (L'essentiel sur place -> Envoyer une carte postale) :
INSERT INTO PAGES VALUES ('8', '17', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>Postcard</h1><p>On your way to New York soon? Like any of your trip you usually want to send a post card from New York to your family and friends? Here are some easy steps to send a personalized post card.</p><p>I have a really cool tip for you since you can send a personalized post card and much cheaper than a regular one. I can guaranty it that your loved ones will be 10 times more happier than with a regular and classic post card.</p><p>Here’s my explanation of this great tip in 6 steps:</p><h3>1st step: TAKE THE PICTURE</h3><p>Make sure to snap a photo with a famous New York background or in front of a memorable view. As an example, here’s a picture that we took while in New York during Easter break. We were on the rooftop of our apartment located in the East Village.</p><p>I placed the camera on a chair, which was on top of a table, then set the timer, and voilà! A beautiful picture was taken with a great background of Manhattan.</p><p>Of course, you can always ask an American or a tourist de snap a picture you and your other half, your family or yourself on your jour');

-- Location 18 (L'essentiel sur place -> Profiter de votre séjour) :
-- INSERT INTO PAGES VALUES ('9', '18', 'HTML', '');

-- Location 19 (L'essentiel sur place -> Acheter des souvenirs) :
INSERT INTO PAGES VALUES ('10', '19', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>You want to purchase original or classic souvenirs of New York but do not know where to buy them? </h1><p>There are several tourist stores in NYC where you can find typical souvenirs at a good price. </p><h3>Where to buy souvenirs “I Love New York”?</h3><p>The cheapest I could find in New York is the one right next to the Empire State Building, 5th Avenue on the corner of 32nd Street New York Souvenirs. </p><p>You will find everything: New York mugs, pens, notebooks, pins, key chains, "Manhattan Portage" bags, t-Shirts (NYPD, FDNY, I Love NY, ...) Everything you can imagine!</p><p>If you want to give some souvenirs to your family or friends, you will see that the t-shirts “I Love NY” cost 4x$10 :D </p><p>You will also find good souvenir stores in Times Square, China Town (Canal Street), and in the Manhattan Mall. </p><h3>Souvenirs of NYC and USA sport teams:</h3><p>For the NBA and basketball, go to the NBA Store on the 5th Avenue (corner of 52th Street). You''ll find everything you can imagine on the NBA: Boris Diaw of t-shirts, Carmelo Anthony, Lebron James ... Michael Jordan, Magic Johnson... </p><p>For the NHL and Hockey, the Rangers Store is at Madison Square Garden, on the left in the back. </p><p>The NHL Store is located at the corner of 47th Street and Avenue of the Americas. </p><p>The official store of the MLB and Baseball (Yankees and Mets) offer a variety of garments with the logo of both teams. </p><p>You can find Baseball equipment in sports stores. I recommend that you go to Paragon Sport Store (near Union Square) because they have a wide range of accessories for baseball. </p><h3>Where to purchase unique T-shirts?</h3><p>One of my favorite store in New York to buy original t-shirts is <Yellow Rat Bastard.</p><p>It''s on 483 Broadway, New York, NY 10013. </p><h3>Do you want to buy a United States flag?</h3><p>It is not always easy to find places that sell American flags in New York above all when it is not a national holiday. However, you can go to the store which is right across from City Hall Park (between Broadway and Beekman Street). </p><p>Also you will find flags in the Manhattan Mall located on 6th Avenue and 33rd Street. </p></body></html>');

-- Location 20 (L'essentiel sur place -> Retirer des dollars) :
INSERT INTO PAGES VALUES ('11', '20', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>The best tips to change your currency before and during your stay in New York</h1><p>Before taking your flight to New York, you must have some dollars in your pockets!</p><p>Don''t worry, there are different ways to get dollars before leaving your country.</p><p>First of all and as I always recommend, it''s important to organize and prepare in advance a trip to New York. You''ll save a maximum of money if you book your visits, attractions, hotel, tours... before heading to NY.</p><p>Since I know that not everything can be purchased in advance online, you also need to come to NYC with a few dollars in your wallet.</p><p>Naturally, when leaving any airport you will have to pay the taxi or metro with cash, unless you already reserved and "prepaid" online your shuttle (<a href="http://http://www.new-york-city-travel-tips.com/summary-bookings-not-to-forget-before-trip/" target="_blank">see more in NYCTT</a>).</p><h3>How can you change dollars before going to New York?</h3><p>You have several options:</p><ul><li>Go to a bank</li><li>Go to a currency exchange office</li></ul><p>First of all, check online the exchange rate between dollars and your national currency.</p><p>If you decide to go to a bank, I recommend going to the bank where you have your savings, because as customers you can buy dollars at best price... But do not hesitate to go to several banks and ask what their exchange rate is, and if you have to pay any commission.</p><p>Indeed, some foreign banks have international agreements with US banks (Bank of America for example) offering the most economical rate. It''s the same with international banks like HSBC.</p><p>I have the same advice with currency exchange offices. If you have an office near your home, check if it has a website. They will have information on its exchange rate you can check from home. :)</p><p>However, you have to be very careful with the exchange offices, especially those in airports: the exchange rate they manage are not so competitive.</p><p>In fact, be very careful with the exchange offices in New York, because they are just waiting for tourists to sell dollars at premium prices...</p><h3>How can you get Travelers Cheques?</h3><p>The Travellers Cheques unfortunately exist anymore since late 2012. </p><p>Since it worked less and less over the credit card, American Express decided to stop selling travelers checks in several countries.</p><h3>Can you pay with your credit / debit card?</h3><p>It seems very practical to make payments with your credit card, as you daily do in your country. But for each payment, your bank may charge fees...</p><p>Now, if you have a Premier / Gold MasterCard or Visa, You won''t be charged commissions. Other banks have similar systems, depending on your home country.</p><p>In both cases, ask your bank for further information before heading to New York!</p><h3>Withdrawing money with your credit card in New York</h3><p>There are two types of ATM’s in the United States: an ATM belonging to a bank or a cash dispenser also called ATM (in stores for example).</p><ul><li>The cash dispenser: Those charge more commissions. Anywhere in New York you will find an ATM. They are very practical when you want to have some cash.<ul><li>An ATM charge $3 to $ 5 each time you withdraw money.</li><li>The ATM can be convenient in case of emergency but is better to withdraw from a bank ATM.</li></ul><li>Bank ATM: In banks, you can withdraw dollars but your bank will charge you the exchange rate.<ul><li>Another good tip is to go to your bank before departing to NY, ask if you have any agreement with an American bank. Such is the case if your bank is international as HSBC, UBS, Citigroup...</li><li>You can also check if there is a branch of your bank in New York. It might be helpful to have the information before going on vacation. In case you have a problem, it will be easier to go to your bank in NY.</li></ul></ul><h3>Conclusion</h3><p>These are the different ways to change your currency or withdraw dollars during your stay in New York. My order of preference to have dollars will be:</p><ul><li>Go to a bank that has an agreement with a US bank in your home country.</li><li>Go to an exchange office in your home country (in all cases verify rates, fees, extra fees charged).</li><li>Withdraw dollars from a US bank.</li><li>Withdrawing money from an ATM.</li><li>Change your currency at the airport.</li><li>Go to a tourist exchange office in New York.</li></ul><p>Before leaving, do not forget to ask your bank about ALL fees and costs they charge, avoid "surprises" coming home and reading your bank statement...</p><h3>Help me!</h3><p>I would appreciate if you can tell me how does it work on your country. These are general recommendations and it will be helpful if you let me know the procedure with your bank. My goal is to help you with my best advices, what if you have the best tips? :D</p><p>Mail NYCTT (nytraveltips@gmail.com) and I will update this article. ;)</p><p>In advance, thank you very much!</p></body></html>');

-- Location 20 (L'essentiel sur place -> Plus d'infos sur New York) :
INSERT INTO PAGES VALUES ('12', '21', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>Things you should know before traveling to the United-States and especially New York</h1><p>Before going on a new adventure and take your New York flight, there are a couple of little things you must know and that you may not be familiar with. Whether it’s alcohol, tobacco, streets rules, tips or pharmacies… Oh yes, several differences can be acknowledged in the US and it’s a real change with our “old Europe” as they like to call it.</p><h3>Tobacco:</h3><p>You must know that you must be 18 to buy cigarettes, and yes they will ask for your ID.</p><p>If you’re under 18, and happen to smoke in the streets, you could have some trouble with people since here, smoking is not well received.</p><p>In addition, just like many countries in Europe smoking is strictly forbidden in public places including: train stations, bars, restaurants, night clubs, hotels, and more recently, parks and beaches.</p><p>Therefore, your only solution is to smoke in the street. Now, each State in America differs and the law is different from state to state. I know for instance that the state of Oregon;you must stand at least 10 feet (=3 meters) away from public places entrances. In New York, as for now, you are still allowed to smoke outside, except for NYU (New York University) in Greenwich Village and in front of High Schools where you have to be at least 15 feet away from  entrances.</p><h3>Alcohol:</h3><p>Just like tobacco, in order to buy alcohol or enter a bar in New York where it’s being sold, you must be 21 and show your ID.</p><p>Besides, United States forbids any alcohol consumption in the streets including New York. If you want to drink in public, hide yourself (brown paper bag) or go to Las Vegas where it’s permitted!</p><h3>ID Card:</h3><p>During your stay, no need to walk around with your passport. Personally, I use my ID card everywhere in NYC and I usually leave my passport in my suitcase and only take my wallet with my ID card in it.</p><p>The best tip I can give you is to use it as your main identification since it’s pocket sized and most importantly, if you happen to loose it, you’d still be able to leave the US with your passport.</p><p>For the anxious or super organized ones (depending on how you see things), make a photocopy of your passport ahead of your trip and leave it with your partner or friend’s bag.</p><h3>Pedestrian Crossing:</h3><p>In United States, drivers usually respect the road very well (with some minor exceptions in Brooklyn) unlike some other countries. Therefore, you’ll never see a driver passing through a red light. Also, you’d rarely see a pedestrian crossing a red light. Therefore, you can not behave just like you do in Europe (hint: France, Italy, Spain..) and force your way to cross in New York. And if you do, I can tell you that both car drivers and cabs will honk at you aggressively. No and no, in NYC, the lights are there for a reason and if it’s green for pedestrians then they can cross, if it’s red, pedestrians will have to let the cars go and wait.</p><p>Lastly, since there are always tourists who do not understand it the first time: for pedestrians, the lights turn green and it’s good to go but after a few seconds, however, with certain lights, a countdown appears like this: “10, 9, 8, 7, 6… 1”. Of course, this means that you have a certain amount of seconds to hurry and cross the street. And not the contrary, as a friend of mine experienced while in Seattle (just like NY);she waited for the countdown to reach 0 and started crossing. She understood very quickly her mistake while she was stuck in the middle of the street with cars passing by and honking in harmony!!!!  </p><p>So pay attention to those little details while in New York. </p><h3>Pharmacies:</h3><p>In United States, if you catch a small thing like a cold, a cough, a sore throat or a stomach virus.. don’t go to the doctor, it going to cost you an arm and a leg. </p><p>However, you can always visit a pharmacy where there’s always a pharmacist to advise you. In United States and especially in New York, pharmacies are often located inside department stores where they share their shelves with food. However, shelved medicine is always available without prescription and you can choose anything without restriction expect for cough syrup where you’d have to show your ID. So, it’s a pretty easy and inexpensive solution if you happen to catch a cold because of the AC on the plane </p><h3>Tips:</h3><p>You must know that in restaurants and bars, waiters barely receive a salary. Therefore, in United States, you pay 15% of the total of your bill in tips. Now, according to certain states and cities, it may vary a little.</p><p>Back in the days, while New York and Portland, we used to tip 5% when very unhappy, 10% for an ok service and 15% for top-notch service. Nowadays, it’s more like 10% for lousy service, 15% when it’s so so service and 20% when for perfect service.</p><p>Lastly, regarding the waiter, like I said, they are paid because of tips. This way, you’ll see everything they do to make you happy: smile, kindness and politeness.. in other words, all this attention in order to appear to be the perfect waiter. </p><h3>Salesman and waiters:</h3><p>You’ll notice that as soon as you enter a store, most salesmen will come to you with a “Hey Guys” or “How are you today?” Then, if you never travel to America (same story in Canada), you could also see the salesman or the waiter introducing himself with his first name. It’s a way of being friendly with the customer, which is really pleasant after all.</p><p>So don’t be surprised gentlemen, if you see a beautiful saleswoman telling you: “how are you doing?”, smiles at you, ask if they can help you and she’ll finally tell you that her name is “Brenda”!!! </p><p>There we are, we rounded some really good tips you should know before getting off the plane at the airport and remember that things are different in the US especially in New York.</p></body></html>');

-- Location 22 (Les plannings -> Planning 1 jour) :
INSERT INTO PAGES VALUES ('13', '22', 'PLANNING', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>Visit New York in 1 day: it’s possible!!!</h1><p>When I say “visit New York in 1 day: it’s possible”, of course you won’t be able to see as much as you do in 1 or 2 weeks or even a month. And yes, I agree, I must admit, it’s a bit presumptuous.</p><p>Nonetheless, I can guaranty you that’s possible to see the majority of the attractions that are the symbols of the city of New York in organizing a one-day tight planning. This is going to be a very busy and exhausting day, but trust me you’ll be amazed!!!</p><p>So yes, it’s possible to visit New York in 1 day!!!</p><p>Ready for 24 hours of craziness in NY? icon smile Visiter New York en 1 jour : c’est possible !!!</p><p>Here is my plan proposal to visit New York in one day: First of all, you’ll need to wake up early and go to bed late to see everything I’m proposing you. I’m not giving you a precise time but the earlier the better especially if you want to see the sunrise. We can say that around 7 am is a good time (I can see sleepy eyes icon smile Visiter New York en 1 jour : c’est possible !!!). Besides, if you just arrived to the US from another country, you might be jetlag especially if you’re flying from Europe so rising up early will be easier.</p><p>You’ll have to invest in a pair of good walking shoes, your bag must contain a water bottle and a camera at hand ready to snap some photos at any moment to keep memories of that unforgettable day.</p><p>Since, you’ll be taking the subway a number of times, make sure to take the one day metrocard!!!</p><p>I’m proposing you a visit of New York from the bottom to the top, meaning from lower Manhattan to Harlem.</p><h3>Part 1</h3><p>You’ll have to head to White Hall Terminal (South Ferry) to take the Staten Island Ferry which is a free shuttle. You can take a look on their website, it operates 24h, every 15 minutes in the morning hours.</p><p>This way, you’ll be able to see the Statue of Liberty, Ellis Island, Governor Island, and underneath the Brooklyn Bridge. Of course, on your way, you’ll enjoy Manhattan and its skyscrapers fading away and vice versa on your way back. It’s truly magical!!!!</p><p>This trip is the perfect occasion to snap some beautiful pictures.</p><h3>Part 2</h3><p>On your way back from the ferry ride, walk toward the Financial District. You’ll pass by Wall Street and its bull that represents the strength of Wall Street. You will also see the imposing building of the New York Stock Exchange along with its huge flag, then the sadly notorious Ground Zero, close by, if you’re looking for a good bargain try Century 21 department store.</p><p>Nearby, you could also visit St. Paul’s Chapel and discover City Hall and its small park.</p><h3>Part 3</h3><p>From City Hall, take Broadway and you’ll find out how long Broadway stretches out, it’s actually one of the biggest streets of United-States.</p><p>Walk from Broadway to Canal Street to enter the heart of Chinatown, New York’s Chinese neighborhood. On Canal Street, you’ll notice that dozen of shops sell similar things. Try not to stay for too long and don’t get ripped off in buying a fake Ipod or perfume.</p><p>Head North by Mulberry Street to enter Little Italy. Over the years, the neighborhood has gotten smaller and smaller and it’s never easy to find it.</p><p>While walking on Mulberry Street starting from Broome Street, you’ll enter Nolita which means (North of Little Italy).</p><p>In Petrosino Square, you can have a breakfast, a snack or taste the best Cheesecake at Eileen Cheesecake (article here) while you’re there. You’ll quickly notice that this is a real New York great tip!!!!!</p><p>Head North by Lafayette Street to discover two other neighborhoods: Soho then NoHo (for the story it’s “South of Houston street” and “North of Houston Street). You’ll notice that the neighborhood has very cute small stories buildings with shops, restaurants and bars. It’s a really lively and fantastic neighborhood to live even for a short term stay.</p><p>Starting from 4th avenue, head west toward the neighborhood of Greenwich Village and discover the beautiful Washington Square Park with its Arch dedicated to Georges Washington. The square is actually surrounded by schools and notably New York University (NYU), school of the wealthy and intellectuals.</p><p>After taking few pictures and crossing the square, head north starting from 5 th avenue to 14th street to find my favorite place in New York: Union Square.</p><p>You’ll see that this part of New York is diverse and holds many protests. Besides, if you pass by the farmer’s market, use this opportunity to take a look at the local organic products being sold.</p><p>If your feet start hurting, my tip for you is to take a break, seat on one of the benches then watch and feel New York. You’ll see how NYC is the reflection of its people, a little bit of everything.</p><p>After a 15 minutes break, head to the Flatiron District home of the Flatiron Building, you must know what I’m talking about, it’s the triangle shaped building (shown in picture).</p><p>In the meantime, discover the Madison Square Park and its famous restaurant the “Shake Shack” (article here). If you’re staying in New York for a couple of days, I will advise you to wait on the line in order to taste the best burger in New York but since it’s usually crowded, it’s wiser to eat somewhere else.</p><p>My advice is to eat quickly by ordering a sandwich, hotdog or burger in a fast-food restaurant. There are a number of them around Madison Square Park.</p><h3>Part 4</h3><p>Rest your feet by taking the N or R train at the 23rd St. station, take the train uptown and get off at the 49th St. station.</p><h3>Part 5</h3><p>Now, you are in Midtown. Take 49th street toward east on your way to Rockefeller Center.</p><p>After walking passed 6th avenue, 150 meters on your left you’ll see the frontcourt of the Rockefeller Center.</p><p>This is the place where the famous Christmas tree is placed during the Holidays and so is the ice rink.</p><p>According to the time of the year you’re travelling, the attractions on the frontcourt may vary but you can still appreciate the heights of the buildings surrounding you, especially the Top of the Rock!!</p><p>After snapping some pictures, head to 50th street between 5th and 6th avenues to enter the Top of the Rock and discover NYC from a different angle (article here).</p><p>From above, you’ll admire Central Park, the Chrysler Building, needless to say, an unforgettable view!!</p><p>After coming down from the sky, walk toward 5th avenue and discover all the fancy stores.</p><p>While walking on 5th avenue, you will see St Patrick’s Cathedral in front of you. Enter for a little tour but don’ stay too long.</p><p>When heading north on 5th avenue, you’ll pass by the NBA store, the very hip Abercrombie &amp;Fitch and some very posh addresses including: G. Armani, Louis Vuitton, Prada..</p><p>While at the Pulitzer Fountain’s place, you’ll notice a big toy store FAO &amp;Schwartz (a must see: article here), and also the most famous Apple Store in the world. On your left, you will recognize a notorious hotel that you must have seen in many movies: the New York Plaza Hotel. Across the street, you’ll find the entrance of Central Park.</p><p>Cross the street and enter Central Park, the heart of the City of New York.By taking the small path heading north of Central Park, you’ll be able to see the well known “The Pond” seen in many movies. Continuing north you’ll find the beautiful Belvedere Castle overlooking part of Central Park. Once again, this is your chance to take some amazing pictures.</p><p>Continue your stroll heading west and pay attention not to get lost between the different paths. At the exit, I have shown you on the map the way to reach the New York’s Museum of Natural History. Unfortunately, you won’t have enough time to visit the museum.</p><h3>Part 6</h3><p>On the other hand, take the “A”, “B”, or “C” train at the 81th Street station and get off at 116th Street.</p><h3>Part 7</h3><p>When getting off the train station, you’ll be in the heart of Harlem!!! Don’t panic, as I explained in this article, Harlem is now a very safe neighborhood. You can safely walk around in the mythical streets of Harlem which became famous with Martin Luther King and Malcolm X. You’ll pass by different churches, the Marcus Garvey Park and the legendary Apollo Theatre, a place where the most famous African-American singers performed!!!</p><p>You can stop by and take some beautiful pictures and pretend that you are acting in an old American movie!!! hahaha icon smile Visiter New York en 1 jour : c’est possible !!!</p><h3>Part 8</h3><p>After discovering Harlem, take the “A” or “C” train at 125th Street to downtown and stop at 50th Street (careful not to take the orange “B” or “D” train).</p><h3>Part 9</h3><p>This is the last part of the day. On 50th Street, walk until you’ve reached Broadway and discover the most craziest thing New York has ever created: Times Square!!!!</p><p>What can I say about Times Square, other that there is no other place like this!!! Walk south of Times Square and while walking watch your step while looking around at all the attractions in an over crowded “Times Square” icon smile Visiter New York en 1 jour : c’est possible !!!</p><p>Once arrived at the corner of 36th street and Broadway, head west and 7th avenue, also known as “Fashion Avenue”. While walking, you’ll arrive at 34th street, take the east side to discover one of the biggest stores in the world: the legendary Macy’s. The neighborhood has reorganized its sidewalks to accommodate the flux of people walking around this shopping area. You’ll notice a majority of clothing stores.</p><p>If you look up on east 34th street, you’ll see the Empire State Building. Precisely, go ahead, enter and discover New York!!!!</p><p>I won’ t know the exact time when you’ll be reaching the top of the Empire State Building but if you go there on the late side, you’ll probably encounter less people waiting in lines. Besides, you should experience New York at night since you’ve already seen the view in daylight from the Top of the Rock. Ideally, try to get there after 10pm knowing that the Empire State building closes at 2 am.</p><p>If it’s too early and depending on your time, go for a quick dinner or not.</p><h3>Conclusion</h3><p>Once you’ve reached the rooftop of Manhattan, you’ll be then done with your “day trip”, in other words your journey in New York!!!</p><p>I hope that you’ve liked this itinerary.</p><p>Don’ t hesitate to share any ideas, information, tips or other you may have since we can always improve.</p><p>Meanwhile, I’m sure that after a day like this, your body will only ask you one thing: SLEEP and REST!!!! icon smile Visiter New York en 1 jour : c’est possible !!!</p><p>Good night to you, and I really hope that you’ve taken some amazing pictures and that you’ll have all the time in the world to enjoy them on your way back. If your trip was a success and you’ve kept fond memories of New York, try to come back for a longer time and discover the “Big Apple” at your own pace!!!</p></body></html>');

-- Location 23 (Les plannings -> Plannings 3 jours) :
INSERT INTO PAGES VALUES ('14', '23', 'PLANNING', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>See the best of New York in 3 days</h1><p>The bet of visiting New York in 3 days is something doable, even if obviously you won’t be able to see and visit everything.</p><p>I’m not only suggesting one plan for 3 days but 4 different plans. But, in order to spend 3 days in New York, you’ll have to think strategically and make choices!!!</p><p>Therefore, you’ll have to choose what type of plan works for you the best.</p><p>Before anything, I’ll have to tell you few things along with the best deals that can’t be missed.</p><h3>Visiting New York in 3 days means:</h3><ul><li>Intensive walking: have some good walking shoes</li><li>Cut the chase: prepare your planning ahead so you won’t be forgetting something</li><li>No time wasted waiting in line and commute: book online ahead of your trip (taxi shuttle, NY City Pass, helicopter tour..) see article: Summary of your booking ahead of your trip</li></ul><p>After careful consideration, the plans will finally be declined in 4 types of plans:</p><h3>The Good Tip type of plan to visit New York in 3 days in “CULTURE” edition:</h3><p>This plan is for those who can’t imagine going to New York without visiting at least 2 to 4 big museums such as: the MET, the MOMA, the Museum of Natural History and the Guggenheim.</p><h3>The Good Tip type of plan to visit New York in 3 days “STREET” edition:</h3><p>This planning is dedicated to those who are not museum fans, but want to discover the streets of New York and its skyscraper.</p><h3>The Good Tip type of plan to visit New York in 3 days “ENJOY” edition:</h3><p>This planning are for those who wants to really enjoy their stay to the fullest with one thing in mind “pleasure” whether it’s with a helicopter tour, a bus tour to spare you time walking, therefore giving you more time shopping!!!!</p><h3>The Good Tip type of planning to visit New York in 3 days “NEVER TIRED” edition:</h3><p>This planning is dedicated to those just like me!!! I must admit, my friends happen to call me the “Makina” (aka the machine) because I gave them the impression of being never tired and always want to see more and more :) :). And yes, in 3 days, if you’re really motivated just like me, you can combine the CULTURAL, STREET and ENJOY editions!!!! :D :D</p><h3>Why 4 different plans 3 Days in New York?</h3><p>Because each of us have different desires, passions and tastes.</p><p>This way, each can see in which type of planning they belong, and of course, you’ll be able to customize these plans as you wish ;)</p></body></html>');

-- Location 24 (Les plannings -> Plannings 1 semaine) :
INSERT INTO PAGES VALUES ('15', '24', 'PLANNING', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>A week planning to visit New York: 4 different types of plan</h1><p>Going to New York for a week? Then, I advise you to plan ahead and organize yourself wisely.</p><p>You don’t have to follow the plans step by step, but this will come handy in case you might be forgetting something and allow you to discover 3 museums and 5 neighborhoods.</p><p>On top of it, I’m not only suggesting one type of planning but 4!!!!</p><h3>A “Cultural” planning, a “Street” planning, an “Enjoy” planning and a “Never Tired” planning.</h3><p>So you’ll be able to choose whether you are more likely the type “Enjoy”, “Street”, “Cultural” or just like me “Never Tired” according to your wishes, what you want to see of New York City and your passions…</p><p>Foremost, just like anywhere on my blog, the proposed plans are only suggestions from my part. You are welcome to change and customize these plans as you wish.</p><p>Here are 4 plans declined in 4 options:</p><h3>The good tip type of plan to visit to New York in one week “CULTURAL” edition:</h3><p>This plan is for those who can’t imagine going to New York without visiting at least 4 big museums such as: the MET, the MOMA, the Museum of Natural History and the Guggenheim.</p><h3>The good tip type of plan to visit New York in one week “STREET” edition:</h3><p>This plan is dedicated to those who are not museum fans, but want to discover the streets of New York, its architecture and New Yorkers in their daily lives.</p><h3>The good tip type of plan to visit New York in one week “ENJOY” edition:</h3><p>This plan is for those who really want to optimize their stay to the fullest with one thing in mind “pleasure” whether it’s with a helicopter or bus tour to spare you time walking. The options are limitless: enjoy a diner cruise on a yacht or a photo shoot in Central Park and give yourself more time for shopping!!!!</p><h3>The good tip type of plan to visit New York in one week “NEVER TIRED” edition:</h3><p>This plan is dedicated to people just like me!!! I must confess, my friends happen to call me the “Makina” (aka the machine) because I gave them the impression of being never tired and always want to see more J J. And yes, in 3 days, if you’re really motivated just like me, you can combine the CULTURAL, STREET and ENJOY editions!!!!</p><p>This plan is for those who just like me don’t want to miss anything on their trip to New York. People who think that resting while travelling is a waste of time especially when visiting a city like New York. Of course, you’ll have plenty of time to rest at night;meanwhile, your schedule will be full in order to experience NEW YORK to the FULLEST. And if you’re motivated, you could do the CUTURAL, STREET and ENJOY editions all at once!!!! icon biggrin Planning pour une semaine à New York : 4 plannings types différents</p><h3>Why 4 different plans?</h3><p>Because each of us have different desires, passions and tastes.</p><p>This way, each can see in which type of planning they belong, and of course, you’ll be able to personalize these plans as you wish icon wink Planning pour une semaine à New York : 4 plannings types différents</p></body></html>');

-- Location 25 (A faire et à voir avant de partir -> 1 mois avant de partir) :
INSERT INTO PAGES VALUES ('16', '25', 'CHECKLIST', '');

-- Location 26 (A faire et à voir avant de partir -> 1 semaine avant de partir) :
INSERT INTO PAGES VALUES ('17', '26', 'CHECKLIST', '');

-- Location 27 (A faire et à voir avant de partir -> 1h avant de partir, dans sa valise) :
INSERT INTO PAGES VALUES ('18', '27', 'CHECKLIST', '');

-- Location 28 (A faire et à voir avant de partir -> 1h avant de partir, dans son bagage à main) :
INSERT INTO PAGES VALUES ('19', '28', 'CHECKLIST', '');

-- Location 5 (Plus d'informations sur New York) :
-- INSERT INTO PAGES VALUES ('20', '5', 'HTML', '');

-- Location 6 (L'affiche "I love Bons Plans New York") :
INSERT INTO PAGES VALUES ('21', '6', 'FULLSCREEN_IMAGE', '');

-- Location 7 (Conversion Euro - Dollar) :
INSERT INTO PAGES VALUES ('22', '7', 'CURRENCY_CONVERTER', '');

-- Location 8 (Les pourboires) :
INSERT INTO PAGES VALUES ('23', '8', 'TIPS_COMPUTER', '');

-- Location 9 (Numéros utiles à New York) :
INSERT INTO PAGES VALUES ('24', '9', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>Useful Numbers</h1><h3>How to call home from New York?</h3><p>Dial "011+country code*+ phone number" (you can replace 011 with a plus sign)</p><p>* Country codes :</p><ul><li>Australia: +61</li><li>Belgium: +32</li><li>Brazil: +55</li><li>Canada: +1</li><li>Denmark: +45</li><li>Finland: +358</li><li>Germany: +49</li><li>Ireland: +353</li><li>Italy: +39</li><li>Netherlands: +31</li><li>Norway: +47</li><li>South Africa: +27</li><li>Spain: +34</li><li>Sweden: +46</li><li>United Kingdom: +44</li></ul><h3>If you want to receive a phone call in New York:</h3><p>If someone wants to call to your mobile phone, the person will have to add the country code to your regular phone number. The cost of calls will be more expensive depending on your company, any doubt, ask your service provider before traveling to New York.</p><p>If someone wants to call to your hotel, the person will have to dial: "00 + 1 + city code (without the first 1) + Phone number.</p><h3>US Local Search:</h3><p>411 (free in public phones)</p><h3>Customs:</h3><p>(212) 466-5550</p><h3>Transit Authority Travel Info:</h3><p>(718) 330-1234</p><h3>Visitor''s Bureau:</h3><p>(212) 397-8222</p><h3>Emergencies and hospitals:</h3><ul><li>Police, Ambulance, Fire: 911 only in case of emergency</li><li>Metropolitan Hospital Center (1901 First Avenue, NYC): 1 (212) 230-6262</li><li>Bellevue Hospital Center (462 First Avenue, NYC): 1(212) 562-4141</li></ul><h3>Poison Control:</h3><p>(212) 764-7667</p><h3>Lost and Found </h3><ul><li>Subway and buses Lost & Found: (212) 712-4500 or (718) 625-6200 (from 8h to 12h on Monday through Friday, on Thursday from 11h to 18h30)</li><li>Taxi Lost & Found: 311 or (212) 639-9675 or (212) 302-TAXI</li></ul><h3>Customer Assistance Service:</h3><ul><li>Visa: 001-303-967-1096</li><li>Mastercard: 011-33-1-4516-6565</li></ul><h3>Please note:</h3><p>Some phone numbers have words on it, for example "1-800-taxicab". Any key on a phone has digits and letters, so to call taxicab, dial: 1-800-8294222.</p></body></html>');
-- ----------------------------------------------------------------------



-- ----------------------------------------------------------------------
-- INSERT PLANNINGS (id, page, position, title) :

-- Page 13 (Les plannings -> Plannings 3 jours) :
INSERT INTO PLANNINGS VALUES ('1', '14', '0', 'Cultural planning');
INSERT INTO PLANNINGS VALUES ('2', '14', '1', 'Enjoy planning');
INSERT INTO PLANNINGS VALUES ('3', '14', '2', 'Street planning');
INSERT INTO PLANNINGS VALUES ('4', '14', '3', 'Never tired planning');

-- Page 14 (Les plannings -> Plannings 1 semaine) :
INSERT INTO PLANNINGS VALUES ('5', '15', '0', 'Cultural planning');
INSERT INTO PLANNINGS VALUES ('6', '15', '1', 'Enjoy planning');
INSERT INTO PLANNINGS VALUES ('7', '15', '2', 'Street planning');
INSERT INTO PLANNINGS VALUES ('8', '15', '3', 'Never tired planning');
INSERT INTO PLANNINGS VALUES ('9', '15', '4', 'Honey moon planning');
-- ----------------------------------------------------------------------



-- ----------------------------------------------------------------------
-- INSERT CHECKLIST_ITEMS (id, page, position, checked, title, description) :

-- Page 15 (A faire et à voir avant de partir -> 1 mois avant de partir) :
INSERT INTO CHECKLIST_ITEMS VALUES ('1', '16', '0', '0', 'Visa', "Apply for your visa (selected countries)");
INSERT INTO CHECKLIST_ITEMS VALUES ('2', '16', '1', '0', 'Housing', "Book your hotel or apartment");
INSERT INTO CHECKLIST_ITEMS VALUES ('3', '16', '2', '0', 'Flight', "Book your flight tickets");
INSERT INTO CHECKLIST_ITEMS VALUES ('4', '16', '3', '0', 'NBA, NFL, MLB, ...', "Purchase your tickets to see an American sports match");
INSERT INTO CHECKLIST_ITEMS VALUES ('5', '16', '4', '0', 'Broadway', "Book a Broadway show or purchase your tickets for a concert");
INSERT INTO CHECKLIST_ITEMS VALUES ('6', '16', '5', '0', 'Dollars', "Order dollars");

-- Page 24 (A faire et à voir avant de partir -> 1 semaine avant de partir) :
INSERT INTO CHECKLIST_ITEMS VALUES ('7', '17', '0', '0', 'Adapter', "Buy a power adapter if different from the US");
INSERT INTO CHECKLIST_ITEMS VALUES ('8', '17', '1', '0', 'New York City Pass', "Purchase a New York City Pass");
INSERT INTO CHECKLIST_ITEMS VALUES ('9', '17', '2', '0', 'Attractions', "Book the activities you plan to do in NYC, helicopter, tours, ...");
INSERT INTO CHECKLIST_ITEMS VALUES ('10', '17', '3', '0', 'Shuttle', "Book a Taxi Shuttle or a Limo :-)");
INSERT INTO CHECKLIST_ITEMS VALUES ('11', '17', '4', '0', 'ESTA', "Apply for your ESTA on line and print the receipt (selected countries)");
 
-- Page 26 (A faire et à voir avant de partir -> 1h avant, dans sa valise) :
INSERT INTO CHECKLIST_ITEMS VALUES ('12', '18', '0', '0', 'Copies', "Copy of your passports, airline tickets, vouchers, ...");
INSERT INTO CHECKLIST_ITEMS VALUES ('13', '18', '1', '0', "Adapter", "Electrical adapter if different from the US");
INSERT INTO CHECKLIST_ITEMS VALUES ('14', '18', '2', '0', 'Sunglasses ', "Very useful on sunny days");
INSERT INTO CHECKLIST_ITEMS VALUES ('15', '18', '3', '0', "Partner's Clothing", "Pack half of your luggage with your personal belongings and the other half with your companion belongings, just in case your luggage got lost");
INSERT INTO CHECKLIST_ITEMS VALUES ('16', '18', '4', '0', 'Clothing', "Pack your clothes according to the weather and don't forget to take comfortable shoes");

-- Page 27 (A faire et à voir avant de partir -> 1h avant, dans son bagage à main) :
INSERT INTO CHECKLIST_ITEMS VALUES ('17', '19', '0', '0', 'Passport', "Don't forget your passport");
INSERT INTO CHECKLIST_ITEMS VALUES ('18', '19', '1', '0', 'ID', "Don't forget your ID, it will be useful to take it with you and safer to leave your passport in your luggage");
INSERT INTO CHECKLIST_ITEMS VALUES ('19', '19', '2', '0', 'Copy of the airline tickets ', "As a precaution, to prove that you booked your flight");
INSERT INTO CHECKLIST_ITEMS VALUES ('20', '19', '3', '0', 'Copy of the hotel reservation', "As a precaution, to prove that you booked a room");
INSERT INTO CHECKLIST_ITEMS VALUES ('21', '19', '4', '0', 'Vouchers', "Voucher of the activities booked (NBA tickets, shuttle, New York City Pass ...)");
INSERT INTO CHECKLIST_ITEMS VALUES ('22', '19', '5', '0', 'Fully charged camera', "Pack it in your carry-on to avoid any degradation in your checked baggage");
INSERT INTO CHECKLIST_ITEMS VALUES ('23', '19', '6', '0', 'Adapter / Power strip ', "If your luggage got lost, you will be able to use your electronic devices, phone, IPad, ...");
INSERT INTO CHECKLIST_ITEMS VALUES ('24', '19', '7', '0', 'Chargers', "Chargers for your phone, camera, ...");
INSERT INTO CHECKLIST_ITEMS VALUES ('25', '19', '8', '0', 'Dollars', "Keep your money safe in your wallet");
INSERT INTO CHECKLIST_ITEMS VALUES ('26', '19', '9', '0', 'Warm clothing', "Take warm clothing so you won't catch cold with the A/C in the plane");
-- ----------------------------------------------------------------------



-- ----------------------------------------------------------------------
-- INSERT TRANSLATIONS (id, location, page, frenchSentence, englishSentence) :
/*
-- Location 29 (Guide de conversation -> Les bases) :
INSERT INTO PAGES VALUES ('25', '29', 'TRANSLATION', '');

-- Location 30 (Guide de conversation -> Les chiffres) :
INSERT INTO PAGES VALUES ('26', '30', 'TRANSLATION', '');

-- Location 31 (Guide de conversation -> Le temps) :
INSERT INTO PAGES VALUES ('27', '31', 'TRANSLATION', '');

-- Location 32 (Guide de conversation -> Discussion avec un américain) :
INSERT INTO PAGES VALUES ('28', '32', 'TRANSLATION', '');

-- Location 33 (Guide de conversation -> Dans la rue) :
INSERT INTO PAGES VALUES ('29', '33', 'TRANSLATION', '');

-- Location 34 (Guide de conversation -> La météo) :
INSERT INTO PAGES VALUES ('30', '34', 'TRANSLATION', '');

-- Location 35 (Guide de conversation -> Nom des lieux communs) :
INSERT INTO PAGES VALUES ('31', '35', 'TRANSLATION', '');

-- Location 36 (Guide de conversation -> En cas d'urgence) :
INSERT INTO PAGES VALUES ('32', '36', 'TRANSLATION', '');

-- Location 37 (Guide de conversation -> Dans un hôtel) :
INSERT INTO PAGES VALUES ('33', '37', 'TRANSLATION', '');

-- Location 38 (Guide de conversation -> Dans une pharmacie ou chez le médecin) :
INSERT INTO PAGES VALUES ('34', '38', 'TRANSLATION', '');

-- Location 39 (Guide de conversation -> Dans les transports en commun) :
INSERT INTO PAGES VALUES ('35', '39', 'TRANSLATION', '');

-- Location 40 (Guide de conversation -> Dans un taxi) :
INSERT INTO PAGES VALUES ('36', '40', 'TRANSLATION', '');

-- Location 41 (Guide de conversation -> Dans l'avion) :
INSERT INTO PAGES VALUES ('37', '41', 'TRANSLATION', '');

-- Location 42 (Guide de conversation -> Dans un magasin, bar ou restaurant) :
INSERT INTO PAGES VALUES ('38', '42', 'TRANSLATION', '');

-- Location 43 (Guide de conversation -> Dans un magasin) :
INSERT INTO PAGES VALUES ('39', '43', 'TRANSLATION', '');

-- Location 44 (Guide de conversation -> Dans un bar ou un restaurant) :
INSERT INTO PAGES VALUES ('40', '44', 'TRANSLATION', '');

-- Location 45 (Guide de conversation -> Dans une excursion) :
INSERT INTO PAGES VALUES ('41', '45', 'TRANSLATION', '');

-- Location 46 (Guide de conversation -> Dans un spectacle) :
INSERT INTO PAGES VALUES ('42', '46', 'TRANSLATION', '');




-- Page 25 (Guide de conversation -> Les bases) :

INSERT INTO TRANSLATIONS VALUES ('1', '25', '0', "Bonjour", "Good morning");
INSERT INTO TRANSLATIONS VALUES ('2', '25', '1', "Bon après-midi", "Good afternoon");
INSERT INTO TRANSLATIONS VALUES ('3', '25', '2', "Bonsoir", "Good evening");
INSERT INTO TRANSLATIONS VALUES ('4', '25', '3', "Bonne nuit", "Good night");
INSERT INTO TRANSLATIONS VALUES ('5', '25', '4', "S'il vous plaît", "Please");
INSERT INTO TRANSLATIONS VALUES ('6', '25', '5', "Merci (beaucoup)", "Thank you (very much)");
INSERT INTO TRANSLATIONS VALUES ('7', '25', '6', "De rien", "you're welcome");
INSERT INTO TRANSLATIONS VALUES ('8', '25', '7', "Pardon", "Sorry");
INSERT INTO TRANSLATIONS VALUES ('9', '25', '8', "Je suis vraiment désolé", "I'm so sorry");
INSERT INTO TRANSLATIONS VALUES ('10', '25', '9', "Je suis désolé(e) d'être en retard", "I'm sorry I'm late");
INSERT INTO TRANSLATIONS VALUES ('11', '25', '10', "Un instant, s'il vous plait", "One moment please");
INSERT INTO TRANSLATIONS VALUES ('12', '25', '11', "Où ?", "Where ?");
INSERT INTO TRANSLATIONS VALUES ('13', '25', '12', "Quand ?", "When ?");
INSERT INTO TRANSLATIONS VALUES ('14', '25', '13', "Quoi ?", "What ?");
INSERT INTO TRANSLATIONS VALUES ('15', '25', '14', "Comment ?", "How ?");
INSERT INTO TRANSLATIONS VALUES ('16', '25', '15', "Combien ?", "How much ?");
INSERT INTO TRANSLATIONS VALUES ('17', '25', '16', "Quel / Quelle ?", "Which ?");
INSERT INTO TRANSLATIONS VALUES ('18', '25', '17', "Qui ?", "Who ?");
INSERT INTO TRANSLATIONS VALUES ('19', '25', '18', "Pourquoi ?", "Why ?");
INSERT INTO TRANSLATIONS VALUES ('20', '25', '19', "Qu'est-ce que c'est ?", "What is it?");
INSERT INTO TRANSLATIONS VALUES ('21', '25', '20', "Comment allez-vous / vas-tu ?", "How are you?");
INSERT INTO TRANSLATIONS VALUES ('22', '25', '21', "Très bien, merci", "Fine, thank you");
INSERT INTO TRANSLATIONS VALUES ('23', '25', '22', "Comment vous appelez-vous ?", "What's your name ?");
INSERT INTO TRANSLATIONS VALUES ('25', '25', '23', "Mon nom est …", "My name is … ");
INSERT INTO TRANSLATIONS VALUES ('26', '25', '25', "Je suis français", "I'm french");
INSERT INTO TRANSLATIONS VALUES ('27', '25', '25', "Bon voyage", "Have a good trip !");
INSERT INTO TRANSLATIONS VALUES ('28', '25', '26', "Amusez-vous bien", "Have fun !");
INSERT INTO TRANSLATIONS VALUES ('29', '25', '27', "Bonne chance", "Good Luck !");
INSERT INTO TRANSLATIONS VALUES ('30', '25', '28', "Bienvenue", "Welcome");
INSERT INTO TRANSLATIONS VALUES ('31', '25', '25', "Je voudrais …", "I'd like …");
INSERT INTO TRANSLATIONS VALUES ('32', '25', '30', "Je cherche …", "I'm looking for …");
INSERT INTO TRANSLATIONS VALUES ('33', '25', '31', "C'est combien ?", "How much is it ?");

-- Page 26 (Guide de conversation -> Les chiffres) :
INSERT INTO TRANSLATIONS VALUES ('34', '26', '0', "1", "one");
INSERT INTO TRANSLATIONS VALUES ('35', '26', '1', "2", "two");
INSERT INTO TRANSLATIONS VALUES ('36', '26', '2', "3", "three");
INSERT INTO TRANSLATIONS VALUES ('37', '26', '3', "4", "four");
INSERT INTO TRANSLATIONS VALUES ('38', '26', '4', "5", "five");
INSERT INTO TRANSLATIONS VALUES ('39', '26', '5', "6", "six");
INSERT INTO TRANSLATIONS VALUES ('40', '26', '6', "7", "seven");
INSERT INTO TRANSLATIONS VALUES ('41', '26', '7', "8", "eight");
INSERT INTO TRANSLATIONS VALUES ('42', '26', '8', "9", "nine");
INSERT INTO TRANSLATIONS VALUES ('43', '26', '9', "10", "ten");
INSERT INTO TRANSLATIONS VALUES ('44', '26', '10', "11", "eleven");
INSERT INTO TRANSLATIONS VALUES ('45', '26', '11', "12", "twelve");
INSERT INTO TRANSLATIONS VALUES ('46', '26', '12', "13", "thirteen");
INSERT INTO TRANSLATIONS VALUES ('47', '26', '13', "14", "fourteen");
INSERT INTO TRANSLATIONS VALUES ('48', '26', '14', "15", "fifteen");
INSERT INTO TRANSLATIONS VALUES ('49', '26', '15', "20", "twenty");
INSERT INTO TRANSLATIONS VALUES ('50', '26', '16', "21", "twenty one");
INSERT INTO TRANSLATIONS VALUES ('51', '26', '17', "30", "thirty");
INSERT INTO TRANSLATIONS VALUES ('52', '26', '18', "40", "fourty");
INSERT INTO TRANSLATIONS VALUES ('53', '26', '19', "50", "fifty");
INSERT INTO TRANSLATIONS VALUES ('54', '26', '20', "60", "sixty");
INSERT INTO TRANSLATIONS VALUES ('55', '26', '21', "70", "seventy");
INSERT INTO TRANSLATIONS VALUES ('56', '26', '22', "80", "eighty");
INSERT INTO TRANSLATIONS VALUES ('57', '26', '23', "90", "ninety");
INSERT INTO TRANSLATIONS VALUES ('58', '26', '24', "100", "one hundred");
INSERT INTO TRANSLATIONS VALUES ('59', '26', '25', "1000", "one thousand");
INSERT INTO TRANSLATIONS VALUES ('60', '26', '26', "2589", "two thousand five hundred and eighty nine");

-- Page 27 (Guide de conversation -> Le temps) :
INSERT INTO TRANSLATIONS VALUES ('61', '27', '0', "Quelle heure est-il ?", "What time is it ?");
INSERT INTO TRANSLATIONS VALUES ('62', '27', '1', "Avant-hier", "The day before yesterday");
INSERT INTO TRANSLATIONS VALUES ('63', '27', '2', "Hier", "Yesterday");
INSERT INTO TRANSLATIONS VALUES ('64', '27', '3', "Aujourd'hui", "Today");
INSERT INTO TRANSLATIONS VALUES ('65', '27', '4', "Demain", "Tomorrow");
INSERT INTO TRANSLATIONS VALUES ('66', '27', '5', "Après-demain", "The day after tomorrow");
INSERT INTO TRANSLATIONS VALUES ('67', '27', '6', "Le matin", "In the morning");
INSERT INTO TRANSLATIONS VALUES ('68', '27', '7', "L'après-midi", "In the afternoon");
INSERT INTO TRANSLATIONS VALUES ('69', '27', '8', "Le soir", "In the evening");
INSERT INTO TRANSLATIONS VALUES ('70', '27', '9', "La nuit", "At night");
INSERT INTO TRANSLATIONS VALUES ('71', '27', '10', "Petit déjeuner", "Breakfast");
INSERT INTO TRANSLATIONS VALUES ('72', '27', '11', "Déjeuner", "Lunch");
INSERT INTO TRANSLATIONS VALUES ('73', '27', '12', "Dîner", "Dinner");
INSERT INTO TRANSLATIONS VALUES ('74', '27', '13', "Janvier", "January");
INSERT INTO TRANSLATIONS VALUES ('75', '27', '14', "Février", "February");
INSERT INTO TRANSLATIONS VALUES ('76', '27', '15', "Mars", "March");
INSERT INTO TRANSLATIONS VALUES ('77', '27', '16', "Avril", "April");
INSERT INTO TRANSLATIONS VALUES ('78', '27', '17', "Mai", "May");
INSERT INTO TRANSLATIONS VALUES ('79', '27', '18', "Juin", "June");
INSERT INTO TRANSLATIONS VALUES ('80', '27', '19', "Juillet", "July");
INSERT INTO TRANSLATIONS VALUES ('81', '27', '20', "Aout", "August");
INSERT INTO TRANSLATIONS VALUES ('82', '27', '21', "Septembre", "September");
INSERT INTO TRANSLATIONS VALUES ('83', '27', '22', "Octobre", "October");
INSERT INTO TRANSLATIONS VALUES ('84', '27', '23', "Novembre", "November");
INSERT INTO TRANSLATIONS VALUES ('85', '27', '24', "Décembre", "December");
INSERT INTO TRANSLATIONS VALUES ('86', '27', '25', "Lundi", "Monday");
INSERT INTO TRANSLATIONS VALUES ('87', '27', '26', "Mardi", "Tuesday");
INSERT INTO TRANSLATIONS VALUES ('88', '27', '27', "Mercredi", "Wednesday");
INSERT INTO TRANSLATIONS VALUES ('89', '27', '28', "Jeudi", "Thursday");
INSERT INTO TRANSLATIONS VALUES ('90', '27', '29', "Vendredi", "Friday");
INSERT INTO TRANSLATIONS VALUES ('91', '27', '30', "Samedi", "Saturday");
INSERT INTO TRANSLATIONS VALUES ('92', '27', '31', "Dimanche", "Sunday");

-- Page 28 (Guide de conversation -> Discussion avec un américain) :

INSERT INTO TRANSLATIONS VALUES ('93', '28', '0', "Je ne sais pas", "I don't know");
INSERT INTO TRANSLATIONS VALUES ('94', '28', '1', "Je ne comprends pas", "I don't understand");
INSERT INTO TRANSLATIONS VALUES ('95', '28', '2', "Excusez-moi, mais je n'ai pas compris", "Sorry, but I didn't understand");
INSERT INTO TRANSLATIONS VALUES ('96', '28', '3', "Pourriez-vous parler plus lentement ?", "Can you speak more slowly ?");
INSERT INTO TRANSLATIONS VALUES ('97', '28', '4', "Pourriez-vous épeler, s'il vous plaît ?", "Could you spell that out, please?");
INSERT INTO TRANSLATIONS VALUES ('98', '28', '5', "Pourriez-vous me l'écrire, s'il vous plaît ?", "Could you write that for me, please?");
INSERT INTO TRANSLATIONS VALUES ('99', '28', '6', "Qu'est-ce que vous dites ?", "What did you say ?");
INSERT INTO TRANSLATIONS VALUES ('100', '28', '7', "Je n'ai pas entendu ce que vous avez dit", "I didn't hear what you said");
INSERT INTO TRANSLATIONS VALUES ('101', '28', '8', "Pouvez-vous parler plus fort ?", "Can you speak up?");
INSERT INTO TRANSLATIONS VALUES ('102', '28', '9', "Qu'est-ce que cela veut dire ?", "What does this mean ?");
INSERT INTO TRANSLATIONS VALUES ('103', '28', '10', "Je peux vous poser une question ?", "Can I ask you a question?");
INSERT INTO TRANSLATIONS VALUES ('104', '28', '11', "Parlez-vous français ?", "Do you speak French ?");
INSERT INTO TRANSLATIONS VALUES ('105', '28', '12', "Je ne parle pas anglais", "I don't speak English");
INSERT INTO TRANSLATIONS VALUES ('106', '28', '13', "Pouvez-vous nous prendre en photo ?", "Can you take a picture of us ?");
INSERT INTO TRANSLATIONS VALUES ('107', '28', '14', "Puis-je fumer ici ?", "Can I smoke in here ?");
INSERT INTO TRANSLATIONS VALUES ('108', '28', '15', "Avez-vous du feu ?", "Do you have a light ?");
INSERT INTO TRANSLATIONS VALUES ('109', '28', '16', "D'où venez-vous ?", "Where are you from ? ");
INSERT INTO TRANSLATIONS VALUES ('110', '28', '17', "Ravi(e) de faire votre connaître", "Nice to meet you");
INSERT INTO TRANSLATIONS VALUES ('111', '28', '18', "Est-ce qu'il y a quelque chose d'intéressant à voir en ville aujourd'hui ?", "Is there anything special happening in the city today ?");
INSERT INTO TRANSLATIONS VALUES ('112', '28', '19', "Quel est le meilleur endroit pour boire un verre ?", "Where is the best place to drink ?");
INSERT INTO TRANSLATIONS VALUES ('113', '28', '20', "Quel est le meilleur endroit pour manger ?", "Where is the best place to eat ?");
INSERT INTO TRANSLATIONS VALUES ('114', '28', '21', "Où est-ce que tu aimes faire les magasins ?", "Where do you like to shop ?");

-- Page 29 (Guide de conversation -> Dans la rue) :

INSERT INTO TRANSLATIONS VALUES ('115', '29', '0', "Pouvez-vous m'indiquer le chemin de … ?", "Can you show me the way to … ?");
INSERT INTO TRANSLATIONS VALUES ('116', '29', '1', "Où est la station de métro la plus proche ?", "Where is the nearest subway station ?");
INSERT INTO TRANSLATIONS VALUES ('117', '29', '2', "Où puis-je trouver un taxi ?", "Where can I find a cab (taxi) ?");
INSERT INTO TRANSLATIONS VALUES ('118', '29', '3', "À gauche", "Left");
INSERT INTO TRANSLATIONS VALUES ('119', '29', '4', "À droite", "Right");
INSERT INTO TRANSLATIONS VALUES ('120', '29', '5', "Tout droit", "Straight ahead");
INSERT INTO TRANSLATIONS VALUES ('121', '29', '6', "Où puis-je trouver du wifi gratuit ?", "Where can I found free wifi ?");
INSERT INTO TRANSLATIONS VALUES ('122', '29', '7', "Où se trouve le distributeur de billets le plus proche ?", "Where is the nearest ATM ?");
INSERT INTO TRANSLATIONS VALUES ('123', '29', '8', "Où est-ce que je peux trouver une banque ?", "Where can I found a bank ?");
INSERT INTO TRANSLATIONS VALUES ('124', '29', '9', "Où est-ce que je peux trouver une boîte aux lettres ?", "Where can I find a mailbox ?");
INSERT INTO TRANSLATIONS VALUES ('125', '29', '10', "Où est-ce que je peux acheter des timbres ?", "Where can I buy stamps ?");

-- Page 30 (Guide de conversation -> La météo) :

INSERT INTO TRANSLATIONS VALUES ('126', '30', '0', "Quel temps va-t-il faire aujourd'hui ?", "What will the weather be like today ?");
INSERT INTO TRANSLATIONS VALUES ('127', '30', '1', "Il fait beau aujourd'hui", "It's a nice day today");
INSERT INTO TRANSLATIONS VALUES ('128', '30', '2', "Y aura t-il du soleil dans les jours qui viennent ?", "Will it be sunny the next few days ?");
INSERT INTO TRANSLATIONS VALUES ('129', '30', '3', "Il fait froid", "It's cold");
INSERT INTO TRANSLATIONS VALUES ('130', '30', '4', "Il fait chaud", "It's warm");
INSERT INTO TRANSLATIONS VALUES ('131', '30', '5', "Il pleut", "It's raining");
INSERT INTO TRANSLATIONS VALUES ('132', '30', '6', "Est-ce que c'est nuageux aujourd'hui ?", "Is it cloudy today ?");

-- Page 31 (Guide de conversation -> Nom des lieux communs) :

INSERT INTO TRANSLATIONS VALUES ('133', '31', '0', "Poste", "Post Office");
INSERT INTO TRANSLATIONS VALUES ('134', '31', '1', "Office du tourisme", "Visitor Center");
INSERT INTO TRANSLATIONS VALUES ('135', '31', '2', "Banque", "Bank");
INSERT INTO TRANSLATIONS VALUES ('136', '31', '3', "Médecin", "Doctor");
INSERT INTO TRANSLATIONS VALUES ('137', '31', '4', "Parmacie", "Pharmacy/Drugstore");
INSERT INTO TRANSLATIONS VALUES ('138', '31', '5', "Hôpital", "Hospital");
INSERT INTO TRANSLATIONS VALUES ('139', '31', '6', "Aéroport", "Airport");
INSERT INTO TRANSLATIONS VALUES ('140', '31', '7', "Gare", "Train Station");
INSERT INTO TRANSLATIONS VALUES ('141', '31', '8', "Toilettes", "Restroom");
INSERT INTO TRANSLATIONS VALUES ('142', '31', '9', "Magasin", "Store");
INSERT INTO TRANSLATIONS VALUES ('143', '31', '10', "Parc", "Park");
INSERT INTO TRANSLATIONS VALUES ('144', '31', '11', "Hôtel", "Hotel");
INSERT INTO TRANSLATIONS VALUES ('145', '31', '12', "Stade", "Stadium");

-- Page 32 (Guide de conversation -> En cas d'urgence) :

INSERT INTO TRANSLATIONS VALUES ('146', '32', '0', "J'ai besoin d'aide immédiatement !", "I need help immediately !");
INSERT INTO TRANSLATIONS VALUES ('147', '32', '1', "C'est une urgence !", "It's an emergency !");
INSERT INTO TRANSLATIONS VALUES ('148', '32', '2', "Attention !", "Watch out !");
INSERT INTO TRANSLATIONS VALUES ('149', '32', '3', "Au secours ! Au voleur !", "Help ! Stop thief !");
INSERT INTO TRANSLATIONS VALUES ('150', '32', '4', "Appelez la police !", "Call the police !");
INSERT INTO TRANSLATIONS VALUES ('151', '32', '5', "Appeler un médecin !", "Call a doctor !");
INSERT INTO TRANSLATIONS VALUES ('152', '32', '6', "Appeler une ambulance !", "Call an ambulance !");
INSERT INTO TRANSLATIONS VALUES ('153', '32', '7', "J'ai perdu mes clés", "I've lost my keys");
INSERT INTO TRANSLATIONS VALUES ('154', '32', '8', "Je suis perdu(e)", "I'm lost");
INSERT INTO TRANSLATIONS VALUES ('155', '32', '9', "Est-ce que vous pourriez faire venir un interprète ?", "Can I have an interpreter ?");
INSERT INTO TRANSLATIONS VALUES ('156', '32', '10', "Où est le commissariat ?", "Where is the Police Station ?");
INSERT INTO TRANSLATIONS VALUES ('157', '32', '11', "Où est l'hopital ?", "Where is the hospital ?");
INSERT INTO TRANSLATIONS VALUES ('158', '32', '12', "J'ai perdu mon portefeuille", "I've lost my wallet");
INSERT INTO TRANSLATIONS VALUES ('159', '32', '13', "J'ai perdu mon passeport", "I've lost my passport");
INSERT INTO TRANSLATIONS VALUES ('160', '32', '14', "J'ai perdu ma carte de crédit", "I've lost my credit card");

-- Page 33 (Guide de conversation -> Dans un hôtel) :

INSERT INTO TRANSLATIONS VALUES ('161', '33', '0', "J'ai réservé une chambre dans votre hôtel", "I booked a room in your hotel");
INSERT INTO TRANSLATIONS VALUES ('162', '33', '1', "Je voudrais régler la note", "I'd like to check out");
INSERT INTO TRANSLATIONS VALUES ('163', '33', '2', "Comme je rentre en France aujourd'hui, où puis-je laisser mes bagages ?", "As I come back in France today, where can I leave my luggage ? ");
INSERT INTO TRANSLATIONS VALUES ('164', '33', '3', "Est-ce que je pourrais avoir une serviette supplémentaire ?", "Could I have another towel ?");
INSERT INTO TRANSLATIONS VALUES ('165', '33', '4', "Est-ce que je peux laver mon linge ici ?", "Can I do my laundry here ?");
INSERT INTO TRANSLATIONS VALUES ('166', '33', '5', "Je voudrais une autre chambre", "I'd like to have another room");
INSERT INTO TRANSLATIONS VALUES ('167', '33', '6', "La chambre n'a pas été nettoyée", "The room hasn't been cleaned");
INSERT INTO TRANSLATIONS VALUES ('168', '33', '7', "La climatisation ne marche pas", "The air conditioning doesn't work");
INSERT INTO TRANSLATIONS VALUES ('169', '33', '8', "Le chauffage ne marche pas", "The heating doesn't work");
INSERT INTO TRANSLATIONS VALUES ('170', '33', '9', "Il n'y a plus de papier toilette", "There is no more toilet paper");
INSERT INTO TRANSLATIONS VALUES ('171', '33', '10', "Les draps sont sales", "The sheets are dirty");
INSERT INTO TRANSLATIONS VALUES ('172', '33', '11', "Est-ce que le petit déjeuner est inclus ?", "Is breakfast included ?");
INSERT INTO TRANSLATIONS VALUES ('173', '33', '12', "La clé de la 12, s’il vous plaît", "The key for room 12, please");
INSERT INTO TRANSLATIONS VALUES ('174', '33', '13', "Disposez-vous du WIFI ?", "Do you have WIFI ?");
INSERT INTO TRANSLATIONS VALUES ('175', '33', '14', "Quel est le mot de passe WIFI ?", "What's the WIFI password ?");
INSERT INTO TRANSLATIONS VALUES ('176', '33', '15', "Je n'arrive pas à me connecter", "I can't get through");
INSERT INTO TRANSLATIONS VALUES ('177', '33', '16', "Je n'arrive pas à m'identifier", "I can't log on");
INSERT INTO TRANSLATIONS VALUES ('178', '33', '17', "Puis-je imprimer quelques pages ici ?", "Can I print few pages here ?");

-- Page 34 (Guide de conversation -> Dans une pharmacie ou chez le médecin) :

INSERT INTO TRANSLATIONS VALUES ('179', '34', '0', "Je suis malade", "I'm ill");
INSERT INTO TRANSLATIONS VALUES ('180', '34', '1', "J'ai mal à la tête", "I have a headache");
INSERT INTO TRANSLATIONS VALUES ('181', '34', '2', "J'ai des vertiges", "I feel dizzy");
INSERT INTO TRANSLATIONS VALUES ('182', '34', '3', "J'ai la tête qui tourne", "My head is spinning");
INSERT INTO TRANSLATIONS VALUES ('183', '34', '4', "J'ai du mal à respirer", "I find it difficult to breathe");
INSERT INTO TRANSLATIONS VALUES ('184', '34', '5', "J'ai mal au coeur", "I feel nauseous");
INSERT INTO TRANSLATIONS VALUES ('185', '34', '6', "J'ai des vomissements", "I vomit");
INSERT INTO TRANSLATIONS VALUES ('186', '34', '7', "J'ai des palpitations", "I have palpitations");
INSERT INTO TRANSLATIONS VALUES ('187', '34', '8', "J'ai de la fièvre", "I have a fever");
INSERT INTO TRANSLATIONS VALUES ('188', '34', '9', "J'ai mal à la gorge", "I have a sore throat");
INSERT INTO TRANSLATIONS VALUES ('189', '34', '10', "J'ai mal aux dents", "I have a toothache");
INSERT INTO TRANSLATIONS VALUES ('190', '34', '11', "J'ai mal à l'oreille", "My ear hurts");
INSERT INTO TRANSLATIONS VALUES ('191', '34', '12', "J'ai mal au dos", "My back hurts");
INSERT INTO TRANSLATIONS VALUES ('192', '34', '13', "J'ai mal à la jambe", "My leg hurts");
INSERT INTO TRANSLATIONS VALUES ('193', '34', '14', "J'ai mal au bras", "My arm hurts");
INSERT INTO TRANSLATIONS VALUES ('194', '34', '15', "Je ne peux rien avaler", "I can't eat anything");
INSERT INTO TRANSLATIONS VALUES ('195', '34', '16', "Je tousse sans arrêt", "I'm coughing all the time");
INSERT INTO TRANSLATIONS VALUES ('196', '34', '17', "C'est une toux sèche", "It's a dry cough");
INSERT INTO TRANSLATIONS VALUES ('197', '34', '18', "C'est une toux grasse", "It's not a dry cough");
INSERT INTO TRANSLATIONS VALUES ('198', '34', '19', "J'ai très mal", "I feel a great pain");
INSERT INTO TRANSLATIONS VALUES ('199', '34', '20', "Je me sens très faible", "I feel very weak");
INSERT INTO TRANSLATIONS VALUES ('200', '34', '21', "J'ai mal au ventre", "I have a stomach ache");
INSERT INTO TRANSLATIONS VALUES ('201', '34', '22', "Je crois que j'ai la grippe", "I think I have flu");
INSERT INTO TRANSLATIONS VALUES ('202', '34', '23', "Je croix que j'ai un rhume", "I think I have a cold");
INSERT INTO TRANSLATIONS VALUES ('203', '34', '24', "Je crois que j'ai une gastro-entérite", "I thnink I have gastroenteritis");
INSERT INTO TRANSLATIONS VALUES ('204', '34', '25', "Avez-vous un remède contre le mal de tête ?", "Do you have something for a headache ?");
INSERT INTO TRANSLATIONS VALUES ('205', '34', '26', "Avez-vous un remède contre le rhume des foins ?", "Do you have something for hay fever ?");
INSERT INTO TRANSLATIONS VALUES ('206', '34', '27', "Avez-vous un remède contre la diarrhée ?", "Do you have something for diarrhea ?");
INSERT INTO TRANSLATIONS VALUES ('207', '34', '28', "Je suis allergique à la penicilline", "I'm allergic to penicillin");
INSERT INTO TRANSLATIONS VALUES ('208', '34', '29', "Je suis diabétique", "I'm a diabetic");
INSERT INTO TRANSLATIONS VALUES ('209', '34', '30', "Je suis cardiaque", "I have a heart condition");
INSERT INTO TRANSLATIONS VALUES ('210', '34', '31', "Je suis enceinte", "I'm pregnant");
INSERT INTO TRANSLATIONS VALUES ('211', '34', '32', "Je suis tombé", "I fell down");
INSERT INTO TRANSLATIONS VALUES ('212', '34', '33', "La consultation ouvre à quelle heure ?", "When is the Doctor's office open ?");
INSERT INTO TRANSLATIONS VALUES ('213', '34', '34', "Je voudrais prendre un rendez-vous", "I would to make an appointment");
INSERT INTO TRANSLATIONS VALUES ('214', '34', '35', "Je dois aller d'urgence chez un médecin", "I urgently need a doctor");
INSERT INTO TRANSLATIONS VALUES ('215', '34', '36', "Je dois aller d'urgence chez un dentiste", "I urgently need a dentist");

-- Page 35 (Guide de conversation -> Dans les transports en commun) :

INSERT INTO TRANSLATIONS VALUES ('216', '35', '0', "Allez simple", "One-way");
INSERT INTO TRANSLATIONS VALUES ('217', '35', '1', "Aller-Retour", "Round-trip");
INSERT INTO TRANSLATIONS VALUES ('218', '35', '2', "Où puis-je acheter un ticket ?", "Where can I buy a ticket ?");
INSERT INTO TRANSLATIONS VALUES ('219', '35', '3', "Je voudrais un ticket pour …", "I'd like to have a ticket to …");
INSERT INTO TRANSLATIONS VALUES ('220', '35', '4', "Combien côute un ticket ?", "How much is a ticket ?");
INSERT INTO TRANSLATIONS VALUES ('221', '35', '5', "Ce bus va-t-il à … ?", "Is this the bus to … ?");
INSERT INTO TRANSLATIONS VALUES ('222', '35', '6', "À quelle heure y a-t-il un bus/train pour … ?", "At what time is there a bus/train to … ?");
INSERT INTO TRANSLATIONS VALUES ('223', '35', '7', "Cette place est-elle prise ?", "Is this seat taken ?");
INSERT INTO TRANSLATIONS VALUES ('224', '35', '8', "Quel numéro de bus dois-je prendre ?", "Which number do I take ?");
INSERT INTO TRANSLATIONS VALUES ('225', '35', '9', "Quand arrive le prochain ?", "When is the next one coming ?");
INSERT INTO TRANSLATIONS VALUES ('226', '35', '10', "Où est la station de métro ?", "Where is the subway station ?");
INSERT INTO TRANSLATIONS VALUES ('227', '35', '11', "De quel quai part le métro X ?", "From which platform is the subway X leaving ?");
INSERT INTO TRANSLATIONS VALUES ('228', '35', '12', "Est-ce que vous pouvez me dire comment aller à la gare ?", "Could you tell me how to get to the station?");

-- Page 36 (Guide de conversation -> Dans un taxi) :

INSERT INTO TRANSLATIONS VALUES ('229', '36', '0', "Pourriez-vous me conduire à cette adresse ?", "Could you bring me to this address ?");
INSERT INTO TRANSLATIONS VALUES ('230', '36', '1', "Quel est le montant de la course ?", "How much for the ride ?");
INSERT INTO TRANSLATIONS VALUES ('231', '36', '2', "Pourriez-vous ralentir un peu s'il vous plait ?", "Could you drive slower please ?");
INSERT INTO TRANSLATIONS VALUES ('232', '36', '3', "À l'aéroport s'il vous plait", "To the airport please");

-- Page 37 (Guide de conversation -> Dans l'avion) :

INSERT INTO TRANSLATIONS VALUES ('233', '37', '0', "Nous voudrions être assis l'un à côté de l'autre", "We would like to sit next to each other");
INSERT INTO TRANSLATIONS VALUES ('234', '37', '1', "Je voudrais un siège côté fenêtre", "I'd like a seat by the window");
INSERT INTO TRANSLATIONS VALUES ('235', '37', '2', "Je voudrais un siège côté couloir", "I'd like an aisle seat");
INSERT INTO TRANSLATIONS VALUES ('236', '37', '3', "Ma valise a disparu", "My suitcase has disappeared");
INSERT INTO TRANSLATIONS VALUES ('237', '37', '4', "Je voudrais boire un Coca Cola", "I'd like to drink a Coke");

-- Page 38 (Guide de conversation -> Dans un magasin, bar ou restaurant) :

INSERT INTO TRANSLATIONS VALUES ('238', '38', '0', "Cela ouvre à quelle heure ?", "What time does it open ?");
INSERT INTO TRANSLATIONS VALUES ('239', '38', '1', "Cela ferme à quelle heure ?", "What time does it close ?");
INSERT INTO TRANSLATIONS VALUES ('240', '38', '2', "Est-ce que je peux payer avec une carte de crédit ?", "Can I pay with a credit card ?");
INSERT INTO TRANSLATIONS VALUES ('241', '38', '3', "Combien ça coûte ?", "How much is this one ?");
INSERT INTO TRANSLATIONS VALUES ('242', '38', '4', "Pourriez-vous m'écrire le prix ?", "Could you write down the price ?");
INSERT INTO TRANSLATIONS VALUES ('243', '38', '5', "Pourriez-vous me l'emballer ?", "Could you wrap it for me ?");
INSERT INTO TRANSLATIONS VALUES ('244', '38', '6', "Pourrais-je avoir un sac s'il vous plaît ?", "Could I have a bag, please ?");
INSERT INTO TRANSLATIONS VALUES ('245', '38', '7', "Puis-je avoir une remise ?", "Can I get a discount ?");
INSERT INTO TRANSLATIONS VALUES ('246', '38', '8', "Puis-je utiliser internet ici ?", "Can I use the internet here ?");
INSERT INTO TRANSLATIONS VALUES ('247', '38', '9', "C'est cher", "It's expensive");
INSERT INTO TRANSLATIONS VALUES ('248', '38', '10', "C'est bon marché", "It's cheap");
INSERT INTO TRANSLATIONS VALUES ('249', '38', '11', "Dois-je payer d'avance ?", "Do I have to pay in advance ?");
INSERT INTO TRANSLATIONS VALUES ('250', '38', '12', "Puis-je avoir un reçu ?", "Can I have a receipt ?");
INSERT INTO TRANSLATIONS VALUES ('251', '38', '13', "Puis-je avoir une facture ?", "Can I have an invoice ?");
INSERT INTO TRANSLATIONS VALUES ('252', '38', '14', "Faut-il payer en espèces ?", "Do I have to pay in cash ?");
INSERT INTO TRANSLATIONS VALUES ('253', '38', '15', "Je vais payer en liquide", "I'll pay cash");

-- Page 39 (Guide de conversation -> Dans un magasin) :

INSERT INTO TRANSLATIONS VALUES ('254', '39', '0', "Je peux regarder ?", "Is it ok if I look around ?");
INSERT INTO TRANSLATIONS VALUES ('255', '39', '1', "Puis-je l'essayer ?", "Can I try it on ?");
INSERT INTO TRANSLATIONS VALUES ('256', '39', '2', "Où sont les cabines d'essayage ?", "Where is the fitting room ?");
INSERT INTO TRANSLATIONS VALUES ('257', '39', '3', "Où puis-je trouver un miroir ?", "Where can I find a mirror ?");
INSERT INTO TRANSLATIONS VALUES ('258', '39', '4', "Cela ne me va pas", "This one doesn't fit");
INSERT INTO TRANSLATIONS VALUES ('259', '39', '5', "Avez-vous la taille au-dessus ?", "Do you have a larger size ?");
INSERT INTO TRANSLATIONS VALUES ('260', '39', '6', "Avez-vous la taille en-dessous ?", "Do you have a smaller size ?");

-- Page 40 (Guide de conversation -> Dans un bar ou un restaurant) :

INSERT INTO TRANSLATIONS VALUES ('261', '40', '0', "Avez-vous une table de libre ?", "Do you have a table for us ?");
INSERT INTO TRANSLATIONS VALUES ('262', '40', '1', "Une table pour 2 s'il vous plaît", "A table for two please");
INSERT INTO TRANSLATIONS VALUES ('263', '40', '2', "Pourriez-vous réserver une table ?", "Can you reserve a table for us ?");
INSERT INTO TRANSLATIONS VALUES ('264', '40', '3', "La carte s'il vous plaît", "The menu, please");
INSERT INTO TRANSLATIONS VALUES ('265', '40', '4', "La carte des vins s'il vous plaît", "The wine list please");
INSERT INTO TRANSLATIONS VALUES ('266', '40', '5', "Quel est le menu du jour ?", "What's the special of the day ?");
INSERT INTO TRANSLATIONS VALUES ('267', '40', '6', "Qu'est-ce que vous me conseillez ?", "What would you recommend ?");
INSERT INTO TRANSLATIONS VALUES ('268', '40', '7', "Nous aimerions commander", " We are ready to order");
INSERT INTO TRANSLATIONS VALUES ('269', '40', '8', "L'addition s'il vous plaît", "The check, please");
INSERT INTO TRANSLATIONS VALUES ('270', '40', '9', "Le pourboire", "the tip / the gratuity");
INSERT INTO TRANSLATIONS VALUES ('271', '40', '10', "Je suis végétarien", "I'm vege");
INSERT INTO TRANSLATIONS VALUES ('272', '40', '11', "Je suis végétalien", " I'm vegan");
INSERT INTO TRANSLATIONS VALUES ('273', '40', '12', "Avez-vous des plats végétariens ?", "Do you have any vegetarian dishes ?");
INSERT INTO TRANSLATIONS VALUES ('274', '40', '13', "Pourrions-nous avoir une corbeille de pain ?", "Could we have some bread ?");
INSERT INTO TRANSLATIONS VALUES ('275', '40', '14', "Pourrions-nous avoir une carafe d'eau ?", "Could we have some water ?");
INSERT INTO TRANSLATIONS VALUES ('276', '40', '15', "Auriez-vous du sel et du poivre ?", "Do you have salt and pepper ?");
INSERT INTO TRANSLATIONS VALUES ('277', '40', '16', "Auriez-vous de l'huile et du vinaigre ?", "Do you have oil and vinegar ?");
INSERT INTO TRANSLATIONS VALUES ('278', '40', '17', "Pourrais-je avoir une serviette ?", "Could I get a napkin ?");
INSERT INTO TRANSLATIONS VALUES ('279', '40', '18', "Quelles bières avez-vous en pression ?", "What beers do you have on tap ?");
INSERT INTO TRANSLATIONS VALUES ('280', '40', '19', "Quel est le meilleur cocktail ?", "Which cocktail is the best ?");
INSERT INTO TRANSLATIONS VALUES ('281', '40', '20', "Est-ce que je peux avoir une paille ?", "Can I have a straw ?");
INSERT INTO TRANSLATIONS VALUES ('282', '40', '21', "Je ne veux pas de glaçons", "I don't want ice");
INSERT INTO TRANSLATIONS VALUES ('283', '40', '22', "Est-ce que je peux avoir une rondelle de citron ?", "Could I have a slice of lemon please ?");
INSERT INTO TRANSLATIONS VALUES ('284', '40', '23', "Oeufs", "Eggs");
INSERT INTO TRANSLATIONS VALUES ('285', '40', '24', "Frites", "French fries");
INSERT INTO TRANSLATIONS VALUES ('286', '40', '25', "Pain", "Bread");
INSERT INTO TRANSLATIONS VALUES ('287', '40', '26', "Fromage", "Cheese");
INSERT INTO TRANSLATIONS VALUES ('288', '40', '27', "Légume", "Vegetable");
INSERT INTO TRANSLATIONS VALUES ('289', '40', '28', "Eau", "Water");
INSERT INTO TRANSLATIONS VALUES ('290', '40', '29', "Eau plate", "Still water");
INSERT INTO TRANSLATIONS VALUES ('291', '40', '30', "Eau pétillante", "Sparkling water");
INSERT INTO TRANSLATIONS VALUES ('292', '40', '31', "Eau du robinet", "Tap water");
INSERT INTO TRANSLATIONS VALUES ('293', '40', '32', "Eau minérale", "Mineral water");
INSERT INTO TRANSLATIONS VALUES ('294', '40', '33', "Eau en bouteille", "Bottled water");
INSERT INTO TRANSLATIONS VALUES ('295', '40', '34', "Lait", "Milk");
INSERT INTO TRANSLATIONS VALUES ('296', '40', '35', "Vin", "Wine");
INSERT INTO TRANSLATIONS VALUES ('297', '40', '36', "Vin rouge (blanc)", "Red (white) wine");
INSERT INTO TRANSLATIONS VALUES ('298', '40', '37', "Café", "Coffee");

-- Page 41 (Guide de conversation -> Dans une excursion) :

INSERT INTO TRANSLATIONS VALUES ('299', '41', '0', "Y a-t-il des réductions pour les étudiants ?", "Are there student reductions ?");
INSERT INTO TRANSLATIONS VALUES ('300', '41', '1', "Y a-t-il des réductions pour les enfants ?", "Are there reductions for children ?");
INSERT INTO TRANSLATIONS VALUES ('301', '41', '2', "Vous reste-t-il des places ?", "Do you have any room left ?");
INSERT INTO TRANSLATIONS VALUES ('302', '41', '3', "Faut-il réserver à l'avance ?", "Do you have to book in advance ?");

-- Page 42 (Guide de conversation -> Dans un spectacle) :

INSERT INTO TRANSLATIONS VALUES ('303', '42', '0', "Cela commence à quelle heure ?", "What time does it start ?");
INSERT INTO TRANSLATIONS VALUES ('304', '42', '1', "Cela dure combien de temps ?", "How long does it take ?");
INSERT INTO TRANSLATIONS VALUES ('305', '42', '2', "Cela finit à quelle heure ?", "What time does it end ?");
INSERT INTO TRANSLATIONS VALUES ('306', '42', '3', "Où est mon siège ?", "Where is my seat ?");
INSERT INTO TRANSLATIONS VALUES ('307', '42', '4', "Où sont les toilettes ?", "Where is the restroom ?");

-- ----------------------------------------------------------------------

*/





