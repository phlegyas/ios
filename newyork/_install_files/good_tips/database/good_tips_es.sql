
-- ----------------------------------------------------------------------
-- DROP ALL TABLES :
DROP TABLE IF EXISTS LOCATIONS;
DROP TABLE IF EXISTS PAGES;
DROP TABLE IF EXISTS PLANNINGS;
DROP TABLE IF EXISTS CHECKLIST_ITEMS;
DROP TABLE IF EXISTS TRANSLATIONS;
-- ----------------------------------------------------------------------



-- ----------------------------------------------------------------------
-- CREATE TABLES :
CREATE TABLE LOCATIONS ( id INTEGER PRIMARY KEY AUTOINCREMENT, location INT NOT NULL, position INT NOT NULL, title VARCHAR(100) NOT NULL );
CREATE TABLE PAGES ( id INTEGER PRIMARY KEY AUTOINCREMENT, location INT NOT NULL, type VARCHAR NOT NULL, content TEXT );
CREATE TABLE PLANNINGS ( id INTEGER PRIMARY KEY AUTOINCREMENT, page INT NOT NULL, position INT NOT NULL, title VARCHAR NOT NULL );
CREATE TABLE CHECKLIST_ITEMS ( id INTEGER PRIMARY KEY AUTOINCREMENT, page INT NOT NULL, position INT NOT NULL, checked BOOLEAN NOT NULL DEFAULT 0, title VARCHAR NOT NULL, description TEXT );
CREATE TABLE TRANSLATIONS ( id INTEGER PRIMARY KEY AUTOINCREMENT, page INT NOT NULL, position INT NOT NULL, frenchSentence VARCHAR NOT NULL, englishSentence VARCHAR NOT NULL );
-- ----------------------------------------------------------------------



-- ----------------------------------------------------------------------
-- INSERT LOCATIONS (id, location, position, title) :

-- Location 0 (top level) :
INSERT INTO LOCATIONS VALUES ('1', '0', '0', "Lo que tienen que saber antes de salir");
INSERT INTO LOCATIONS VALUES ('2', '0', '2', "Lo que tienen que saber una vez en NYC");
INSERT INTO LOCATIONS VALUES ('3', '0', '3', "Planes");
INSERT INTO LOCATIONS VALUES ('4', '0', '1', "Lo que tienen que hacer antes de salir");
INSERT INTO LOCATIONS VALUES ('5', '0', '5', "Guía de conversación");
INSERT INTO LOCATIONS VALUES ('6', '0', '4', 'La foto I \ue022 "MPVNY"');
INSERT INTO LOCATIONS VALUES ('7', '0', '7', "€ - $\nconvertidor");
INSERT INTO LOCATIONS VALUES ('8', '0', '6', "IVA y Propinas");
INSERT INTO LOCATIONS VALUES ('9', '0', '8', "Números Útiles\nen Nueva York");

-- Location 1 (L'essentiel avant de partir) :
INSERT INTO LOCATIONS VALUES ('10', '1', '0', "Antes de subir al avión");
INSERT INTO LOCATIONS VALUES ('11', '1', '1', "Acerca del equipaje");
INSERT INTO LOCATIONS VALUES ('12', '1', '2', "Cambio de dólares");
INSERT INTO LOCATIONS VALUES ('13', '1', '3', "Aparatos electrónicos en Nueva York");
INSERT INTO LOCATIONS VALUES ('14', '1', '4', "Transporte en Nueva York");
INSERT INTO LOCATIONS VALUES ('15', '1', '5', "Sacar fotos en NYC");

-- Location 2 (L'essentiel sur place) :
INSERT INTO LOCATIONS VALUES ('17', '2', '0', "Enviar una postal");
-- INSERT INTO LOCATIONS VALUES ('18', '2', '1', "Disfrutar de su estancia en NY");
INSERT INTO LOCATIONS VALUES ('19', '2', '2', "Comprar recuerdos");
INSERT INTO LOCATIONS VALUES ('20', '2', '3', "Cambio de dólares");
INSERT INTO LOCATIONS VALUES ('21', '2', '4', "Más información sobre Nueva York");

-- Location 3 (Les plannings) :
INSERT INTO LOCATIONS VALUES ('22', '3', '0', "1 día");
INSERT INTO LOCATIONS VALUES ('23', '3', '1', "3 días");
INSERT INTO LOCATIONS VALUES ('24', '3', '2', "1 semana");

-- Location 4 (A faire et à voir avant de partir) :
INSERT INTO LOCATIONS VALUES ('25', '4', '0', "A 1 mes antes de salir de viaje");
INSERT INTO LOCATIONS VALUES ('26', '4', '1', " 1 semana antes de salir de viaje");
INSERT INTO LOCATIONS VALUES ('27', '4', '2', "Su equipaje facturado a 1 hora antes de salir");
INSERT INTO LOCATIONS VALUES ('28', '4', '3', "Su equipaje de mano a 1 hora antes de salir");

-- Location 5 (Guide de conversation) :
INSERT INTO LOCATIONS VALUES ('29', '5', '0', "Lo básico");
INSERT INTO LOCATIONS VALUES ('30', '5', '1', "Los números");
INSERT INTO LOCATIONS VALUES ('31', '5', '2', "El tiempo");
INSERT INTO LOCATIONS VALUES ('32', '5', '6', "Hablar con un americano");
INSERT INTO LOCATIONS VALUES ('33', '5', '7', "En la calle");
INSERT INTO LOCATIONS VALUES ('34', '5', '3', "El clima");
INSERT INTO LOCATIONS VALUES ('35', '5', '4', "Nombres de lugares comunes");
INSERT INTO LOCATIONS VALUES ('36', '5', '5', "En caso de emergencias");
INSERT INTO LOCATIONS VALUES ('37', '5', '8', "En un hotel");
INSERT INTO LOCATIONS VALUES ('38', '5', '9', "En una farmacia o con el médico");
INSERT INTO LOCATIONS VALUES ('39', '5', '11', "En el transporte público");
INSERT INTO LOCATIONS VALUES ('40', '5', '12', "En un taxi");
INSERT INTO LOCATIONS VALUES ('41', '5', '10', "En el avión");
INSERT INTO LOCATIONS VALUES ('42', '5', '13', "En una tienda, bar o restaurante");
INSERT INTO LOCATIONS VALUES ('43', '5', '14', "En una tienda");
INSERT INTO LOCATIONS VALUES ('44', '5', '15', "En un bar o restaurante");
INSERT INTO LOCATIONS VALUES ('45', '5', '16', "Para un tour");
INSERT INTO LOCATIONS VALUES ('46', '5', '17', "Para un espectáculo");

-- ----------------------------------------------------------------------




-- ----------------------------------------------------------------------
-- INSERT PAGES (id, location, type, content) :

-- Location 10 (L'essentiel avant de partir -> Avant de prendre l'avion) :
INSERT INTO PAGES VALUES ('1', '10', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>Recordatorio de lo que no pueden olvidar justo antes de salir de casa para ir a Nueva York</h1><p>Les propongo un recordatorio muy útil a la hora de cerrar las valijas y asegurarse de no olvidar nada antes de ir al aeropuerto.</p><p>Antes que todo, les recomiendo revisar las reglas de las compañías aéreas sobre el peso, tamaño y número de sus maletas.</p><p>¡Ahora les digo lo que no pueden olvidar! ¿Listos?</p><h3>¿Qué poner en su equipaje de mano?</h3><ul><li>Los <strong>pasaportes</strong>: ¡es lo más importante!</li><li>Una copia de su forma <b>ESTA</b> si son de una nacionalidad que lo requiere (<a href="http://www.mejores-planes-viaje-nueva-york.com/recordatorio-reservaciones-antes-partir-nueva-york/">ver más aquí</a>)</li><li>Su <b>visado vigente</b> (<a href="http://www.visamapper.com/" target="_blank">dependiendo de su nacionalidad</a>).</li><li>Una copia de sus billetes electrónicos de avión</li><li>Sus <b>vouchers</b>: si han comprado tours con NY City Pass, NY PASS… para un vuelo en helicóptero, su hotel o apartamento</li><li>Sus <b>dólares</b>, no pongan dinero en su equipaje facturado</li><li>Sus <b>guías</b> sobre NYC</li><li>La <b>dirección</b> de su hotel o apartamento, con el número de teléfono</li><li>Su <b>PC</b> o <b>Mac</b></li><li>Su <b>cámara</b></li><li>Los <b>cargadores</b> de sus aparatos electrónicos: casi siempre los tengo en el equipaje de mano, por si pierden mi maleta facturada, puedo utilizar mis aparatos al llegar ;) </li><li>El adaptador de enchufe, por si lo necesitan</li><li>Todas sus <b>pertenencias de valor o frágiles</b> (joyería, gafas de sol…)</li><li>Si necesitan <b>medicamentos</b>, es mejor ponerlos en su equipaje de mano, con la prescripción de su médico sobre todo si es un medicamento líquido</li><li>Un pequeño <b>neceser</b> con su cepillo de diente, pasta de diente, crema...</li><li>Si quieren <b>dormir</b> en el avión, compren unos tapones y un antifaz</li><li>¡¡Todos sus aparatos electrónicos deberán ser cargados!! Es una nueva medida de seguridad obligatoria para entrar a Estados Unidos</li></ul><h3>Mis mejores planes para su equipaje de mano:</h3><ul><li>Un <b>multi-enchufe</b>: ya se los recomendaba en este artículo MPVNY, pero es muy útil ya que ahora todos viajamos con una cámara de fotos, una laptop, un teléfono móvil… Si quieren cargar todos sus aparatos al mismo momento, ¡un multi-enchufe será muy práctico! Además sólo tienen que comprar un adaptador de tensión.</li><li><b>Ropa extra</b>: es un buen plan por su equipaje facturado se pierde en los aeropuertos. Pueden poner ropa interior y una camisa en su equipaje de mano que utilizarán mientras encuentran su valija.</li><li>Una <b>bufanda</b> y una <b>prenda gruesa</b>, pues el aire acondicionado está muy potente en el avión. Sería una lástima llegar a Nueva York con catarro y/o tener que ir al médico.</li></ul><h3>Su equipaje facturado</h3><ul><li>Una copia de su <b>pasaporte</b>, <b>billetes</b> de avión, <b>vouchers</b>… ¡Por si a caso!</li><li>Los <b>líquidos</b> de más de 100 ml</li><li>Dejen un <b>espacio</b> en su maleta, regresarán con algunas compras de NY, que sean recuerdos o ropa ;) </li><li><b>Medicamentos</b> que podrían necesitar (paracetamol, vitaminas…) y de “primeros auxilios”, con curitas, crema para los pies, crema solar...</li><li>Obviamente pongan todo lo necesario para <b>vestirse</b> dependiendo de la temporada. Si viajan durante el invierno, no pueden olvidar las bufandas, guantes, mallas… Un paraguas si viajan en otoño o primavera. Si viajan en verano, lleven ropa ligera porque hace mucho calor.</li><li>Para cualquier temporada, les aconsejo llevar <b>tenis</b> o <b>zapatos cómodos</b> para poder caminar y visitar bien la ciudad.</li><li><strong>Mi mejor plan para su equipaje facturado:</strong></li><li>Si viajan con otra persona, un buen plan consejo es <b>repartir su ropa entre las dos maletas</b>, sobre todo ropa interior y un cambio. ¿Por qué? Si se pierde su valija, tendrán un poco de ropa en la maleta de su acompañante y no tendrán que ir corriendo a comprarse ropa de repuesto.</li></ul><h3>¿Qué hacer con sus documentos de identidad, carnet de conducir…?</h3><p>Unos días antes de salir de viaje, les aconsejo<b> escanear sus papeles importantes</b>: pasaporte, número de tarjeta de crédito, número a llamar si pierden su tarjeta bancaria...</p><p>Nueva York es una ciudad segura, pero siempre tienen que ser precavidos.</p><p>Podrán guardar las copias de estos papeles y documentos en <b>internet</b>. Les explico, existen ahora muchos “lugares” donde guardar dichas copias, en los llamados Clouds. Google propone este servicio por ejemplo. ¡Lo mejor es que son gratuitos!</p><p>Por cualquier cosa que les pueda pasar durante sus vacaciones, o hasta en su vida cotidiana, es muy práctico tener acceso a sus documentos personales y poder enseñarlos.</p><p>De hecho, existen aplicaciones para IPhone, Blackberry y Android :D</p><h3>Últimos mejores planes antes de salir de viaje</h3><p>¡Pueden tener hasta dos equipajes de mano!</p><p>¿Cómo?</p><p>Casi todas las compañías aéreas autorizan un equipaje de mano para un accesorio: funda para laptop, bolso de mano...</p><p>Así que podrán tener un equipaje de mano grande (respetando las dimensiones legales o de la compañía) y otro para su laptop por ejemplo que podrán llenar al tope :D </p><p>Por ejemplo en la funda de mi laptop, suelo poner mis guías, papeles, y en otro equipaje de mano pongo mi cámara, ropa...</p><h3>Conclusión</h3><p>¡Espero que con este recordatorio no olvidarán nada para viajar a Nueva York!</p><p>Ahora si creen que olvido algo, no duden en decírmelo.</p></body></html>');

-- Location 11 (L'essentiel avant de partir -> Concernant les bagages) :
INSERT INTO PAGES VALUES ('2', '11', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>Reglas de equipaje para un vuelo internacional con destino a Estados Unidos</h1><p>Antes de viajar a USA, es importante saber qué pueden poner en su equipaje de mano, el peso de su maleta, etc...</p><p>Intento repasar las reglas más básicas para que no tengan problemas al llegar a New York.</p><p>Las reglas pueden variar de una compañía a otra, así les recomiendo verificar con la aerolínea para mayor seguridad. Este artículo resume las reglas de las compañías aéreas que llegan a EEUU.</p><h3>Equipaje de mano:</h3><ul><li>Las medidas de su equipaje de mano no deben exceder <b>55x35x25 cm</b>, las ruedas y asas incluidas.</li><li>Se considera que una <b>funda para ropa</b> es una valija estándar</li><li>Tienen derecho a <b>un accesorio</b>: bolso de mano, funda de cámara y otro tipo de aparato electrónico...</li><li>El peso total máximo autorizado en vuelo internacional (bulto estándar + accesorio) debe ser de entre <b>5 a 23 kg</b> dependiendo de las compañías. Por lo regular, no se verifica el peso de su equipaje de mano, pero si sienten que su maleta supera el peso admitido, traten de levantarla como si estuviera pesando casi nada :D </li><li>Los líquidos están prohibidos en  el equipaje de mano, menos en el caso: de ser <b>un envase de máximo 100 ml </b>y colocado en una <b>bolsita de plástico sellables</b> (como las bolsitas de congelación), que puedan contener un volumen máximo de 1 litro. Las bolsita debe estar cerrada y los artículos deben caber fácilmente. Una sola bolsita de plástico está permitida por pasajero.</li><li>Algunos <b>líquidos están permitidos sin restricción</b>:<ul><li>Los alimentos y sustancias médicas necesarias para su salud: los encargados de seguridad podrán pedirles que comprueben el uso de estas sustancias y en su caso, probarlas</li><li>Los alimentos nutricionales específicos sin los cuales no pueden viajar (caso de alergias a la lactosa, gluten...)</li><li>Los alimentos para bebe</li><li>Medicamentos recetados por un médico, que deberán justificar mediante una receta o certificado médico a su nombre</li></ul></li><li><b>elementos prohibidos en un avión</b>: cualquier objeto cortante (tijeras, cuchillos, navajas, …), con filo, armas blancas, armas de fuego, productos químicos, líquidos inflamables...</li></ul><h3>El buen plan:</h3><p>Tengan un gran equipaje de mano donde podrán colocar todo lo que necesiten (pensando que para el regreso podrán poner recuerdos frágiles por ejemplo) y una funda de ordenador/computadora en la cual podrán guardar muchas cosas: guías de viaje...</p><h3>Equipaje a facturar:</h3><p>Se trata de las maletas que irán en el compartimiento de equipajes del avión.</p><ul><li>La mayoría de las compañías con destino a Estados Unidos, admiten un peso máximo para un equipaje a facturar de <b>20 a 23 kg</b> dependiendo de cada compañías.</li><li>La valija no debe rebasar los <b>158 cm</b> (de alto+ancho+profundo).</li><li>Si su maleta <b>supera los 23kg pero no pesa más de 32 kg</b>, deberán <b>pagar un exceso de equipaje,</b> dependiendo de la compañía aérea (de 40 a 60 USD).</li></ul><p><b>El buen plan:</b> las aerolíneas tienen alianzas entre sí mismas de las cuales pueden ser miembros. Dependiendo del nivel que se les otorgue como miembros, cada alianza les ofrece ciertas ventajas sobre su equipaje, entre otras como;ascensos a primera clase, acceso a salones VIP, etc...</p></body></html>');

-- Location 12 (L'essentiel avant de partir -> Retirer des dollars) :
INSERT INTO PAGES VALUES ('3', '12', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>Los mejores planes para sacar dólares antes y durante su estancia en Nueva York</h1><p>Antes de subir al avión para Nueva York, sepan que hay diferentes maneras para sacar dinero y pagar sus compras allá.</p><p>Antes que todo y como siempre se los recomiendo, un viaje a Nueva York se organiza y prepara con tiempo. Para ahorrar un máximum de dinero, reserven y paguen sus visitas, atracciones, hotel, tours… antes de su salida a NY.</p><p>Como sé que no todo se puede comprar con antelación en internet, también es necesario que lleguen a NYC con algunos dólares en la cartera.</p><p>Naturalmente, desde cualquier aeropuerto de la ciudad tendrán que pagar el metro o el taxi en efectivo. Al menos que hayan reservado y “prepagado” por internet su shuttle (<a href="http://www.mejores-planes-viaje-nueva-york.com/reservar-shuttle-go-airlink-nyc/" target="_blank">ver más en MPVNY</a>) :D </p><h3>Sacar dólares antes de ir a Nueva York </h3><p>Para ello tienen varias opciones:</p><ul><li>Ir a un banco</li><li>Ir a una oficina de cambio.</li></ul><p>Primero, revisen en internet el tipo de cambio que aplica entre los dólares y su moneda nacional. Por ejemplo aquí: <a href="http://www.xe.com/es/" target="_blank">http://www.xe.com/es/</a>.</p><p>Si deciden acudir a un banco, les recomiendo ir al banco donde tienen su cuenta, pues por ser clientes pueden venderles dólares a mejor precio… Sin embargo no duden en ir a varios bancos y preguntar cuál es su tipo de cambio y si tienen alguna comisión.</p><p>Efectivamente algunos bancos, por tener acuerdos internacionales con bancos americanos (Bank of America por ejemplo), ofrecen un tipo de cambio más económico. Pasa lo mismo con los bancos internacionales como HSBC.</p><p>Tengo el mismo consejo con las oficinas de cambio. Si tienen una oficina cerca de su casa, revisen si ésta tiene un sitio internet. Tendrán información sobre su tipo de cambio sin desplazarse.</p><p>Sin embargo, tienen que ser muy precavidos con las oficinas de cambio, sobre todo las que están en los aeropuertos: el tipo de cambio que manejan no son competitivos.</p><p>De hecho, tengan mucho cuidado también con las oficinas de cambio en Nueva York: esperan a los turistas para venderles dólares a precio de oro...</p><h3>Comprar Travellers Cheques</h3><p>Es un medio de pago seguro y sencillo:</p><p>Los travellers cheques se compran en un banco. Cuando los reciben, tienen que firmarlos. Cuando quieran utilizarlos para pagar algo en NY, sólo tendrán que enseñar su pasaporte (o su credencial o su carnet de identidad) al vendedor y volver a firmar el cheque.</p><p>Si pierden o si les roban sus travellers cheques, tienen que llamar al servico cliente de American Express y podrán ir a una agencia American Express en New York. Tendrán que dar los números de serie de los travellers e indicar dónde los compraron. ¡Se los cambian por unos nuevos cheques! Los cheques robados o perdidos ya no servirán.</p><p>Algo importantes es apuntar los números de serie de sus travellers cheques y guardarlos en un lugar seguro. Lo mejor es dejarlos en su hotel en una de sus valijas. Al regresar al hotel después de algunas compras, tachen el número que corresponde a los travellers cheques usados...</p><p>Las ventajas de los travellers cheques son:</p><ul><li>Seguridad en caso de robo</li><li>No caducan</li><li>Utilizables por menores de edad</li><li>Utilizables para pagar en varios hoteles, comercios, restaurantes...</li><li>Intercambiables contra moneda local en varios establecimientos financieros, bancos u oficinas de cambio.</li></ul><p>Inconvenientes de los travelers cheques:</p><ul><li>La mayoría de los lugares que aceptan travellers cheques cobran comisiones</li><li>Algunos lugares turísticos no los aceptan (me pasó en el Empire State Building hace tres años)</li><li>Son nominativos, así que no pueden ser utilizados por otra persona y no pueden intercambiarse entre particulares.</li></ul><h3>Pagar con su tarjeta de crédito/débito</h3><p>Les va a parecer muy práctico hacer sus compras con su tarjeta bancaria, como en su país de origen. Pero en cada pago, su banco le puede cobrar comisiones...</p><p>Ahora, si tienen una Mastercard Gold o una Visa Premier, no tendrán comisiones por pagar con su tarjeta. Otros bancos tienen sistemas parecidos, depende de su país de origen.</p><p>En ambos casos ¡Pidan a su banco mayor información antes de partir a Nueva York!</p><h3>Sacar dinero con su tarjeta bancaria en Nueva York</h3><p>Existen dos tipos de distribuidores automáticos en Estados Unidos: los de los bancos o los que se encuentran en comercios u otros lugares, los ATM.</p><ul><li>Los ATM: Son los que cobran más comisiones. En cualquier parte de Nueva York encontrarán ATM. Son muy prácticos cuando quieren tener en la mano un poco de efectivo. Lo malo es que cobran unos 3 hasta 5 dólares cada vez que sacan dinero. Los ATM pueden ser prácticos en caso de emergencia pero mejor vayan a un distribuidor de algún banco.</li><li>Los Bancos: En los bancos, si quieren sacar dólares, no tendrán gastos extras, nada más los de su banco y del tipo de cambio.</li></ul><p>Otro buen plan es ir a su banco antes de su salida a NY, pregunten si tiene algún convenio con un banco americano. Suele pasar si su banco es internacional como HSBC, UBS, Citigroup…<p>También pregunten si su banco tiene oficinas en Nueva York, es el caso de BBVA por ejemplo. Puede ser útil tener la información antes de irse de vacaciones. Por si tienen algún problema, será más fácil acudir a la agencia de su banco en NY, pues son clientes.<h3>Conclusión </h3><p>Estos son las diferentes maneras de cambiar dinero o sacar dólares en EEUU para su estancia en Nueva York. Mi orden de preferencia para tener dólares sería:</p><ul><li>Ir a un banco que tiene convenio con un banco de EEUU en su país de origen.</li><li>Ir a una oficina de cambio en su país de origen (verificando en todos casos los tipos de cambio, comisiones, gastos extras que cobran).</li><li>Sacar dólares en un distribuidor de banco.</li><li>Sacar dinero en un ATM.</li><li>Cambiar dinero en una oficina de cambio en un aeropuerto.</li><li>Ir a una oficina de cambio para turistas en Nueva York.</li></ul><p>Antes de partir, no olviden preguntar a su banco TODO sobre las comisiones y gastos extras que les podrían cobrar, evitaran las “sorpresas” regresando a casa y leyendo su recibo mensual del banco…<h3>¡Ayúdenme!</h3><p>Por último, les agradecería que me cuenten cómo es en su país. Estas son recomendaciones generales, y puede ser que en algunos países los bancos tengan otro tipo de funcionamiento. Mi objetivo es ayudarles con mis mejores consejos pero ¿qué tal si son ustedes quienes tienen mejores consejos?</p><p>Déjenme un comentario abajo de este artículo, en el foro de MPVNY o por mail (mpvnuevayork@gmail.com) y actualizo este artículo.</p><p>¡De antemano muchas gracias!</p></body></html>');

-- Location 13 (L'essentiel avant de partir -> L'electronique à New York) :
INSERT INTO PAGES VALUES ('4', '13', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>¿Cómo funcionan sus aparatos eléctricos en Nueva York?</h1><p>Salir de viaje significa preparar valijas y maletas... sin olvidar los cargadores de sus aparatos eléctricos:</p><ul><li>Cargador de teléfono</li><li>Cargador para su cámara de fotos</li><li>Laptop</li><li>Rasuradora eléctrica</li><li>Secadora de cabello...</li></ul><p>¡Además, a partir del 8 de julio, es obligatorio tener sus aparatos electrónicos cargados y en buen estado! (<a href="http://www.mejores-planes-viaje-nueva-york.com/nueva-medida-de-seguridad-vuelos-nueva-york-estados-unidos/" target="_blank">más información en MPVNY</a>)</p><h3>El tipo de enchufe en Estados Unidos</h3><p>Es importante saber que los enchufes de Estados Unidos pueden ser diferentes al que usan en su país de origen. Así que podrían necesitar comprar un adaptador para que sus aparatos funcionen durante su viaje a Nueva York.</p><p>Los adaptadores se compran en cualquier tienda especializada o tiendas de grandes superficies.</p><p>Si viajan mucho a diferentes lugares del mundo, les aconsejo comprar un adaptador múltiple como el de la foto. Pero también existen adaptadores sencillos, aún más económicos y que funcionan muy bien si sólo prevén viajar a EEUU ;) </p><h3>El voltaje de sus aparatos eléctricos y electrónicos</h3><p>Otro punto importante, el voltaje también pueden ser diferente al de su país: 110V en EEUU.</p><p>Antes de llevar sus aparatos a NY, revisen que tengan una nota indicando "voltaje 110/220V". Si no es el caso, su aparato se “quemaría” al enchufarlo y utilizarlo con un voltaje no adaptado. No tendrían más remedios que tirar a la basura su aparato...</p><p>Pequeño consejo, aunque su secadora de cabello indique 110/220V, déjenla en casa… ¡Quemé varias secadoras! :D </p><p>También sepan que en la mayoría de los hoteles, hay secadoras de cabello.</p><h3>¿Quieren un buen plan de viajero?</h3><p>No compren un adaptador por cada aparato electrónico que tienen. Lo más económico es comprar un solo adaptador y una <b>base de enchufe múltiple</b>. Además, estas bases suelen tener una protección de sobretensiones.</p><p>¡Es mi mejor plan para viajar seguro, sin sorpresas y sin tener que comprar aparatos de repuesto en Nueva York… que no funcionarían de regreso a casa!</p></body></html>');

-- Location 14 (L'essentiel avant de partir -> Les transports à New York) :
INSERT INTO PAGES VALUES ('5', '14', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>El autobús y el metro con la MetroCard en Nueva York</h1><p>Si se quedan una semana o más en Nueva York, les podría ser útil subir al metro o en un autobús para poder desplazarse en la ciudad.</p><p>Sé que la mejor manera para visitar una ciudad es caminando, pero NY es muy grande, y rápidamente se cansarán :D</p><p>Para sus desplazamientos en Manhattan o para ir a Queens, Bronx, Brooklyn o Staten Island, el metro y el autobús serán buenos aliados de sus visitas.</p><h3>¿Qué ofrecen el metro y el bus en Nueva York?</h3><p>Las línes de metro y bus en Nueva York son muy completas y sobre todo complementarias.</p><p>La mayoría líneas de metro van de Norte-Sur y la mayoría de las líneas de autobús de Manhattan van de Este-Oeste.</p><h3>¿Cuáles son las líneas de Metro en Nueva York?</h3><p>Hay 22 líneas de metro en NYC.</p><p>Se diferencian por sur colores.</p><p>Los mismos colores tienen el mismo recorrido en una gran parte de Manhattan, pero se dividen en secciones en los boroughs de NYC (Queens, Bronx, Brooklyn, Staten Island y Harlem).</p><h3>¿Qué significan "local" y "express" en el metro de NY?</h3><p>Cada línea de metro tiene trenes "local" y "express".<p><strong>Local</strong>: significa que los trenes se paran en cada estaciones sin excepción. El recorrido será un poco más lento si quieren ir lejos.</p><p><strong>Express</strong>: los trenes se paran cada 2, 3, 4 o 5 estaciones dependiendo de las zonas de NY. Son trenes más rápidos.</p><p>El buen plan es subir a un Express para largos trayectos, permite llegar más rápidamente.</p><p>Lo malo es equivocarse al subir en un Express para bajar en 2-3 estaciones, porque llegarán demasiado lejos y tendrán que regresar.</p><p>Les va a pasar al menos una vez durante su estancia. Sí sí, cuando van corriendo entre dos metros y que no se fijan en los trenes... </p><h3>¿Cuáles son las tarifas del metro y del bus en Nueva York?</h3><p>Para el autobús y el metro, los tickets son los mismos.</p><p>El precio para un trayecto único en metro y bus local es de 2.75USD y de 6 USD para un Express Bus.</p><h3>¿Qué es la “7-Day Unlimited MetroCard”?</h3><p>¡Este pass les permite utilizar el metro o el autobús sin límite durante siete días!</p><p>Es bastante interesante porque en Nueva York, el metro funciona las 24 horas del día.</p><p>La tarjeta puede ser utilizada por una sola persona.</p><h3>¿Cuánto cuesta la “7-Day Unlimited MetroCard”?</h3><p>Esta MetroCard para 7 días cuesta 30 USD. Si se quedan un poco más tiempo en Nueva York, podrán comprar más créditos en cualquiera máquina distribuidora de MetroCard.</p><p>El pass de 7 días existe también para 30 días. En este caso, les costará 112 USD.</p><p>La tarjeta MetroCard tiene un coste de 1USD. Este coste aplica desde marzo 2013 para evitar que las tarjetas sean de un sólo uso.</p><p>Los usuarios del metro y de los autobuses neoyorquinos pueden recargar su tarjeta el número de veces que quieran, y evita que las tarjetas usadas acaben en la basura… ¡o estén tiradas en la calle!</p><h3>¿Qué es la Pay-Per-Ride MetroCard o Regular MetroCard?</h3><p>Si deciden comprar una tarjeta Pay-Per-Ride MetroCard, ésta les permite comprar el número de viajes que quieran.</p><p>Puede ser utilizada por 4 personas a la vez para el mismo viaje.</p><p>Al adquirirla, pongan la cantidad de dólares que quieran (a partir de 5 USD) y podrán utilizar tanto el metro como el autobús.</p><p>Su número de viajes está limitado al monto de dinero que compraron (o recargaron) en su tarjeta. Un viaje se entiende como la utilización combinada o no del metro y del autobús durante 2 horas.</p><p>El viaje sólo cuesta 2,75 USD. Si compran 10 USD, tienen un descuento del 5% y el viaje sale en 2,38 USD.</p><h3>¿Cuál tarjeta comprar?</h3><p>¡Depende de su estancia en Nueva York y de su uso del transporte público!</p><p>Para menos de 13 viajes en una semana: La Pay-Per-Ride cuesta 10 USD o sea 2,38 USD por viaje.</p><p>Para 13 viajes por semana: La 7-Day Unlimited cuesta 30 USD, o sea 2,31 USD por viaje.</p><p>Para 20 viajes por semana: La 7-Day Unlimited cuesta 30 USD, o sea 1,50 USD por viaje.</p><p>Para 25 viajes por semana: La 7-Day Unlimited cuesta 30 USD, o sea 1,20 USD por viaje.</p><p>En fin, si se quedan una semana en Nueva York, no duden en comprar la MetroCard inlimitada. A partir de 12 viajes en metro o autobús, la tarjeta es rentable en 7 días. </p><p>Si se suben 4 veces por día, es rentable en sólo 3 días ;)</p><p>La MetroCard inlimitada de 30 días es rentable con 45 viajes. Así que si utilizan 4 veces al día el autobús o el metro, la tarjeta es rentable a partir de 12 días :D</p><h3>¿Dónde pueden comprar su MetroCard? </h3><p>En las máquinas distribuidoras o <h3>MetroCard Vending Machines</h3>, les recomiendo las que acepan dinero en efectivo. Pueden seleccionar su idioma en las máquinas ;)</p><p>En las taquillas del Metro o <h3>Subway Station Booths</h3>, sólo aceptan dinero en efectivo.</p><p>¡No todas las bocas de metro tienen taquillas o máquinas, fíjense en los carteles al entrar al metro!</p><p>En el bus o el van MetroCard o <h3>MetroCard Bus and Vans. </h3>Se encuentran en diferentes zonas de la ciudad por un tiempo definido, dependiendo de los días.</p><p>En algunos comercios o <h3>Neighborhood Merchants.</h3> Se pueden localizar desde el sitio internet de MTA.</p><p>Más info aquí: <a href="http://www.mta.info/metrocard/zipmaps.htm">http://www.mta.info/metrocard/zipmaps.htm</a>.</p><h3>¿Cómo pueden comprar en las máquinas?</h3><p>Pueden poner billetes y utilizar sus tarjetas de débito en las máquinas.</p><p>OJO, al pagar con su tarjeta bancaria, la máquina les pide un ZIP Code (un código postal local). Para los que viven en el extranjero tienen que indicar el siguiente código: 99999 (9 veces el número 9).</p><h3>Lo que tienen que saber antes de subir al metro:</h3><p>Si utilizan por primera vez su Unlimited Metrocard, no la podrán volver a deslizar antes de 18 minutos. Así que cuando suben a una estación de metro, no se equivoquen. Algunas están divididas en 2 (una hacia Uptown y otra hacia Downtown) y no tienen escaleras que las conecten. Así que si se equivocan de dirección, tendrán que esperar 18 minutos antes de volver a utilizar su Metrocard.</p><p>Si tienen una carrito para bebés o una valija grande que no pasa por el torniquete, tendrán que pasar por la gran puerta que es la salida de emergencia, justo al lado de los torniquetes. Las puertas no se abren desde el exterior así que alguien tiene que pasar por el torniquete con su MetroCard y luego abrirles la puerta. Si viajan sólos, pueden pedir a un neoyorquino que les ayudará por seguro.</p><h3>Comparativo entre el autobús y el metro de Nueva York</h3><p>Son dos medios de transporte diferentes y no ofrecen las mismas posibilidades.</p><p>No ofrecen las mismas posibilidades</p><ul><li>Lo más rápido: el metro. Es lo más rápido que el autobús porque éste tiene una parada en cada cuadra y tiene que respetar los semáforos. Al final, se para más de 2 veces cada 100 o 200 metros… cuando no hay tráfico.</li><li>Para viajar Este-Oeste: el autobús. Sus recorridos van esencialmente de Este-Oeste, donde no hay metro.</li><li>Lo mejor para descubrir Nueva York: el autobús. Los túneles del metro no dejan ver bien el paisaje :D</li><li>Lo mejor para encontrar neoyorquinos: el autobús. Hay mucho menos turistas en el autobús ;)</li><li>Lo más confortable: el metro, siempre encontrarán donde sentarse, al menos que viajen en horas pico.</li><li>Lo más preciso: el autobús porque tiene más paradas.</li><li>El que recorre más distancia: el metro, sus líneas son más largas que las del autobús.</li><li>Lo más económico: empatados porque tienen el mismo precio… salvo los Express Bus.</li></ul><h3>Comparativo entre el taxi y el metro de Nueva York</h3><p>A menudo escucho que desplazarse en taxi es lo mejor en NY.</p><p>Les dejo mi comparativo y a ver qué opinan:</p><ul><li>Lo más rápido: depende a dónde quieren ir y si hay tráfico. Por lo regular, pueden ahorrar unos 5-10 minutos gracias al taxi, pero en horas pico el metro es mucho más rápido.</li><li>Lo mejor para descubrir NY: el taxi, por las mismas razones que el autobús.</li><li>Lo mejor para encontrar neoyorquinos: depende porque en el metro verán mucha gente. En el taxi, es fácil conversar con el chófer.</li><li>Lo más confortable: diría que el metro. Los taxis son coches confortables pero la mayoría de los taxista conducen muy rápidamente y hasta les puede asustar.</li><li>Lo menos cansado: el taxi. Hay por todos lados, pueden subirse donde quieran y a donde quieran para que no caminen mucho.</li><li>Lo más preciso: el taxi, por las mismas razones.</li><li>El más económico: el metro porque unos 10-15 minutos de taxi cuesta de 6 a 10 USD.</li></ul><h3>Conclusión</h3><p>Ya saben, para visitar Nueva York caminar es lo mejor. Sin embargo la ciudad es demasiado grande para que puedan verlo todo caminando... El autobús y el metro son dos medios de transporte seguros que les va a resultar muy útiles durante sus vacaciones. Ahora si prefieren utilizar un transporte más ecológico, existen las Citibike. Les explico cómo funcionan en <a href="http://www.mejores-planes-viaje-nueva-york.com/fotos-construccion-estatua-de-la-libertad-paris/" target="_blank">MPVNY</a>.</p></body></html>');

-- Location 15 (L'essentiel avant de partir -> Prendre des photos) :
INSERT INTO PAGES VALUES ('6', '15', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>¿Dónde sacar las mejores fotos de Nueva York?</h1><p>Para los amateurs de fotografía, Nueva York es una ciudad perfecta para sacar fotos: sus famosos rascacielos, la arquitectura neoyorquina, las diferentes perspectivas de NYC...</p><p>En este artículo intentaré darles mis mejores consejos para poder sacar increíbles fotos de Nueva York desde diferentes lugares. No les voy a poder indicar todos los lugares, son los que más me llaman la atención ;) </p><h3>La vista desde las calles de Nueva York</h3><p>Este tipo de fotos tendrá obviamente un toque “Street”, podrán divertirse con la arquitectura y este tipo de lugares que parecen de algunas películas.</p><p>Una calle con una buena luz, unos edificios en perspectiva y algunos taxis y ya tienen su foto perfecta de Nueva York.</p><p>De hecho les recomiendo acercarse al piso para poder sacar fotos muy originales como la que sigue, en Meatpacking District.</p><p>En las calles neoyorquinas, lo mejor es tener un lente gran angular, sobre todo cuando los buildings son muy altos o las calles muy anchas, como en la siguiente foto.</p><h3>La vista desde un building o un rooftop</h3><p>Nueva York es una ciudad muy conocida por sus rascacielos, así que si pueden sacar sus fotos desde uno de ellos, tendrán unas muy buenas fotos.</p><p>Puede ser desde un bar como el <a href="http://www.mejores-planes-viaje-nueva-york.com/230-fifth-rooftop-bar-terraza-panoramica-esb/" target="_blank">230 Fifth Rooftop</a>.</p><p>Las siguientes fotos fueron sacadas desde un balcón de un apartamento en Soho:</p><p>Otra foto desde una terraza rooftop de un edificio en el cual había alquilado un apartamento:</p><p>Luego hay lugares que no se pueden perder como la vista desde el Empire State Building.</p><p>Pueden hacer un zoom y sacar una foto del Flatiron Building (con forma triangular), el Madison Square Park (a la izquierda) y el 230 Fifth Rooftop (abajo a la derecha):</p><p>También la vista desde el Top of the Rock, hacia el Sur es preciosa, se ve el Empire State Building y Financial District:</p><p>Hacia el Norte, se ve Central Park.</p><p>En <a href="http://www.mejores-planes-viaje-nueva-york.com/subir-empire-state-building-top-of-the-rock-nueva-york/" target="_blank">MPVNY</a> les doy recomendaciones para saber cuándo subir al ESB y al Top of the Rock.</p><h3>Sacar fotos de Nueva York desde un barco</h3><p>Podrán sacar muy bonitas fotos de NYC desde el ferry que les lleva de Staten Island a Ellis Island o en el barco del Circle Line (del <a href="http://http://es.citypass.com/new-york?mv_source=mejoresplanesviajeny " target="_blank">New York City Pass</a>).</p><p>Lo interesante es que van a ver Manhattan “alejarse” poco a poco, con una vista diferente cada 30 segundos… van a tener ganas de sacar muchas fotos ;) </p><p>Los tours de noche en barco son muy agradables, pero para sacar fotos es más complicado. Efectivamente, el barco está en movimiento y es difícil sacar una foto que no sea borrosa.</p><h3>La vista desde Central Park</h3><p>En <a href="http://www.mejores-planes-viaje-nueva-york.com/que-hacer-central-park/" target="_blank">Central Park</a>, recomiendo 5 lugares que tienen una linda vista sobre la cuidad.</p><p>Primero está la vista desde Belvedere Castle, desde donde se ve el Turtle Pond, las canchas de béisbol, los caminos...</p><p>Al Sureste del parque hay una roca llamada "The Roc" en la cual podrán subir para sacar fotos. A mí me encanta esta foto de Gaëtan Habrand.</p><p>Desde The Pond, un pequeño lago al Sureste del parque es también un lugar mu lindo para sacar fotos. Tiene un puente de piedra, y que sea en invierno con nieve, durante la primavera o el verano con los árboles verdes, o durante el otoño con el cambio de colores, sacaran bonitas fotos.</p><p>The Reservoir es un lugar perfecto para correr... ¡pero también para sacar fotos!</p><p>The Lake es otro lugar de interés para las fotos, porque se ven las lanchas y los edificios de Upper West Side. Además en esta zona podrán hacer una parada para ir a comer al Loeb Boathouse.</p><h3>La vista desde la High Line</h3><p>La <a href="http://www.mejores-planes-viaje-nueva-york.com/paseo-descubrir-high-line-nueva-york/" target="_blank">High Line</a> es un paseo que les recomiendo hacer en MPVNY. Está a unos 10 metros de altura así que tendrán una buena perspectiva de las calles de NY y de las zonas alrededor.</p><h3>La vista desde la escalera de TKTS en Times Square</h3><p>Times Square es un lugar mítico de Nueva York.</p><p>Lo mejor es sentarse en la escalera rojas de la <a href="http://www.mejores-planes-viaje-nueva-york.com/show-broadway-descuento/" target="_blank">empresa TKTS</a> que vende tickets para musicales en Broadway.</p><h3>La vista desde un helicóptero</h3><p>Sacar una foto desde un helicóptero es una de las maneras más divertidas de sacar fotos de Nueva York. El vuelo sobre la ciudad es por sí una gran experiencia, y tendrán un panorama excepcional para una foto de Nueva York desde el cielo. Es algo que recomiendo mucho.</p><p>Para más información sobre los diferentes vuelos, revisen este<a href="http://www.mejores-planes-viaje-nueva-york.com/vuelan-sobre-nueva-york-helicoptero/" target="_blank"> artículo MPVNY</a>.</p><h3>Sacar una foto de la escultura LOVE</h3><p>Esta escultura fue creada por el artista americano Robert Indiana, es bonita y perfecta para una foto, sobre todo con su pareja ;) </p><p>Esta cerca del museo del MoMA.</p><h3>La vista desde el puente Tudor City</h3><p>Este puente se encuentra en Midtown, cerda de Grand Central. Ofrece una vista sobre la calle 42 con el Chrysler Building en la derecha y la parte baja de Times Square en el fondo.</p><h3>Sacar una foto durante el Manhattanhenge</h3><p>2 veces por año, el sol se pone justo en el eje de las calles de Nueva York. Es un fenómeno que no se pueden perder porque se ve increíble.</p><p>Para verlo, todas las <a href="http://www.mejores-planes-viaje-nueva-york.com/manhattanhenge-en-2014-nueva-york/" target="_blank">informaciones están aquí</a>.</p><h3>La vista desde el Apple Store de Grand Central</h3><p>Grand Central es uno de los lugares imperdibles de Nueva York. Para ver el hall principal en una sola foto, tendrán que ir al Apple Store en el medio, para poder sacar los grandes ventanales, la bandera americana y la gente que transita por la estación de tren.</p><p>Si quieren quedarse un rato más por la estación de tren, <a href="http://www.mejores-planes-viaje-nueva-york.com/grand-central-terminal-buen-plan-nueva-york-florent/" target="_blank">lean aquí algunos consejos en MPVNY para visitarla</a>.</p><h3>La vista desde Brooklyn</h3><p>El lugar que más me llamó la atención es Brooklyn o mejor dicho <b><a href="http://www.mejores-planes-viaje-nueva-york.com/portafolio-mpvny-brooklyn-bridge-park/" target="_blank">Brooklyn Heights Promenade</a>: </b>es un paseo muy lindo de unos 4,5 km con vista excepcional sobre Manhattan, el puente de Brooklyn (Brooklyn Bridge) y la Estatua de la Libertad.</p><p>El buen plan es que este lugar es de fácil acceso desde Manhattan con el metro o caminando.</p><p>Podrán ir tanto de noche que de día, pero si tienen que escoger entre los dos, les recomiendo ir de noche: el panorama iluminado se ve increíble.</p><p>Desde Williamsburg, podrán sacar lindas fotos, por ejemplo cerca del Flea Market y del Water Way.</p><p>O desde el rooftop del <a href="http://www.mejores-planes-viaje-nueva-york.com/the-ides-bar-rooftop-whyte-hotel-williamsburg-brooklyn/" target="_blank">Whyte Hotel</a>:</p><h3>La vista desde el Queens</h3><p>Si visitan Queens, les recomiendo ir a <a href="http://www.mejores-planes-viaje-nueva-york.com/gantry-plaza-state-park-queens/" target="_blank">Gantry Plaza State Park</a>.</p><p>Es un lugar que da un lindo panorama de la skyline y de Midtown.</p><h3>La vista desde New Jersey</h3><p>El paseo sobre las riberas de Hoboken y Weehawken es muy agradable.</p><p>De hecho también les tengo un buen plan para ir a cenar en un restaurante de <a href="http://www.mejores-planes-viaje-nueva-york.com/chart-house-restaurante-new-jersey-vista-manhattan-buen-plan-nueva-york-olivier/" target="_blank">New Jersey aquí</a>.</p><h3>La vista desde la Corona de la Estatua de la Libertad</h3><p>¡Es uno de los monumentos más famosos del mundo! Se puede subir a la corona (<a href="http://www.mejores-planes-viaje-nueva-york.com/corona-estatua-libertad/" target="_blank">ver más aquí</a>) y sacar su brazo para sacar una foto como la de abajo. También les recomiendo tener un lente gran angular si quieren subir a la corona.</p><h3>Las fotos que no pueden faltar: los taxis neoyorquinos</h3><p>¿Qué es más típico que los taxis amarillos de Nueva York?</p><p>Pueden sacarles diferentes fotos, desde varios puntos de vista y jugando con la velocidad de éstos.</p><h3>Sacar Street Foto en Nueva York</h3><p>Para los que quieren sacar fotos más originales con la gente de Nueva York, verán que tendrán muchos retratos que hacer.</p><p>Sin embargo, les recomiendo ser discretos porque el tipo de la foto no parecía muy feliz.</p><p>Si les gusta este tipo de fotos, pueden comprarse el libro de <a href="http://www.mejores-planes-viaje-nueva-york.com/humans-of-new-york-libro-street-photo/" target="_blank">Humans of New York</a> ;) </p><h3>Sacar fotos de noche en Nueva York</h3><p>El tripie es obligatorio para sacar fotos de Nueva York de noche.</p><p>Pueden regresar a los lugares que les gustaron de día o ir a uno de los lugares que les recomiendo más arriba para sacar fotos.</p><h3>Nueva york de blanco y negro</h3><p>Si quieren sacar fotos originales, pongan su cámara en la función blanco y negro.</p><h3>Tomar clases de fotografía en Nueva York</h3><p>Si quieren aprender a sacar fotos o no perderse de algunos lugares, podrían contratar una clase de fotografía con Citifari.</p><h3>Lo más importante para sacar las mejores fotos de Nueva York</h3><p>Saquen todas las fotos que puedan, nunca se arrepentirán porque luego podrán borrar las que no les gusta.</p><p>Traten de sacar fotos únicas y originales para no tener la misma fotos que los demás turistas.</p><p>Si no tienen todo el material necesario, pueden ir a alquilarlo en <a href="http://www.mejores-planes-viaje-nueva-york.com/adorama-rental-co-alquiler-material-foto-video-nueva-york/" target="_blank">Adorama</a> en NY.</p><h3>Conclusión</h3><p>Espero haberles dado un máximum de consejos para sacar sus fotos una vez en Nueva york.</p><p>Les invito a compartir sus fotos con la comunidad de MPVNY en <a href="https://www.facebook.com/MejoresPlanesViajeNuevaYork" target="_blank">Facebook</a>, <a href="https://twitter.com/MejoresPlanesNY" target="_blank">Twitter</a>, el <a href="http://www.mejores-planes-viaje-nueva-york.com/foro/" target="_blank">foro</a>, sobre todo si descubren otros lugares de interés y si quieren sacar su foto <a href="http://www.mejores-planes-viaje-nueva-york.com/foto-i-love-mejores-planes-para-un-viaje-a-nueva-york/" target="_blank">I Love MPVNY</a>.</p></body></html>');

-- Location 17 (L'essentiel sur place -> Envoyer une carte postale) :
INSERT INTO PAGES VALUES ('8', '17', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>Salen de viaje a Nueva York próximamente, y quieren enviar una postal a su familia y amigos. </h1><p>Les propongo un buen plan que les permitirá enviar una postal personalizada y más barata que una verdadera postal. ¡Además les garantizo que les gustará mucho a sus amigos!</p><p>Les explico cómo enviar su postal personalizada en 6 pasos:</p><h3>1r paso: SACAR LA FOTO</h3><p>Sáquense una foto enfrente de un lugar famoso de Nueva York o enfrente de un monumento representativo de NYC. Por ejemplo, les enseño una foto que sacamos durante unas vacaciones en Nueva York . Estábamos en el techo de nuestro apartamento ubicado en East Village. <p>Puse la cámara en una silla, ella misma en una mesa, el temporizador de la cámara y listo: resulta una bonita foto de mi pareja y los rascacielos de Nueva York en fondo. </p><p>Obviamente, pueden pedir a un americano u otro turista sacarles una foto. Ahora sí un consejo, si piden este tipo de ayuda a un turista, fíjense si esta persona tiene ya una bonita cámara (pues puede significar que sabe sacar fotos… ¡y no estará interesado en fugarse con su cámara!). Luego verifiquen si la foto les gusta, o saquen 2-3 fotos. </p><p>Idea de lugares míticos: podrán sacar su foto en la estatua de la Libertad, en Times Square, en el Rockfeller Center, en Central Park, enfrente del MOMA, del Flatiron Building, arriba del Empire State Building o del Top of the Rock, en Brooklyn Heights… ¡o en muchos otros lugares más! </p><h3>2nda etapa: Poner un título a la foto</h3><p>se llevaron su computadora u ordenador a Nueva York, y si saben utilizar programas de foto o imágenes, podrán poner un título a su foto. </p><p>En diciembre del 2009, fuimos a Nueva York para las fiestas de fin de año. Un turista nos sacó una foto en frente de las decoraciones del Rockfeller Center. Añadí el “Merry Christmas from New York”, es una postal que gustó mucho a nuestros amigos. </p><p>También habíamos sacado una muy bonita foto en la terraza de nuestro hotel, el Warwick, y la utilicé para enviar nuestros deseos de año nuevo por mail a nuestro amigos.</p><h3>3er paso: imprimir la foto</h3><p>Ahora que escogieron la foto, y que le hayan puesto un texto personalizado o no, pueden ir a muchos supermercados tipo Walgreens, Whole &amp;Foods,… e imprimir su foto en una de las máquinas. </p><p>Son aparatos muy fáciles de utilizar, sólo hay que insertar la memoria de su cámara y sigan las instrucciones que aparecen en la pantalla. Siempre habrá un vendedor por ahí listo para ayudarles. </p><p> La máquina les propondrá:</p><ul><li>Qué tipo de impresión quieren: foto, postal, calendario… </li><li>Qué formato quieren: si quieren un tamaño clásico de postal, 10cm x 15cm, escogen el formato 4x6. </li><li>En algunas máquinas, hasta podrán aplicar un marco negro o blanco a su foto. </li><li>Luego tienen que escoger la foto que está en la memoria de su cámara. </li><li>Confirmen el pedido y paguen. </li></ul><p>En abril del año pasado, me había costado 49 cents/foto, o sea 4,90 USD por 10 fotos en 4x6. </p><h3>4to paso (opcional): COMPRAR LOS SOBRES</h3><p>Si quieren que su foto llegue en mejor estado, pónganlas en un sobre. Aprovechen la impresión de sus fotos para comprar sobres, los encontrarán en Walgreens por ejemplo. </p><p>También pueden llevarse unos sobres en la maleta y un plumón que sirva para escribir en el papel de la foto. Perderán menos tiempo si de una vez preparen los sobres con las dirección de sus familiares y amigos ;) </p><h3>5ta etapa: ESCRIBIR UN MENSAJE EN SUS NUEVAS POSTALES</h3><p>Ahora les toca escribir algo atrás de su postal… no les puedo ayudar mucho con este paso :D </p><p>Pero bueno, ahora gracias a internet, facebook y otros programas, ya no contamos todo nuestro viaje en una postal, sólo aprovechamos para mandar un abrazo y besos, lo importante es que nuestros seres queridos sientan que pensamos en ellos durante nuestras vacaciones. </p><h3>6to paso: COMPRAR SELLOS y DEJAR SUS POSTALES EN EL BUZÓN</h3><p>Encontrar sellos postales en Nueva York es fácil, tienen que ir a los Post Office. </p><p>A mi me gusta mucho ir al Post Office que se encuentra cerca de Penn Station, justo enfrente del Madison Square Garden. El edificio es muy bonito tanto desde el exterior que en su interior. Además cuenta con muchos demostradores, así que no se pierde mucho tiempo en colas. </p><h3>CONCLUSIÓN</h3><p>Ya está, tienen una bonita postal personalizada de Nueva York para sus amigos y familiares. </p><p>Es muy fácil crear una postal que gustará a todos, además, los que la recibirán podrán colocar la foto en un bonito marco y presumirla en su salón ;) </p></body></html>');

-- Location 18 (L'essentiel sur place -> Profiter de votre séjour) :
-- INSERT INTO PAGES VALUES ('9', '18', 'HTML', '');

-- Location 19 (L'essentiel sur place -> Acheter des souvenirs) :
INSERT INTO PAGES VALUES ('10', '19', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>¿Quieren comprar recuerdos originales o clásicos y no saben dónde comprarlos?</h1><p>Existen varios lugares muy turísticos donde se pueden encontrar recuerdos clásicos y a buen precio.</p><h3>Recuerdos con "I Love New York"</h3><p>El más económico que he podido descubrir en Nueva York es el que está justo al lado del Empire State Building, en la 5ta avenida en la esquina con la calle 32 New York Souvenirs.</p><p>Encontrarán de todo: mugs New York, bolígrafos, cuadernos, broches, llaveros, bolsos “Manhattan Portage”, playeras y camisetas (NYPD, FDNY, I Love NY, …) ¡De todo! </p><p>Si quieren regalar algunos recuerdos a toda su familia o amigos, verán que los t-shirts I Love NY cuestan 10 USD los 4 :D </p><p>Encontrarán también buenas tiendas de recuerdos en Times Square, China Town (por Canal Street), y en el centro comercial Manhattan Mall. </p><h3>Recuerdos de los equipos de NYC y de Estados Unidos: </h3><p>Para la NBA y el basketball, existe el famoso NBA Store en la 5ta avenida (esquina con la calle 52). Encontrarán todo lo que puedan imaginar sobre la NBA: playeras de Boris Diaw, Carmelo Antony, Lebron James… Michael Jordan, Magic Jonhson... </p><p>Para la NHL y el Hockey, la tienda de los Rangers está en el Madison Square Garden, a mano izquierda en el fondo. </p><p>El NHL Store se encuentra en la esquina de la calle 47 y de la Avenue of the Americas. </p><p>Las tiendas oficiales de la MLB y Baseball de NY (Yankees y Mets) proponen una gran variedad de productos con el logo de ambos equipos. </p><p>El equipamiento de baseball se encuentra en las tiendas especializadas de deportes. Para ello les recomiendo la tienda Paragon Sport (cerca de Union Square) porque tienen una amplia gama de accesorios para el baseball. </p><p>Si quieren un bate de béisbol para decorar su casa, lo mejor es comprar un bate de madera, pues es más barato y además es el que utilizan los profesionales. </p><h3>T-shirts originales:</h3><p>Una de las tiendas que más me gusta en Nueva York para comprar t-shirts originales es Yellow Rat Bastard.</p><p>Está en 483 Broadway, New York, NY 10013. </p><h3>Comprar una bandera de Estados Unidos:</h3><p>No es siempre fácil encontrar lugares donde venden banderas de EEUU en Nueva York cuando no es un día de fiesta nacional. Sin embargo pueden ir a la tienda que está justo enfrente del City Hall Park (entre Broadway y Beekman Street). </p><p>También las encontrarán en el centro comercial Manhattan Mall ubicado en la 6ta avenida y la calle 33. </p></body></html>');

-- Location 20 (L'essentiel sur place -> Retirer des dollars) :
INSERT INTO PAGES VALUES ('11', '20', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>Los mejores planes para sacar dólares antes y durante su estancia en Nueva York</h1><p>Antes de subir al avión para Nueva York, sepan que hay diferentes maneras para sacar dinero y pagar sus compras allá.</p><p>Antes que todo y como siempre se los recomiendo, un viaje a Nueva York se organiza y prepara con tiempo. Para ahorrar un máximum de dinero, reserven y paguen sus visitas, atracciones, hotel, tours… antes de su salida a NY.</p><p>Como sé que no todo se puede comprar con antelación en internet, también es necesario que lleguen a NYC con algunos dólares en la cartera.</p><p>Naturalmente, desde cualquier aeropuerto de la ciudad tendrán que pagar el metro o el taxi en efectivo. Al menos que hayan reservado y “prepagado” por internet su shuttle (<a href="http://www.mejores-planes-viaje-nueva-york.com/reservar-shuttle-go-airlink-nyc/" target="_blank">ver más en MPVNY</a>) :D </p><h3>Sacar dólares antes de ir a Nueva York </h3><p>Para ello tienen varias opciones:</p><ul><li>Ir a un banco</li><li>Ir a una oficina de cambio.</li></ul><p>Primero, revisen en internet el tipo de cambio que aplica entre los dólares y su moneda nacional. Por ejemplo aquí: <a href="http://www.xe.com/es/" target="_blank">http://www.xe.com/es/</a>.</p><p>Si deciden acudir a un banco, les recomiendo ir al banco donde tienen su cuenta, pues por ser clientes pueden venderles dólares a mejor precio… Sin embargo no duden en ir a varios bancos y preguntar cuál es su tipo de cambio y si tienen alguna comisión.</p><p>Efectivamente algunos bancos, por tener acuerdos internacionales con bancos americanos (Bank of America por ejemplo), ofrecen un tipo de cambio más económico. Pasa lo mismo con los bancos internacionales como HSBC.</p><p>Tengo el mismo consejo con las oficinas de cambio. Si tienen una oficina cerca de su casa, revisen si ésta tiene un sitio internet. Tendrán información sobre su tipo de cambio sin desplazarse.</p><p>Sin embargo, tienen que ser muy precavidos con las oficinas de cambio, sobre todo las que están en los aeropuertos: el tipo de cambio que manejan no son competitivos.</p><p>De hecho, tengan mucho cuidado también con las oficinas de cambio en Nueva York: esperan a los turistas para venderles dólares a precio de oro...</p><h3>Comprar Travellers Cheques</h3><p>Es un medio de pago seguro y sencillo:</p><p>Los travellers cheques se compran en un banco. Cuando los reciben, tienen que firmarlos. Cuando quieran utilizarlos para pagar algo en NY, sólo tendrán que enseñar su pasaporte (o su credencial o su carnet de identidad) al vendedor y volver a firmar el cheque.</p><p>Si pierden o si les roban sus travellers cheques, tienen que llamar al servico cliente de American Express y podrán ir a una agencia American Express en New York. Tendrán que dar los números de serie de los travellers e indicar dónde los compraron. ¡Se los cambian por unos nuevos cheques! Los cheques robados o perdidos ya no servirán.</p><p>Algo importantes es apuntar los números de serie de sus travellers cheques y guardarlos en un lugar seguro. Lo mejor es dejarlos en su hotel en una de sus valijas. Al regresar al hotel después de algunas compras, tachen el número que corresponde a los travellers cheques usados...</p><p>Las ventajas de los travellers cheques son:</p><ul><li>Seguridad en caso de robo</li><li>No caducan</li><li>Utilizables por menores de edad</li><li>Utilizables para pagar en varios hoteles, comercios, restaurantes...</li><li>Intercambiables contra moneda local en varios establecimientos financieros, bancos u oficinas de cambio.</li></ul><p>Inconvenientes de los travelers cheques:</p><ul><li>La mayoría de los lugares que aceptan travellers cheques cobran comisiones</li><li>Algunos lugares turísticos no los aceptan (me pasó en el Empire State Building hace tres años)</li><li>Son nominativos, así que no pueden ser utilizados por otra persona y no pueden intercambiarse entre particulares.</li></ul><h3>Pagar con su tarjeta de crédito/débito</h3><p>Les va a parecer muy práctico hacer sus compras con su tarjeta bancaria, como en su país de origen. Pero en cada pago, su banco le puede cobrar comisiones...</p><p>Ahora, si tienen una Mastercard Gold o una Visa Premier, no tendrán comisiones por pagar con su tarjeta. Otros bancos tienen sistemas parecidos, depende de su país de origen.</p><p>En ambos casos ¡Pidan a su banco mayor información antes de partir a Nueva York!</p><h3>Sacar dinero con su tarjeta bancaria en Nueva York</h3><p>Existen dos tipos de distribuidores automáticos en Estados Unidos: los de los bancos o los que se encuentran en comercios u otros lugares, los ATM.</p><ul><li>Los ATM: Son los que cobran más comisiones. En cualquier parte de Nueva York encontrarán ATM. Son muy prácticos cuando quieren tener en la mano un poco de efectivo. Lo malo es que cobran unos 3 hasta 5 dólares cada vez que sacan dinero. Los ATM pueden ser prácticos en caso de emergencia pero mejor vayan a un distribuidor de algún banco.</li><li>Los Bancos: En los bancos, si quieren sacar dólares, no tendrán gastos extras, nada más los de su banco y del tipo de cambio.</li></ul><p>Otro buen plan es ir a su banco antes de su salida a NY, pregunten si tiene algún convenio con un banco americano. Suele pasar si su banco es internacional como HSBC, UBS, Citigroup…<p>También pregunten si su banco tiene oficinas en Nueva York, es el caso de BBVA por ejemplo. Puede ser útil tener la información antes de irse de vacaciones. Por si tienen algún problema, será más fácil acudir a la agencia de su banco en NY, pues son clientes.<h3>Conclusión </h3><p>Estos son las diferentes maneras de cambiar dinero o sacar dólares en EEUU para su estancia en Nueva York. Mi orden de preferencia para tener dólares sería:</p><ul><li>Ir a un banco que tiene convenio con un banco de EEUU en su país de origen.</li><li>Ir a una oficina de cambio en su país de origen (verificando en todos casos los tipos de cambio, comisiones, gastos extras que cobran).</li><li>Sacar dólares en un distribuidor de banco.</li><li>Sacar dinero en un ATM.</li><li>Cambiar dinero en una oficina de cambio en un aeropuerto.</li><li>Ir a una oficina de cambio para turistas en Nueva York.</li></ul><p>Antes de partir, no olviden preguntar a su banco TODO sobre las comisiones y gastos extras que les podrían cobrar, evitaran las “sorpresas” regresando a casa y leyendo su recibo mensual del banco…<h3>¡Ayúdenme!</h3><p>Por último, les agradecería que me cuenten cómo es en su país. Estas son recomendaciones generales, y puede ser que en algunos países los bancos tengan otro tipo de funcionamiento. Mi objetivo es ayudarles con mis mejores consejos pero ¿qué tal si son ustedes quienes tienen mejores consejos?</p><p>Déjenme un comentario abajo de este artículo, en el foro de MPVNY o por mail (mpvnuevayork@gmail.com) y actualizo este artículo.</p><p>¡De antemano muchas gracias!</p></body></html>');

-- Location 20 (L'essentiel sur place -> Plus d'infos sur New York) :
INSERT INTO PAGES VALUES ('12', '21', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>Algunos tips antes de salir de viaje a Estados Unidos y a Nueva York</h1><p>Antes de salir de viaje a Nueva York, sepan que algunas reglas pueden ser diferentes a las que acostumbramos en nuestro país de origen: que sean relativas al alcohol, el tabaco, la carretera, las farmacias, las propinas…</p><h3>El tabaco:</h3><p>La venta de tabaco está prohibida a menores de 18 años.</p><p>Si tienen menos de 18 años y fuman en la calle, podrían provocar cierta tensión con la gente que concurre el lugar, es bastante mal visto ahí.</p><p>Por si quieren fumar, tengan sus precauciones porque está prohibido en los lugares públicos: estaciones de tren, restaurantes, bares, discotecas, hoteles… y ahora los parques.</p><p>Luego, en cada Estado americano la ley es diferente. Por ejemplo en Oregon, la ley indica que no se puede fumar a menos de 10 pies (3 metros) de las puertas  de entrada de algún lugar público. En Nueva York, por el momento la ley no es más específica, salvo en el caso de la NYU (New York University) en Greenwich Village y en frente de las High School donde no se puede fumar a menos de 15 pies de las entradas (4,5metros).</p><h3>El alcohol:</h3><p>Como para el tabaco, para poder comprar o entrar en los bares donde se vende,  hay que tener 21 años mínimo y enseñar una identificación oficial o carnet de identidad.</p><p>Sin embargo, el consumo de alcohol está estrictamente prohibido en la calle. Sólo se permite en Las Vegas ;)</p><h3>Identificación:</h3><p>No es necesario que tengan su pasaporte en su cartera a lo largo de su estancia. Siempre que voy a NYC llevo mi pasaporte y mi carnet de identidad/identificación (ID). En cuanto llego al hotel, dejo mi pasaporte en la maleta y sólo me llevo mi identificación en la cartera.</p><p>En caso de perder su cartera, su pasaporte guardado en el hotel les permitirá identificarse y luego salir del país.</p><p>Los más ansiosos o precavidos harán una copia de su pasaporte antes de salir de viaje, y así pueden dejar la copia en su cartera o el bolso de su pareja.</p><h3>Los pases peatonales:</h3><p>En Estados Unidos, el automovilista respecta muy bien las señales de tránsito. Así que casi nunca verán un automovilista pasarse un semáforo rojo. En paralela, raramente verán un peatón cruzar la calle sin respetar la señalización. Eviten absolutamente cruzar la calle sin verificar si tienen derecho a pasar o no. De lo contrario les aseguro que los automovilistas y taxis les harán saber. La verdad que en NYC la señalización sirve y es muy respetada.</p><p>Otra cosa, para los peatones, el semáforo verde les permite cruzar la calle, pero en algunos semáforos, una indicación “10, 9, 8, 7, 6, …1” aparece. Significa que les queda sólo este tiempo para cruzar antes del semáforo rojo. Lo digo porque una amiga que visitaba Seattle vio esta indicación pero se esperó el “0” para cruzar… ¡Se dio cuenta de su error al encontrarse en medio de la calle con los coches arrancandos!</p><p>Así que tengan cuidado y apunten estos tips antes de ir a Nueva York.</p><h3>Las farmacias:</h3><p>En Estados Unidos, si se enferman de cátaro, resfriamiento, dolor de garganta, gastroenteritis… no vayan al médico, les puede resultar muy costoso. Ahora pueden ir a una farmacia donde el farmacéutico les podrá ayudar. En Estados Unidos y NYC, encontrarán farmacias en muchas tiendas. Los medicamentos están en venta libre y pueden  tomar lo que quieren. Es bastante sencillo y los precios no son muy elevados. Es un buen plan por si no han aguantado el aire acondicionado del avión.</p><h3>Las propinas:</h3><p>Las propinas o tips son, en muchos bares y restaurantes, parte del ingreso de los camareros. Así que se acostumbra dar un 10% de de propina si el servicio no fue tan bueno, 15% si fue bueno y 20% si lo consideramos perfecto.</p><p>Por lo mismo verán que la mayoría de los camareros harán lo posible para que pasen un buen momento y que el servicio les guste: sonríen, son lindos y educados.</p><h3>Los vendedores y camareros:</h3><p>Al entrar en las tiendas, casi todos los vendedores les dirá algo como “Hey guys!” o “How You doin’?”. Además verán que en muchos lugares el vendedor o camarero les dará su nombre.</p><p>Así que Señores, no se sonrojen si la vendedora les pregunta “How you doin’?” y les sonríe, y si luego les pregunta si les puede ayudar diciéndoles que se llama “Brenda”.</p><p>En fin, son pequeños tips que les comparto antes de que se vayan a Nueva York.</p></body></html>');

-- Location 22 (Les plannings -> Planning 1 jour) :
INSERT INTO PAGES VALUES ('13', '22', 'PLANNING', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>Visitar Nueva York en un sólo día: ¡Sí se puede!</h1><p>Sí se puede pero, seamos honestos, tampoco podrán ver todo como si se quedaran una semana en Nueva York, dos semanas o un mes.</p><p>Aún así se puede ver la mayoría de los símbolos de la ciudad de Nueva York gracias a un plan de visitas de un sólo día. Será pesado y un poco agotador, pero podrán ver muchas cosas interesantes.</p><p>¡Así que sí, se puede visitar Nueva York en un día!</p><p>¿ Listos para un día de locos en NY?</p><p>Mi propuesta de plan de visitas de Nueva York en un sólo día. Sepan que tendrán que pararse temprano y acostarse tarde si quieren ver todo lo que les propongo. No les doy un horario para empezar las visitas, pero más temprano mejor, al amanecer. Digamos sobre las 7 de la mañana (no se espanten ;)). Si su viaje a Nueva York supone un cambio de horario, verán que despertarán pronto…</p><p>También necesitan prever un buen par de zapatos o tenis, una botella de agua, cámara de fotos en la mano, listo para fijar los buenos recuerdos de su día.</p><p>También sepan que tendrán que trasladarse mucho en metro, así que de una vez compren su Pass por un día.</p><p>Les propongo una visita de Nueva York de Lower Manhatan a Harlem.</p><p>Por orden cronológico, podrán seguir el siguiente mapa: Google Map Visitar NYC en un día</p><h3>Parte 1</h3><p>Vayan al White Hall Terminal (South Ferry) para coger el Ferry de Staten Island gratuito. Podrán ver en su sitio web que navega las 24h del día, y pasa cada 15 minutos en la mañana.</p><p>Gracias a este ferry, pasarán cerca de la Estatua de la Libertad, Ellis Island, Governor Island y bajo el puente de Brooklyn. De ida podrán ver Manhattan y sus rascacielos desde otra perspectiva, la vista es impresionante.</p><p>Este recorrido en barco les permitirá sacar unas muy bonitas fotos.</p><h3>Parte 2</h3><p>Desde el muelle del ferry, caminen hasta el Financial District. Pasarán en frente de Wall Street y de su toro que representa la fuerza de Wall Street. También verán el gran edificio del New York Stock Exchange con una bandera americana gigantesca, y el tristemente famoso Ground Zero y “LA” tienda barata, Century 21.</p><p>Luego podrán visitar rápidamente la St Paul’s Chapel y descubrir el City Hall de Nueva York con su pequeño parque.</p><h3>Parte 3</h3><p>Luego pasen por la calle Broadway a partir del City hall, descubrirán lo largo de esta calle… es una de las más largas de Estados Unidos.</p><p>Caminen sobre Broadway hasta Canal Street para entrar en Chinatown, el barrio chino de Nueva York. Podrán pasear sobre Canal Street para descubrir decenas de comercios que venden todos casi lo mismo. De hecho tomen sus precauciones, no se queden demasiado tiempo ahí, para que no les timen al comprar mercancía de contrabando (iPod falso, perfumes falsos…).</p><p>Luego pasen por la Mulberry Street y lleguen a Little Italy. Verán que este barrio italiano de NY es muy pequeño y es difícil reconocerlo.</p><p>Sigan sobre Mulberry Street, a partir de Broome Street, entrarán en el barrio NoLIta que significa (North of Little Italy).</p><p>Pueden ir a Petrosino Square para comer algo, y probar de los mejores cheesecake de NYC, como en Eileen Cheescake (ver artículo aquí). ¡ Es uno de mis Mejores Planes!</p><p>Pasen por Lafayette Street para descubrir otros dos barrios: Soho y NoHo (South of Houston Street y North of Houston Street, respectivamente). Los edificios de estos barrios no son muy altos, hay muchos comercios, restaurants y bares. Es un barrio con mucha vida, muy agradable.</p><p>A partir de la 4ta calle, diríjanse al oeste, hacia el barrio de Greenwich Village y podrán descubrir el Washington Square Park donde se erige un Ardo dedicado a George Washington. La plaza está rodeada por escuelas y la universidad de New York, la gente que pasea por este barrio es un poco marginal, intelectual y no conformista.</p><p>Después de haber sacado algunas fotos y haber cruzado esta plaza, caminen hacia el principio de la 5ta avenida hasta la 14va calle. Llegarán luego a una de mis plazas favoritas de Nueva York: Union Square. Verán que en este barrio de Nueva York conviven mucha gente: blancos, negros, pobres, ricos… y hay mucha gente de onda ecologista. De hecho si pasen por ahí un día de mercado, verán que sólo se venden productos bio. Si sus pies empiezan a dolerles, mi consejo es descubrir aún mejor Nueva York y a los neoyorquinos sentándose en una de las bancas de la plaza y quedarse viendo a la gente pasar. Verán la diversidad, reflejo de la población de NYC.</p><p>Después de haber dejado sus pies descansar unos 15 minutos, regresen sobre la calle Broadway para alcanzar el Flatiron District donde verán al… ¡Flatiron Building por supuesto! Tiene una forma triangular (como si fuera una plancha), es único en el mundo.</p><p>Saquen una foto de recuerdo en frente del Flatiron Building, y sigan su visita descubriendo el Madison Square Park y su famoso restaurante, el Shake Shake (ver artículo aquí). Si se quedan varios días en Nueva York, les aconsejo ir a este lugar para que coman la mejor hamburguesa de Nueva York! Lo único es que siempre hay mucha gente y pueden estar esperando mucho tiempo en la cola antes de que les atienden…</p><p>Así que para una visita de un sólo día en Nueva York, mejor comen en otro lugar, alrededor del Madison Square Park encontrarán muchos lugares o puestos de bocadillos, hotdogs o fastfood.</p><h3>Parte 4</h3><p>Descansen al sentarse en el metro N o R en la estación del 23rd ST hacia el Norte de Manhattan, párense en la estación de metro 49th St.</p><h3>Parte 5</h3><p>Se encontrarán en el barrio Midtown. Caminen de la 49th Sreet hacia el Este para ir al Rockfeller Center. Después de haber pasado la 6ta avenida, a 150 a mano izquierda, podrán ver la plaza del Rockfeller Center. Es donde durante las fiestas de Navidad, ponen el famoso árbol de Navidad de Nueva York (el Christmas Tree), y la pista de hielo. Dependiendo de cuando visiten Nueva York, la plaza luce diferente. De todo modos, podrán apreciar la altura de los edificios alrededor, como el top of the Rock.</p><p>De hecho, sigan sobre la calle 50, entre la 5ta y la 6ta avenida para llegar a la entrada del Top of the Rock. Suban a la torre y descubran NYC desde otra perspectiva (ver artículo MPVNY aquí). Desde arriba podrán ver Central park, el Empire state Building… bueno ¡una vista inolvidable!</p><p>De regreso a la calle, caminen sobre la 5ta Avenida, comparados a los Campos Elíseos parisinos, con muchas tiendas de renombre internacional.</p><p>Es donde también podrán ver la Catedral St Patrick. Entren ahí y visítenla si tardarse mucho tampoco.</p><p>En la 5ta Avenida encontrarán tiendas muy famosas como el NBA store, Abercrombie &amp;Fitch y las tiendas Haute Couture como G.Armani, Louis Vuitton, Prada…</p><p>Llegando a la plaza de Pullitzer Fountain, verán a su derecha la tienda de juguetes FAO ­&amp;Schwartz (impresionante, ver artículo MPVNY aquí), y el Apple Store más famoso del mundo. A su izquierda, verán un hotel que aparece en muchas películas: el Plaza New York Hotel. Justo enfrente, está la entrada de Central Park.</p><p>Crucen la calle y entren en Central Park, el pulmón de la ciudad de Nueva York.</p><p>Pasen por el primer caminito que va hacia el Norte de Central Park, podrán ver el famoso puente llamado The Pond. Se ve este puente en muchas películas. Luego sigan en Central Park hasta el Belvedere Castle, es muy bonito y domina una parte del parque. Aprovechen para sacar bonitas fotos ahí.</p><p>Luego vayan hacia el oeste, teniendo cuidado a no perderse en el medio de tantos caminitos. En la salida que apuntada en el mapa, descubrirán el Museo de Historia Natural de Nueva York. Desafortunadamente no tendrán el tiempo suficiente para visitarlo.</p><h3>Parte 6</h3><p>Sin embargo suban al metro A, B o C en la estación de la calle 81, párense en la estación 116th Street.</p><h3>Parte 7</h3><p>Saliendo del metro, se encontrarán en Harlem. Que no les de miedo este barrio, porque como ya comento en este artículo, Harlem es ahora un barrio seguro. Pueden entonces caminar tranquilamente en las míticas calles de Harlem, ahora famosas gracias a Martin Luther King y Malcom X. Pasarán por algunas iglesias, el Marcus Garvey Park y especialmente el Apollo Theater, sala de espectáculos donde cantaron los cantantes afroamericanos más famosos.</p><p>Podrán hacer como yo, sacándose una foto como si estuvieran en una vieja película americana :D</p><h3>Parte 8</h3><p>Después de este paseo por Harlem, con el metro A o C de la estación de la 125th Street, vayan hacia el Sur y párense en la 50th Street Estación (cuidado y no se suban al metro B o D de color naranja).</p><h3>Parte 9</h3><p>Ya han llegado a la última etapa del día. Por la 50th calle, caminen hacia Broadway y descubrán lo más loco de Nueva York: ¡Times Square!</p><p>Ningún lugar se parece a Times Square. Caminen hacia el sur de Times Square, y vean los espectaculares y otras pantallas gigantes. No se les olviden poner atención en ver donde caminan también jeje.</p><p>En el cruce de la calle 36 y de Broadway, diríjanse hacia el oeste y la 7ma avenida, también llamada: Fashion Avenue.</p><p>Luego pasen por la calle 34, hacia el Este y pasen en frente de una de las tiendas más grandes del mundo: el famoso Macy’s. En este barrio renovado con calles peatonales, verán que hay muchas tiendas de ropa.</p><p>Si levantan la cabeza en dirección del Este de la calle 34, verán también el Empire State Building.</p><p>Pues caminen hacia el Empire State Building y suban hasta su rooftop para decubrir Nueva York.</p><p>No sé a qué hora habrán llegado, pero más tarde subirán, menos tiempo pasarán en la cola. Les aconsejo subirse ahí cuando llega la noche. Así hubieran visto Nueva York de día desde el top of the Rock, y Nueva York de noche desde el Empire tate Building. Lo mejor es subir ahí después de las 22h, sabiendo que el sitio cierra a las 2h de la mañana.</p><p>Si llegan más temprano, aprovechen para cenar un un restaurante del barrio.</p><h3>Conclusión</h3><p>Una vez en el techo de Manhattan, ¡se acaba su “day trip” en Nueva York!</p><p>Espero que el plan de visitas les guste. Si prefieren tener un tour organizado, con guías que hablen español, lres recomiendo reservar su tour con Siempre New York Tours.</p><p>No duden en compartir sus ideas, informaciones, mejores planes, porque es siempre posible mejorar la visita que les propongo.</p><p>Mientras, sé que después de un día tan agotador como este, sus pies y piernas sólo les pedirán una cosa: ¡DORMIR!</p><p>Así que buenas noches a todos, espero que sacaron bonitas fotos, dándoles muchas ganas de regresar a Nueva York para descubrir la ciudad de manera menos agotadora.</p></body></html>');

-- Location 23 (Les plannings -> Plannings 3 jours) :
INSERT INTO PAGES VALUES ('14', '23', 'PLANNING', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>¿Cómo visitar Nueva York en tres días?</h1><p>í se puede visitar Nueva York en tan sólo tres días, pero obviamente no podrán ver todo.</p><p>En vez de proponerles un plan de visitas, les sugiero cuatro planes, facilitándoles la preparación de su viaje a Nueva York según sus intereses.</p><p>Antes de presentarles estas cuatro propuestas, tengan en cuenta lo siguiente.</p><h3>Visiting New York in 3 days means:</h3><ul><li>Caminar mucho: necesitarán unos zapatos o tenis cómodos</li><li>Evitar dar vueltas inútiles: preparen bien su viaje antes de salir, apuntando lo que más les interesaría visitar o ver</li><li>Evitar las colas, esperas, largos transbordes en los transportes: reserven antes de salir lo más que puedan en internet antes de salir (taxi schuttle, NY City Pass, Vuelos en helicóptero…) lean el artículo MPVNY sobre las Reservaciones antes de irse a Nueva York</li></ul><p>Les propongo entonces cuatro temas diferentes para que planeen su viaje a Nueva York:</p><h3>El plan de visitas de Mejores Planes para visitar Nueva York en tres días estilo “CULTURAL”:</h3><p>Este plan es para los que no podrían ir a Nueva York sin visitar al menos 2 de los 4 grandes museos de la ciudad: el MET, el MOMA, el Museo de Historia Natural y el Guggenheim.</p><h3>El plan de visitas de Mejores Planes para visitar Nueva York en tres días estilo “STREET”:</h3><p>Este plan será útil a los que quieren pasar más tiempo en las calles de Nueva York que en los museos. Este plan les permitirá pasear por las calles más míticas de NYC, ver los rascacielos y otros puntos de interés…</p><h3>El plan de visitas de Mejores Planes para visitar Nueva York en tres días estilo “ENJOY”:</h3><p>Este plan es para los que quieran disfrutar lo más que se pueda su estancia tratando de ver todo NYC, que sea con un vuelo en helicóptero, un tour en autobús turístico para ir más rápido que caminando… y ahorrar tiempo para el shopping.</p><h3>El plan de visitas de Mejores Planes para visitar Nueva York en tres días estilo “MAS y MAS”:</h3><p>Este plan es para los que nunca se cansan y siempre quieren ver más y más. Sí se puede hacer lo de los tres planes en tres días.</p><h3>¿Por qué cuatro planes diferentes?</h3><p>Porque cada uno tiene sus propios gustos, intereses diferentes.</p><p>Así cada uno podrá definir qué tipo de plan le corresponda más, además, están para ayudarles a planificar su viaje y pueden modificarlos como quieran.</p></body></html>');

-- Location 24 (Les plannings -> Plannings 1 semaine) :
INSERT INTO PAGES VALUES ('15', '24', 'PLANNING', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>¿Cómo visitar Nueva York en 1 semana?</h1><p>Si van a Nueva York por una semana, les aconsejo organizar correctamente su viaje preparando un plan de visitas.</p><p>Les podrá ser útil para no olvidar hacer una actividad que les interesaría, y también les evitará estar corriendo de último minuto para ver todo lo que querían.</p><p>¡Así les propongo no sólo un plan de visitas sino 4!</p><h3>Un plan de visitas “culturales”, “street”, “enjoy” y “más y más”.</h3><p>Escogen el plan de visitas que más les convenga, que sea “enjoy”, street”, “cultural” o si nunca se cansan y son muy curiosos les aconsejo el “más y más”…</p><p>Antes que todo, sepan que estos planes de visitas son subjetivos y son propuestas que les comparto (así es el blog entero).</p><p>Entonces pueden modificar y adaptar estos planes a su gusto.</p><p>Descripción de los 4 planes de visitas:</p><h3>El plan de visitas de Mejores Planes para viajar a Nueva York en una semana estilo “CULTURAL”:</h3><p>Este plan de visitas es para los que no podrían ir a Nueva York sin visitar al menos 4 grandes museos. El MET, el MOMA, el Museo de Historia Natural y el Guggenheim.</p><h3>El plan de visitas de Mejores Planes para viajar a Nueva York en una semana estilo “STREET”:</h3><p>Este plan de visitas corresponde más a los que no están interesados en los museos, sino en visitar las calles de Nueva York, sus rascacielos, ver los neoyorquinos…</p><h3>El plan de visitas de Mejores Planes para viajar a Nueva York en una semana estilo “ENJOY”:</h3><p>Este plan de visitas es para los que quieren disfrutar de su estancia lo más que se pueda, tratando de ver todo Nueva York con un vuelo sobre la ciudad, una vuelta en autobús para no perder su tiempo caminando, cenando en un yate, o sacando fotos en Central Park con un profesional… ¡y al final tener más tiempo para ir de shopping!</p><h3>El plan de visitas de Mejores Planes para viajar a Nueva York en una semana estilo “MÁS y MÁS”:</h3><p>Este plan está previsto para los que nunca se cansan, que no quieren perderse de nada durante su viaje a Nueva York. Es para los que piensan que descansar es sinónimo de pérdida de tiempo… ¡Tendrán todas las noches para descansar, y durante el día podrán disfrutar al 100% de su estancia en NUEVA YORK! Además, podrán hacer tanto visitas “culturales” como “street” o de estilo “enjoy”.</p><h3>¿Por qué 4 planes de visitas diferentes?</h3><p>Porque cada uno tiene diferentes gustos e intereses.</p><p>Cada quien puede utilizar los planes de visitas propuestos como quiera, seguirlos cuidadosamente o al contrario modificarlos ;)</p></body></html>');

-- Location 25 (A faire et à voir avant de partir -> 1 mois avant de partir) :
INSERT INTO PAGES VALUES ('16', '25', 'CHECKLIST', '');

-- Location 26 (A faire et à voir avant de partir -> 1 semaine avant de partir) :
INSERT INTO PAGES VALUES ('17', '26', 'CHECKLIST', '');

-- Location 27 (A faire et à voir avant de partir -> 1h avant de partir, dans sa valise) :
INSERT INTO PAGES VALUES ('18', '27', 'CHECKLIST', '');

-- Location 28 (A faire et à voir avant de partir -> 1h avant de partir, dans son bagage à main) :
INSERT INTO PAGES VALUES ('19', '28', 'CHECKLIST', '');

-- Location 6 (L'affiche "I love Bons Plans New York") :
INSERT INTO PAGES VALUES ('21', '6', 'FULLSCREEN_IMAGE', '');

-- Location 7 (Conversion Euro - Dollar) :
INSERT INTO PAGES VALUES ('22', '7', 'CURRENCY_CONVERTER', '');

-- Location 8 (Les pourboires) :
INSERT INTO PAGES VALUES ('23', '8', 'TIPS_COMPUTER', '');

-- Location 9 (Numéros utiles à New York) :
INSERT INTO PAGES VALUES ('24', '9', 'HTML', '<!DOCTYPE html><html><head><meta http-equiv="content-type" content="text/html;charset=utf-8"/><style>html{margin:0px;padding: 0px;}body{margin:0px;padding:20px;text-align:justify;}h1{margin: 0px;font-size: 20px;}h3{margin: 0px;font-size: 16px;}ul{padding: 0px;padding-left: 20px;}li{list-style-type: disk;margin-top: 10px;}</style></head><body><h1>Números útiles</h1><h3>Para llamar a su país desde Nueva York:</h3><p>Teclean el "011+nº de su país*+ n° de su contacto)"</p><p>* :</p><ul><li>Andorra: +376</li><li>Argentina: + 54</li><li>Bolivia: +591</li><li>Chile: +56</li><li>Colombia: +57</li><li>Cuba: +53</li><li>Costa Rica: +506</li><li>Ecuador: +593</li><li>El Salvador: +503</li><li>España: +34</li><li>Guatemala: +502</li><li>Honduras: +504</li><li>México: +52</li><li>Nicaragua: +505</li><li>Panamá: +507</li><li>Paraguay: +595</li><li>Perú: +51</li><li>República Dominicana: ++1-809, +1-829, +1-849</li><li>Uruguay: +598</li><li>Venezuela: +58</li></ul><h3>Para que le llamen a Nueva York desde su país:</h3><p>Las personas que quieran llamarles a su teléfono móvil no tienen que cambiar el número de llamada. Funciona como si estuvieran en su propio país, sólo hay que añadir el nº de su país (ver arriba). El costo de las llamadas será más caro dependiendo de su compañía, por cualquier duda, pregunten a su proveedor de servicio antes de salir de viaje. </p><p>Si les hablan a una línea fija (hotel, teléfono público), tendrán que teclear: "00+1+código de la ciudad (sin el primer 1) + n° de teléfono.</p><h3>Servicio de información:</h3><p>411 (llamadas gratuitas desde los teléfonos públicos pero de pago en los hoteles)</p><h3>Servicio de las aduanas:</h3><p>(212) 466-5550 </p><h3>Transit Authority Travel Info:</h3><p>(718) 330-1234</p><h3>Visitor''s Bureau (Oficina de Turismo):</h3><p>(212) 397-8222</p><h3>Emergencias y hospitales:</h3><ul><li>Emergencias, Policía, Ambulancias, Bomberos : 911</li><li>Metropolitan Hospital Center (1901 First Avenue, NYC) : 1 (212) 230-6262</li><li>Bellevue Hospital Center (462 First Avenue, NYC) : 1(212) 562-4141</li></ul><h3>Centro de desintoxicación:</h3><p>(212) 764-7667</p><h3>Oficina de objetos perdidos (Lost and Founds):</h3><ul><li>en el metro o autobús: (212) 712-4500 o (718) 625-6200 (abierto de las 8h a las 12h de lunes a viernes menos el jueves de las 11h a las 18h30).</li><li>en un taxi: 311 ou (212) 639-9675 o (212) 302-TAXI</li></ul><h3>Tarjetas bancarias:</h3><ul><li>Visa: 001-303-967-1096</li><li>Mastercard: 011-33-1-4516-6565</li></ul><h3>Nota:</h3><p>Algunos números aparecen con palabras, por ejemplo "1-800-taxicab". Cada tecla del teléfono tiene un número y tres letras. Así para llamar al número del ejemplo, hay que teclear: 1-800-8294222.</p></body></html>');
-- ----------------------------------------------------------------------



-- ----------------------------------------------------------------------
-- INSERT PLANNINGS (id, page, position, title) :

-- Page 13 (Les plannings -> Plannings 3 jours) :
INSERT INTO PLANNINGS VALUES ('1', '14', '0', 'Plan de visitas Cultural');
INSERT INTO PLANNINGS VALUES ('2', '14', '1', 'Plan de visitas Enjoy');
INSERT INTO PLANNINGS VALUES ('3', '14', '2', 'Plan de visitas Street');
INSERT INTO PLANNINGS VALUES ('4', '14', '3', 'Plan de visitas Más y Más');

-- Page 14 (Les plannings -> Plannings 1 semaine) :
INSERT INTO PLANNINGS VALUES ('5', '15', '0', 'Plan de visitas Cultural');
INSERT INTO PLANNINGS VALUES ('6', '15', '1', 'Plan de visitas Enjoy');
INSERT INTO PLANNINGS VALUES ('7', '15', '2', 'Plan de visitas Street');
INSERT INTO PLANNINGS VALUES ('8', '15', '3', 'Plan de visitas Más y Más');
INSERT INTO PLANNINGS VALUES ('9', '15', '4', 'Plan de visitas Honey moon');
-- ----------------------------------------------------------------------



-- ----------------------------------------------------------------------
-- INSERT CHECKLIST_ITEMS (id, page, position, checked, title, description) :

-- Page 15 (A faire et à voir avant de partir -> 1 mois avant de partir) :
INSERT INTO CHECKLIST_ITEMS VALUES ('1', '16', '0', '0', 'Visa', "Tramiten su visa lo antes posible (depende de los países)");
INSERT INTO CHECKLIST_ITEMS VALUES ('2', '16', '1', '0', 'Alojamiento', "Reserven su hotel o apartamento");
INSERT INTO CHECKLIST_ITEMS VALUES ('3', '16', '2', '0', 'Avión', "Compren su ticket de avión");
INSERT INTO CHECKLIST_ITEMS VALUES ('4', '16', '3', '0', 'NBA, NFL, MLB, ...', "Reserven sus entradas para la NBA u otros deportes americanos");
INSERT INTO CHECKLIST_ITEMS VALUES ('5', '16', '4', '0', 'Broadway', "Reserven sus entradas para un show en Brodway o un concierto");
INSERT INTO CHECKLIST_ITEMS VALUES ('6', '16', '5', '0', 'Dólares', "Compren dólares");

-- Page 24 (A faire et à voir avant de partir -> 1 semaine avant de partir) :
INSERT INTO CHECKLIST_ITEMS VALUES ('7', '17', '0', '0', 'Adaptador', "Compren un adaptador de enchufes (si diferente al de EEUU)");
INSERT INTO CHECKLIST_ITEMS VALUES ('8', '17', '1', '0', 'New York City Pass', "Reserven el New York City Pass");
INSERT INTO CHECKLIST_ITEMS VALUES ('9', '17', '2', '0', 'Atracciones', "Reserven sus tours: helicóptero, cena en un barco, ...");
INSERT INTO CHECKLIST_ITEMS VALUES ('10', '17', '3', '0', 'Shuttle', "Reserven un Taxi Shuttle o una limusina:-)");
INSERT INTO CHECKLIST_ITEMS VALUES ('11', '17', '4', '0', 'ESTA', "Tramiten el ESTA en internet e imprimen el recibo (depende de los países)");

-- Page 26 (A faire et à voir avant de partir -> 1h avant, dans sa valise) :
INSERT INTO CHECKLIST_ITEMS VALUES ('12', '18', '0', '0', 'Copias', "Copias de pasaporte, tickets de avión, vouchers, reserva del hotel, ...");
INSERT INTO CHECKLIST_ITEMS VALUES ('13', '18', '1', '0', 'Adaptador', "Pongan el adaptador de enchufes");
INSERT INTO CHECKLIST_ITEMS VALUES ('14', '18', '2', '0', 'Gafas de sol', "Para cuando caminen en las calles un día soleado");
INSERT INTO CHECKLIST_ITEMS VALUES ('15', '18', '3', '0', 'Ropa', "Llenen la mitad de su equipaje con su ropa personal y otra mitad con la ropa de su acompañante por si se perderían una de sus maletas");
INSERT INTO CHECKLIST_ITEMS VALUES ('16', '18', '4', '0', 'Ropa adaptada', "Pongan ropa adaptada al clima y sobre todo un calzado confortable para poder caminar en la ciudad");

-- Page 27 (A faire et à voir avant de partir -> 1h avant, dans son bagage à main) :
INSERT INTO CHECKLIST_ITEMS VALUES ('17', '19', '0', '0', 'Pasaporte', "No olviden su pasaporte");
INSERT INTO CHECKLIST_ITEMS VALUES ('18', '19', '1', '0', 'Carnet de identidad', "No olviden su carnet de identidad o credencial, será muy útil para enseñar su ID en NY y dejar su pasaporte en el hotel");
INSERT INTO CHECKLIST_ITEMS VALUES ('19', '19', '2', '0', 'Copia de los tickets de avión', "Por seguridad, para comprobar su compra");
INSERT INTO CHECKLIST_ITEMS VALUES ('20', '19', '3', '0', 'Copia de reserva del hotel', "Por seguridad, para comprobar su compra");
INSERT INTO CHECKLIST_ITEMS VALUES ('21', '19', '4', '0', 'Vouchers', "Vouchers de las actividades reservadas (NBA, shuttle, New York City Pass, ...)");
INSERT INTO CHECKLIST_ITEMS VALUES ('22', '19', '5', '0', 'Cámara', "Más vale tener su camára en su equipaje de mano y evitar el transporte en su valija facturada");
INSERT INTO CHECKLIST_ITEMS VALUES ('23', '19', '6', '0', 'Adaptador / multicontacto', "Si pierden su equipaje facturado, podrán cargar sus aparatos electrónicos (laptop, IPad...)");
INSERT INTO CHECKLIST_ITEMS VALUES ('24', '19', '7', '0', 'Cargadores', "Cargadores del teléfono, de su cámara de fotos ...");
INSERT INTO CHECKLIST_ITEMS VALUES ('25', '19', '8', '0', 'Dólares', "Más vale tener sus dólares a la mano");
INSERT INTO CHECKLIST_ITEMS VALUES ('26', '19', '9', '0', 'Ropa caliente', "Ropa caliente para no pasar frío en el avión");
-- ----------------------------------------------------------------------







-- ----------------------------------------------------------------------
-- INSERT TRANSLATIONS (id, location, page, frenchSentence, englishSentence) :

-- Location 29 (Guide de conversation -> Les bases) :
INSERT INTO PAGES VALUES ('25', '29', 'TRANSLATION', '');

-- Location 30 (Guide de conversation -> Les chiffres) :
INSERT INTO PAGES VALUES ('26', '30', 'TRANSLATION', '');

-- Location 31 (Guide de conversation -> Le temps) :
INSERT INTO PAGES VALUES ('27', '31', 'TRANSLATION', '');

-- Location 32 (Guide de conversation -> Discussion avec un américain) :
INSERT INTO PAGES VALUES ('28', '32', 'TRANSLATION', '');

-- Location 33 (Guide de conversation -> Dans la rue) :
INSERT INTO PAGES VALUES ('29', '33', 'TRANSLATION', '');

-- Location 34 (Guide de conversation -> La météo) :
INSERT INTO PAGES VALUES ('30', '34', 'TRANSLATION', '');

-- Location 35 (Guide de conversation -> Nom des lieux communs) :
INSERT INTO PAGES VALUES ('31', '35', 'TRANSLATION', '');

-- Location 36 (Guide de conversation -> En cas d'urgence) :
INSERT INTO PAGES VALUES ('32', '36', 'TRANSLATION', '');

-- Location 37 (Guide de conversation -> Dans un hôtel) :
INSERT INTO PAGES VALUES ('33', '37', 'TRANSLATION', '');

-- Location 38 (Guide de conversation -> Dans une pharmacie ou chez le médecin) :
INSERT INTO PAGES VALUES ('34', '38', 'TRANSLATION', '');

-- Location 39 (Guide de conversation -> Dans les transports en commun) :
INSERT INTO PAGES VALUES ('35', '39', 'TRANSLATION', '');

-- Location 40 (Guide de conversation -> Dans un taxi) :
INSERT INTO PAGES VALUES ('36', '40', 'TRANSLATION', '');

-- Location 41 (Guide de conversation -> Dans l'avion) :
INSERT INTO PAGES VALUES ('37', '41', 'TRANSLATION', '');

-- Location 42 (Guide de conversation -> Dans un magasin, bar ou restaurant) :
INSERT INTO PAGES VALUES ('38', '42', 'TRANSLATION', '');

-- Location 43 (Guide de conversation -> Dans un magasin) :
INSERT INTO PAGES VALUES ('39', '43', 'TRANSLATION', '');

-- Location 44 (Guide de conversation -> Dans un bar ou un restaurant) :
INSERT INTO PAGES VALUES ('40', '44', 'TRANSLATION', '');

-- Location 45 (Guide de conversation -> Dans une excursion) :
INSERT INTO PAGES VALUES ('41', '45', 'TRANSLATION', '');

-- Location 46 (Guide de conversation -> Dans un spectacle) :
INSERT INTO PAGES VALUES ('42', '46', 'TRANSLATION', '');





-- Page 25 (Guía de conversación -> Lo básico) :

INSERT INTO TRANSLATIONS VALUES ('1', '25', '0', "Buenos días", "Good morning");
INSERT INTO TRANSLATIONS VALUES ('2', '25', '1', "Buenas tardes", "Good afternoon");
INSERT INTO TRANSLATIONS VALUES ('3', '25', '2', "Buenas noches", "Good evening");
INSERT INTO TRANSLATIONS VALUES ('4', '25', '3', "Buenas noches", "Good night");
INSERT INTO TRANSLATIONS VALUES ('5', '25', '4', "Por favor", "Please");
INSERT INTO TRANSLATIONS VALUES ('6', '25', '5', "(Muchas) gracias", "Thank you (very much)");
INSERT INTO TRANSLATIONS VALUES ('7', '25', '6', "De nada", "you're welcome");
INSERT INTO TRANSLATIONS VALUES ('8', '25', '7', "Perdona", "Sorry");
INSERT INTO TRANSLATIONS VALUES ('9', '25', '8', "De verdad lo siento mucho", "I'm so sorry");
INSERT INTO TRANSLATIONS VALUES ('10', '25', '9', "Disculpe por la tardanza", "I'm sorry I'm late");
INSERT INTO TRANSLATIONS VALUES ('11', '25', '10', "Un momento por favor", "One moment please");
INSERT INTO TRANSLATIONS VALUES ('12', '25', '11', "¿ Dónde ?", "Where ?");
INSERT INTO TRANSLATIONS VALUES ('13', '25', '12', "¿ Cuándo ?", "When ?");
INSERT INTO TRANSLATIONS VALUES ('14', '25', '13', "¿ Qué ?", "What ?");
INSERT INTO TRANSLATIONS VALUES ('15', '25', '14', "¿ Cómo ?", "How ?");
INSERT INTO TRANSLATIONS VALUES ('16', '25', '15', "¿ Cuánto ?", "How much ?");
INSERT INTO TRANSLATIONS VALUES ('17', '25', '16', "¿ Cuál ?", "Which ?");
INSERT INTO TRANSLATIONS VALUES ('18', '25', '17', "¿ Quién ?", "Who ?");
INSERT INTO TRANSLATIONS VALUES ('19', '25', '18', "¿ Por qué ?", "Why ?");
INSERT INTO TRANSLATIONS VALUES ('20', '25', '19', "¿ Qué es esto ?", "What is it?");
INSERT INTO TRANSLATIONS VALUES ('21', '25', '20', "¿ Cómo está Ud ?", "How are you?");
INSERT INTO TRANSLATIONS VALUES ('22', '25', '21', "Muy bien, gracias", "Fine, thank you");
INSERT INTO TRANSLATIONS VALUES ('23', '25', '22', "¿ Cómo se llama Ud ?", "What's your name ?");
INSERT INTO TRANSLATIONS VALUES ('25', '25', '23', "Me llamo...", "My name is … ");
INSERT INTO TRANSLATIONS VALUES ('26', '25', '25', "¡ Soy español !", "I'm Spanish");
INSERT INTO TRANSLATIONS VALUES ('27', '25', '25', "¡ Buen viaje !", "Have a good trip !");
INSERT INTO TRANSLATIONS VALUES ('28', '25', '26', "¡ Qué se divierten !", "Have fun !");
INSERT INTO TRANSLATIONS VALUES ('29', '25', '27', "¡ Suerte !", "Good Luck !");
INSERT INTO TRANSLATIONS VALUES ('30', '25', '28', "Bienvenido", "Welcome");
INSERT INTO TRANSLATIONS VALUES ('31', '25', '25', "Me gustaría …", "I'd like …");
INSERT INTO TRANSLATIONS VALUES ('32', '25', '30', "Estoy buscando …", "I'm looking for …");
INSERT INTO TRANSLATIONS VALUES ('33', '25', '31', "¿ Cuánto cuesta esto ?", "How much is it ?");

-- Page 26 (Guía de conversación-> Los números) :

INSERT INTO TRANSLATIONS VALUES ('34', '26', '0', "1", "one");
INSERT INTO TRANSLATIONS VALUES ('35', '26', '1', "2", "two");
INSERT INTO TRANSLATIONS VALUES ('36', '26', '2', "3", "three");
INSERT INTO TRANSLATIONS VALUES ('37', '26', '3', "4", "four");
INSERT INTO TRANSLATIONS VALUES ('38', '26', '4', "5", "five");
INSERT INTO TRANSLATIONS VALUES ('39', '26', '5', "6", "six");
INSERT INTO TRANSLATIONS VALUES ('40', '26', '6', "7", "seven");
INSERT INTO TRANSLATIONS VALUES ('41', '26', '7', "8", "eight");
INSERT INTO TRANSLATIONS VALUES ('42', '26', '8', "9", "nine");
INSERT INTO TRANSLATIONS VALUES ('43', '26', '9', "10", "ten");
INSERT INTO TRANSLATIONS VALUES ('44', '26', '10', "11", "eleven");
INSERT INTO TRANSLATIONS VALUES ('45', '26', '11', "12", "twelve");
INSERT INTO TRANSLATIONS VALUES ('46', '26', '12', "13", "thirteen");
INSERT INTO TRANSLATIONS VALUES ('47', '26', '13', "14", "fourteen");
INSERT INTO TRANSLATIONS VALUES ('48', '26', '14', "15", "fifteen");
INSERT INTO TRANSLATIONS VALUES ('49', '26', '15', "20", "twenty");
INSERT INTO TRANSLATIONS VALUES ('50', '26', '16', "21", "twenty one");
INSERT INTO TRANSLATIONS VALUES ('51', '26', '17', "30", "thirty");
INSERT INTO TRANSLATIONS VALUES ('52', '26', '18', "40", "forty");
INSERT INTO TRANSLATIONS VALUES ('53', '26', '19', "50", "fifty");
INSERT INTO TRANSLATIONS VALUES ('54', '26', '20', "60", "sixty");
INSERT INTO TRANSLATIONS VALUES ('55', '26', '21', "70", "seventy");
INSERT INTO TRANSLATIONS VALUES ('56', '26', '22', "80", "eighty");
INSERT INTO TRANSLATIONS VALUES ('57', '26', '23', "90", "ninety");
INSERT INTO TRANSLATIONS VALUES ('58', '26', '24', "100", "one hundred");
INSERT INTO TRANSLATIONS VALUES ('59', '26', '25', "1000", "one thousand");
INSERT INTO TRANSLATIONS VALUES ('60', '26', '26', "2589", "two thousand five hundred and eighty nine");

-- Page 27 (Guía de conversación-> El tiempo) :

INSERT INTO TRANSLATIONS VALUES ('61', '27', '0', "¿ Qué hora es ?", "What time is it ?");
INSERT INTO TRANSLATIONS VALUES ('62', '27', '1', "Antes de ayer", "The day before yesterday");
INSERT INTO TRANSLATIONS VALUES ('63', '27', '2', "Ayer", "Yesterday");
INSERT INTO TRANSLATIONS VALUES ('64', '27', '3', "Hoy", "Today");
INSERT INTO TRANSLATIONS VALUES ('65', '27', '4', "Mañana", "Tomorrow");
INSERT INTO TRANSLATIONS VALUES ('66', '27', '5', "Pasado mañana", "The day after tomorrow");
INSERT INTO TRANSLATIONS VALUES ('67', '27', '6', "En la mañana", "In the morning");
INSERT INTO TRANSLATIONS VALUES ('68', '27', '7', "En la tarde", "In the afternoon");
INSERT INTO TRANSLATIONS VALUES ('69', '27', '8', "En la noche", "In the evening");
INSERT INTO TRANSLATIONS VALUES ('70', '27', '9', "En la noche", "At night");
INSERT INTO TRANSLATIONS VALUES ('71', '27', '10', "El desayuno", "Breakfast");
INSERT INTO TRANSLATIONS VALUES ('72', '27', '11', "Comida", "Lunch");
INSERT INTO TRANSLATIONS VALUES ('73', '27', '12', "Cena", "Dinner");
INSERT INTO TRANSLATIONS VALUES ('74', '27', '13', "Enero", "January");
INSERT INTO TRANSLATIONS VALUES ('75', '27', '14', "Febrero", "February");
INSERT INTO TRANSLATIONS VALUES ('76', '27', '15', "Marzo", "March");
INSERT INTO TRANSLATIONS VALUES ('77', '27', '16', "Abril", "April");
INSERT INTO TRANSLATIONS VALUES ('78', '27', '17', "Mayo", "May");
INSERT INTO TRANSLATIONS VALUES ('79', '27', '18', "Junio", "June");
INSERT INTO TRANSLATIONS VALUES ('80', '27', '19', "Julio", "July");
INSERT INTO TRANSLATIONS VALUES ('81', '27', '20', "Agosto", "August");
INSERT INTO TRANSLATIONS VALUES ('82', '27', '21', "Septiembre", "September");
INSERT INTO TRANSLATIONS VALUES ('83', '27', '22', "Octubre", "October");
INSERT INTO TRANSLATIONS VALUES ('84', '27', '23', "Noviembre", "November");
INSERT INTO TRANSLATIONS VALUES ('85', '27', '24', "Diciembre", "December");
INSERT INTO TRANSLATIONS VALUES ('86', '27', '25', "Lunes", "Monday");
INSERT INTO TRANSLATIONS VALUES ('87', '27', '26', "Martes", "Tuesday");
INSERT INTO TRANSLATIONS VALUES ('88', '27', '27', "Miércoles", "Wednesday");
INSERT INTO TRANSLATIONS VALUES ('89', '27', '28', "Jueves", "Thursday");
INSERT INTO TRANSLATIONS VALUES ('90', '27', '29', "Viernes", "Friday");
INSERT INTO TRANSLATIONS VALUES ('91', '27', '30', "Sábado", "Saturday");
INSERT INTO TRANSLATIONS VALUES ('92', '27', '31', "Domingo", "Sunday");

-- Page 28 (Guía de conversacón -> Hablar con un americano) :

INSERT INTO TRANSLATIONS VALUES ('93', '28', '0', "No sé", "I don't know");
INSERT INTO TRANSLATIONS VALUES ('94', '28', '1', "No entiendo", "I don't understand");
INSERT INTO TRANSLATIONS VALUES ('95', '28', '2', "Lo siento, no le entiendo", "Sorry, but I didn't understand");
INSERT INTO TRANSLATIONS VALUES ('96', '28', '3', "¿ Podría hablar más despacio ?", "Can you speak more slowly ?");
INSERT INTO TRANSLATIONS VALUES ('97', '28', '4', "¿ Lo podría deletrear por favor ?", "Could you spell that out, please?");
INSERT INTO TRANSLATIONS VALUES ('98', '28', '5', "¿ Lo podría escribir por favor ?", "Could you write that for me, please?");
INSERT INTO TRANSLATIONS VALUES ('99', '28', '6', "¿ Qué dice ?", "What did you say ?");
INSERT INTO TRANSLATIONS VALUES ('100', '28', '7', "No oígo lo que está diciendo", "I didn't hear what you said");
INSERT INTO TRANSLATIONS VALUES ('101', '28', '8', "¿ Podría hablar más alto ?", "Can you speak up?");
INSERT INTO TRANSLATIONS VALUES ('102', '28', '9', "¿ Qué significa esto ?", "What does this mean ?");
INSERT INTO TRANSLATIONS VALUES ('103', '28', '10', "¿ Le puedo preguntar algo ?", "Can I ask you a question?");
INSERT INTO TRANSLATIONS VALUES ('104', '28', '11', "¿ Ud habla español ?", "Do you speak Spanish ?");
INSERT INTO TRANSLATIONS VALUES ('105', '28', '12', "No hablo inglés", "I don't speak English");
INSERT INTO TRANSLATIONS VALUES ('106', '28', '13', "¿ Nos podría sacar una foto ?", "Can you take a picture of us ?");
INSERT INTO TRANSLATIONS VALUES ('107', '28', '14', "¿ Se puede fumar aquí ?", "Can I smoke in here ?");
INSERT INTO TRANSLATIONS VALUES ('108', '28', '15', "¿ Tiene un encendedor ?", "Do you have a light ?");
INSERT INTO TRANSLATIONS VALUES ('109', '28', '16', "¿ De dónde viene ?", "Where are you from ? ");
INSERT INTO TRANSLATIONS VALUES ('110', '28', '17', "Mucho gusto", "Nice to meet you");
INSERT INTO TRANSLATIONS VALUES ('111', '28', '18', "¿ Hay algo en especial que se puede hacer hoy en esta ciudad ?", "Is there anything special happening in the city today ?");
INSERT INTO TRANSLATIONS VALUES ('112', '28', '19', "¿ Cuál es el mejor lugar para tomar algo ?", "Where is the best place to drink ?");
INSERT INTO TRANSLATIONS VALUES ('113', '28', '20', "¿ Cuál es el mejor lugar para comer ?", "Where is the best place to eat ?");
INSERT INTO TRANSLATIONS VALUES ('114', '28', '21', "¿ Dónde te gusta ir de tiendas ?", "Where do you like to shop ?");

-- Page 29 (Guía de conversación -> En la calle) :

INSERT INTO TRANSLATIONS VALUES ('115', '29', '0', "¿ Me podría indicar el camino...?", "Can you show me the way to … ?");
INSERT INTO TRANSLATIONS VALUES ('116', '29', '1', "¿ Dónde está el metro más cercano ?", "Where is the nearest subway station ?");
INSERT INTO TRANSLATIONS VALUES ('117', '29', '2', "¿ Dónde puedo encontrar un taxi ?", "Where can I find a cab (taxi) ?");
INSERT INTO TRANSLATIONS VALUES ('118', '29', '3', "A la izquierda", "Left");
INSERT INTO TRANSLATIONS VALUES ('119', '29', '4', "A la derecha", "Right");
INSERT INTO TRANSLATIONS VALUES ('120', '29', '5', "Derecho", "Straight ahead");
INSERT INTO TRANSLATIONS VALUES ('121', '29', '6', "¿ Dónde puedo encontrar Wi-Fi gratuito ?", "Where can I found free wifi ?");
INSERT INTO TRANSLATIONS VALUES ('122', '29', '7', "¿ Dónde está el cajero más cercano ?", "Where is the nearest ATM ?");
INSERT INTO TRANSLATIONS VALUES ('123', '29', '8', "¿ Dónde puedo encontrar un banco ?", "Where can I found a bank ?");
INSERT INTO TRANSLATIONS VALUES ('124', '29', '9', "¿ Dónde puedo encontrar un buzón ?", "Where can I find a mailbox ?");
INSERT INTO TRANSLATIONS VALUES ('125', '29', '10', "¿ Dónde puedo comprar sellos postales ?", "Where can I buy stamps ?");

-- Page 30 (Guía de conversación -> El clima) :

INSERT INTO TRANSLATIONS VALUES ('126', '30', '0', "¿ Cómo será el clima el día de hoy ?", "What will the weather be like today ?");
INSERT INTO TRANSLATIONS VALUES ('127', '30', '1', "Hoy es un buen día ", "It's a nice day today");
INSERT INTO TRANSLATIONS VALUES ('128', '30', '2', "¿ Habrá sol en los próximos días ?", "Will it be sunny the next few days ?");
INSERT INTO TRANSLATIONS VALUES ('129', '30', '3', "Está haciendo frío", "It's cold");
INSERT INTO TRANSLATIONS VALUES ('130', '30', '4', "Está haciendo calor", "It's warm");
INSERT INTO TRANSLATIONS VALUES ('131', '30', '5', "Está lloviendo", "It's raining");
INSERT INTO TRANSLATIONS VALUES ('132', '30', '6', "¿ Será hoy un día nublado ?", "Is it cloudy today ?");

-- Page 31 (Guía de conversación -> Nombres de lugares comunes) :

INSERT INTO TRANSLATIONS VALUES ('133', '31', '0', "Correos", "Post Office");
INSERT INTO TRANSLATIONS VALUES ('134', '31', '1', "Oficina de Turismo", "Visitor Center");
INSERT INTO TRANSLATIONS VALUES ('135', '31', '2', "Banco", "Bank");
INSERT INTO TRANSLATIONS VALUES ('136', '31', '3', "Médico", "Doctor");
INSERT INTO TRANSLATIONS VALUES ('137', '31', '4', "Farmacia", "Pharmacy/Drugstore");
INSERT INTO TRANSLATIONS VALUES ('138', '31', '5', "Hospital", "Hospital");
INSERT INTO TRANSLATIONS VALUES ('139', '31', '6', "Aeropuerto", "Airport");
INSERT INTO TRANSLATIONS VALUES ('140', '31', '7', "Estación de tren", "Train Station");
INSERT INTO TRANSLATIONS VALUES ('141', '31', '8', "Servicios", "Restroom");
INSERT INTO TRANSLATIONS VALUES ('142', '31', '9', "Tienda", "Store");
INSERT INTO TRANSLATIONS VALUES ('143', '31', '10', "Parque", "Park");
INSERT INTO TRANSLATIONS VALUES ('144', '31', '11', "Hotel", "Hotel");
INSERT INTO TRANSLATIONS VALUES ('145', '31', '12', "Estadio", "Stadium");

-- Page 32 (Guía de conversación -> En caso de emergencias) :

INSERT INTO TRANSLATIONS VALUES ('146', '32', '0', "¡ Necesito ayuda !", "I need help immediately !");
INSERT INTO TRANSLATIONS VALUES ('147', '32', '1', "¡ Es una emergencia !", "It's an emergency !");
INSERT INTO TRANSLATIONS VALUES ('148', '32', '2', "¡ Cuidado !", "Watch out !");
INSERT INTO TRANSLATIONS VALUES ('149', '32', '3', "¡ Ayuda me están robando !", "Help ! Stop thief !");
INSERT INTO TRANSLATIONS VALUES ('150', '32', '4', "¡ Llamen a la policía !", "Call the police !");
INSERT INTO TRANSLATIONS VALUES ('151', '32', '5', "¡ Llamen al médico !", "Call a doctor !");
INSERT INTO TRANSLATIONS VALUES ('152', '32', '6', "¡ Llamen una ambulancia !", "Call an ambulance !");
INSERT INTO TRANSLATIONS VALUES ('153', '32', '7', "Perdí mis llaves", "I've lost my keys");
INSERT INTO TRANSLATIONS VALUES ('154', '32', '8', "Estoy perdido(a)", "I'm lost");
INSERT INTO TRANSLATIONS VALUES ('155', '32', '9', "¿ Alguien podría traducirme esto ?", "Can I have an interpreter ?");
INSERT INTO TRANSLATIONS VALUES ('156', '32', '10', "¿ Dónde está la policía ?", "Where is the Police Station ?");
INSERT INTO TRANSLATIONS VALUES ('157', '32', '11', "¿ Dónde está el hospital ?", "Where is the hospital ?");
INSERT INTO TRANSLATIONS VALUES ('158', '32', '12', "He perdido mi cartera", "I've lost my wallet");
INSERT INTO TRANSLATIONS VALUES ('159', '32', '13', "He perdido mi pasaporte", "I've lost my passport");
INSERT INTO TRANSLATIONS VALUES ('160', '32', '14', "He perdido mi tarjeta bancaria", "I've lost my credit card");

-- Page 33 (Guía de conversación -> En un hotel) :

INSERT INTO TRANSLATIONS VALUES ('161', '33', '0', "Tengo una reservación en su hotel", "I booked a room in your hotel");
INSERT INTO TRANSLATIONS VALUES ('162', '33', '1', "Me gustaría pagar", "I'd like to check out");
INSERT INTO TRANSLATIONS VALUES ('163', '33', '2', "Como hoy regreso a mi país, ¿ Dónde puedo dejar mis valijas ?", "As I come back home today, where can I leave my luggage ? ");
INSERT INTO TRANSLATIONS VALUES ('164', '33', '3', "¿ Me podría dar una toalla más ?", "Could I have another towel ?");
INSERT INTO TRANSLATIONS VALUES ('165', '33', '4', "¿ Puedo lavar mi ropa aquí ?", "Can I do my laundry here ?");
INSERT INTO TRANSLATIONS VALUES ('166', '33', '5', "Me gustaría tener otro cuarto", "I'd like to have another room");
INSERT INTO TRANSLATIONS VALUES ('167', '33', '6', "No han limpiado mi cuarto", "The room hasn't been cleaned");
INSERT INTO TRANSLATIONS VALUES ('168', '33', '7', "El aire acondicionado no funciona", "The air conditioning doesn't work");
INSERT INTO TRANSLATIONS VALUES ('169', '33', '8', "la calefacción no funciona", "The heating doesn't work");
INSERT INTO TRANSLATIONS VALUES ('170', '33', '9', "Ya no hay papel de baño", "There is no more toilet paper");
INSERT INTO TRANSLATIONS VALUES ('171', '33', '10', "Las sábanas están sucias", "The sheets are dirty");
INSERT INTO TRANSLATIONS VALUES ('172', '33', '11', "¿ Está incluido el desayuno ?", "Is breakfast included ?");
INSERT INTO TRANSLATIONS VALUES ('173', '33', '12', "Me da la llave del cuarto 12, por favor", "The key for room 12, please");
INSERT INTO TRANSLATIONS VALUES ('174', '33', '13', "¿ Tiene Wi-Fi ?", "Do you have WIFI ?");
INSERT INTO TRANSLATIONS VALUES ('175', '33', '14', "¿ Cuál es la contraseña del Wi-Fi ?", "What's the WIFI password ?");
INSERT INTO TRANSLATIONS VALUES ('176', '33', '15', "No logro conectarme", "I can't get through");
INSERT INTO TRANSLATIONS VALUES ('177', '33', '16', "No puedo identificarme", "I can't log on");
INSERT INTO TRANSLATIONS VALUES ('178', '33', '17', "¿ Podría imprimir algunas páginas aquí ?", "Can I print few pages here ?");

-- Page 34 (Guía de conversación -> En una farmacia o con el médico) :

INSERT INTO TRANSLATIONS VALUES ('179', '34', '0', "Me siento mal", "I'm ill");
INSERT INTO TRANSLATIONS VALUES ('180', '34', '1', "Me duele la cabeza", "I have a headache");
INSERT INTO TRANSLATIONS VALUES ('181', '34', '2', "Tengo mareos", "I feel dizzy");
INSERT INTO TRANSLATIONS VALUES ('182', '34', '3', "Me siento mareado", "My head is spinning");
INSERT INTO TRANSLATIONS VALUES ('183', '34', '4', "No puedo respirar bien", "I find it difficult to breathe");
INSERT INTO TRANSLATIONS VALUES ('184', '34', '5', "Tengo náuseas", "I feel nauseous");
INSERT INTO TRANSLATIONS VALUES ('185', '34', '6', "He vomitado", "I vomit");
INSERT INTO TRANSLATIONS VALUES ('186', '34', '7', "Tengo palpitaciones", "I have palpitations");
INSERT INTO TRANSLATIONS VALUES ('187', '34', '8', "Tengo fiebre", "I have a fever");
INSERT INTO TRANSLATIONS VALUES ('188', '34', '9', "Me duele la garganta", "I have a sore throat");
INSERT INTO TRANSLATIONS VALUES ('189', '34', '10', "Tengo un dolor de muellas", "I have a toothache");
INSERT INTO TRANSLATIONS VALUES ('190', '34', '11', "Me duelen los oídos", "My ear hurts");
INSERT INTO TRANSLATIONS VALUES ('191', '34', '12', "Me duele la espalda", "My back hurts");
INSERT INTO TRANSLATIONS VALUES ('192', '34', '13', "Me duele la pierna", "My leg hurts");
INSERT INTO TRANSLATIONS VALUES ('193', '34', '14', "Me duele el brazo", "My arm hurts");
INSERT INTO TRANSLATIONS VALUES ('194', '34', '15', "No puedo ni comer", "I can't eat anything");
INSERT INTO TRANSLATIONS VALUES ('195', '34', '16', "Estoy tosiendo mucho", "I'm coughing all the time");
INSERT INTO TRANSLATIONS VALUES ('196', '34', '17', "Tengo tos seca", "It's a dry cough");
INSERT INTO TRANSLATIONS VALUES ('197', '34', '18', "Tengo tos con flema", "It's not a dry cough");
INSERT INTO TRANSLATIONS VALUES ('198', '34', '19', "Me duele mucho", "I feel a great pain");
INSERT INTO TRANSLATIONS VALUES ('199', '34', '20', "Me siento muy débil", "I feel very weak");
INSERT INTO TRANSLATIONS VALUES ('200', '34', '21', "Me duele el estómago", "I have a stomach ache");
INSERT INTO TRANSLATIONS VALUES ('201', '34', '22', "Creo que tengo gripe", "I think I have flu");
INSERT INTO TRANSLATIONS VALUES ('202', '34', '23', "Creo que tengo un catarro", "I think I have a cold");
INSERT INTO TRANSLATIONS VALUES ('203', '34', '24', "Creo que tengo gastroenteritis", "I thnink I have gastroenteritis");
INSERT INTO TRANSLATIONS VALUES ('204', '34', '25', "¿ Tiene un remedio contra el dolor de cabeza ?", "Do you have something for a headache ?");
INSERT INTO TRANSLATIONS VALUES ('205', '34', '26', "¿ Tiene un remedio contra fiebre del heno ?", "Do you have something for hay fever ?");
INSERT INTO TRANSLATIONS VALUES ('206', '34', '27', "¿ Tiene un remedio contra la diarea ?", "Do you have something for diarrhea ?");
INSERT INTO TRANSLATIONS VALUES ('207', '34', '28', "Soy alérgico a la penicilina", "I'm allergic to penicillin");
INSERT INTO TRANSLATIONS VALUES ('208', '34', '29', "Soy diábetico", "I'm a diabetic");
INSERT INTO TRANSLATIONS VALUES ('209', '34', '30', "Soy cárdiaco", "I have a heart condition");
INSERT INTO TRANSLATIONS VALUES ('210', '34', '31', "Estoy embarazada", "I'm pregnant");
INSERT INTO TRANSLATIONS VALUES ('211', '34', '32', "Me caí", "I fell down");
INSERT INTO TRANSLATIONS VALUES ('212', '34', '33', "¿ Cuándo da consultas el médico ?", "When is the Doctor's office open ?");
INSERT INTO TRANSLATIONS VALUES ('213', '34', '34', "Me gustaría tener una cita", "I would to make an appointment");
INSERT INTO TRANSLATIONS VALUES ('214', '34', '35', "Tengo que ir al médico urgentemente", "I urgently need a doctor");
INSERT INTO TRANSLATIONS VALUES ('215', '34', '36', "Tengo quer ir al dentista urgentemente", "I urgently need a dentist");

-- Page 35 (Guía de conversación -> En el transporte público) :

INSERT INTO TRANSLATIONS VALUES ('216', '35', '0', "Viaje sencillo", "One-way");
INSERT INTO TRANSLATIONS VALUES ('217', '35', '1', "Ida y Vuelta", "Round-trip");
INSERT INTO TRANSLATIONS VALUES ('218', '35', '2', "¿ Dónde puedo comprar un ticket ?", "Where can I buy a ticket ?");
INSERT INTO TRANSLATIONS VALUES ('219', '35', '3', "Quisiera comprar un ticket para …", "I'd like to have a ticket to …");
INSERT INTO TRANSLATIONS VALUES ('220', '35', '4', "¿ Cuánto cuesta un ticket ?", "How much is a ticket ?");
INSERT INTO TRANSLATIONS VALUES ('221', '35', '5', "¿ Este autobus va hacia … ?", "Is this the bus to … ?");
INSERT INTO TRANSLATIONS VALUES ('222', '35', '6', "¿ A qué hora hay un autobus/tren para … ?", "At what time is there a bus/train to … ?");
INSERT INTO TRANSLATIONS VALUES ('223', '35', '7', "¿ Está ocupado este lugar ?", "Is this seat taken ?");
INSERT INTO TRANSLATIONS VALUES ('224', '35', '8', "¿ A cuál autobus tengo que subir ?", "Which number do I take ?");
INSERT INTO TRANSLATIONS VALUES ('225', '35', '9', "¿ Cuándo llega el próximo ?", "When is the next one coming ?");
INSERT INTO TRANSLATIONS VALUES ('226', '35', '10', "¿ Dónde está el metro ?", "Where is the subway station ?");
INSERT INTO TRANSLATIONS VALUES ('227', '35', '11', "¿ En qué ánden sale el metro X ?", "From which platform is the subway X leaving ?");
INSERT INTO TRANSLATIONS VALUES ('228', '35', '12', "¿ Me podría indicar cómo llegar a la estación de tren ?", "Could you tell me how to get to the station?");

-- Page 36 (Guía de conversación -> En un taxi) :

INSERT INTO TRANSLATIONS VALUES ('229', '36', '0', "¿ Me podría llevar a esta dirección ?", "Could you bring me to this address ?");
INSERT INTO TRANSLATIONS VALUES ('230', '36', '1', "¿ Cuánto le debo ?", "How much for the ride ?");
INSERT INTO TRANSLATIONS VALUES ('231', '36', '2', "¿ Podría ir más despacio por favor ?", "Could you drive slower please ?");
INSERT INTO TRANSLATIONS VALUES ('232', '36', '3', "¿ Para el aeropuerto por favor ", "To the airport please");

-- Page 37 (Guía de conversación -> En el avión) :

INSERT INTO TRANSLATIONS VALUES ('233', '37', '0', "Nos gustaría estar sentados al lado", "We would like to sit next to each other");
INSERT INTO TRANSLATIONS VALUES ('234', '37', '1', "Me gustaría tener un asiento por la ventana", "I'd like a seat by the window");
INSERT INTO TRANSLATIONS VALUES ('235', '37', '2', "Me gustaría tener un asiento por el pasillo", "I'd like an aisle seat");
INSERT INTO TRANSLATIONS VALUES ('236', '37', '3', "Mi valija ha desaparecido", "My suitcase has disappeared");
INSERT INTO TRANSLATIONS VALUES ('237', '37', '4', "Quisiera tomar una Coca", "I'd like to drink a Coke");

-- Page 38 (Guía de conversación -> En una tienda, bar o restaurante) :

INSERT INTO TRANSLATIONS VALUES ('238', '38', '0', "¿ A qué hora abren?", "What time does it open ?");
INSERT INTO TRANSLATIONS VALUES ('239', '38', '1', "¿ A qué hora cierran ?", "What time does it close ?");
INSERT INTO TRANSLATIONS VALUES ('240', '38', '2', "¿ Puedo pagar con una tarjeta de crédito ?", "Can I pay with a credit card ?");
INSERT INTO TRANSLATIONS VALUES ('241', '38', '3', "¿ Cuánto cuesta esto ?", "How much is this one ?");
INSERT INTO TRANSLATIONS VALUES ('242', '38', '4', "¿ Me podría escribir el precio ?", "Could you write down the price ?");
INSERT INTO TRANSLATIONS VALUES ('243', '38', '5', "¿ Podría envolverlo ?", "Could you wrap it for me ?");
INSERT INTO TRANSLATIONS VALUES ('244', '38', '6', "¿ Podria darme una bolsa ?", "Could I have a bag, please ?");
INSERT INTO TRANSLATIONS VALUES ('245', '38', '7', "¿ Puedo obtener un descuento ?", "Can I get a discount ?");
INSERT INTO TRANSLATIONS VALUES ('246', '38', '8', "¿ Puedo usar internet aquí ?", "Can I use the internet here ?");
INSERT INTO TRANSLATIONS VALUES ('247', '38', '9', "Está caro", "It's expensive");
INSERT INTO TRANSLATIONS VALUES ('248', '38', '10', "Está barato", "It's cheap");
INSERT INTO TRANSLATIONS VALUES ('249', '38', '11', "¿ Tengo que pagar antes ?", "Do I have to pay in advance ?");
INSERT INTO TRANSLATIONS VALUES ('250', '38', '12', "¿ Me podría dar un recibo ?", "Can I have a receipt ?");
INSERT INTO TRANSLATIONS VALUES ('251', '38', '13', "¿ Me podría dar una factura ?", "Can I have an invoice ?");
INSERT INTO TRANSLATIONS VALUES ('252', '38', '14', "¿ Tengo que pagar en efectivo ?", "Do I have to pay in cash ?");
INSERT INTO TRANSLATIONS VALUES ('253', '38', '15', "Voy a pagar en efectivo", "I'll pay cash");

-- Page 39 (Guía de conversación -> En una tienda) :

INSERT INTO TRANSLATIONS VALUES ('254', '39', '0', "¿ Puedo ver qué tienen ?", "Is it ok if I look around ?");
INSERT INTO TRANSLATIONS VALUES ('255', '39', '1', "¿ Puedo probarlo ?", "Can I try it on ?");
INSERT INTO TRANSLATIONS VALUES ('256', '39', '2', "¿ Dónde están los probadores ?", "Where is the fitting room ?");
INSERT INTO TRANSLATIONS VALUES ('257', '39', '3', "¿ Dónde está el espejo ?", "Where can I find a mirror ?");
INSERT INTO TRANSLATIONS VALUES ('258', '39', '4', "No quepo", "This one doesn't fit");
INSERT INTO TRANSLATIONS VALUES ('259', '39', '5', "¿ Tiene la talla siguiente ?", "Do you have a larger size ?");
INSERT INTO TRANSLATIONS VALUES ('260', '39', '6', "¿ Lo tiene en más pequeño ?", "Do you have a smaller size ?");

-- Page 40 (Guía de conversación -> En un bar o restaurante) :

INSERT INTO TRANSLATIONS VALUES ('261', '40', '0', "¿ Tiene una mesa libre ?", "Do you have a table for us ?");
INSERT INTO TRANSLATIONS VALUES ('262', '40', '1', "Una mesa para dos por favor", "A table for two please");
INSERT INTO TRANSLATIONS VALUES ('263', '40', '2', "¿ Podría reservarnos una mesa ?", "Can you reserve a table for us ?");
INSERT INTO TRANSLATIONS VALUES ('264', '40', '3', "El menú por favor", "The menu, please");
INSERT INTO TRANSLATIONS VALUES ('265', '40', '4', "La carta de los vinos por favor", "The wine list please");
INSERT INTO TRANSLATIONS VALUES ('266', '40', '5', "¿ Cuál es el menú de hoy ?", "What's the special of the day ?");
INSERT INTO TRANSLATIONS VALUES ('267', '40', '6', "¿ Qué me recomienda ?", "What would you recommend ?");
INSERT INTO TRANSLATIONS VALUES ('268', '40', '7', "Nos gustaría pedir", " We are ready to order");
INSERT INTO TRANSLATIONS VALUES ('269', '40', '8', "La cuenta por favor", "The check, please");
INSERT INTO TRANSLATIONS VALUES ('270', '40', '9', "La propina", "the tip / the gratuity");
INSERT INTO TRANSLATIONS VALUES ('271', '40', '10', "Soy vegetariano", "I'm vege");
INSERT INTO TRANSLATIONS VALUES ('272', '40', '11', "Soy vegano", " I'm vegan");
INSERT INTO TRANSLATIONS VALUES ('273', '40', '12', "¿ Tiene platillos vegetarianos ?", "Do you have any vegetarian dishes ?");
INSERT INTO TRANSLATIONS VALUES ('274', '40', '13', "No podría dar un poco de pan ?", "Could we have some bread ?");
INSERT INTO TRANSLATIONS VALUES ('275', '40', '14', "¿ Nos podría dar agua ?", "Could we have some water ?");
INSERT INTO TRANSLATIONS VALUES ('276', '40', '15', "¿ Tiene sal y pimienta ?", "Do you have salt and pepper ?");
INSERT INTO TRANSLATIONS VALUES ('277', '40', '16', "¿ Tiene aceite y vinagre ?", "Do you have oil and vinegar ?");
INSERT INTO TRANSLATIONS VALUES ('278', '40', '17', "¿ Me podría dar una servilleta ?", "Could I get a napkin ?");
INSERT INTO TRANSLATIONS VALUES ('279', '40', '18', "¿ Qué tipo de cervezas de barril tienen ?", "What beers do you have on tap ?");
INSERT INTO TRANSLATIONS VALUES ('280', '40', '19', "¿ Cuál es su mejor cóctel ?", "Which cocktail is the best ?");
INSERT INTO TRANSLATIONS VALUES ('281', '40', '20', "¿ Me podría dar un popote ?", "Can I have a straw ?");
INSERT INTO TRANSLATIONS VALUES ('282', '40', '21', "No quiero hielos", "I don't want ice");
INSERT INTO TRANSLATIONS VALUES ('283', '40', '22', "¿ Me podría poner una rodaja de limón?", "Could I have a slice of lemon please ?");
INSERT INTO TRANSLATIONS VALUES ('284', '40', '23', "Huevos", "Eggs");
INSERT INTO TRANSLATIONS VALUES ('285', '40', '24', "Patatas fritas", "French fries");
INSERT INTO TRANSLATIONS VALUES ('286', '40', '25', "Pan", "Bread");
INSERT INTO TRANSLATIONS VALUES ('287', '40', '26', "Queso", "Cheese");
INSERT INTO TRANSLATIONS VALUES ('288', '40', '27', "Verdura", "Vegetable");
INSERT INTO TRANSLATIONS VALUES ('289', '40', '28', "Agua", "Water");
INSERT INTO TRANSLATIONS VALUES ('290', '40', '29', "Agua sin gas", "Still water");
INSERT INTO TRANSLATIONS VALUES ('291', '40', '30', "Agua con gas", "Sparkling water");
INSERT INTO TRANSLATIONS VALUES ('292', '40', '31', "Agua del grifo", "Tap water");
INSERT INTO TRANSLATIONS VALUES ('293', '40', '32', "Agua mineral", "Mineral water");
INSERT INTO TRANSLATIONS VALUES ('294', '40', '33', "Agua embotellada", "Bottled water");
INSERT INTO TRANSLATIONS VALUES ('295', '40', '34', "Leche", "Milk");
INSERT INTO TRANSLATIONS VALUES ('296', '40', '35', "Vino", "Wine");
INSERT INTO TRANSLATIONS VALUES ('297', '40', '36', "Vino tinto (blanco)", "Red (white) wine");
INSERT INTO TRANSLATIONS VALUES ('298', '40', '37', "Café", "Coffee");

-- Page 41 (Guía de conversación -> Para un tour) :

INSERT INTO TRANSLATIONS VALUES ('299', '41', '0', "¿ Tiene descuentos para estudiantes ?", "Are there student reductions ?");
INSERT INTO TRANSLATIONS VALUES ('300', '41', '1', "¿ Hay una tarifa especial para niños ?", "Are there reductions for children ?");
INSERT INTO TRANSLATIONS VALUES ('301', '41', '2', "¿ Les quedan lugares ?", "Do you have any room left ?");
INSERT INTO TRANSLATIONS VALUES ('302', '41', '3', "¿ Hay que reservar desde antes?", "Do you have to book in advance ?");

-- Page 42 (Guía de conversación -> Para un espectáculo) :

INSERT INTO TRANSLATIONS VALUES ('303', '42', '0', "¿ Cuánto empieza ?", "What time does it start ?");
INSERT INTO TRANSLATIONS VALUES ('304', '42', '1', "¿ Cuánto tiempo dura ?", "How long does it take ?");
INSERT INTO TRANSLATIONS VALUES ('305', '42', '2', "¿ A qué hora acaba ?", "What time does it end ?");
INSERT INTO TRANSLATIONS VALUES ('306', '42', '3', "¿ Dónde está mi asiento ?", "Where is my seat ?");
INSERT INTO TRANSLATIONS VALUES ('307', '42', '4', "¿ Dónde están los servicios ?", "Where is the restroom ?");








