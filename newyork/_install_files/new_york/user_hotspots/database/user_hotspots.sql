
-- ----------------------------------------------------------------------
-- DROP ALL TABLES :

DROP TABLE USER_HOTSPOTS;

-- ----------------------------------------------------------------------



-- ----------------------------------------------------------------------
-- CREATE TABLES :

CREATE TABLE USER_HOTSPOTS ( id INTEGER PRIMARY KEY AUTOINCREMENT, title VARCHAR NOT NULL, description TEXT NOT NULL, longitude VARCHAR NOT NULL, latitude VARCHAR NOT NULL, type VARCHAR NOT NULL);

-- ----------------------------------------------------------------------



-- ----------------------------------------------------------------------
-- INSERT HOTSPOTS (id, title, description, longitude, latitude, type) :

INSERT INTO "USER_HOTSPOTS" VALUES(1,'Hotspot perso','Ceci est un hotspot perso, vous pouvez le supprimer, le modifier, ou en ajouter d''autres.
Pour en ajouter d''autres, appuyez longuement sur la carte à l''endroit désiré.
Pour modifier / supprimer un Hotspot perso, appuyez longuement dessus.
Enjoy !','-73.989315','40.746737','PICTURE');

-- ----------------------------------------------------------------------







