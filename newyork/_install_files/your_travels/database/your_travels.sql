
-- ----------------------------------------------------------------------
-- DROP ALL TABLES :
DROP TABLE TRAVELS;
DROP TABLE DAYS;
DROP TABLE ACTIVITIES;
-- ----------------------------------------------------------------------



-- ----------------------------------------------------------------------
-- CREATE TABLES :
CREATE TABLE TRAVELS ( id INTEGER PRIMARY KEY AUTOINCREMENT, title VARCHAR NOT NULL, beginDate DATE NOT NULL DEFAULT "0000-00-00", endDate DATE NOT NULL DEFAULT "0000-00-00", isCurrentTravel BOOLEAN NOT NULL );
CREATE TABLE DAYS ( id INTEGER PRIMARY KEY AUTOINCREMENT, date DATE NOT NULL DEFAULT "0000-00-00", travel INT NOT NULL );
CREATE TABLE ACTIVITIES ( id INTEGER PRIMARY KEY AUTOINCREMENT, description TEXT NOT NULL, dayTime VARCHAR NOT NULL, day INT NOT NULL );
-- ----------------------------------------------------------------------



-- ----------------------------------------------------------------------
-- INSERT TRAVELS (id, title, beginDate, endDate, isCurrentTravel) :

-- INSERT INTO TRAVELS VALUES ('1', 'NY oct / nov 2012', '2012-10-29', '2012-11-05', '1');
-- ----------------------------------------------------------------------



-- ----------------------------------------------------------------------
-- INSERT DAYS (id, date, travel) :

-- INSERT INTO DAYS VALUES ('1', '2012-10-29', '1');
-- INSERT INTO DAYS VALUES ('2', '2012-10-30', '1');
-- INSERT INTO DAYS VALUES ('3', '2012-10-31', '1');
-- INSERT INTO DAYS VALUES ('4', '2012-11-01', '1');
-- INSERT INTO DAYS VALUES ('5', '2012-11-02', '1');
-- INSERT INTO DAYS VALUES ('6', '2012-11-03', '1');
-- INSERT INTO DAYS VALUES ('7', '2012-11-04', '1');
-- INSERT INTO DAYS VALUES ('8', '2012-11-05', '1');
-- ----------------------------------------------------------------------



-- ----------------------------------------------------------------------
-- INSERT ACTIVITIES (id, description, dayTime, day) :

-- INSERT INTO ACTIVITIES VALUES ('1', "10h : récupérer bagages et aller trouver l'hôtel", 'MORNING', '1');
-- INSERT INTO ACTIVITIES VALUES ('2', '14h : balade dans Central Park', 'AFTERNOON', '1');
-- INSERT INTO ACTIVITIES VALUES ('3', '19h : premier burger !', 'EVENNING', '1');

-- INSERT INTO ACTIVITIES VALUES ('4', '', 'MORNING', '2');
-- INSERT INTO ACTIVITIES VALUES ('5', '', 'AFTERNOON', '2');
-- INSERT INTO ACTIVITIES VALUES ('6', '', 'EVENNING', '2');

-- INSERT INTO ACTIVITIES VALUES ('7', '', 'MORNING', '3');
-- INSERT INTO ACTIVITIES VALUES ('8', '', 'AFTERNOON', '3');
-- INSERT INTO ACTIVITIES VALUES ('9', '', 'EVENNING', '3');

-- INSERT INTO ACTIVITIES VALUES ('10', '', 'MORNING', '4');
-- INSERT INTO ACTIVITIES VALUES ('11', '', 'AFTERNOON', '4');
-- INSERT INTO ACTIVITIES VALUES ('12', '', 'EVENNING', '4');

-- INSERT INTO ACTIVITIES VALUES ('13', '', 'MORNING', '5');
-- INSERT INTO ACTIVITIES VALUES ('14', '', 'AFTERNOON', '5');
-- INSERT INTO ACTIVITIES VALUES ('15', '', 'EVENNING', '5');

-- INSERT INTO ACTIVITIES VALUES ('16', '', 'MORNING', '6');
-- INSERT INTO ACTIVITIES VALUES ('17', '', 'AFTERNOON', '6');
-- INSERT INTO ACTIVITIES VALUES ('18', '', 'EVENNING', '6');

-- INSERT INTO ACTIVITIES VALUES ('19', '', 'MORNING', '7');
-- INSERT INTO ACTIVITIES VALUES ('20', '', 'AFTERNOON', '7');
-- INSERT INTO ACTIVITIES VALUES ('21', '', 'EVENNING', '7');

-- INSERT INTO ACTIVITIES VALUES ('22', '', 'MORNING', '8');
-- INSERT INTO ACTIVITIES VALUES ('23', '', 'AFTERNOON', '8');
-- INSERT INTO ACTIVITIES VALUES ('24', '', 'EVENNING', '8');
-- ----------------------------------------------------------------------







